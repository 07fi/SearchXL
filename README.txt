

Search XL is a program designed to search multiple Microsoft Excel documents in a Directory. Originally written in python and then converted to C++ with the wonderful but temperamental tool Nuitka. Written for the Nebraska Department of Corrections by Austin Geisert

Installation 
Click the Search XL.msi file included in this folder this will install all the C++ files and a shortcut to the desktop. It is to note that it can install in any location on your computer, it is not required to install in "Program Files"
The Source_C++ folder can be copied and the pyExcel.exe can be executed with everything included as a stand alone folder.


Usage 

Enter your keyword into the keyword line this can be anything you wish to search for but it will take everything that you put into the line and qury for it, this includes spaces. So be aware of your spelling and spaceing.

The folder path is self explanetory put in your folder path and the program will find all the xl files in that folder. It will not however decend into any other folders in your folder path. 

