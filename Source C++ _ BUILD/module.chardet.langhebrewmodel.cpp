// Generated code for Python source for module 'chardet.langhebrewmodel'
// created by Nuitka version 0.5.13.4

// This code is in part copyright 2015 Kay Hayen.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "nuitka/prelude.hpp"

#include "__helpers.hpp"

// The _module_chardet$langhebrewmodel is a Python object pointer of module type.

// Note: For full compatibility with CPython, every module variable access
// needs to go through it except for cases where the module cannot possibly
// have changed in the mean time.

PyObject *module_chardet$langhebrewmodel;
PyDictObject *moduledict_chardet$langhebrewmodel;

// The module constants used
extern PyObject *const_str_plain_keepEnglishLetter;
extern PyObject *const_str_plain_precedenceMatrix;
static PyObject *const_str_digest_0c1cd9ef85238939ee414fdcf0856701;
static PyObject *const_str_digest_097f2ffc7a10d04e6e4bbed03fb6b4c0;
extern PyObject *const_str_digest_1b316304b0dcc59a28e4ea7f9a62d8f1;
extern PyObject *const_str_plain___file__;
static PyObject *const_tuple_96aea69e7c6c4b34b6c5234187423b87_tuple;
extern PyObject *const_str_plain_langhebrewmodel;
static PyObject *const_float_0_984004;
extern PyObject *const_str_plain_charToOrderMap;
static PyObject *const_str_plain_HebrewLangModel;
extern PyObject *const_str_plain_charsetName;
static PyObject *const_str_plain_win1255_CharToOrderMap;
extern PyObject *const_str_plain_Win1255HebrewModel;
extern PyObject *const_str_plain___doc__;
static PyObject *const_tuple_4f19fa3634bb485716df7f8f5e81152b_tuple;
extern PyObject *const_str_plain_mTypicalPositiveRatio;
extern PyObject *const_tuple_empty;
static PyObject *module_filename_obj;

static void _initModuleConstants( void )
{
    const_str_digest_0c1cd9ef85238939ee414fdcf0856701 = UNSTREAM_STRING( &constant_bin[ 380941 ], 26, 0 );
    const_str_digest_097f2ffc7a10d04e6e4bbed03fb6b4c0 = UNSTREAM_STRING( &constant_bin[ 380967 ], 23, 0 );
    const_tuple_96aea69e7c6c4b34b6c5234187423b87_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 380990 ], 20485 );
    const_float_0_984004 = UNSTREAM_FLOAT( &constant_bin[ 401475 ] );
    const_str_plain_HebrewLangModel = UNSTREAM_STRING( &constant_bin[ 5187 ], 15, 1 );
    const_str_plain_win1255_CharToOrderMap = UNSTREAM_STRING( &constant_bin[ 5143 ], 22, 1 );
    const_tuple_4f19fa3634bb485716df7f8f5e81152b_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 401483 ], 1285 );
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_chardet$langhebrewmodel( void )
{

}
#endif

// The module code objects.
static PyCodeObject *codeobj_a143c97e2dd23b8eee77d4c9f6e08bbd;

static void _initModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_0c1cd9ef85238939ee414fdcf0856701 );
    codeobj_a143c97e2dd23b8eee77d4c9f6e08bbd = MAKE_CODEOBJ( module_filename_obj, const_str_plain_langhebrewmodel, 0, const_tuple_empty, 0, CO_NOFREE );
}

// The module function declarations.


// The module function definitions.



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_chardet$langhebrewmodel =
{
    PyModuleDef_HEAD_INIT,
    "chardet.langhebrewmodel",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

#define _MODULE_UNFREEZER 0

#if _MODULE_UNFREEZER

#include "nuitka/unfreezing.hpp"

// Table for lookup to find "frozen" modules or DLLs, i.e. the ones included in
// or along this binary.

static struct Nuitka_MetaPathBasedLoaderEntry meta_path_loader_entries[] =
{

    { NULL, NULL, 0 }
};

#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( chardet$langhebrewmodel )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_chardet$langhebrewmodel );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    // Initialize the compiled types of Nuitka.
    PyType_Ready( &Nuitka_Generator_Type );
    PyType_Ready( &Nuitka_Function_Type );
    PyType_Ready( &Nuitka_Method_Type );
    PyType_Ready( &Nuitka_Frame_Type );
#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

#endif

#if _MODULE_UNFREEZER
    registerMetaPathBasedUnfreezer( meta_path_loader_entries );
#endif

    _initModuleConstants();
    _initModuleCodeObjects();

    // puts( "in initchardet$langhebrewmodel" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_chardet$langhebrewmodel = Py_InitModule4(
        "chardet.langhebrewmodel",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No __doc__ is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else
    module_chardet$langhebrewmodel = PyModule_Create( &mdef_chardet$langhebrewmodel );
#endif

    moduledict_chardet$langhebrewmodel = (PyDictObject *)((PyModuleObject *)module_chardet$langhebrewmodel)->md_dict;

    CHECK_OBJECT( module_chardet$langhebrewmodel );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_097f2ffc7a10d04e6e4bbed03fb6b4c0, module_chardet$langhebrewmodel );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    PyObject *module_dict = PyModule_GetDict( module_chardet$langhebrewmodel );

    if ( PyDict_GetItem( module_dict, const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

#ifndef __NUITKA_NO_ASSERT__
        int res =
#endif
            PyDict_SetItem( module_dict, const_str_plain___builtins__, value );

        assert( res == 0 );
    }

#if PYTHON_VERSION >= 330
#if _MODULE_UNFREEZER
    PyDict_SetItem( module_dict, const_str_plain___loader__, metapath_based_loader );
#else
    PyDict_SetItem( module_dict, const_str_plain___loader__, Py_None );
#endif
#endif

    // Temp variables if any
    PyObject *exception_type, *exception_value;
    PyTracebackObject *exception_tb;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_key_3;
    PyObject *tmp_dict_key_4;
    PyObject *tmp_dict_key_5;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dict_value_3;
    PyObject *tmp_dict_value_4;
    PyObject *tmp_dict_value_5;
    PyFrameObject *frame_module;


    // Module code.
    tmp_assign_source_1 = Py_None;
    UPDATE_STRING_DICT0( moduledict_chardet$langhebrewmodel, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = module_filename_obj;
    UPDATE_STRING_DICT0( moduledict_chardet$langhebrewmodel, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = const_tuple_4f19fa3634bb485716df7f8f5e81152b_tuple;
    UPDATE_STRING_DICT0( moduledict_chardet$langhebrewmodel, (Nuitka_StringObject *)const_str_plain_win1255_CharToOrderMap, tmp_assign_source_3 );
    tmp_assign_source_4 = const_tuple_96aea69e7c6c4b34b6c5234187423b87_tuple;
    UPDATE_STRING_DICT0( moduledict_chardet$langhebrewmodel, (Nuitka_StringObject *)const_str_plain_HebrewLangModel, tmp_assign_source_4 );
    // Frame without reuse.
    frame_module = MAKE_FRAME( codeobj_a143c97e2dd23b8eee77d4c9f6e08bbd, module_chardet$langhebrewmodel );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_module );
    assert( Py_REFCNT( frame_module ) == 1 );

#if PYTHON_VERSION >= 340
    frame_module->f_executing += 1;
#endif

    // Framed code:
    tmp_assign_source_5 = _PyDict_NewPresized( 5 );
    tmp_dict_value_1 = GET_STRING_DICT_VALUE( moduledict_chardet$langhebrewmodel, (Nuitka_StringObject *)const_str_plain_win1255_CharToOrderMap );

    if (unlikely( tmp_dict_value_1 == NULL ))
    {
        tmp_dict_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_win1255_CharToOrderMap );
    }

    if ( tmp_dict_value_1 == NULL )
    {
        Py_DECREF( tmp_assign_source_5 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 5137 ], 44, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 194;
        goto frame_exception_exit_1;
    }

    tmp_dict_key_1 = const_str_plain_charToOrderMap;
    PyDict_SetItem( tmp_assign_source_5, tmp_dict_key_1, tmp_dict_value_1 );
    tmp_dict_value_2 = GET_STRING_DICT_VALUE( moduledict_chardet$langhebrewmodel, (Nuitka_StringObject *)const_str_plain_HebrewLangModel );

    if (unlikely( tmp_dict_value_2 == NULL ))
    {
        tmp_dict_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HebrewLangModel );
    }

    if ( tmp_dict_value_2 == NULL )
    {
        Py_DECREF( tmp_assign_source_5 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 5181 ], 37, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 195;
        goto frame_exception_exit_1;
    }

    tmp_dict_key_2 = const_str_plain_precedenceMatrix;
    PyDict_SetItem( tmp_assign_source_5, tmp_dict_key_2, tmp_dict_value_2 );
    tmp_dict_value_3 = const_float_0_984004;
    tmp_dict_key_3 = const_str_plain_mTypicalPositiveRatio;
    PyDict_SetItem( tmp_assign_source_5, tmp_dict_key_3, tmp_dict_value_3 );
    tmp_dict_value_4 = Py_False;
    tmp_dict_key_4 = const_str_plain_keepEnglishLetter;
    PyDict_SetItem( tmp_assign_source_5, tmp_dict_key_4, tmp_dict_value_4 );
    tmp_dict_value_5 = const_str_digest_1b316304b0dcc59a28e4ea7f9a62d8f1;
    tmp_dict_key_5 = const_str_plain_charsetName;
    PyDict_SetItem( tmp_assign_source_5, tmp_dict_key_5, tmp_dict_value_5 );
    UPDATE_STRING_DICT1( moduledict_chardet$langhebrewmodel, (Nuitka_StringObject *)const_str_plain_Win1255HebrewModel, tmp_assign_source_5 );

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_module );
#endif
    popFrameStack();

    assertFrameObject( frame_module );
    Py_DECREF( frame_module );

    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_module );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_module );
    }
    else if ( exception_tb->tb_frame != frame_module )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_module );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }

    // Put the previous frame back on top.
    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_module->f_executing -= 1;
#endif
    Py_DECREF( frame_module );

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_chardet$langhebrewmodel );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
