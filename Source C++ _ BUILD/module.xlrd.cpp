// Generated code for Python source for module 'xlrd'
// created by Nuitka version 0.5.13.4

// This code is in part copyright 2015 Kay Hayen.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "nuitka/prelude.hpp"

#include "__helpers.hpp"

// The _module_xlrd is a Python object pointer of module type.

// Note: For full compatibility with CPython, every module variable access
// needs to go through it except for cases where the module cannot possibly
// have changed in the mean time.

PyObject *module_xlrd;
PyDictObject *moduledict_xlrd;

// The module constants used
static PyObject *const_tuple_str_plain_path_tuple;
extern PyObject *const_str_plain_file_contents;
static PyObject *const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple;
extern PyObject *const_tuple_str_chr_42_tuple;
extern PyObject *const_str_plain_biff2_8_load;
extern PyObject *const_dict_empty;
extern PyObject *const_str_plain_startswith;
extern PyObject *const_str_plain_empty_cell;
static PyObject *const_str_plain_pprint;
extern PyObject *const_str_plain_encodings;
extern PyObject *const_str_plain_read;
extern PyObject *const_str_plain_bk;
static PyObject *const_tuple_str_plain_Book_str_plain_colname_tuple;
static PyObject *const_str_digest_4cf32320aa3ddff505589fa6cf78ca12;
static PyObject *const_str_plain_peeksz;
extern PyObject *const_str_plain_Book;
extern PyObject *const_str_chr_42;
static PyObject *const_str_chr_92;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain___path__;
extern PyObject *const_str_plain_xldate;
extern PyObject *const_str_plain_sheet;
extern PyObject *const_str_plain_close;
extern PyObject *const_str_plain_ragged_rows;
extern PyObject *const_str_plain_colname;
extern PyObject *const_str_plain_on_demand;
static PyObject *const_tuple_str_plain_biff_count_records_tuple;
extern PyObject *const_str_plain_MMAP_AVAILABLE;
extern PyObject *const_str_plain_biff_dump;
extern PyObject *const_str_plain_rb;
extern PyObject *const_str_plain_base;
extern PyObject *const_str_plain_write;
static PyObject *const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple;
extern PyObject *const_str_plain_verbosity;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain_use_mmap;
static PyObject *const_tuple_str_plain_book_tuple;
static PyObject *const_str_plain_ZipFile;
extern PyObject *const_str_plain_component_names;
static PyObject *const_tuple_str_plain_biff_dump_tuple;
extern PyObject *const_str_plain_xldate_as_tuple;
static PyObject *const_str_digest_3e6fd7fd1c77330b0409e6c8469d2388;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_XLRDError;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_digest_10893fd7cff66dca193f658ca9438d15;
extern PyObject *const_str_plain_encoding_override;
extern PyObject *const_str_plain_XL_CELL_BOOLEAN;
extern PyObject *const_int_pos_4;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_formula;
static PyObject *const_tuple_str_plain_XLDateError_str_plain_xldate_as_tuple_tuple;
extern PyObject *const_str_plain_lower;
extern PyObject *const_str_plain_mmap;
static PyObject *const_str_plain_namelist;
extern PyObject *const_str_plain_dirname;
static PyObject *const_str_plain_licences;
extern PyObject *const_str_plain_open_workbook_xls;
extern PyObject *const_str_plain_error_text_from_code;
extern PyObject *const_str_plain_biffh;
extern PyObject *const_str_plain_xlrd;
extern PyObject *const_str_chr_47;
extern PyObject *const_str_plain_IronPython;
extern PyObject *const_str_plain_replace;
extern PyObject *const_str_plain_timemachine;
static PyObject *const_str_digest_fe43db75ca6b370f5d56a6513ba6fcdc;
static PyObject *const_str_digest_7f6e04e15ba50427cd2014c6a8450f14;
extern PyObject *const_str_plain_info;
extern PyObject *const_str_plain_version;
extern PyObject *const_str_plain_path;
extern PyObject *const_str_plain_outfile;
extern PyObject *const_str_plain_XL_CELL_NUMBER;
extern PyObject *const_str_plain_name;
extern PyObject *const_str_plain_dump;
static PyObject *const_str_digest_0aee4052822580a28ec5d0219bba3814;
extern PyObject *const_str_plain_open_workbook;
extern PyObject *const_str_plain_open_workbook_2007_xml;
static PyObject *const_tuple_32249a008444d81aafd296f158c2b726_tuple;
static PyObject *const_str_digest_e885fcc67d8d845a867449c2412297e0;
extern PyObject *const_str_plain_dict;
static PyObject *const_tuple_str_plain_timemachine_tuple;
extern PyObject *const_str_plain_BYTES_IO;
extern PyObject *const_str_plain_stdout;
extern PyObject *const_str_plain_XLDateError;
extern PyObject *const_str_plain_XL_CELL_EMPTY;
static PyObject *const_str_digest_20d8d2f7a3b2aad6a2df3e9c36bfaf22;
static PyObject *const_str_plain_zipfile;
extern PyObject *const_str_plain_f;
static PyObject *const_tuple_str_plain_xlsx_tuple;
extern PyObject *const_str_plain_stream_len;
extern PyObject *const_str_plain_filename;
extern PyObject *const_str_plain_XL_CELL_TEXT;
extern PyObject *const_str_plain_formatting_info;
extern PyObject *const_str_plain_zf;
extern PyObject *const_str_plain_XL_CELL_BLANK;
static PyObject *const_tuple_bd6f4a683a0dcda6652f70c1896a7986_tuple;
extern PyObject *const_str_plain_unnumbered;
static PyObject *const_str_digest_d831389daee80ab6adeb0fc85fa26d37;
extern PyObject *const_str_plain_mem;
extern PyObject *const_str_plain_biff_text_from_num;
static PyObject *const_tuple_str_plain_empty_cell_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_sys;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_USE_MMAP;
extern PyObject *const_str_plain_xlsx;
extern PyObject *const_str_plain_count_records;
static PyObject *const_tuple_str_plain___VERSION___tuple;
extern PyObject *const_str_empty;
extern PyObject *const_str_plain_XL_CELL_ERROR;
extern PyObject *const_str_plain_logfile;
extern PyObject *const_str_plain___VERSION__;
extern PyObject *const_str_plain_XL_CELL_DATE;
extern PyObject *const_str_plain_biff_count_records;
extern PyObject *const_str_plain_book;
static PyObject *const_str_plain_peek;
static PyObject *const_tuple_str_plain_licences_tuple;
static PyObject *module_filename_obj;

static void _initModuleConstants( void )
{
    const_tuple_str_plain_path_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_path_tuple, 0, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple = PyTuple_New( 18 );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 0, const_str_plain_filename ); Py_INCREF( const_str_plain_filename );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 1, const_str_plain_logfile ); Py_INCREF( const_str_plain_logfile );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 2, const_str_plain_verbosity ); Py_INCREF( const_str_plain_verbosity );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 3, const_str_plain_use_mmap ); Py_INCREF( const_str_plain_use_mmap );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 4, const_str_plain_file_contents ); Py_INCREF( const_str_plain_file_contents );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 5, const_str_plain_encoding_override ); Py_INCREF( const_str_plain_encoding_override );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 6, const_str_plain_formatting_info ); Py_INCREF( const_str_plain_formatting_info );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 7, const_str_plain_on_demand ); Py_INCREF( const_str_plain_on_demand );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 8, const_str_plain_ragged_rows ); Py_INCREF( const_str_plain_ragged_rows );
    const_str_plain_peeksz = UNSTREAM_STRING( &constant_bin[ 461071 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 9, const_str_plain_peeksz ); Py_INCREF( const_str_plain_peeksz );
    const_str_plain_peek = UNSTREAM_STRING( &constant_bin[ 461071 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 10, const_str_plain_peek ); Py_INCREF( const_str_plain_peek );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 11, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 12, const_str_plain_zf ); Py_INCREF( const_str_plain_zf );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 13, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 14, const_str_plain_component_names ); Py_INCREF( const_str_plain_component_names );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 15, const_str_plain_xlsx ); Py_INCREF( const_str_plain_xlsx );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 16, const_str_plain_bk ); Py_INCREF( const_str_plain_bk );
    PyTuple_SET_ITEM( const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 17, const_str_plain_book ); Py_INCREF( const_str_plain_book );
    const_str_plain_pprint = UNSTREAM_STRING( &constant_bin[ 9557 ], 6, 1 );
    const_tuple_str_plain_Book_str_plain_colname_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Book_str_plain_colname_tuple, 0, const_str_plain_Book ); Py_INCREF( const_str_plain_Book );
    PyTuple_SET_ITEM( const_tuple_str_plain_Book_str_plain_colname_tuple, 1, const_str_plain_colname ); Py_INCREF( const_str_plain_colname );
    const_str_digest_4cf32320aa3ddff505589fa6cf78ca12 = UNSTREAM_STRING( &constant_bin[ 461077 ], 21, 0 );
    const_str_chr_92 = UNSTREAM_CHAR( 92, 0 );
    const_tuple_str_plain_biff_count_records_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_biff_count_records_tuple, 0, const_str_plain_biff_count_records ); Py_INCREF( const_str_plain_biff_count_records );
    const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, 0, const_str_plain_XLRDError ); Py_INCREF( const_str_plain_XLRDError );
    PyTuple_SET_ITEM( const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, 1, const_str_plain_biff_text_from_num ); Py_INCREF( const_str_plain_biff_text_from_num );
    PyTuple_SET_ITEM( const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, 2, const_str_plain_error_text_from_code ); Py_INCREF( const_str_plain_error_text_from_code );
    PyTuple_SET_ITEM( const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, 3, const_str_plain_XL_CELL_BLANK ); Py_INCREF( const_str_plain_XL_CELL_BLANK );
    PyTuple_SET_ITEM( const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, 4, const_str_plain_XL_CELL_TEXT ); Py_INCREF( const_str_plain_XL_CELL_TEXT );
    PyTuple_SET_ITEM( const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, 5, const_str_plain_XL_CELL_BOOLEAN ); Py_INCREF( const_str_plain_XL_CELL_BOOLEAN );
    PyTuple_SET_ITEM( const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, 6, const_str_plain_XL_CELL_ERROR ); Py_INCREF( const_str_plain_XL_CELL_ERROR );
    PyTuple_SET_ITEM( const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, 7, const_str_plain_XL_CELL_EMPTY ); Py_INCREF( const_str_plain_XL_CELL_EMPTY );
    PyTuple_SET_ITEM( const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, 8, const_str_plain_XL_CELL_DATE ); Py_INCREF( const_str_plain_XL_CELL_DATE );
    PyTuple_SET_ITEM( const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, 9, const_str_plain_XL_CELL_NUMBER ); Py_INCREF( const_str_plain_XL_CELL_NUMBER );
    const_tuple_str_plain_book_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_book_tuple, 0, const_str_plain_book ); Py_INCREF( const_str_plain_book );
    const_str_plain_ZipFile = UNSTREAM_STRING( &constant_bin[ 461098 ], 7, 1 );
    const_tuple_str_plain_biff_dump_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_biff_dump_tuple, 0, const_str_plain_biff_dump ); Py_INCREF( const_str_plain_biff_dump );
    const_str_digest_3e6fd7fd1c77330b0409e6c8469d2388 = UNSTREAM_STRING( &constant_bin[ 461105 ], 38, 0 );
    const_str_digest_10893fd7cff66dca193f658ca9438d15 = UNSTREAM_STRING( &constant_bin[ 461143 ], 4, 0 );
    const_tuple_str_plain_XLDateError_str_plain_xldate_as_tuple_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_XLDateError_str_plain_xldate_as_tuple_tuple, 0, const_str_plain_XLDateError ); Py_INCREF( const_str_plain_XLDateError );
    PyTuple_SET_ITEM( const_tuple_str_plain_XLDateError_str_plain_xldate_as_tuple_tuple, 1, const_str_plain_xldate_as_tuple ); Py_INCREF( const_str_plain_xldate_as_tuple );
    const_str_plain_namelist = UNSTREAM_STRING( &constant_bin[ 461147 ], 8, 1 );
    const_str_plain_licences = UNSTREAM_STRING( &constant_bin[ 461155 ], 8, 1 );
    const_str_digest_fe43db75ca6b370f5d56a6513ba6fcdc = UNSTREAM_STRING( &constant_bin[ 461163 ], 15, 0 );
    const_str_digest_7f6e04e15ba50427cd2014c6a8450f14 = UNSTREAM_STRING( &constant_bin[ 461178 ], 11, 0 );
    const_str_digest_0aee4052822580a28ec5d0219bba3814 = UNSTREAM_STRING( &constant_bin[ 461189 ], 15, 0 );
    const_tuple_32249a008444d81aafd296f158c2b726_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_32249a008444d81aafd296f158c2b726_tuple, 0, const_str_plain_filename ); Py_INCREF( const_str_plain_filename );
    PyTuple_SET_ITEM( const_tuple_32249a008444d81aafd296f158c2b726_tuple, 1, const_str_plain_outfile ); Py_INCREF( const_str_plain_outfile );
    PyTuple_SET_ITEM( const_tuple_32249a008444d81aafd296f158c2b726_tuple, 2, const_str_plain_unnumbered ); Py_INCREF( const_str_plain_unnumbered );
    PyTuple_SET_ITEM( const_tuple_32249a008444d81aafd296f158c2b726_tuple, 3, const_str_plain_biff_dump ); Py_INCREF( const_str_plain_biff_dump );
    PyTuple_SET_ITEM( const_tuple_32249a008444d81aafd296f158c2b726_tuple, 4, const_str_plain_bk ); Py_INCREF( const_str_plain_bk );
    const_str_digest_e885fcc67d8d845a867449c2412297e0 = UNSTREAM_STRING( &constant_bin[ 461204 ], 46, 0 );
    const_tuple_str_plain_timemachine_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_timemachine_tuple, 0, const_str_plain_timemachine ); Py_INCREF( const_str_plain_timemachine );
    const_str_digest_20d8d2f7a3b2aad6a2df3e9c36bfaf22 = UNSTREAM_STRING( &constant_bin[ 461250 ], 16, 0 );
    const_str_plain_zipfile = UNSTREAM_STRING( &constant_bin[ 9481 ], 7, 1 );
    const_tuple_str_plain_xlsx_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_xlsx_tuple, 0, const_str_plain_xlsx ); Py_INCREF( const_str_plain_xlsx );
    const_tuple_bd6f4a683a0dcda6652f70c1896a7986_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_bd6f4a683a0dcda6652f70c1896a7986_tuple, 0, const_str_plain_filename ); Py_INCREF( const_str_plain_filename );
    PyTuple_SET_ITEM( const_tuple_bd6f4a683a0dcda6652f70c1896a7986_tuple, 1, const_str_plain_outfile ); Py_INCREF( const_str_plain_outfile );
    PyTuple_SET_ITEM( const_tuple_bd6f4a683a0dcda6652f70c1896a7986_tuple, 2, const_str_plain_biff_count_records ); Py_INCREF( const_str_plain_biff_count_records );
    PyTuple_SET_ITEM( const_tuple_bd6f4a683a0dcda6652f70c1896a7986_tuple, 3, const_str_plain_bk ); Py_INCREF( const_str_plain_bk );
    const_str_digest_d831389daee80ab6adeb0fc85fa26d37 = UNSTREAM_STRING( &constant_bin[ 461266 ], 35, 0 );
    const_tuple_str_plain_empty_cell_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_empty_cell_tuple, 0, const_str_plain_empty_cell ); Py_INCREF( const_str_plain_empty_cell );
    const_tuple_str_plain___VERSION___tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain___VERSION___tuple, 0, const_str_plain___VERSION__ ); Py_INCREF( const_str_plain___VERSION__ );
    const_tuple_str_plain_licences_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_licences_tuple, 0, const_str_plain_licences ); Py_INCREF( const_str_plain_licences );
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_xlrd( void )
{

}
#endif

// The module code objects.
static PyCodeObject *codeobj_adcb06198f09404874f6145013022edd;
static PyCodeObject *codeobj_70e733aa06c1152176286b7c783331f5;
static PyCodeObject *codeobj_f966c108fee28d98c0de6a12e32abd80;
static PyCodeObject *codeobj_773ea5bc0dfcfa383ea7f7ee6f6e9dff;

static void _initModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_20d8d2f7a3b2aad6a2df3e9c36bfaf22 );
    codeobj_adcb06198f09404874f6145013022edd = MAKE_CODEOBJ( module_filename_obj, const_str_plain_count_records, 463, const_tuple_bd6f4a683a0dcda6652f70c1896a7986_tuple, 2, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE );
    codeobj_70e733aa06c1152176286b7c783331f5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_dump, 451, const_tuple_32249a008444d81aafd296f158c2b726_tuple, 3, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE );
    codeobj_f966c108fee28d98c0de6a12e32abd80 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_open_workbook, 380, const_tuple_b29960be0070e2d3b6f28c055bdab711_tuple, 9, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE );
    codeobj_773ea5bc0dfcfa383ea7f7ee6f6e9dff = MAKE_CODEOBJ( module_filename_obj, const_str_plain_xlrd, 0, const_tuple_empty, 0, CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_function_1_open_workbook_of_xlrd( PyObject *defaults );


static PyObject *MAKE_FUNCTION_function_2_dump_of_xlrd( PyObject *defaults );


static PyObject *MAKE_FUNCTION_function_3_count_records_of_xlrd( PyObject *defaults );


// The module function definitions.
static PyObject *impl_function_1_open_workbook_of_xlrd( Nuitka_FunctionObject *self, PyObject *_python_par_filename, PyObject *_python_par_logfile, PyObject *_python_par_verbosity, PyObject *_python_par_use_mmap, PyObject *_python_par_file_contents, PyObject *_python_par_encoding_override, PyObject *_python_par_formatting_info, PyObject *_python_par_on_demand, PyObject *_python_par_ragged_rows )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_filename = _python_par_filename;
    PyObject *par_logfile = _python_par_logfile;
    PyObject *par_verbosity = _python_par_verbosity;
    PyObject *par_use_mmap = _python_par_use_mmap;
    PyObject *par_file_contents = _python_par_file_contents;
    PyObject *par_encoding_override = _python_par_encoding_override;
    PyObject *par_formatting_info = _python_par_formatting_info;
    PyObject *par_on_demand = _python_par_on_demand;
    PyObject *par_ragged_rows = _python_par_ragged_rows;
    PyObject *var_peeksz = NULL;
    PyObject *var_peek = NULL;
    PyObject *var_f = NULL;
    PyObject *var_zf = NULL;
    PyObject *var_name = NULL;
    PyObject *var_component_names = NULL;
    PyObject *var_xlsx = NULL;
    PyObject *var_bk = NULL;
    PyObject *var_book = NULL;
    PyObject *tmp_listcontr_1__listcontr_iter = NULL;
    PyObject *tmp_listcontr_1__listcontr_result = NULL;
    PyObject *tmp_listcontr_1__iter_value_0 = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    PyObject *tmp_append_to_1;
    PyObject *tmp_append_value_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_name_1;
    PyObject *tmp_args_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_call_arg_element_1;
    PyObject *tmp_call_arg_element_2;
    PyObject *tmp_call_arg_element_3;
    PyObject *tmp_call_arg_element_4;
    PyObject *tmp_call_arg_element_5;
    PyObject *tmp_call_arg_element_6;
    PyObject *tmp_call_arg_element_7;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    PyObject *tmp_called_name_6;
    PyObject *tmp_called_name_7;
    PyObject *tmp_called_name_8;
    PyObject *tmp_called_name_9;
    PyObject *tmp_called_name_10;
    PyObject *tmp_called_name_11;
    PyObject *tmp_called_name_12;
    PyObject *tmp_called_name_13;
    PyObject *tmp_called_name_14;
    PyObject *tmp_called_name_15;
    PyObject *tmp_called_name_16;
    int tmp_cmp_Eq_1;
    int tmp_cmp_In_1;
    int tmp_cmp_In_2;
    int tmp_cmp_In_3;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_left_4;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    PyObject *tmp_compare_right_4;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    int tmp_cond_truth_3;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_cond_value_3;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_key_3;
    PyObject *tmp_dict_key_4;
    PyObject *tmp_dict_key_5;
    PyObject *tmp_dict_key_6;
    PyObject *tmp_dict_key_7;
    PyObject *tmp_dict_key_8;
    PyObject *tmp_dict_key_9;
    PyObject *tmp_dict_key_10;
    PyObject *tmp_dict_key_11;
    PyObject *tmp_dict_key_12;
    PyObject *tmp_dict_key_13;
    PyObject *tmp_dict_key_14;
    PyObject *tmp_dict_key_15;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dict_value_3;
    PyObject *tmp_dict_value_4;
    PyObject *tmp_dict_value_5;
    PyObject *tmp_dict_value_6;
    PyObject *tmp_dict_value_7;
    PyObject *tmp_dict_value_8;
    PyObject *tmp_dict_value_9;
    PyObject *tmp_dict_value_10;
    PyObject *tmp_dict_value_11;
    PyObject *tmp_dict_value_12;
    PyObject *tmp_dict_value_13;
    PyObject *tmp_dict_value_14;
    PyObject *tmp_dict_value_15;
    PyObject *tmp_frame_locals;
    PyObject *tmp_import_globals_1;
    PyObject *tmp_import_globals_2;
    PyObject *tmp_import_locals_1;
    PyObject *tmp_import_locals_2;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_import_name_from_2;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_next_source_1;
    PyObject *tmp_open_filename_1;
    PyObject *tmp_open_mode_1;
    PyObject *tmp_raise_type_1;
    PyObject *tmp_raise_type_2;
    PyObject *tmp_raise_type_3;
    int tmp_res;
    PyObject *tmp_return_value;
    Py_ssize_t tmp_slice_index_upper_1;
    PyObject *tmp_slice_source_1;
    Py_ssize_t tmp_sliceslicedel_index_lower_1;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_source_name_6;
    PyObject *tmp_source_name_7;
    PyObject *tmp_source_name_8;
    PyObject *tmp_source_name_9;
    PyObject *tmp_source_name_10;
    PyObject *tmp_source_name_11;
    PyObject *tmp_source_name_12;
    int tmp_tried_lineno_1;
    int tmp_tried_lineno_2;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    PyObject *tmp_tuple_element_3;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = const_int_pos_4;
    assert( var_peeksz == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var_peeksz = tmp_assign_source_1;

    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_f966c108fee28d98c0de6a12e32abd80, module_xlrd );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_cond_value_1 = par_file_contents;

    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 391;
        goto frame_exception_exit_1;
    }
    if (tmp_cond_truth_1 == 1)
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_sliceslicedel_index_lower_1 = 0;
    tmp_slice_index_upper_1 = 4;
    tmp_slice_source_1 = par_file_contents;

    tmp_assign_source_2 = LOOKUP_INDEX_SLICE( tmp_slice_source_1, tmp_sliceslicedel_index_lower_1, tmp_slice_index_upper_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 392;
        goto frame_exception_exit_1;
    }
    assert( var_peek == NULL );
    var_peek = tmp_assign_source_2;

    goto branch_end_1;
    branch_no_1:;
    tmp_open_filename_1 = par_filename;

    tmp_open_mode_1 = const_str_plain_rb;
    tmp_assign_source_3 = BUILTIN_OPEN( tmp_open_filename_1, tmp_open_mode_1, NULL );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 394;
        goto frame_exception_exit_1;
    }
    assert( var_f == NULL );
    var_f = tmp_assign_source_3;

    tmp_source_name_1 = var_f;

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_read );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 395;
        goto frame_exception_exit_1;
    }
    tmp_call_arg_element_1 = const_int_pos_4;
    frame_function->f_lineno = 395;
    tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, tmp_call_arg_element_1 );
    Py_DECREF( tmp_called_name_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 395;
        goto frame_exception_exit_1;
    }
    assert( var_peek == NULL );
    var_peek = tmp_assign_source_4;

    tmp_source_name_2 = var_f;

    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_close );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 396;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 396;
    tmp_unused = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
    Py_DECREF( tmp_called_name_2 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 396;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    branch_end_1:;
    tmp_compare_left_1 = var_peek;

    tmp_compare_right_1 = const_str_digest_10893fd7cff66dca193f658ca9438d15;
    tmp_cmp_Eq_1 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_cmp_Eq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 397;
        goto frame_exception_exit_1;
    }
    if (tmp_cmp_Eq_1 == 1)
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_cond_value_2 = par_file_contents;

    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 398;
        goto frame_exception_exit_1;
    }
    if (tmp_cond_truth_2 == 1)
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_source_name_3 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_zipfile );

    if (unlikely( tmp_source_name_3 == NULL ))
    {
        tmp_source_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zipfile );
    }

    if ( tmp_source_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9468 ], 36, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 399;
        goto frame_exception_exit_1;
    }

    tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_ZipFile );
    if ( tmp_called_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 399;
        goto frame_exception_exit_1;
    }
    tmp_source_name_4 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_timemachine );

    if (unlikely( tmp_source_name_4 == NULL ))
    {
        tmp_source_name_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_timemachine );
    }

    if ( tmp_source_name_4 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9504 ], 40, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 399;
        goto frame_exception_exit_1;
    }

    tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_BYTES_IO );
    if ( tmp_called_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );

        frame_function->f_lineno = 399;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_2 = par_file_contents;

    frame_function->f_lineno = 399;
    tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, tmp_args_element_name_2 );
    Py_DECREF( tmp_called_name_4 );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_3 );

        frame_function->f_lineno = 399;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 399;
    tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, tmp_args_element_name_1 );
    Py_DECREF( tmp_called_name_3 );
    Py_DECREF( tmp_args_element_name_1 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 399;
        goto frame_exception_exit_1;
    }
    assert( var_zf == NULL );
    var_zf = tmp_assign_source_5;

    goto branch_end_3;
    branch_no_3:;
    tmp_source_name_5 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_zipfile );

    if (unlikely( tmp_source_name_5 == NULL ))
    {
        tmp_source_name_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zipfile );
    }

    if ( tmp_source_name_5 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9468 ], 36, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 401;
        goto frame_exception_exit_1;
    }

    tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_ZipFile );
    if ( tmp_called_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 401;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_3 = par_filename;

    frame_function->f_lineno = 401;
    tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, tmp_args_element_name_3 );
    Py_DECREF( tmp_called_name_5 );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 401;
        goto frame_exception_exit_1;
    }
    assert( var_zf == NULL );
    var_zf = tmp_assign_source_6;

    branch_end_3:;
    tmp_called_name_6 = LOOKUP_BUILTIN( const_str_plain_dict );
    if ( tmp_called_name_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 406;
        goto frame_exception_exit_1;
    }
    tmp_args_name_1 = NULL;
    // Tried code
    tmp_args_name_1 = PyTuple_New( 1 );
    tmp_tuple_element_1 = NULL;
    // Tried code
    tmp_source_name_6 = var_zf;

    tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_namelist );
    if ( tmp_called_name_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 407;
        goto try_finally_handler_3;
    }
    frame_function->f_lineno = 407;
    tmp_iter_arg_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_7 );
    Py_DECREF( tmp_called_name_7 );
    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 407;
        goto try_finally_handler_3;
    }
    tmp_assign_source_8 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 406;
        goto try_finally_handler_3;
    }
    assert( tmp_listcontr_1__listcontr_iter == NULL );
    tmp_listcontr_1__listcontr_iter = tmp_assign_source_8;

    tmp_assign_source_9 = PyList_New( 0 );
    assert( tmp_listcontr_1__listcontr_result == NULL );
    tmp_listcontr_1__listcontr_result = tmp_assign_source_9;

    loop_start_1:;
    tmp_next_source_1 = tmp_listcontr_1__listcontr_iter;

    tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_1 );
    if ( tmp_assign_source_10 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_1;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 406;
            goto try_finally_handler_3;
        }
    }

    {
        PyObject *old = tmp_listcontr_1__iter_value_0;
        tmp_listcontr_1__iter_value_0 = tmp_assign_source_10;
        Py_XDECREF( old );
    }

    tmp_assign_source_11 = tmp_listcontr_1__iter_value_0;

    {
        PyObject *old = var_name;
        var_name = tmp_assign_source_11;
        Py_INCREF( var_name );
        Py_XDECREF( old );
    }

    tmp_append_to_1 = tmp_listcontr_1__listcontr_result;

    tmp_append_value_1 = PyTuple_New( 2 );
    tmp_source_name_8 = var_name;

    tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_replace );
    if ( tmp_called_name_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_append_value_1 );

        frame_function->f_lineno = 406;
        goto try_finally_handler_3;
    }
    tmp_call_arg_element_2 = const_str_chr_92;
    tmp_call_arg_element_3 = const_str_chr_47;
    frame_function->f_lineno = 406;
    tmp_source_name_7 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_9, tmp_call_arg_element_2, tmp_call_arg_element_3 );
    Py_DECREF( tmp_called_name_9 );
    if ( tmp_source_name_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_append_value_1 );

        frame_function->f_lineno = 406;
        goto try_finally_handler_3;
    }
    tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_lower );
    Py_DECREF( tmp_source_name_7 );
    if ( tmp_called_name_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_append_value_1 );

        frame_function->f_lineno = 406;
        goto try_finally_handler_3;
    }
    frame_function->f_lineno = 406;
    tmp_tuple_element_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_8 );
    Py_DECREF( tmp_called_name_8 );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_append_value_1 );

        frame_function->f_lineno = 406;
        goto try_finally_handler_3;
    }
    PyTuple_SET_ITEM( tmp_append_value_1, 0, tmp_tuple_element_2 );
    tmp_tuple_element_2 = var_name;

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_append_value_1, 1, tmp_tuple_element_2 );
    tmp_res = PyList_Append( tmp_append_to_1, tmp_append_value_1 );
    Py_DECREF( tmp_append_value_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 406;
        goto try_finally_handler_3;
    }
    tmp_unused = Py_None;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 406;
        goto try_finally_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    tmp_tuple_element_1 = tmp_listcontr_1__listcontr_result;

    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_1 = frame_function->f_lineno;
    Py_XDECREF( tmp_listcontr_1__iter_value_0 );
    tmp_listcontr_1__iter_value_0 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_1;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto try_finally_handler_2;
    }

    goto finally_end_1;
    finally_end_1:;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_2 = frame_function->f_lineno;
    Py_XDECREF( tmp_listcontr_1__listcontr_result );
    tmp_listcontr_1__listcontr_result = NULL;

    Py_XDECREF( tmp_listcontr_1__listcontr_iter );
    tmp_listcontr_1__listcontr_iter = NULL;

    frame_function->f_lineno = tmp_tried_lineno_2;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_2 != NULL )
    {
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;

        goto frame_exception_exit_1;
    }

    goto finally_end_2;
    finally_end_2:;
    frame_function->f_lineno = 406;
    tmp_assign_source_7 = CALL_FUNCTION_WITH_POSARGS( tmp_called_name_6, tmp_args_name_1 );
    Py_DECREF( tmp_args_name_1 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 406;
        goto frame_exception_exit_1;
    }
    assert( var_component_names == NULL );
    var_component_names = tmp_assign_source_7;

    tmp_cond_value_3 = par_verbosity;

    tmp_cond_truth_3 = CHECK_IF_TRUE( tmp_cond_value_3 );
    if ( tmp_cond_truth_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 409;
        goto frame_exception_exit_1;
    }
    if (tmp_cond_truth_3 == 1)
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_source_name_9 = par_logfile;

    tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_write );
    if ( tmp_called_name_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 410;
        goto frame_exception_exit_1;
    }
    tmp_call_arg_element_4 = const_str_digest_4cf32320aa3ddff505589fa6cf78ca12;
    frame_function->f_lineno = 410;
    tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, tmp_call_arg_element_4 );
    Py_DECREF( tmp_called_name_10 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 410;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_source_name_10 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_pprint );

    if (unlikely( tmp_source_name_10 == NULL ))
    {
        tmp_source_name_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pprint );
    }

    if ( tmp_source_name_10 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9544 ], 35, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 411;
        goto frame_exception_exit_1;
    }

    tmp_called_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_pprint );
    if ( tmp_called_name_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 411;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_4 = var_component_names;

    tmp_args_element_name_5 = par_logfile;

    frame_function->f_lineno = 411;
    tmp_unused = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_11, tmp_args_element_name_4, tmp_args_element_name_5 );
    Py_DECREF( tmp_called_name_11 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 411;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    branch_no_4:;
    tmp_compare_left_2 = const_str_digest_0aee4052822580a28ec5d0219bba3814;
    tmp_compare_right_2 = var_component_names;

    tmp_cmp_In_1 = PySequence_Contains( tmp_compare_right_2, tmp_compare_left_2 );
    if ( tmp_cmp_In_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 412;
        goto frame_exception_exit_1;
    }
    if (tmp_cmp_In_1 == 1)
    {
        goto branch_yes_5;
    }
    else
    {
        goto branch_no_5;
    }
    branch_yes_5:;
    tmp_import_globals_1 = ((PyModuleObject *)module_xlrd)->md_dict;
    tmp_import_locals_1 = PyDict_New();
    if ( var_peeksz != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_peeksz,
            var_peeksz
        );
        assert( tmp_res != -1 );

    }
    if ( var_peek != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_peek,
            var_peek
        );
        assert( tmp_res != -1 );

    }
    if ( var_f != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_f,
            var_f
        );
        assert( tmp_res != -1 );

    }
    if ( var_zf != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_zf,
            var_zf
        );
        assert( tmp_res != -1 );

    }
    if ( var_name != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_name,
            var_name
        );
        assert( tmp_res != -1 );

    }
    if ( var_component_names != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_component_names,
            var_component_names
        );
        assert( tmp_res != -1 );

    }
    if ( var_xlsx != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_xlsx,
            var_xlsx
        );
        assert( tmp_res != -1 );

    }
    if ( var_bk != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_bk,
            var_bk
        );
        assert( tmp_res != -1 );

    }
    if ( var_book != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_book,
            var_book
        );
        assert( tmp_res != -1 );

    }
    if ( par_filename != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_filename,
            par_filename
        );
        assert( tmp_res != -1 );

    }
    if ( par_logfile != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_logfile,
            par_logfile
        );
        assert( tmp_res != -1 );

    }
    if ( par_verbosity != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_verbosity,
            par_verbosity
        );
        assert( tmp_res != -1 );

    }
    if ( par_use_mmap != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_use_mmap,
            par_use_mmap
        );
        assert( tmp_res != -1 );

    }
    if ( par_file_contents != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_file_contents,
            par_file_contents
        );
        assert( tmp_res != -1 );

    }
    if ( par_encoding_override != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_encoding_override,
            par_encoding_override
        );
        assert( tmp_res != -1 );

    }
    if ( par_formatting_info != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_formatting_info,
            par_formatting_info
        );
        assert( tmp_res != -1 );

    }
    if ( par_on_demand != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_on_demand,
            par_on_demand
        );
        assert( tmp_res != -1 );

    }
    if ( par_ragged_rows != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_ragged_rows,
            par_ragged_rows
        );
        assert( tmp_res != -1 );

    }
    frame_function->f_lineno = 413;
    tmp_import_name_from_1 = IMPORT_MODULE( const_str_empty, tmp_import_globals_1, tmp_import_locals_1, const_tuple_str_plain_xlsx_tuple, const_int_pos_1 );
    Py_DECREF( tmp_import_locals_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 413;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_xlsx );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 413;
        goto frame_exception_exit_1;
    }
    assert( var_xlsx == NULL );
    var_xlsx = tmp_assign_source_12;

    tmp_source_name_11 = var_xlsx;

    tmp_called_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_open_workbook_2007_xml );
    if ( tmp_called_name_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 414;
        goto frame_exception_exit_1;
    }
    tmp_args_name_2 = PyTuple_New( 2 );
    tmp_tuple_element_3 = var_zf;

    Py_INCREF( tmp_tuple_element_3 );
    PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
    tmp_tuple_element_3 = var_component_names;

    Py_INCREF( tmp_tuple_element_3 );
    PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
    tmp_kw_name_1 = _PyDict_NewPresized( 6 );
    tmp_dict_value_1 = par_logfile;

    tmp_dict_key_1 = const_str_plain_logfile;
    PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    tmp_dict_value_2 = par_verbosity;

    tmp_dict_key_2 = const_str_plain_verbosity;
    PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    tmp_dict_value_3 = par_use_mmap;

    tmp_dict_key_3 = const_str_plain_use_mmap;
    PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_3, tmp_dict_value_3 );
    tmp_dict_value_4 = par_formatting_info;

    tmp_dict_key_4 = const_str_plain_formatting_info;
    PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_4, tmp_dict_value_4 );
    tmp_dict_value_5 = par_on_demand;

    tmp_dict_key_5 = const_str_plain_on_demand;
    PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_5, tmp_dict_value_5 );
    tmp_dict_value_6 = par_ragged_rows;

    tmp_dict_key_6 = const_str_plain_ragged_rows;
    PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_6, tmp_dict_value_6 );
    frame_function->f_lineno = 422;
    tmp_assign_source_13 = CALL_FUNCTION( tmp_called_name_12, tmp_args_name_2, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_12 );
    Py_DECREF( tmp_args_name_2 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 422;
        goto frame_exception_exit_1;
    }
    assert( var_bk == NULL );
    var_bk = tmp_assign_source_13;

    tmp_return_value = var_bk;

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;
    branch_no_5:;
    tmp_compare_left_3 = const_str_digest_fe43db75ca6b370f5d56a6513ba6fcdc;
    tmp_compare_right_3 = var_component_names;

    tmp_cmp_In_2 = PySequence_Contains( tmp_compare_right_3, tmp_compare_left_3 );
    if ( tmp_cmp_In_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 425;
        goto frame_exception_exit_1;
    }
    if (tmp_cmp_In_2 == 1)
    {
        goto branch_yes_6;
    }
    else
    {
        goto branch_no_6;
    }
    branch_yes_6:;
    tmp_called_name_13 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_XLRDError );

    if (unlikely( tmp_called_name_13 == NULL ))
    {
        tmp_called_name_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XLRDError );
    }

    if ( tmp_called_name_13 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9579 ], 38, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 426;
        goto frame_exception_exit_1;
    }

    tmp_call_arg_element_5 = const_str_digest_d831389daee80ab6adeb0fc85fa26d37;
    frame_function->f_lineno = 426;
    tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, tmp_call_arg_element_5 );
    if ( tmp_raise_type_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 426;
        goto frame_exception_exit_1;
    }
    exception_type = tmp_raise_type_1;
    frame_function->f_lineno = 426;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    goto frame_exception_exit_1;
    branch_no_6:;
    tmp_compare_left_4 = const_str_digest_7f6e04e15ba50427cd2014c6a8450f14;
    tmp_compare_right_4 = var_component_names;

    tmp_cmp_In_3 = PySequence_Contains( tmp_compare_right_4, tmp_compare_left_4 );
    if ( tmp_cmp_In_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 427;
        goto frame_exception_exit_1;
    }
    if (tmp_cmp_In_3 == 1)
    {
        goto branch_yes_7;
    }
    else
    {
        goto branch_no_7;
    }
    branch_yes_7:;
    tmp_called_name_14 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_XLRDError );

    if (unlikely( tmp_called_name_14 == NULL ))
    {
        tmp_called_name_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XLRDError );
    }

    if ( tmp_called_name_14 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9579 ], 38, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 428;
        goto frame_exception_exit_1;
    }

    tmp_call_arg_element_6 = const_str_digest_3e6fd7fd1c77330b0409e6c8469d2388;
    frame_function->f_lineno = 428;
    tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, tmp_call_arg_element_6 );
    if ( tmp_raise_type_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 428;
        goto frame_exception_exit_1;
    }
    exception_type = tmp_raise_type_2;
    frame_function->f_lineno = 428;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    goto frame_exception_exit_1;
    branch_no_7:;
    tmp_called_name_15 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_XLRDError );

    if (unlikely( tmp_called_name_15 == NULL ))
    {
        tmp_called_name_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XLRDError );
    }

    if ( tmp_called_name_15 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9579 ], 38, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 429;
        goto frame_exception_exit_1;
    }

    tmp_call_arg_element_7 = const_str_digest_e885fcc67d8d845a867449c2412297e0;
    frame_function->f_lineno = 429;
    tmp_raise_type_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, tmp_call_arg_element_7 );
    if ( tmp_raise_type_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 429;
        goto frame_exception_exit_1;
    }
    exception_type = tmp_raise_type_3;
    frame_function->f_lineno = 429;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    goto frame_exception_exit_1;
    branch_no_2:;
    tmp_import_globals_2 = ((PyModuleObject *)module_xlrd)->md_dict;
    tmp_import_locals_2 = PyDict_New();
    if ( var_peeksz != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_peeksz,
            var_peeksz
        );
        assert( tmp_res != -1 );

    }
    if ( var_peek != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_peek,
            var_peek
        );
        assert( tmp_res != -1 );

    }
    if ( var_f != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_f,
            var_f
        );
        assert( tmp_res != -1 );

    }
    if ( var_zf != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_zf,
            var_zf
        );
        assert( tmp_res != -1 );

    }
    if ( var_name != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_name,
            var_name
        );
        assert( tmp_res != -1 );

    }
    if ( var_component_names != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_component_names,
            var_component_names
        );
        assert( tmp_res != -1 );

    }
    if ( var_xlsx != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_xlsx,
            var_xlsx
        );
        assert( tmp_res != -1 );

    }
    if ( var_bk != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_bk,
            var_bk
        );
        assert( tmp_res != -1 );

    }
    if ( var_book != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_book,
            var_book
        );
        assert( tmp_res != -1 );

    }
    if ( par_filename != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_filename,
            par_filename
        );
        assert( tmp_res != -1 );

    }
    if ( par_logfile != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_logfile,
            par_logfile
        );
        assert( tmp_res != -1 );

    }
    if ( par_verbosity != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_verbosity,
            par_verbosity
        );
        assert( tmp_res != -1 );

    }
    if ( par_use_mmap != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_use_mmap,
            par_use_mmap
        );
        assert( tmp_res != -1 );

    }
    if ( par_file_contents != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_file_contents,
            par_file_contents
        );
        assert( tmp_res != -1 );

    }
    if ( par_encoding_override != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_encoding_override,
            par_encoding_override
        );
        assert( tmp_res != -1 );

    }
    if ( par_formatting_info != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_formatting_info,
            par_formatting_info
        );
        assert( tmp_res != -1 );

    }
    if ( par_on_demand != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_on_demand,
            par_on_demand
        );
        assert( tmp_res != -1 );

    }
    if ( par_ragged_rows != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_2,
            const_str_plain_ragged_rows,
            par_ragged_rows
        );
        assert( tmp_res != -1 );

    }
    frame_function->f_lineno = 431;
    tmp_import_name_from_2 = IMPORT_MODULE( const_str_empty, tmp_import_globals_2, tmp_import_locals_2, const_tuple_str_plain_book_tuple, const_int_pos_1 );
    Py_DECREF( tmp_import_locals_2 );
    if ( tmp_import_name_from_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 431;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_book );
    Py_DECREF( tmp_import_name_from_2 );
    if ( tmp_assign_source_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 431;
        goto frame_exception_exit_1;
    }
    assert( var_book == NULL );
    var_book = tmp_assign_source_14;

    tmp_source_name_12 = var_book;

    tmp_called_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_open_workbook_xls );
    if ( tmp_called_name_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 432;
        goto frame_exception_exit_1;
    }
    tmp_kw_name_2 = _PyDict_NewPresized( 9 );
    tmp_dict_value_7 = par_filename;

    tmp_dict_key_7 = const_str_plain_filename;
    PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_7, tmp_dict_value_7 );
    tmp_dict_value_8 = par_logfile;

    tmp_dict_key_8 = const_str_plain_logfile;
    PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_8, tmp_dict_value_8 );
    tmp_dict_value_9 = par_verbosity;

    tmp_dict_key_9 = const_str_plain_verbosity;
    PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_9, tmp_dict_value_9 );
    tmp_dict_value_10 = par_use_mmap;

    tmp_dict_key_10 = const_str_plain_use_mmap;
    PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_10, tmp_dict_value_10 );
    tmp_dict_value_11 = par_file_contents;

    tmp_dict_key_11 = const_str_plain_file_contents;
    PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_11, tmp_dict_value_11 );
    tmp_dict_value_12 = par_encoding_override;

    tmp_dict_key_12 = const_str_plain_encoding_override;
    PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_12, tmp_dict_value_12 );
    tmp_dict_value_13 = par_formatting_info;

    tmp_dict_key_13 = const_str_plain_formatting_info;
    PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_13, tmp_dict_value_13 );
    tmp_dict_value_14 = par_on_demand;

    tmp_dict_key_14 = const_str_plain_on_demand;
    PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_14, tmp_dict_value_14 );
    tmp_dict_value_15 = par_ragged_rows;

    tmp_dict_key_15 = const_str_plain_ragged_rows;
    PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_15, tmp_dict_value_15 );
    frame_function->f_lineno = 441;
    tmp_assign_source_15 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_16, tmp_kw_name_2 );
    Py_DECREF( tmp_called_name_16 );
    Py_DECREF( tmp_kw_name_2 );
    if ( tmp_assign_source_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 441;
        goto frame_exception_exit_1;
    }
    assert( var_bk == NULL );
    var_bk = tmp_assign_source_15;


#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto try_finally_handler_start_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var_peeksz != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_peeksz,
            var_peeksz
        );
        assert( tmp_res != -1 );

    }
    if ( var_peek != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_peek,
            var_peek
        );
        assert( tmp_res != -1 );

    }
    if ( var_f != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_f,
            var_f
        );
        assert( tmp_res != -1 );

    }
    if ( var_zf != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_zf,
            var_zf
        );
        assert( tmp_res != -1 );

    }
    if ( var_name != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_name,
            var_name
        );
        assert( tmp_res != -1 );

    }
    if ( var_component_names != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_component_names,
            var_component_names
        );
        assert( tmp_res != -1 );

    }
    if ( var_xlsx != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_xlsx,
            var_xlsx
        );
        assert( tmp_res != -1 );

    }
    if ( var_bk != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_bk,
            var_bk
        );
        assert( tmp_res != -1 );

    }
    if ( var_book != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_book,
            var_book
        );
        assert( tmp_res != -1 );

    }
    if ( par_filename != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_filename,
            par_filename
        );
        assert( tmp_res != -1 );

    }
    if ( par_logfile != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_logfile,
            par_logfile
        );
        assert( tmp_res != -1 );

    }
    if ( par_verbosity != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_verbosity,
            par_verbosity
        );
        assert( tmp_res != -1 );

    }
    if ( par_use_mmap != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_use_mmap,
            par_use_mmap
        );
        assert( tmp_res != -1 );

    }
    if ( par_file_contents != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_file_contents,
            par_file_contents
        );
        assert( tmp_res != -1 );

    }
    if ( par_encoding_override != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_encoding_override,
            par_encoding_override
        );
        assert( tmp_res != -1 );

    }
    if ( par_formatting_info != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_formatting_info,
            par_formatting_info
        );
        assert( tmp_res != -1 );

    }
    if ( par_on_demand != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_on_demand,
            par_on_demand
        );
        assert( tmp_res != -1 );

    }
    if ( par_ragged_rows != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_ragged_rows,
            par_ragged_rows
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    tmp_return_value = var_bk;

    Py_INCREF( tmp_return_value );
    goto try_finally_handler_start_1;
    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    CHECK_OBJECT( (PyObject *)par_logfile );
    Py_DECREF( par_logfile );
    par_logfile = NULL;

    CHECK_OBJECT( (PyObject *)par_verbosity );
    Py_DECREF( par_verbosity );
    par_verbosity = NULL;

    CHECK_OBJECT( (PyObject *)par_use_mmap );
    Py_DECREF( par_use_mmap );
    par_use_mmap = NULL;

    CHECK_OBJECT( (PyObject *)par_file_contents );
    Py_DECREF( par_file_contents );
    par_file_contents = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding_override );
    Py_DECREF( par_encoding_override );
    par_encoding_override = NULL;

    CHECK_OBJECT( (PyObject *)par_formatting_info );
    Py_DECREF( par_formatting_info );
    par_formatting_info = NULL;

    CHECK_OBJECT( (PyObject *)par_on_demand );
    Py_DECREF( par_on_demand );
    par_on_demand = NULL;

    CHECK_OBJECT( (PyObject *)par_ragged_rows );
    Py_DECREF( par_ragged_rows );
    par_ragged_rows = NULL;

    CHECK_OBJECT( (PyObject *)var_peeksz );
    Py_DECREF( var_peeksz );
    var_peeksz = NULL;

    Py_XDECREF( var_peek );
    var_peek = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_zf );
    var_zf = NULL;

    Py_XDECREF( var_name );
    var_name = NULL;

    Py_XDECREF( var_component_names );
    var_component_names = NULL;

    Py_XDECREF( var_xlsx );
    var_xlsx = NULL;

    Py_XDECREF( var_bk );
    var_bk = NULL;

    Py_XDECREF( var_book );
    var_book = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_3 != NULL )
    {
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_3;
    finally_end_3:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_1_open_workbook_of_xlrd );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_1_open_workbook_of_xlrd( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_filename = NULL;
    PyObject *_python_par_logfile = NULL;
    PyObject *_python_par_verbosity = NULL;
    PyObject *_python_par_use_mmap = NULL;
    PyObject *_python_par_file_contents = NULL;
    PyObject *_python_par_encoding_override = NULL;
    PyObject *_python_par_formatting_info = NULL;
    PyObject *_python_par_on_demand = NULL;
    PyObject *_python_par_ragged_rows = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "open_workbook() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_filename == key )
            {
                assert( _python_par_filename == NULL );
                _python_par_filename = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_logfile == key )
            {
                assert( _python_par_logfile == NULL );
                _python_par_logfile = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_verbosity == key )
            {
                assert( _python_par_verbosity == NULL );
                _python_par_verbosity = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_use_mmap == key )
            {
                assert( _python_par_use_mmap == NULL );
                _python_par_use_mmap = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_file_contents == key )
            {
                assert( _python_par_file_contents == NULL );
                _python_par_file_contents = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_encoding_override == key )
            {
                assert( _python_par_encoding_override == NULL );
                _python_par_encoding_override = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_formatting_info == key )
            {
                assert( _python_par_formatting_info == NULL );
                _python_par_formatting_info = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_on_demand == key )
            {
                assert( _python_par_on_demand == NULL );
                _python_par_on_demand = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_ragged_rows == key )
            {
                assert( _python_par_ragged_rows == NULL );
                _python_par_ragged_rows = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_filename, key ) == 1 )
            {
                assert( _python_par_filename == NULL );
                _python_par_filename = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_logfile, key ) == 1 )
            {
                assert( _python_par_logfile == NULL );
                _python_par_logfile = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_verbosity, key ) == 1 )
            {
                assert( _python_par_verbosity == NULL );
                _python_par_verbosity = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_use_mmap, key ) == 1 )
            {
                assert( _python_par_use_mmap == NULL );
                _python_par_use_mmap = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_file_contents, key ) == 1 )
            {
                assert( _python_par_file_contents == NULL );
                _python_par_file_contents = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_encoding_override, key ) == 1 )
            {
                assert( _python_par_encoding_override == NULL );
                _python_par_encoding_override = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_formatting_info, key ) == 1 )
            {
                assert( _python_par_formatting_info == NULL );
                _python_par_formatting_info = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_on_demand, key ) == 1 )
            {
                assert( _python_par_on_demand == NULL );
                _python_par_on_demand = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_ragged_rows, key ) == 1 )
            {
                assert( _python_par_ragged_rows == NULL );
                _python_par_ragged_rows = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "open_workbook() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 9 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_filename != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_filename = args[ 0 ];
        Py_INCREF( _python_par_filename );
    }
    else if ( _python_par_filename == NULL )
    {
        if ( 0 + self->m_defaults_given >= 9  )
        {
            _python_par_filename = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 9 );
            Py_INCREF( _python_par_filename );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 1 < args_given ))
    {
         if (unlikely( _python_par_logfile != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 1 );
             goto error_exit;
         }

        _python_par_logfile = args[ 1 ];
        Py_INCREF( _python_par_logfile );
    }
    else if ( _python_par_logfile == NULL )
    {
        if ( 1 + self->m_defaults_given >= 9  )
        {
            _python_par_logfile = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 1 - 9 );
            Py_INCREF( _python_par_logfile );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 2 < args_given ))
    {
         if (unlikely( _python_par_verbosity != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 2 );
             goto error_exit;
         }

        _python_par_verbosity = args[ 2 ];
        Py_INCREF( _python_par_verbosity );
    }
    else if ( _python_par_verbosity == NULL )
    {
        if ( 2 + self->m_defaults_given >= 9  )
        {
            _python_par_verbosity = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 2 - 9 );
            Py_INCREF( _python_par_verbosity );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 3 < args_given ))
    {
         if (unlikely( _python_par_use_mmap != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 3 );
             goto error_exit;
         }

        _python_par_use_mmap = args[ 3 ];
        Py_INCREF( _python_par_use_mmap );
    }
    else if ( _python_par_use_mmap == NULL )
    {
        if ( 3 + self->m_defaults_given >= 9  )
        {
            _python_par_use_mmap = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 3 - 9 );
            Py_INCREF( _python_par_use_mmap );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 4 < args_given ))
    {
         if (unlikely( _python_par_file_contents != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 4 );
             goto error_exit;
         }

        _python_par_file_contents = args[ 4 ];
        Py_INCREF( _python_par_file_contents );
    }
    else if ( _python_par_file_contents == NULL )
    {
        if ( 4 + self->m_defaults_given >= 9  )
        {
            _python_par_file_contents = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 4 - 9 );
            Py_INCREF( _python_par_file_contents );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 5 < args_given ))
    {
         if (unlikely( _python_par_encoding_override != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 5 );
             goto error_exit;
         }

        _python_par_encoding_override = args[ 5 ];
        Py_INCREF( _python_par_encoding_override );
    }
    else if ( _python_par_encoding_override == NULL )
    {
        if ( 5 + self->m_defaults_given >= 9  )
        {
            _python_par_encoding_override = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 5 - 9 );
            Py_INCREF( _python_par_encoding_override );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 6 < args_given ))
    {
         if (unlikely( _python_par_formatting_info != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 6 );
             goto error_exit;
         }

        _python_par_formatting_info = args[ 6 ];
        Py_INCREF( _python_par_formatting_info );
    }
    else if ( _python_par_formatting_info == NULL )
    {
        if ( 6 + self->m_defaults_given >= 9  )
        {
            _python_par_formatting_info = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 6 - 9 );
            Py_INCREF( _python_par_formatting_info );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 7 < args_given ))
    {
         if (unlikely( _python_par_on_demand != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 7 );
             goto error_exit;
         }

        _python_par_on_demand = args[ 7 ];
        Py_INCREF( _python_par_on_demand );
    }
    else if ( _python_par_on_demand == NULL )
    {
        if ( 7 + self->m_defaults_given >= 9  )
        {
            _python_par_on_demand = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 7 - 9 );
            Py_INCREF( _python_par_on_demand );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 8 < args_given ))
    {
         if (unlikely( _python_par_ragged_rows != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 8 );
             goto error_exit;
         }

        _python_par_ragged_rows = args[ 8 ];
        Py_INCREF( _python_par_ragged_rows );
    }
    else if ( _python_par_ragged_rows == NULL )
    {
        if ( 8 + self->m_defaults_given >= 9  )
        {
            _python_par_ragged_rows = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 8 - 9 );
            Py_INCREF( _python_par_ragged_rows );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_filename == NULL || _python_par_logfile == NULL || _python_par_verbosity == NULL || _python_par_use_mmap == NULL || _python_par_file_contents == NULL || _python_par_encoding_override == NULL || _python_par_formatting_info == NULL || _python_par_on_demand == NULL || _python_par_ragged_rows == NULL ))
    {
        PyObject *values[] = { _python_par_filename, _python_par_logfile, _python_par_verbosity, _python_par_use_mmap, _python_par_file_contents, _python_par_encoding_override, _python_par_formatting_info, _python_par_on_demand, _python_par_ragged_rows };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_1_open_workbook_of_xlrd( self, _python_par_filename, _python_par_logfile, _python_par_verbosity, _python_par_use_mmap, _python_par_file_contents, _python_par_encoding_override, _python_par_formatting_info, _python_par_on_demand, _python_par_ragged_rows );

error_exit:;

    Py_XDECREF( _python_par_filename );
    Py_XDECREF( _python_par_logfile );
    Py_XDECREF( _python_par_verbosity );
    Py_XDECREF( _python_par_use_mmap );
    Py_XDECREF( _python_par_file_contents );
    Py_XDECREF( _python_par_encoding_override );
    Py_XDECREF( _python_par_formatting_info );
    Py_XDECREF( _python_par_on_demand );
    Py_XDECREF( _python_par_ragged_rows );

    return NULL;
}

static PyObject *dparse_function_1_open_workbook_of_xlrd( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 9 )
    {
        return impl_function_1_open_workbook_of_xlrd( self, INCREASE_REFCOUNT( args[ 0 ] ), INCREASE_REFCOUNT( args[ 1 ] ), INCREASE_REFCOUNT( args[ 2 ] ), INCREASE_REFCOUNT( args[ 3 ] ), INCREASE_REFCOUNT( args[ 4 ] ), INCREASE_REFCOUNT( args[ 5 ] ), INCREASE_REFCOUNT( args[ 6 ] ), INCREASE_REFCOUNT( args[ 7 ] ), INCREASE_REFCOUNT( args[ 8 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_1_open_workbook_of_xlrd( self, args, size, NULL );
        return result;
    }

}



static PyObject *impl_function_2_dump_of_xlrd( Nuitka_FunctionObject *self, PyObject *_python_par_filename, PyObject *_python_par_outfile, PyObject *_python_par_unnumbered )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_filename = _python_par_filename;
    PyObject *par_outfile = _python_par_outfile;
    PyObject *par_unnumbered = _python_par_unnumbered;
    PyObject *var_biff_dump = NULL;
    PyObject *var_bk = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_frame_locals;
    PyObject *tmp_import_globals_1;
    PyObject *tmp_import_locals_1;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_kw_name_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_70e733aa06c1152176286b7c783331f5, module_xlrd );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_import_globals_1 = ((PyModuleObject *)module_xlrd)->md_dict;
    tmp_import_locals_1 = PyDict_New();
    if ( var_biff_dump != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_biff_dump,
            var_biff_dump
        );
        assert( tmp_res != -1 );

    }
    if ( var_bk != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_bk,
            var_bk
        );
        assert( tmp_res != -1 );

    }
    if ( par_filename != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_filename,
            par_filename
        );
        assert( tmp_res != -1 );

    }
    if ( par_outfile != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_outfile,
            par_outfile
        );
        assert( tmp_res != -1 );

    }
    if ( par_unnumbered != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_unnumbered,
            par_unnumbered
        );
        assert( tmp_res != -1 );

    }
    frame_function->f_lineno = 452;
    tmp_import_name_from_1 = IMPORT_MODULE( const_str_plain_biffh, tmp_import_globals_1, tmp_import_locals_1, const_tuple_str_plain_biff_dump_tuple, const_int_pos_1 );
    Py_DECREF( tmp_import_locals_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 452;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_biff_dump );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 452;
        goto frame_exception_exit_1;
    }
    assert( var_biff_dump == NULL );
    var_biff_dump = tmp_assign_source_1;

    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_Book );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Book );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9617 ], 33, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 453;
        goto frame_exception_exit_1;
    }

    frame_function->f_lineno = 453;
    tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 453;
        goto frame_exception_exit_1;
    }
    assert( var_bk == NULL );
    var_bk = tmp_assign_source_2;

    tmp_source_name_1 = var_bk;

    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_biff2_8_load );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 454;
        goto frame_exception_exit_1;
    }
    tmp_kw_name_1 = _PyDict_NewPresized( 2 );
    tmp_dict_value_1 = par_filename;

    tmp_dict_key_1 = const_str_plain_filename;
    PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    tmp_dict_value_2 = par_outfile;

    tmp_dict_key_2 = const_str_plain_logfile;
    PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    frame_function->f_lineno = 454;
    tmp_unused = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 454;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_called_name_3 = var_biff_dump;

    tmp_source_name_2 = var_bk;

    tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_mem );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 455;
        goto frame_exception_exit_1;
    }
    tmp_source_name_3 = var_bk;

    tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_base );
    if ( tmp_args_element_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_element_name_1 );

        frame_function->f_lineno = 455;
        goto frame_exception_exit_1;
    }
    tmp_source_name_4 = var_bk;

    tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_stream_len );
    if ( tmp_args_element_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );

        frame_function->f_lineno = 455;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_4 = const_int_0;
    tmp_args_element_name_5 = par_outfile;

    tmp_args_element_name_6 = par_unnumbered;

    frame_function->f_lineno = 455;
    tmp_unused = CALL_FUNCTION_WITH_ARGS6( tmp_called_name_3, tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 );
    Py_DECREF( tmp_args_element_name_1 );
    Py_DECREF( tmp_args_element_name_2 );
    Py_DECREF( tmp_args_element_name_3 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 455;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var_biff_dump != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_biff_dump,
            var_biff_dump
        );
        assert( tmp_res != -1 );

    }
    if ( var_bk != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_bk,
            var_bk
        );
        assert( tmp_res != -1 );

    }
    if ( par_filename != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_filename,
            par_filename
        );
        assert( tmp_res != -1 );

    }
    if ( par_outfile != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_outfile,
            par_outfile
        );
        assert( tmp_res != -1 );

    }
    if ( par_unnumbered != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_unnumbered,
            par_unnumbered
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_finally_handler_start_1;
    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    CHECK_OBJECT( (PyObject *)par_outfile );
    Py_DECREF( par_outfile );
    par_outfile = NULL;

    CHECK_OBJECT( (PyObject *)par_unnumbered );
    Py_DECREF( par_unnumbered );
    par_unnumbered = NULL;

    Py_XDECREF( var_biff_dump );
    var_biff_dump = NULL;

    Py_XDECREF( var_bk );
    var_bk = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_1;
    finally_end_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_2_dump_of_xlrd );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_2_dump_of_xlrd( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_filename = NULL;
    PyObject *_python_par_outfile = NULL;
    PyObject *_python_par_unnumbered = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "dump() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_filename == key )
            {
                assert( _python_par_filename == NULL );
                _python_par_filename = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_outfile == key )
            {
                assert( _python_par_outfile == NULL );
                _python_par_outfile = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_unnumbered == key )
            {
                assert( _python_par_unnumbered == NULL );
                _python_par_unnumbered = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_filename, key ) == 1 )
            {
                assert( _python_par_filename == NULL );
                _python_par_filename = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_outfile, key ) == 1 )
            {
                assert( _python_par_outfile == NULL );
                _python_par_outfile = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_unnumbered, key ) == 1 )
            {
                assert( _python_par_unnumbered == NULL );
                _python_par_unnumbered = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "dump() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 3 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_filename != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_filename = args[ 0 ];
        Py_INCREF( _python_par_filename );
    }
    else if ( _python_par_filename == NULL )
    {
        if ( 0 + self->m_defaults_given >= 3  )
        {
            _python_par_filename = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 3 );
            Py_INCREF( _python_par_filename );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 1 < args_given ))
    {
         if (unlikely( _python_par_outfile != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 1 );
             goto error_exit;
         }

        _python_par_outfile = args[ 1 ];
        Py_INCREF( _python_par_outfile );
    }
    else if ( _python_par_outfile == NULL )
    {
        if ( 1 + self->m_defaults_given >= 3  )
        {
            _python_par_outfile = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 1 - 3 );
            Py_INCREF( _python_par_outfile );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 2 < args_given ))
    {
         if (unlikely( _python_par_unnumbered != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 2 );
             goto error_exit;
         }

        _python_par_unnumbered = args[ 2 ];
        Py_INCREF( _python_par_unnumbered );
    }
    else if ( _python_par_unnumbered == NULL )
    {
        if ( 2 + self->m_defaults_given >= 3  )
        {
            _python_par_unnumbered = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 2 - 3 );
            Py_INCREF( _python_par_unnumbered );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_filename == NULL || _python_par_outfile == NULL || _python_par_unnumbered == NULL ))
    {
        PyObject *values[] = { _python_par_filename, _python_par_outfile, _python_par_unnumbered };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_2_dump_of_xlrd( self, _python_par_filename, _python_par_outfile, _python_par_unnumbered );

error_exit:;

    Py_XDECREF( _python_par_filename );
    Py_XDECREF( _python_par_outfile );
    Py_XDECREF( _python_par_unnumbered );

    return NULL;
}

static PyObject *dparse_function_2_dump_of_xlrd( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 3 )
    {
        return impl_function_2_dump_of_xlrd( self, INCREASE_REFCOUNT( args[ 0 ] ), INCREASE_REFCOUNT( args[ 1 ] ), INCREASE_REFCOUNT( args[ 2 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_2_dump_of_xlrd( self, args, size, NULL );
        return result;
    }

}



static PyObject *impl_function_3_count_records_of_xlrd( Nuitka_FunctionObject *self, PyObject *_python_par_filename, PyObject *_python_par_outfile )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_filename = _python_par_filename;
    PyObject *par_outfile = _python_par_outfile;
    PyObject *var_biff_count_records = NULL;
    PyObject *var_bk = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_frame_locals;
    PyObject *tmp_import_globals_1;
    PyObject *tmp_import_locals_1;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_kw_name_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_adcb06198f09404874f6145013022edd, module_xlrd );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_import_globals_1 = ((PyModuleObject *)module_xlrd)->md_dict;
    tmp_import_locals_1 = PyDict_New();
    if ( var_biff_count_records != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_biff_count_records,
            var_biff_count_records
        );
        assert( tmp_res != -1 );

    }
    if ( var_bk != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_bk,
            var_bk
        );
        assert( tmp_res != -1 );

    }
    if ( par_filename != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_filename,
            par_filename
        );
        assert( tmp_res != -1 );

    }
    if ( par_outfile != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_import_locals_1,
            const_str_plain_outfile,
            par_outfile
        );
        assert( tmp_res != -1 );

    }
    frame_function->f_lineno = 464;
    tmp_import_name_from_1 = IMPORT_MODULE( const_str_plain_biffh, tmp_import_globals_1, tmp_import_locals_1, const_tuple_str_plain_biff_count_records_tuple, const_int_pos_1 );
    Py_DECREF( tmp_import_locals_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 464;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_biff_count_records );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 464;
        goto frame_exception_exit_1;
    }
    assert( var_biff_count_records == NULL );
    var_biff_count_records = tmp_assign_source_1;

    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_Book );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Book );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9617 ], 33, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 465;
        goto frame_exception_exit_1;
    }

    frame_function->f_lineno = 465;
    tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 465;
        goto frame_exception_exit_1;
    }
    assert( var_bk == NULL );
    var_bk = tmp_assign_source_2;

    tmp_source_name_1 = var_bk;

    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_biff2_8_load );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 466;
        goto frame_exception_exit_1;
    }
    tmp_kw_name_1 = _PyDict_NewPresized( 2 );
    tmp_dict_value_1 = par_filename;

    tmp_dict_key_1 = const_str_plain_filename;
    PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    tmp_dict_value_2 = par_outfile;

    tmp_dict_key_2 = const_str_plain_logfile;
    PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
    frame_function->f_lineno = 466;
    tmp_unused = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
    Py_DECREF( tmp_called_name_2 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 466;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_called_name_3 = var_biff_count_records;

    tmp_source_name_2 = var_bk;

    tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_mem );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 467;
        goto frame_exception_exit_1;
    }
    tmp_source_name_3 = var_bk;

    tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_base );
    if ( tmp_args_element_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_element_name_1 );

        frame_function->f_lineno = 467;
        goto frame_exception_exit_1;
    }
    tmp_source_name_4 = var_bk;

    tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_stream_len );
    if ( tmp_args_element_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );

        frame_function->f_lineno = 467;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_4 = par_outfile;

    frame_function->f_lineno = 467;
    tmp_unused = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_3, tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 );
    Py_DECREF( tmp_args_element_name_1 );
    Py_DECREF( tmp_args_element_name_2 );
    Py_DECREF( tmp_args_element_name_3 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 467;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var_biff_count_records != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_biff_count_records,
            var_biff_count_records
        );
        assert( tmp_res != -1 );

    }
    if ( var_bk != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_bk,
            var_bk
        );
        assert( tmp_res != -1 );

    }
    if ( par_filename != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_filename,
            par_filename
        );
        assert( tmp_res != -1 );

    }
    if ( par_outfile != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_outfile,
            par_outfile
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_finally_handler_start_1;
    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    CHECK_OBJECT( (PyObject *)par_outfile );
    Py_DECREF( par_outfile );
    par_outfile = NULL;

    Py_XDECREF( var_biff_count_records );
    var_biff_count_records = NULL;

    Py_XDECREF( var_bk );
    var_bk = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_1;
    finally_end_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_3_count_records_of_xlrd );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_3_count_records_of_xlrd( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_filename = NULL;
    PyObject *_python_par_outfile = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "count_records() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_filename == key )
            {
                assert( _python_par_filename == NULL );
                _python_par_filename = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_outfile == key )
            {
                assert( _python_par_outfile == NULL );
                _python_par_outfile = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_filename, key ) == 1 )
            {
                assert( _python_par_filename == NULL );
                _python_par_filename = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_outfile, key ) == 1 )
            {
                assert( _python_par_outfile == NULL );
                _python_par_outfile = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "count_records() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 2 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_filename != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_filename = args[ 0 ];
        Py_INCREF( _python_par_filename );
    }
    else if ( _python_par_filename == NULL )
    {
        if ( 0 + self->m_defaults_given >= 2  )
        {
            _python_par_filename = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 2 );
            Py_INCREF( _python_par_filename );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 1 < args_given ))
    {
         if (unlikely( _python_par_outfile != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 1 );
             goto error_exit;
         }

        _python_par_outfile = args[ 1 ];
        Py_INCREF( _python_par_outfile );
    }
    else if ( _python_par_outfile == NULL )
    {
        if ( 1 + self->m_defaults_given >= 2  )
        {
            _python_par_outfile = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 1 - 2 );
            Py_INCREF( _python_par_outfile );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_filename == NULL || _python_par_outfile == NULL ))
    {
        PyObject *values[] = { _python_par_filename, _python_par_outfile };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_3_count_records_of_xlrd( self, _python_par_filename, _python_par_outfile );

error_exit:;

    Py_XDECREF( _python_par_filename );
    Py_XDECREF( _python_par_outfile );

    return NULL;
}

static PyObject *dparse_function_3_count_records_of_xlrd( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 2 )
    {
        return impl_function_3_count_records_of_xlrd( self, INCREASE_REFCOUNT( args[ 0 ] ), INCREASE_REFCOUNT( args[ 1 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_3_count_records_of_xlrd( self, args, size, NULL );
        return result;
    }

}




static PyObject *MAKE_FUNCTION_function_1_open_workbook_of_xlrd( PyObject *defaults )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_1_open_workbook_of_xlrd,
        dparse_function_1_open_workbook_of_xlrd,
        const_str_plain_open_workbook,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_f966c108fee28d98c0de6a12e32abd80,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd,
        Py_None
    );

    return result;
}



static PyObject *MAKE_FUNCTION_function_2_dump_of_xlrd( PyObject *defaults )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_2_dump_of_xlrd,
        dparse_function_2_dump_of_xlrd,
        const_str_plain_dump,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_70e733aa06c1152176286b7c783331f5,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd,
        Py_None
    );

    return result;
}



static PyObject *MAKE_FUNCTION_function_3_count_records_of_xlrd( PyObject *defaults )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_3_count_records_of_xlrd,
        dparse_function_3_count_records_of_xlrd,
        const_str_plain_count_records,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_adcb06198f09404874f6145013022edd,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd,
        Py_None
    );

    return result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_xlrd =
{
    PyModuleDef_HEAD_INIT,
    "xlrd",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

#define _MODULE_UNFREEZER 0

#if _MODULE_UNFREEZER

#include "nuitka/unfreezing.hpp"

// Table for lookup to find "frozen" modules or DLLs, i.e. the ones included in
// or along this binary.

static struct Nuitka_MetaPathBasedLoaderEntry meta_path_loader_entries[] =
{

    { NULL, NULL, 0 }
};

#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( xlrd )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_xlrd );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    // Initialize the compiled types of Nuitka.
    PyType_Ready( &Nuitka_Generator_Type );
    PyType_Ready( &Nuitka_Function_Type );
    PyType_Ready( &Nuitka_Method_Type );
    PyType_Ready( &Nuitka_Frame_Type );
#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

#endif

#if _MODULE_UNFREEZER
    registerMetaPathBasedUnfreezer( meta_path_loader_entries );
#endif

    _initModuleConstants();
    _initModuleCodeObjects();

    // puts( "in initxlrd" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_xlrd = Py_InitModule4(
        "xlrd",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No __doc__ is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else
    module_xlrd = PyModule_Create( &mdef_xlrd );
#endif

    moduledict_xlrd = (PyDictObject *)((PyModuleObject *)module_xlrd)->md_dict;

    CHECK_OBJECT( module_xlrd );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_plain_xlrd, module_xlrd );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    PyObject *module_dict = PyModule_GetDict( module_xlrd );

    if ( PyDict_GetItem( module_dict, const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

#ifndef __NUITKA_NO_ASSERT__
        int res =
#endif
            PyDict_SetItem( module_dict, const_str_plain___builtins__, value );

        assert( res == 0 );
    }

#if PYTHON_VERSION >= 330
#if _MODULE_UNFREEZER
    PyDict_SetItem( module_dict, const_str_plain___loader__, metapath_based_loader );
#else
    PyDict_SetItem( module_dict, const_str_plain___loader__, Py_None );
#endif
#endif

    // Temp variables if any
    PyObject *exception_type, *exception_value;
    PyTracebackObject *exception_tb;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_call_arg_element_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_right_1;
    int tmp_cond_truth_1;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_defaults_1;
    PyObject *tmp_defaults_2;
    PyObject *tmp_defaults_3;
    int tmp_exc_match_exception_match_1;
    PyObject *tmp_import_globals_1;
    PyObject *tmp_import_globals_2;
    PyObject *tmp_import_globals_3;
    PyObject *tmp_import_globals_4;
    PyObject *tmp_import_globals_5;
    PyObject *tmp_import_globals_6;
    PyObject *tmp_import_globals_7;
    PyObject *tmp_import_globals_8;
    PyObject *tmp_import_globals_9;
    PyObject *tmp_import_globals_10;
    PyObject *tmp_import_globals_11;
    PyObject *tmp_import_globals_12;
    PyObject *tmp_import_globals_13;
    PyObject *tmp_import_globals_14;
    PyObject *tmp_import_globals_15;
    PyObject *tmp_import_globals_16;
    PyObject *tmp_import_globals_17;
    PyObject *tmp_import_globals_18;
    PyObject *tmp_import_globals_19;
    PyObject *tmp_import_globals_20;
    PyObject *tmp_import_globals_21;
    PyObject *tmp_import_globals_22;
    PyObject *tmp_import_globals_23;
    PyObject *tmp_import_globals_24;
    PyObject *tmp_import_globals_25;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_import_name_from_2;
    PyObject *tmp_import_name_from_3;
    PyObject *tmp_import_name_from_4;
    PyObject *tmp_import_name_from_5;
    PyObject *tmp_import_name_from_6;
    PyObject *tmp_import_name_from_7;
    PyObject *tmp_import_name_from_8;
    PyObject *tmp_import_name_from_9;
    PyObject *tmp_import_name_from_10;
    PyObject *tmp_import_name_from_11;
    PyObject *tmp_import_name_from_12;
    PyObject *tmp_import_name_from_13;
    PyObject *tmp_import_name_from_14;
    PyObject *tmp_import_name_from_15;
    PyObject *tmp_import_name_from_16;
    PyObject *tmp_import_name_from_17;
    PyObject *tmp_import_name_from_18;
    PyObject *tmp_import_name_from_19;
    PyObject *tmp_list_element_1;
    bool tmp_result;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_source_name_6;
    PyObject *tmp_star_imported_1;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    PyObject *tmp_tuple_element_3;
    PyFrameObject *frame_module;


    // Module code.
    tmp_assign_source_1 = Py_None;
    UPDATE_STRING_DICT0( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = module_filename_obj;
    UPDATE_STRING_DICT0( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    // Frame without reuse.
    frame_module = MAKE_FRAME( codeobj_773ea5bc0dfcfa383ea7f7ee6f6e9dff, module_xlrd );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_module );
    assert( Py_REFCNT( frame_module ) == 1 );

#if PYTHON_VERSION >= 340
    frame_module->f_executing += 1;
#endif

    // Framed code:
    tmp_assign_source_3 = PyList_New( 1 );
    tmp_source_name_1 = PyObject_GetAttrString(PyImport_ImportModule("os"), "path");
    if ( tmp_source_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_3 );


        goto frame_exception_exit_1;
    }
    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_dirname );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_3 );


        goto frame_exception_exit_1;
    }
    tmp_args_element_name_1 = module_filename_obj;
    tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, tmp_args_element_name_1 );
    Py_DECREF( tmp_called_name_1 );
    if ( tmp_list_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_3 );


        goto frame_exception_exit_1;
    }
    PyList_SET_ITEM( tmp_assign_source_3, 0, tmp_list_element_1 );
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain___path__, tmp_assign_source_3 );
    tmp_import_globals_1 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 1;
    tmp_import_name_from_1 = IMPORT_MODULE( const_str_plain_os, tmp_import_globals_1, tmp_import_globals_1, const_tuple_str_plain_path_tuple, const_int_neg_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 1;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_path );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 1;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_path, tmp_assign_source_4 );
    tmp_import_globals_2 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 3;
    tmp_import_name_from_2 = IMPORT_MODULE( const_str_plain_info, tmp_import_globals_2, tmp_import_globals_2, const_tuple_str_plain___VERSION___tuple, const_int_pos_1 );
    if ( tmp_import_name_from_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 3;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain___VERSION__ );
    Py_DECREF( tmp_import_name_from_2 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 3;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain___VERSION__, tmp_assign_source_5 );
    tmp_import_globals_3 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 9;
    tmp_import_name_from_3 = IMPORT_MODULE( const_str_empty, tmp_import_globals_3, tmp_import_globals_3, const_tuple_str_plain_licences_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 9;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_licences );
    Py_DECREF( tmp_import_name_from_3 );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 9;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_licences, tmp_assign_source_6 );
    tmp_import_globals_4 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 306;
    tmp_assign_source_7 = IMPORT_MODULE( const_str_plain_sys, tmp_import_globals_4, tmp_import_globals_4, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 306;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_7 );
    tmp_import_globals_5 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 306;
    tmp_assign_source_8 = IMPORT_MODULE( const_str_plain_zipfile, tmp_import_globals_5, tmp_import_globals_5, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 306;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_zipfile, tmp_assign_source_8 );
    tmp_import_globals_6 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 306;
    tmp_assign_source_9 = IMPORT_MODULE( const_str_plain_pprint, tmp_import_globals_6, tmp_import_globals_6, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 306;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_pprint, tmp_assign_source_9 );
    tmp_import_globals_7 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 307;
    tmp_import_name_from_4 = IMPORT_MODULE( const_str_empty, tmp_import_globals_7, tmp_import_globals_7, const_tuple_str_plain_timemachine_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 307;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_timemachine );
    Py_DECREF( tmp_import_name_from_4 );
    if ( tmp_assign_source_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 307;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_timemachine, tmp_assign_source_10 );
    tmp_import_globals_8 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 308;
    tmp_import_name_from_5 = IMPORT_MODULE( const_str_plain_biffh, tmp_import_globals_8, tmp_import_globals_8, const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_XLRDError );
    Py_DECREF( tmp_import_name_from_5 );
    if ( tmp_assign_source_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_XLRDError, tmp_assign_source_11 );
    tmp_import_globals_9 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 308;
    tmp_import_name_from_6 = IMPORT_MODULE( const_str_plain_biffh, tmp_import_globals_9, tmp_import_globals_9, const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_biff_text_from_num );
    Py_DECREF( tmp_import_name_from_6 );
    if ( tmp_assign_source_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_biff_text_from_num, tmp_assign_source_12 );
    tmp_import_globals_10 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 308;
    tmp_import_name_from_7 = IMPORT_MODULE( const_str_plain_biffh, tmp_import_globals_10, tmp_import_globals_10, const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_error_text_from_code );
    Py_DECREF( tmp_import_name_from_7 );
    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_error_text_from_code, tmp_assign_source_13 );
    tmp_import_globals_11 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 308;
    tmp_import_name_from_8 = IMPORT_MODULE( const_str_plain_biffh, tmp_import_globals_11, tmp_import_globals_11, const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_XL_CELL_BLANK );
    Py_DECREF( tmp_import_name_from_8 );
    if ( tmp_assign_source_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_XL_CELL_BLANK, tmp_assign_source_14 );
    tmp_import_globals_12 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 308;
    tmp_import_name_from_9 = IMPORT_MODULE( const_str_plain_biffh, tmp_import_globals_12, tmp_import_globals_12, const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_XL_CELL_TEXT );
    Py_DECREF( tmp_import_name_from_9 );
    if ( tmp_assign_source_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_XL_CELL_TEXT, tmp_assign_source_15 );
    tmp_import_globals_13 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 308;
    tmp_import_name_from_10 = IMPORT_MODULE( const_str_plain_biffh, tmp_import_globals_13, tmp_import_globals_13, const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_XL_CELL_BOOLEAN );
    Py_DECREF( tmp_import_name_from_10 );
    if ( tmp_assign_source_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_XL_CELL_BOOLEAN, tmp_assign_source_16 );
    tmp_import_globals_14 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 308;
    tmp_import_name_from_11 = IMPORT_MODULE( const_str_plain_biffh, tmp_import_globals_14, tmp_import_globals_14, const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_XL_CELL_ERROR );
    Py_DECREF( tmp_import_name_from_11 );
    if ( tmp_assign_source_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_XL_CELL_ERROR, tmp_assign_source_17 );
    tmp_import_globals_15 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 308;
    tmp_import_name_from_12 = IMPORT_MODULE( const_str_plain_biffh, tmp_import_globals_15, tmp_import_globals_15, const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_XL_CELL_EMPTY );
    Py_DECREF( tmp_import_name_from_12 );
    if ( tmp_assign_source_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_XL_CELL_EMPTY, tmp_assign_source_18 );
    tmp_import_globals_16 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 308;
    tmp_import_name_from_13 = IMPORT_MODULE( const_str_plain_biffh, tmp_import_globals_16, tmp_import_globals_16, const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_XL_CELL_DATE );
    Py_DECREF( tmp_import_name_from_13 );
    if ( tmp_assign_source_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_XL_CELL_DATE, tmp_assign_source_19 );
    tmp_import_globals_17 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 308;
    tmp_import_name_from_14 = IMPORT_MODULE( const_str_plain_biffh, tmp_import_globals_17, tmp_import_globals_17, const_tuple_accbde64e62d5231c0afbed3c34b93f3_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_XL_CELL_NUMBER );
    Py_DECREF( tmp_import_name_from_14 );
    if ( tmp_assign_source_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 308;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_XL_CELL_NUMBER, tmp_assign_source_20 );
    tmp_import_globals_18 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 320;
    tmp_star_imported_1 = IMPORT_MODULE( const_str_plain_formula, tmp_import_globals_18, tmp_import_globals_18, const_tuple_str_chr_42_tuple, const_int_pos_1 );
    if ( tmp_star_imported_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 320;
        goto frame_exception_exit_1;
    }
    tmp_result = IMPORT_MODULE_STAR( module_xlrd, true, tmp_star_imported_1 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_star_imported_1 );

        frame_module->f_lineno = 320;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_star_imported_1 );
    tmp_import_globals_19 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 321;
    tmp_import_name_from_15 = IMPORT_MODULE( const_str_plain_book, tmp_import_globals_19, tmp_import_globals_19, const_tuple_str_plain_Book_str_plain_colname_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 321;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_Book );
    Py_DECREF( tmp_import_name_from_15 );
    if ( tmp_assign_source_21 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 321;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_Book, tmp_assign_source_21 );
    tmp_import_globals_20 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 321;
    tmp_import_name_from_16 = IMPORT_MODULE( const_str_plain_book, tmp_import_globals_20, tmp_import_globals_20, const_tuple_str_plain_Book_str_plain_colname_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 321;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_colname );
    Py_DECREF( tmp_import_name_from_16 );
    if ( tmp_assign_source_22 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 321;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_colname, tmp_assign_source_22 );
    tmp_import_globals_21 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 322;
    tmp_import_name_from_17 = IMPORT_MODULE( const_str_plain_sheet, tmp_import_globals_21, tmp_import_globals_21, const_tuple_str_plain_empty_cell_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 322;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_empty_cell );
    Py_DECREF( tmp_import_name_from_17 );
    if ( tmp_assign_source_23 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 322;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_empty_cell, tmp_assign_source_23 );
    tmp_import_globals_22 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 323;
    tmp_import_name_from_18 = IMPORT_MODULE( const_str_plain_xldate, tmp_import_globals_22, tmp_import_globals_22, const_tuple_str_plain_XLDateError_str_plain_xldate_as_tuple_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 323;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_XLDateError );
    Py_DECREF( tmp_import_name_from_18 );
    if ( tmp_assign_source_24 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 323;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_XLDateError, tmp_assign_source_24 );
    tmp_import_globals_23 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 323;
    tmp_import_name_from_19 = IMPORT_MODULE( const_str_plain_xldate, tmp_import_globals_23, tmp_import_globals_23, const_tuple_str_plain_XLDateError_str_plain_xldate_as_tuple_tuple, const_int_pos_1 );
    if ( tmp_import_name_from_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 323;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_19, const_str_plain_xldate_as_tuple );
    Py_DECREF( tmp_import_name_from_19 );
    if ( tmp_assign_source_25 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 323;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_xldate_as_tuple, tmp_assign_source_25 );
    tmp_source_name_3 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_3 == NULL ))
    {
        tmp_source_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 646 ], 25, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 325;
        goto frame_exception_exit_1;
    }

    tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_version );
    if ( tmp_source_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 325;
        goto frame_exception_exit_1;
    }
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_startswith );
    Py_DECREF( tmp_source_name_2 );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 325;
        goto frame_exception_exit_1;
    }
    tmp_call_arg_element_1 = const_str_plain_IronPython;
    frame_module->f_lineno = 325;
    tmp_cond_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, tmp_call_arg_element_1 );
    Py_DECREF( tmp_called_name_2 );
    if ( tmp_cond_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 325;
        goto frame_exception_exit_1;
    }
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        frame_module->f_lineno = 325;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_1 );
    if (tmp_cond_truth_1 == 1)
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_import_globals_24 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 327;
    tmp_assign_source_26 = IMPORT_MODULE( const_str_plain_encodings, tmp_import_globals_24, tmp_import_globals_24, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_26 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 327;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_encodings, tmp_assign_source_26 );
    branch_no_1:;
    // Tried block of try/except
    tmp_import_globals_25 = ((PyModuleObject *)module_xlrd)->md_dict;
    frame_module->f_lineno = 330;
    tmp_assign_source_27 = IMPORT_MODULE( const_str_plain_mmap, tmp_import_globals_25, tmp_import_globals_25, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_27 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 330;
        goto try_except_handler_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_mmap, tmp_assign_source_27 );
    tmp_assign_source_28 = const_int_pos_1;
    UPDATE_STRING_DICT0( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_MMAP_AVAILABLE, tmp_assign_source_28 );
    goto try_except_end_1;
    try_except_handler_1:;
    // Exception handler of try/except
    // Preserve existing published exception.
    PRESERVE_FRAME_EXCEPTION( frame_module );
    if (exception_tb == NULL)
    {
        exception_tb = MAKE_TRACEBACK( frame_module );
    }
    else if ( exception_tb->tb_frame != frame_module || exception_tb->tb_lineno != frame_module->f_lineno )
    {
        exception_tb = ADD_TRACEBACK( frame_module, exception_tb );
    }

    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    PUBLISH_EXCEPTION( &exception_type, &exception_value, &exception_tb );
    tmp_compare_left_1 = PyThreadState_GET()->exc_type;
    tmp_compare_right_1 = PyExc_ImportError;
    tmp_exc_match_exception_match_1 = EXCEPTION_MATCH_BOOL( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_exc_match_exception_match_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 332;
        goto frame_exception_exit_1;
    }
    if (tmp_exc_match_exception_match_1 == 1)
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_assign_source_29 = const_int_0;
    UPDATE_STRING_DICT0( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_MMAP_AVAILABLE, tmp_assign_source_29 );
    goto branch_end_2;
    branch_no_2:;
    RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (exception_tb && exception_tb->tb_frame == frame_module)     frame_module->f_lineno = exception_tb->tb_lineno;
    goto frame_exception_exit_1;
    branch_end_2:;
    try_except_end_1:;
    tmp_assign_source_30 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_MMAP_AVAILABLE );

    if (unlikely( tmp_assign_source_30 == NULL ))
    {
        tmp_assign_source_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MMAP_AVAILABLE );
    }

    if ( tmp_assign_source_30 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9402 ], 36, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 334;
        goto frame_exception_exit_1;
    }

    UPDATE_STRING_DICT0( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_USE_MMAP, tmp_assign_source_30 );
    tmp_defaults_1 = PyTuple_New( 9 );
    tmp_tuple_element_1 = Py_None;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_defaults_1, 0, tmp_tuple_element_1 );
    tmp_source_name_4 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_4 == NULL ))
    {
        tmp_source_name_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_4 == NULL )
    {
        Py_DECREF( tmp_defaults_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 646 ], 25, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 381;
        goto frame_exception_exit_1;
    }

    tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_stdout );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_defaults_1 );

        frame_module->f_lineno = 381;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_defaults_1, 1, tmp_tuple_element_1 );
    tmp_tuple_element_1 = const_int_0;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_defaults_1, 2, tmp_tuple_element_1 );
    tmp_tuple_element_1 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_USE_MMAP );

    if (unlikely( tmp_tuple_element_1 == NULL ))
    {
        tmp_tuple_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_USE_MMAP );
    }

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_defaults_1 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9438 ], 30, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 383;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_defaults_1, 3, tmp_tuple_element_1 );
    tmp_tuple_element_1 = Py_None;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_defaults_1, 4, tmp_tuple_element_1 );
    tmp_tuple_element_1 = Py_None;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_defaults_1, 5, tmp_tuple_element_1 );
    tmp_tuple_element_1 = Py_False;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_defaults_1, 6, tmp_tuple_element_1 );
    tmp_tuple_element_1 = Py_False;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_defaults_1, 7, tmp_tuple_element_1 );
    tmp_tuple_element_1 = Py_False;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_defaults_1, 8, tmp_tuple_element_1 );
    tmp_assign_source_31 = MAKE_FUNCTION_function_1_open_workbook_of_xlrd( tmp_defaults_1 );
    if ( tmp_assign_source_31 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_31 );

        frame_module->f_lineno = 380;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_open_workbook, tmp_assign_source_31 );
    tmp_defaults_2 = PyTuple_New( 2 );
    tmp_source_name_5 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_5 == NULL ))
    {
        tmp_source_name_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_5 == NULL )
    {
        Py_DECREF( tmp_defaults_2 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 646 ], 25, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 451;
        goto frame_exception_exit_1;
    }

    tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_stdout );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_defaults_2 );

        frame_module->f_lineno = 451;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_defaults_2, 0, tmp_tuple_element_2 );
    tmp_tuple_element_2 = Py_False;
    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_defaults_2, 1, tmp_tuple_element_2 );
    tmp_assign_source_32 = MAKE_FUNCTION_function_2_dump_of_xlrd( tmp_defaults_2 );
    if ( tmp_assign_source_32 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_32 );

        frame_module->f_lineno = 451;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_dump, tmp_assign_source_32 );
    tmp_defaults_3 = PyTuple_New( 1 );
    tmp_source_name_6 = GET_STRING_DICT_VALUE( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_6 == NULL ))
    {
        tmp_source_name_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_6 == NULL )
    {
        Py_DECREF( tmp_defaults_3 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 646 ], 25, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 463;
        goto frame_exception_exit_1;
    }

    tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_stdout );
    if ( tmp_tuple_element_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_defaults_3 );

        frame_module->f_lineno = 463;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_defaults_3, 0, tmp_tuple_element_3 );
    tmp_assign_source_33 = MAKE_FUNCTION_function_3_count_records_of_xlrd( tmp_defaults_3 );
    if ( tmp_assign_source_33 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_33 );

        frame_module->f_lineno = 463;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd, (Nuitka_StringObject *)const_str_plain_count_records, tmp_assign_source_33 );

    // Restore frame exception if necessary.
#if 1
    RESTORE_FRAME_EXCEPTION( frame_module );
#endif
    popFrameStack();

    assertFrameObject( frame_module );
    Py_DECREF( frame_module );

    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 1
    RESTORE_FRAME_EXCEPTION( frame_module );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_module );
    }
    else if ( exception_tb->tb_frame != frame_module )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_module );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }

    // Put the previous frame back on top.
    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_module->f_executing -= 1;
#endif
    Py_DECREF( frame_module );

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_xlrd );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
