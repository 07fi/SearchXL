// Generated code for Python source for module 'chardet.langcyrillicmodel'
// created by Nuitka version 0.5.13.4

// This code is in part copyright 2015 Kay Hayen.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "nuitka/prelude.hpp"

#include "__helpers.hpp"

// The _module_chardet$langcyrillicmodel is a Python object pointer of module type.

// Note: For full compatibility with CPython, every module variable access
// needs to go through it except for cases where the module cannot possibly
// have changed in the mean time.

PyObject *module_chardet$langcyrillicmodel;
PyDictObject *moduledict_chardet$langcyrillicmodel;

// The module constants used
extern PyObject *const_str_plain_Ibm855Model;
extern PyObject *const_str_plain_charsetName;
static PyObject *const_str_plain_MacCyrillic;
static PyObject *const_tuple_b24235f9a763afce8962d2938259e649_tuple;
static PyObject *const_str_plain_latin5_CharToOrderMap;
static PyObject *const_str_plain_KOI8R_CharToOrderMap;
static PyObject *const_tuple_534421f243659b6bda00351214dd02e2_tuple;
static PyObject *const_str_plain_IBM866;
static PyObject *const_str_plain_RussianLangModel;
static PyObject *const_float_0_976601;
extern PyObject *const_str_plain_Koi8rModel;
static PyObject *const_tuple_f8e41cfab8cb474168ecd9a8e1d7cea1_tuple;
extern PyObject *const_str_plain_Ibm866Model;
static PyObject *const_str_digest_2e2a3395ab9ac90674d2ab70654e58a6;
static PyObject *const_tuple_ffd1e1f94e28d6aae394e365bb952ebe_tuple;
extern PyObject *const_str_plain_MacCyrillicModel;
static PyObject *const_str_plain_win1251_CharToOrderMap;
extern PyObject *const_str_plain_Win1251CyrillicModel;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_Latin5CyrillicModel;
static PyObject *const_tuple_9222b66a29def824eb1194a0241b88ae_tuple;
static PyObject *const_str_plain_IBM866_CharToOrderMap;
static PyObject *const_str_plain_IBM855;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_digest_6a571d5337d976f42e397ac95a6f5f28;
static PyObject *const_tuple_f18bdee9536a45fe5ae287fb6355492a_tuple;
static PyObject *const_str_plain_IBM855_CharToOrderMap;
extern PyObject *const_str_plain_langcyrillicmodel;
extern PyObject *const_str_digest_ae1a25c93760c3bc3caf27b7e359fc27;
static PyObject *const_str_digest_2c073a95993100a9e4bc0694ab9dfec4;
extern PyObject *const_str_plain_keepEnglishLetter;
extern PyObject *const_str_plain_precedenceMatrix;
static PyObject *const_str_plain_macCyrillic_CharToOrderMap;
extern PyObject *const_str_plain_mTypicalPositiveRatio;
extern PyObject *const_str_plain_charToOrderMap;
static PyObject *const_tuple_666844a46456f676cd51b40483cca4f3_tuple;
extern PyObject *const_str_digest_54a2cda3786988c89a40c17bdd7636c9;
static PyObject *module_filename_obj;

static void _initModuleConstants( void )
{
    const_str_plain_MacCyrillic = UNSTREAM_STRING( &constant_bin[ 7926 ], 11, 1 );
    const_tuple_b24235f9a763afce8962d2938259e649_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 329547 ], 1285 );
    const_str_plain_latin5_CharToOrderMap = UNSTREAM_STRING( &constant_bin[ 4843 ], 21, 1 );
    const_str_plain_KOI8R_CharToOrderMap = UNSTREAM_STRING( &constant_bin[ 4719 ], 20, 1 );
    const_tuple_534421f243659b6bda00351214dd02e2_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 330832 ], 1285 );
    const_str_plain_IBM866 = UNSTREAM_STRING( &constant_bin[ 4934 ], 6, 1 );
    const_str_plain_RussianLangModel = UNSTREAM_STRING( &constant_bin[ 4761 ], 16, 1 );
    const_float_0_976601 = UNSTREAM_FLOAT( &constant_bin[ 332117 ] );
    const_tuple_f8e41cfab8cb474168ecd9a8e1d7cea1_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 332125 ], 1285 );
    const_str_digest_2e2a3395ab9ac90674d2ab70654e58a6 = UNSTREAM_STRING( &constant_bin[ 333410 ], 25, 0 );
    const_tuple_ffd1e1f94e28d6aae394e365bb952ebe_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 333435 ], 1285 );
    const_str_plain_win1251_CharToOrderMap = UNSTREAM_STRING( &constant_bin[ 4799 ], 22, 1 );
    const_tuple_9222b66a29def824eb1194a0241b88ae_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 334720 ], 20485 );
    const_str_plain_IBM866_CharToOrderMap = UNSTREAM_STRING( &constant_bin[ 4934 ], 21, 1 );
    const_str_plain_IBM855 = UNSTREAM_STRING( &constant_bin[ 4977 ], 6, 1 );
    const_str_digest_6a571d5337d976f42e397ac95a6f5f28 = UNSTREAM_STRING( &constant_bin[ 355205 ], 28, 0 );
    const_tuple_f18bdee9536a45fe5ae287fb6355492a_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 355233 ], 1285 );
    const_str_plain_IBM855_CharToOrderMap = UNSTREAM_STRING( &constant_bin[ 4977 ], 21, 1 );
    const_str_digest_2c073a95993100a9e4bc0694ab9dfec4 = UNSTREAM_STRING( &constant_bin[ 356518 ], 6, 0 );
    const_str_plain_macCyrillic_CharToOrderMap = UNSTREAM_STRING( &constant_bin[ 4886 ], 26, 1 );
    const_tuple_666844a46456f676cd51b40483cca4f3_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 356524 ], 1285 );
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_chardet$langcyrillicmodel( void )
{

}
#endif

// The module code objects.
static PyCodeObject *codeobj_7f8463d9f405e59464f4019e333f7c5e;

static void _initModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_6a571d5337d976f42e397ac95a6f5f28 );
    codeobj_7f8463d9f405e59464f4019e333f7c5e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_langcyrillicmodel, 0, const_tuple_empty, 0, CO_NOFREE );
}

// The module function declarations.


// The module function definitions.



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_chardet$langcyrillicmodel =
{
    PyModuleDef_HEAD_INIT,
    "chardet.langcyrillicmodel",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

#define _MODULE_UNFREEZER 0

#if _MODULE_UNFREEZER

#include "nuitka/unfreezing.hpp"

// Table for lookup to find "frozen" modules or DLLs, i.e. the ones included in
// or along this binary.

static struct Nuitka_MetaPathBasedLoaderEntry meta_path_loader_entries[] =
{

    { NULL, NULL, 0 }
};

#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( chardet$langcyrillicmodel )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_chardet$langcyrillicmodel );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    // Initialize the compiled types of Nuitka.
    PyType_Ready( &Nuitka_Generator_Type );
    PyType_Ready( &Nuitka_Function_Type );
    PyType_Ready( &Nuitka_Method_Type );
    PyType_Ready( &Nuitka_Frame_Type );
#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

#endif

#if _MODULE_UNFREEZER
    registerMetaPathBasedUnfreezer( meta_path_loader_entries );
#endif

    _initModuleConstants();
    _initModuleCodeObjects();

    // puts( "in initchardet$langcyrillicmodel" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_chardet$langcyrillicmodel = Py_InitModule4(
        "chardet.langcyrillicmodel",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No __doc__ is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else
    module_chardet$langcyrillicmodel = PyModule_Create( &mdef_chardet$langcyrillicmodel );
#endif

    moduledict_chardet$langcyrillicmodel = (PyDictObject *)((PyModuleObject *)module_chardet$langcyrillicmodel)->md_dict;

    CHECK_OBJECT( module_chardet$langcyrillicmodel );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_2e2a3395ab9ac90674d2ab70654e58a6, module_chardet$langcyrillicmodel );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    PyObject *module_dict = PyModule_GetDict( module_chardet$langcyrillicmodel );

    if ( PyDict_GetItem( module_dict, const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

#ifndef __NUITKA_NO_ASSERT__
        int res =
#endif
            PyDict_SetItem( module_dict, const_str_plain___builtins__, value );

        assert( res == 0 );
    }

#if PYTHON_VERSION >= 330
#if _MODULE_UNFREEZER
    PyDict_SetItem( module_dict, const_str_plain___loader__, metapath_based_loader );
#else
    PyDict_SetItem( module_dict, const_str_plain___loader__, Py_None );
#endif
#endif

    // Temp variables if any
    PyObject *exception_type, *exception_value;
    PyTracebackObject *exception_tb;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_key_3;
    PyObject *tmp_dict_key_4;
    PyObject *tmp_dict_key_5;
    PyObject *tmp_dict_key_6;
    PyObject *tmp_dict_key_7;
    PyObject *tmp_dict_key_8;
    PyObject *tmp_dict_key_9;
    PyObject *tmp_dict_key_10;
    PyObject *tmp_dict_key_11;
    PyObject *tmp_dict_key_12;
    PyObject *tmp_dict_key_13;
    PyObject *tmp_dict_key_14;
    PyObject *tmp_dict_key_15;
    PyObject *tmp_dict_key_16;
    PyObject *tmp_dict_key_17;
    PyObject *tmp_dict_key_18;
    PyObject *tmp_dict_key_19;
    PyObject *tmp_dict_key_20;
    PyObject *tmp_dict_key_21;
    PyObject *tmp_dict_key_22;
    PyObject *tmp_dict_key_23;
    PyObject *tmp_dict_key_24;
    PyObject *tmp_dict_key_25;
    PyObject *tmp_dict_key_26;
    PyObject *tmp_dict_key_27;
    PyObject *tmp_dict_key_28;
    PyObject *tmp_dict_key_29;
    PyObject *tmp_dict_key_30;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dict_value_3;
    PyObject *tmp_dict_value_4;
    PyObject *tmp_dict_value_5;
    PyObject *tmp_dict_value_6;
    PyObject *tmp_dict_value_7;
    PyObject *tmp_dict_value_8;
    PyObject *tmp_dict_value_9;
    PyObject *tmp_dict_value_10;
    PyObject *tmp_dict_value_11;
    PyObject *tmp_dict_value_12;
    PyObject *tmp_dict_value_13;
    PyObject *tmp_dict_value_14;
    PyObject *tmp_dict_value_15;
    PyObject *tmp_dict_value_16;
    PyObject *tmp_dict_value_17;
    PyObject *tmp_dict_value_18;
    PyObject *tmp_dict_value_19;
    PyObject *tmp_dict_value_20;
    PyObject *tmp_dict_value_21;
    PyObject *tmp_dict_value_22;
    PyObject *tmp_dict_value_23;
    PyObject *tmp_dict_value_24;
    PyObject *tmp_dict_value_25;
    PyObject *tmp_dict_value_26;
    PyObject *tmp_dict_value_27;
    PyObject *tmp_dict_value_28;
    PyObject *tmp_dict_value_29;
    PyObject *tmp_dict_value_30;
    PyFrameObject *frame_module;


    // Module code.
    tmp_assign_source_1 = Py_None;
    UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = module_filename_obj;
    UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = const_tuple_f18bdee9536a45fe5ae287fb6355492a_tuple;
    UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_KOI8R_CharToOrderMap, tmp_assign_source_3 );
    tmp_assign_source_4 = const_tuple_b24235f9a763afce8962d2938259e649_tuple;
    UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_win1251_CharToOrderMap, tmp_assign_source_4 );
    tmp_assign_source_5 = const_tuple_ffd1e1f94e28d6aae394e365bb952ebe_tuple;
    UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_latin5_CharToOrderMap, tmp_assign_source_5 );
    tmp_assign_source_6 = const_tuple_f8e41cfab8cb474168ecd9a8e1d7cea1_tuple;
    UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_macCyrillic_CharToOrderMap, tmp_assign_source_6 );
    tmp_assign_source_7 = const_tuple_666844a46456f676cd51b40483cca4f3_tuple;
    UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_IBM855_CharToOrderMap, tmp_assign_source_7 );
    tmp_assign_source_8 = const_tuple_534421f243659b6bda00351214dd02e2_tuple;
    UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_IBM866_CharToOrderMap, tmp_assign_source_8 );
    tmp_assign_source_9 = const_tuple_9222b66a29def824eb1194a0241b88ae_tuple;
    UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_RussianLangModel, tmp_assign_source_9 );
    // Frame without reuse.
    frame_module = MAKE_FRAME( codeobj_7f8463d9f405e59464f4019e333f7c5e, module_chardet$langcyrillicmodel );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_module );
    assert( Py_REFCNT( frame_module ) == 1 );

#if PYTHON_VERSION >= 340
    frame_module->f_executing += 1;
#endif

    // Framed code:
    tmp_assign_source_10 = _PyDict_NewPresized( 5 );
    tmp_dict_value_1 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_KOI8R_CharToOrderMap );

    if (unlikely( tmp_dict_value_1 == NULL ))
    {
        tmp_dict_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_KOI8R_CharToOrderMap );
    }

    if ( tmp_dict_value_1 == NULL )
    {
        Py_DECREF( tmp_assign_source_10 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 4713 ], 42, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 282;
        goto frame_exception_exit_1;
    }

    tmp_dict_key_1 = const_str_plain_charToOrderMap;
    PyDict_SetItem( tmp_assign_source_10, tmp_dict_key_1, tmp_dict_value_1 );
    tmp_dict_value_2 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_RussianLangModel );

    if (unlikely( tmp_dict_value_2 == NULL ))
    {
        tmp_dict_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RussianLangModel );
    }

    if ( tmp_dict_value_2 == NULL )
    {
        Py_DECREF( tmp_assign_source_10 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 4755 ], 38, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 283;
        goto frame_exception_exit_1;
    }

    tmp_dict_key_2 = const_str_plain_precedenceMatrix;
    PyDict_SetItem( tmp_assign_source_10, tmp_dict_key_2, tmp_dict_value_2 );
    tmp_dict_value_3 = const_float_0_976601;
    tmp_dict_key_3 = const_str_plain_mTypicalPositiveRatio;
    PyDict_SetItem( tmp_assign_source_10, tmp_dict_key_3, tmp_dict_value_3 );
    tmp_dict_value_4 = Py_False;
    tmp_dict_key_4 = const_str_plain_keepEnglishLetter;
    PyDict_SetItem( tmp_assign_source_10, tmp_dict_key_4, tmp_dict_value_4 );
    tmp_dict_value_5 = const_str_digest_2c073a95993100a9e4bc0694ab9dfec4;
    tmp_dict_key_5 = const_str_plain_charsetName;
    PyDict_SetItem( tmp_assign_source_10, tmp_dict_key_5, tmp_dict_value_5 );
    UPDATE_STRING_DICT1( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_Koi8rModel, tmp_assign_source_10 );
    tmp_assign_source_11 = _PyDict_NewPresized( 5 );
    tmp_dict_value_6 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_win1251_CharToOrderMap );

    if (unlikely( tmp_dict_value_6 == NULL ))
    {
        tmp_dict_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_win1251_CharToOrderMap );
    }

    if ( tmp_dict_value_6 == NULL )
    {
        Py_DECREF( tmp_assign_source_11 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 4793 ], 44, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 290;
        goto frame_exception_exit_1;
    }

    tmp_dict_key_6 = const_str_plain_charToOrderMap;
    PyDict_SetItem( tmp_assign_source_11, tmp_dict_key_6, tmp_dict_value_6 );
    tmp_dict_value_7 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_RussianLangModel );

    if (unlikely( tmp_dict_value_7 == NULL ))
    {
        tmp_dict_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RussianLangModel );
    }

    if ( tmp_dict_value_7 == NULL )
    {
        Py_DECREF( tmp_assign_source_11 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 4755 ], 38, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 291;
        goto frame_exception_exit_1;
    }

    tmp_dict_key_7 = const_str_plain_precedenceMatrix;
    PyDict_SetItem( tmp_assign_source_11, tmp_dict_key_7, tmp_dict_value_7 );
    tmp_dict_value_8 = const_float_0_976601;
    tmp_dict_key_8 = const_str_plain_mTypicalPositiveRatio;
    PyDict_SetItem( tmp_assign_source_11, tmp_dict_key_8, tmp_dict_value_8 );
    tmp_dict_value_9 = Py_False;
    tmp_dict_key_9 = const_str_plain_keepEnglishLetter;
    PyDict_SetItem( tmp_assign_source_11, tmp_dict_key_9, tmp_dict_value_9 );
    tmp_dict_value_10 = const_str_digest_54a2cda3786988c89a40c17bdd7636c9;
    tmp_dict_key_10 = const_str_plain_charsetName;
    PyDict_SetItem( tmp_assign_source_11, tmp_dict_key_10, tmp_dict_value_10 );
    UPDATE_STRING_DICT1( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_Win1251CyrillicModel, tmp_assign_source_11 );
    tmp_assign_source_12 = _PyDict_NewPresized( 5 );
    tmp_dict_value_11 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_latin5_CharToOrderMap );

    if (unlikely( tmp_dict_value_11 == NULL ))
    {
        tmp_dict_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_latin5_CharToOrderMap );
    }

    if ( tmp_dict_value_11 == NULL )
    {
        Py_DECREF( tmp_assign_source_12 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 4837 ], 43, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 298;
        goto frame_exception_exit_1;
    }

    tmp_dict_key_11 = const_str_plain_charToOrderMap;
    PyDict_SetItem( tmp_assign_source_12, tmp_dict_key_11, tmp_dict_value_11 );
    tmp_dict_value_12 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_RussianLangModel );

    if (unlikely( tmp_dict_value_12 == NULL ))
    {
        tmp_dict_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RussianLangModel );
    }

    if ( tmp_dict_value_12 == NULL )
    {
        Py_DECREF( tmp_assign_source_12 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 4755 ], 38, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 299;
        goto frame_exception_exit_1;
    }

    tmp_dict_key_12 = const_str_plain_precedenceMatrix;
    PyDict_SetItem( tmp_assign_source_12, tmp_dict_key_12, tmp_dict_value_12 );
    tmp_dict_value_13 = const_float_0_976601;
    tmp_dict_key_13 = const_str_plain_mTypicalPositiveRatio;
    PyDict_SetItem( tmp_assign_source_12, tmp_dict_key_13, tmp_dict_value_13 );
    tmp_dict_value_14 = Py_False;
    tmp_dict_key_14 = const_str_plain_keepEnglishLetter;
    PyDict_SetItem( tmp_assign_source_12, tmp_dict_key_14, tmp_dict_value_14 );
    tmp_dict_value_15 = const_str_digest_ae1a25c93760c3bc3caf27b7e359fc27;
    tmp_dict_key_15 = const_str_plain_charsetName;
    PyDict_SetItem( tmp_assign_source_12, tmp_dict_key_15, tmp_dict_value_15 );
    UPDATE_STRING_DICT1( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_Latin5CyrillicModel, tmp_assign_source_12 );
    tmp_assign_source_13 = _PyDict_NewPresized( 5 );
    tmp_dict_value_16 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_macCyrillic_CharToOrderMap );

    if (unlikely( tmp_dict_value_16 == NULL ))
    {
        tmp_dict_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_macCyrillic_CharToOrderMap );
    }

    if ( tmp_dict_value_16 == NULL )
    {
        Py_DECREF( tmp_assign_source_13 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 4880 ], 48, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 306;
        goto frame_exception_exit_1;
    }

    tmp_dict_key_16 = const_str_plain_charToOrderMap;
    PyDict_SetItem( tmp_assign_source_13, tmp_dict_key_16, tmp_dict_value_16 );
    tmp_dict_value_17 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_RussianLangModel );

    if (unlikely( tmp_dict_value_17 == NULL ))
    {
        tmp_dict_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RussianLangModel );
    }

    if ( tmp_dict_value_17 == NULL )
    {
        Py_DECREF( tmp_assign_source_13 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 4755 ], 38, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 307;
        goto frame_exception_exit_1;
    }

    tmp_dict_key_17 = const_str_plain_precedenceMatrix;
    PyDict_SetItem( tmp_assign_source_13, tmp_dict_key_17, tmp_dict_value_17 );
    tmp_dict_value_18 = const_float_0_976601;
    tmp_dict_key_18 = const_str_plain_mTypicalPositiveRatio;
    PyDict_SetItem( tmp_assign_source_13, tmp_dict_key_18, tmp_dict_value_18 );
    tmp_dict_value_19 = Py_False;
    tmp_dict_key_19 = const_str_plain_keepEnglishLetter;
    PyDict_SetItem( tmp_assign_source_13, tmp_dict_key_19, tmp_dict_value_19 );
    tmp_dict_value_20 = const_str_plain_MacCyrillic;
    tmp_dict_key_20 = const_str_plain_charsetName;
    PyDict_SetItem( tmp_assign_source_13, tmp_dict_key_20, tmp_dict_value_20 );
    UPDATE_STRING_DICT1( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_MacCyrillicModel, tmp_assign_source_13 );
    tmp_assign_source_14 = _PyDict_NewPresized( 5 );
    tmp_dict_value_21 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_IBM866_CharToOrderMap );

    if (unlikely( tmp_dict_value_21 == NULL ))
    {
        tmp_dict_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IBM866_CharToOrderMap );
    }

    if ( tmp_dict_value_21 == NULL )
    {
        Py_DECREF( tmp_assign_source_14 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 4928 ], 43, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 314;
        goto frame_exception_exit_1;
    }

    tmp_dict_key_21 = const_str_plain_charToOrderMap;
    PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_21, tmp_dict_value_21 );
    tmp_dict_value_22 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_RussianLangModel );

    if (unlikely( tmp_dict_value_22 == NULL ))
    {
        tmp_dict_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RussianLangModel );
    }

    if ( tmp_dict_value_22 == NULL )
    {
        Py_DECREF( tmp_assign_source_14 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 4755 ], 38, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 315;
        goto frame_exception_exit_1;
    }

    tmp_dict_key_22 = const_str_plain_precedenceMatrix;
    PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_22, tmp_dict_value_22 );
    tmp_dict_value_23 = const_float_0_976601;
    tmp_dict_key_23 = const_str_plain_mTypicalPositiveRatio;
    PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_23, tmp_dict_value_23 );
    tmp_dict_value_24 = Py_False;
    tmp_dict_key_24 = const_str_plain_keepEnglishLetter;
    PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_24, tmp_dict_value_24 );
    tmp_dict_value_25 = const_str_plain_IBM866;
    tmp_dict_key_25 = const_str_plain_charsetName;
    PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_25, tmp_dict_value_25 );
    UPDATE_STRING_DICT1( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_Ibm866Model, tmp_assign_source_14 );
    tmp_assign_source_15 = _PyDict_NewPresized( 5 );
    tmp_dict_value_26 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_IBM855_CharToOrderMap );

    if (unlikely( tmp_dict_value_26 == NULL ))
    {
        tmp_dict_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IBM855_CharToOrderMap );
    }

    if ( tmp_dict_value_26 == NULL )
    {
        Py_DECREF( tmp_assign_source_15 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 4971 ], 43, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 322;
        goto frame_exception_exit_1;
    }

    tmp_dict_key_26 = const_str_plain_charToOrderMap;
    PyDict_SetItem( tmp_assign_source_15, tmp_dict_key_26, tmp_dict_value_26 );
    tmp_dict_value_27 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_RussianLangModel );

    if (unlikely( tmp_dict_value_27 == NULL ))
    {
        tmp_dict_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RussianLangModel );
    }

    if ( tmp_dict_value_27 == NULL )
    {
        Py_DECREF( tmp_assign_source_15 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 4755 ], 38, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 323;
        goto frame_exception_exit_1;
    }

    tmp_dict_key_27 = const_str_plain_precedenceMatrix;
    PyDict_SetItem( tmp_assign_source_15, tmp_dict_key_27, tmp_dict_value_27 );
    tmp_dict_value_28 = const_float_0_976601;
    tmp_dict_key_28 = const_str_plain_mTypicalPositiveRatio;
    PyDict_SetItem( tmp_assign_source_15, tmp_dict_key_28, tmp_dict_value_28 );
    tmp_dict_value_29 = Py_False;
    tmp_dict_key_29 = const_str_plain_keepEnglishLetter;
    PyDict_SetItem( tmp_assign_source_15, tmp_dict_key_29, tmp_dict_value_29 );
    tmp_dict_value_30 = const_str_plain_IBM855;
    tmp_dict_key_30 = const_str_plain_charsetName;
    PyDict_SetItem( tmp_assign_source_15, tmp_dict_key_30, tmp_dict_value_30 );
    UPDATE_STRING_DICT1( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_Ibm855Model, tmp_assign_source_15 );

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_module );
#endif
    popFrameStack();

    assertFrameObject( frame_module );
    Py_DECREF( frame_module );

    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_module );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_module );
    }
    else if ( exception_tb->tb_frame != frame_module )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_module );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }

    // Put the previous frame back on top.
    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_module->f_executing -= 1;
#endif
    Py_DECREF( frame_module );

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_chardet$langcyrillicmodel );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
