// Generated code for Python source for module 'xlrd.biffh'
// created by Nuitka version 0.5.13.4

// This code is in part copyright 2015 Kay Hayen.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "nuitka/prelude.hpp"

#include "__helpers.hpp"

// The _module_xlrd$biffh is a Python object pointer of module type.

// Note: For full compatibility with CPython, every module variable access
// needs to go through it except for cases where the module cannot possibly
// have changed in the mean time.

PyObject *module_xlrd$biffh;
PyDictObject *moduledict_xlrd$biffh;

// The module constants used
static PyObject *const_str_plain_tgt_obj;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_data;
extern PyObject *const_tuple_str_chr_42_tuple;
extern PyObject *const_str_plain_unpack_string;
extern PyObject *const_str_plain_XL_FORMAT2;
static PyObject *const_tuple_9fd131eedafe66de2cf3bd725fc3092c_tuple;
extern PyObject *const_str_plain_upkbits;
static PyObject *const_int_pos_351;
static PyObject *const_str_digest_73bc75123e178cc9716a872958726225;
extern PyObject *const_str_plain_latin_1;
extern PyObject *const_str_plain_XL_RSTRING;
extern PyObject *const_str_plain_int;
extern PyObject *const_str_plain_min;
extern PyObject *const_str_plain_XL_WORKSHEET;
extern PyObject *const_str_plain_FTX;
static PyObject *const_int_pos_438;
extern PyObject *const_int_pos_129;
static PyObject *const_int_pos_125;
static PyObject *const_int_pos_430;
static PyObject *const_int_pos_433;
static PyObject *const_int_pos_432;
extern PyObject *const_int_neg_1;
static PyObject *const_dict_3ea0089e9e3dc196147eafc31e4c1ee9;
static PyObject *const_str_plain_numbered;
extern PyObject *const_str_plain_XL_VERTICALPAGEBREAKS;
static PyObject *const_str_plain_manifest;
extern PyObject *const_str_plain_XL_RK;
extern PyObject *const_str_plain_XL_SHEETSOFFSET;
static PyObject *const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple;
extern PyObject *const_str_plain_XL_EXTERNNAME;
extern PyObject *const_str_plain_XL_LABELSST;
extern PyObject *const_str_plain_unpack_unicode;
static PyObject *const_str_digest_c8684a7fbdb42f7b66b506461dffd581;
extern PyObject *const_str_plain_upkbitsL;
static PyObject *const_int_pos_545;
static PyObject *const_str_plain_slist;
static PyObject *const_int_pos_549;
extern PyObject *const_int_pos_224;
extern PyObject *const_str_plain_encoding;
extern PyObject *const_str_digest_b8d2060bb898fb3742d05b0e0db99007;
extern PyObject *const_str_plain___slots__;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain_BIFF_FIRST_UNICODE;
static PyObject *const_str_plain_endsub;
static PyObject *const_str_plain_XL_FORMULA4;
static PyObject *const_str_plain_XL_FORMULA3;
extern PyObject *const_int_pos_2048;
static PyObject *const_tuple_c5a46ceff3868bca5656e2faaa793860_tuple;
static PyObject *const_tuple_int_pos_2_none_tuple;
static PyObject *const_tuple_int_pos_2057_int_pos_1033_int_pos_521_int_pos_9_tuple;
extern PyObject *const_str_plain_XL_WINDOW2;
extern PyObject *const_str_plain_items;
static PyObject *const_str_digest_4d4f1eb43dba0133c343318478063cbc;
extern PyObject *const_str_plain_XL_TABLEOP;
extern PyObject *const_int_pos_32768;
static PyObject *const_int_pos_32769;
static PyObject *const_str_plain_recname;
extern PyObject *const_str_plain_timemachine;
static PyObject *const_int_pos_38;
static PyObject *const_int_pos_39;
extern PyObject *const_int_pos_36;
extern PyObject *const_str_plain___name__;
extern PyObject *const_int_pos_35;
extern PyObject *const_int_pos_32;
static PyObject *const_int_pos_33;
extern PyObject *const_int_pos_30;
static PyObject *const_int_pos_1033;
static PyObject *const_str_digest_8a11c0c083f755e41606cc49dac48c2e;
static PyObject *const_int_pos_1030;
static PyObject *const_str_plain_dummies;
static PyObject *const_str_plain_adj;
extern PyObject *const_str_plain_XL_EOF;
extern PyObject *const_str_plain_XL_BOOLERR;
extern PyObject *const_str_plain_nchars;
extern PyObject *const_str_plain_XL_XF4;
extern PyObject *const_str_plain_XL_XF2;
extern PyObject *const_str_plain_XL_XF3;
static PyObject *const_str_digest_035ddedf6744ab690912d979cd8804ae;
static PyObject *const_dict_6ca13ccd13dd5d5b66b3d9a86dbc0eb0;
extern PyObject *const_str_plain_XL_COUNTRY;
extern PyObject *const_int_pos_161;
extern PyObject *const_int_pos_160;
static PyObject *const_str_digest_d30f5965de4596eef50172dfa5661286;
static PyObject *const_str_plain_substrg;
extern PyObject *const_str_plain__unused;
extern PyObject *const_str_plain_sys;
static PyObject *const_str_plain_stream_end;
static PyObject *const_str_digest_b6139e4269dc05287b03c22a344c5df7;
static PyObject *const_str_digest_fafacfd47cf13238a8c1faf6984b28af;
static PyObject *const_int_pos_10029;
extern PyObject *const_str_plain_XL_PALETTE;
extern PyObject *const_str_plain_XL_BLANK_B2;
static PyObject *const_tuple_str_plain___module___str_plain__repr_these_str_plain_dump_tuple;
static PyObject *const_str_plain_mac_greek;
static PyObject *const_str_plain__cell_opcode_dict;
extern PyObject *const_str_plain_biff_count_records;
extern PyObject *const_str_plain_book;
static PyObject *const_str_plain_XL_FORMULA;
static PyObject *const_tuple_0f3a1163ae378483907f1114e471f41f_tuple;
static PyObject *const_str_digest_279b5d5b7f24193f9814ec32a3d4e5c8;
static PyObject *const_int_pos_214;
extern PyObject *const_str_plain_XL_CODEPAGE;
static PyObject *const_str_digest_f794daa1e63029828ec58248549815d2;
static PyObject *const_tuple_11afd00d8994dbdff702e5548795eb51_tuple;
extern PyObject *const_str_plain_XL_PANE;
static PyObject *const_str_plain_DATEFORMAT;
extern PyObject *const_str_plain_XL_SUPBOOK;
static PyObject *const_int_pos_1054;
extern PyObject *const_str_plain_strg;
extern PyObject *const_str_plain_fprintf;
extern PyObject *const_str_plain_bofcodes;
static PyObject *const_str_plain_local_setattr;
static PyObject *const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple;
extern PyObject *const_str_plain_XL_OBJ;
extern PyObject *const_str_plain_XL_MULBLANK;
static PyObject *const_int_pos_189;
extern PyObject *const_str_digest_525193cd98a1d97690ef9daa0960a42a;
extern PyObject *const_str_plain_XL_MERGEDCELLS;
static PyObject *const_int_pos_1091;
static PyObject *const_int_pos_92;
static PyObject *const_int_pos_93;
extern PyObject *const_int_pos_94;
extern PyObject *const_str_chr_42;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_XL_SST;
extern PyObject *const_str_plain_UNICODE_LITERAL;
extern PyObject *const_str_plain_rt;
extern PyObject *const_str_plain_XL_FORMAT;
static PyObject *const_str_digest_14b1d9542392632c1dd58ad837d7d5e0;
extern PyObject *const_str_plain_newpos;
extern PyObject *const_str_plain_FGE;
extern PyObject *const_str_plain_rc;
extern PyObject *const_str_plain_rb;
static PyObject *const_str_plain_ra;
extern PyObject *const_str_plain_XL_STYLE;
extern PyObject *const_str_plain_XL_WINDOW2_B2;
static PyObject *const_str_plain_indent;
static PyObject *const_str_plain_is_cell_opcode;
extern PyObject *const_str_plain_XL_INTEGER;
static PyObject *const_str_plain_mac_iceland;
static PyObject *const_str_digest_2dd278a1d486e1dec430092103f8a661;
static PyObject *const_str_plain__brecstrg;
extern PyObject *const_str_plain_XL_DIMENSION2;
static PyObject *const_str_plain_XL_PAGESETUP;
static PyObject *const_str_digest_d1092b79338523976efa4188834ad524;
extern PyObject *const_str_plain_XLRDError;
extern PyObject *const_str_digest_ce040a5a2e567cbd88761767ab4cb887;
extern PyObject *const_str_plain_XL_TABLEOP_B2;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_plain_num_prefix;
static PyObject *const_int_pos_131;
static PyObject *const_int_pos_132;
static PyObject *const_int_pos_133;
static PyObject *const_str_plain_mac_cyrillic;
extern PyObject *const_str_plain__repr_these;
static PyObject *const_int_pos_66;
extern PyObject *const_str_plain___metaclass__;
extern PyObject *const_str_plain_xrange;
static PyObject *const_str_plain__cell_opcode_list;
extern PyObject *const_int_pos_255;
extern PyObject *const_int_pos_256;
static PyObject *const_int_pos_253;
extern PyObject *const_int_pos_252;
static PyObject *const_str_chr_126;
extern PyObject *const_str_digest_ca7becc9d49589e000ad29ce1ad566e8;
static PyObject *const_str_digest_9d755e04da81084ffe307f7654bf55a3;
extern PyObject *const_str_plain_XL_BOUNDSHEET;
static PyObject *const_str_plain_dlen;
extern PyObject *const_tuple_int_0_int_pos_1_int_pos_2_int_pos_3_int_pos_4_tuple;
static PyObject *const_str_plain_XL_PRINTGRIDLINES;
static PyObject *const_str_plain_mac_roman;
extern PyObject *const_str_plain_footer;
static PyObject *const_str_digest_6d43b3c93240dbf6ee5b33f97f4493f7;
extern PyObject *const_str_plain_DEBUG;
static PyObject *const_str_plain_XL_INDEX;
extern PyObject *const_str_plain_XL_CELL_EMPTY;
extern PyObject *const_str_chr_0;
extern PyObject *const_int_pos_21;
extern PyObject *const_int_pos_20;
extern PyObject *const_int_pos_23;
extern PyObject *const_int_pos_24;
extern PyObject *const_int_pos_27;
extern PyObject *const_int_pos_26;
extern PyObject *const_int_pos_29;
extern PyObject *const_int_pos_28;
extern PyObject *const_str_plain_XL_DEFAULTROWHEIGHT;
extern PyObject *const_str_plain_sorted;
static PyObject *const_str_plain_ca;
extern PyObject *const_str_plain_cb;
extern PyObject *const_str_plain_struct;
extern PyObject *const_str_plain_mem;
extern PyObject *const_str_plain_XL_GCW;
extern PyObject *const_str_plain_XL_ARRAY2;
extern PyObject *const_str_plain_endpos;
static PyObject *const_str_digest_151a9c921b189f1612ad426f27c62dd9;
extern PyObject *const_str_plain_XL_DIMENSION;
extern PyObject *const_tuple_int_pos_1_tuple;
extern PyObject *const_str_plain_print;
extern PyObject *const_str_plain_utf_16_le;
extern PyObject *const_str_plain_XL_TABLEOP2;
extern PyObject *const_str_plain_stderr;
static PyObject *const_str_plain_XL_BOTTOMMARGIN;
extern PyObject *const_str_plain_XL_FORMULA_OPCODES;
extern PyObject *const_str_plain_pos;
extern PyObject *const_int_pos_65535;
extern PyObject *const_str_plain_FNU;
static PyObject *const_int_pos_171;
extern PyObject *const_str_plain_header;
extern PyObject *const_str_plain_XL_COLUMNDEFAULT;
extern PyObject *const_str_plain_XL_CF;
extern PyObject *const_str_plain_XL_CONTINUE;
static PyObject *const_str_plain__numh;
static PyObject *const_str_plain_NUMBERFORMAT;
static PyObject *const_str_digest_4468a499e275b96988b2c7c7fbfc545b;
static PyObject *const_tuple_int_pos_2_tuple;
extern PyObject *const_str_plain_XL_CELL_DATE;
extern PyObject *const_str_plain_XL_BUILTINFMTCOUNT;
extern PyObject *const_str_plain_XL_STRING;
extern PyObject *const_str_plain_XL_DEFCOLWIDTH;
extern PyObject *const_str_plain_XL_NAME;
static PyObject *const_str_plain_XL_HEADER;
extern PyObject *const_str_plain_known_len;
static PyObject *const_int_pos_518;
static PyObject *const_int_pos_519;
static PyObject *const_int_pos_516;
static PyObject *const_int_pos_517;
static PyObject *const_int_pos_515;
extern PyObject *const_int_pos_512;
static PyObject *const_int_pos_513;
extern PyObject *const_str_plain_biff_version;
extern PyObject *const_str_plain_biff_dump;
static PyObject *const_str_plain_stream_offset;
static PyObject *const_tuple_52142698930a28821d51b9a6e51acf25_tuple;
static PyObject *const_str_digest_8989e5819a70580cc3034b2f9b4fbe1b;
extern PyObject *const_str_plain_unpack_unicode_update_pos;
static PyObject *const_str_plain_cp1252;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain_XL_EFONT;
static PyObject *const_str_plain_XL_UNKNOWN;
static PyObject *const_str_digest_81a9a087be28ba712bbfe67d1d7a8bf1;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_file;
static PyObject *const_int_pos_190;
static PyObject *const_int_pos_69;
static PyObject *const_int_pos_68;
extern PyObject *const_int_pos_65;
extern PyObject *const_int_pos_67;
static PyObject *const_str_plain_local_int;
extern PyObject *const_int_pos_60;
extern PyObject *const_int_pos_62;
extern PyObject *const_str_plain_value;
static PyObject *const_dict_a3a2e5d3cfb5ecd8166a51f69633413e;
static PyObject *const_int_pos_229;
extern PyObject *const_str_plain_addr_size;
extern PyObject *const_int_pos_80;
static PyObject *const_str_plain_chard;
static PyObject *const_int_pos_86;
static PyObject *const_int_pos_85;
static PyObject *const_str_plain_WRKSHEET;
static PyObject *const_str_plain_phonetic;
extern PyObject *const_str_plain_XL_BLANK;
extern PyObject *const_int_pos_10;
static PyObject *const_str_plain_XL_EXTSST;
extern PyObject *const_str_plain_XL_WORKBOOK_GLOBALS;
extern PyObject *const_int_pos_15;
extern PyObject *const_int_pos_16;
extern PyObject *const_str_plain_XL_BOOLERR_B2;
extern PyObject *const_str_plain_FDT;
static PyObject *const_str_digest_aecd01c0995302cd41850de69810d8f6;
static PyObject *const_tuple_str_plain_c_tuple;
static PyObject *const_str_plain_XL_FOOTER;
extern PyObject *const_int_pos_6;
extern PyObject *const_int_pos_7;
extern PyObject *const_int_pos_4;
extern PyObject *const_int_pos_5;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_XL_IXFE;
extern PyObject *const_int_pos_1;
extern PyObject *const_int_pos_8;
extern PyObject *const_int_pos_9;
static PyObject *const_int_pos_638;
extern PyObject *const_str_plain_XL_COLINFO;
extern PyObject *const_str_plain_XL_CONDFMT;
extern PyObject *const_str_plain_boflen;
extern PyObject *const_str_plain_unpack;
static PyObject *const_str_plain_dict_type;
static PyObject *const_tuple_int_pos_1_none_tuple;
static PyObject *const_str_plain_list_type;
extern PyObject *const_str_plain_XL_MSO_DRAWING;
extern PyObject *const_str_plain_biffh;
static PyObject *const_int_pos_579;
extern PyObject *const_str_plain_XL_LABELRANGES;
static PyObject *const_int_pos_146;
static PyObject *const_tuple_int_pos_6_tuple;
static PyObject *const_int_pos_140;
extern PyObject *const_int_pos_143;
extern PyObject *const_int_pos_142;
extern PyObject *const_tuple_str_plain_unpack_tuple;
static PyObject *const_str_plain_XL_MSO_DRAWING_GROUP;
static PyObject *const_str_plain_XL_BOUNDSHEET_CHART;
extern PyObject *const_str_plain_join;
static PyObject *const_str_plain_mac_latin2;
extern PyObject *const_str_plain_rawstrg;
static PyObject *const_str_angle_UNKNOWN;
extern PyObject *const_str_plain_XL_SHEETHDR;
extern PyObject *const_str_plain_attr;
static PyObject *const_str_digest_9756e42be4b940a8f413b6a411e53934;
extern PyObject *const_str_plain_XL_CELL_NUMBER;
static PyObject *const_int_pos_566;
static PyObject *const_int_pos_561;
static PyObject *const_str_plain_output_list;
static PyObject *const_int_pos_10000;
static PyObject *const_int_pos_10007;
static PyObject *const_int_pos_10006;
static PyObject *const_tuple_int_pos_6_int_pos_1030_int_pos_518_tuple;
static PyObject *const_str_plain_src;
extern PyObject *const_str_plain_stdout;
static PyObject *const_str_plain_XL_LEFTMARGIN;
static PyObject *const_str_plain_n;
extern PyObject *const_str_plain_XL_NUMBER_B2;
extern PyObject *const_str_plain_f;
extern PyObject *const_str_plain_XL_STANDARDWIDTH;
static PyObject *const_str_plain_XL_VCENTER;
extern PyObject *const_str_plain_c;
static PyObject *const_str_plain__buff;
extern PyObject *const_str_plain_XL_COLWIDTH;
extern PyObject *const_int_pos_1200;
extern PyObject *const_str_plain_XL_CELL_TEXT;
extern PyObject *const_str_plain_XL_CELL_BLANK;
extern PyObject *const_str_plain_BH;
extern PyObject *const_str_plain_unnumbered;
extern PyObject *const_str_plain_unpack_string_update_pos;
static PyObject *const_str_digest_c8a0af0c0a1527d7b677e5153ac3cf44;
static PyObject *const_str_plain_XL_WSBOOL;
extern PyObject *const_str_digest_c789ca952478ee28f98e94d83dd276b2;
extern PyObject *const_str_plain_BaseObject;
extern PyObject *const_str_plain_XL_NUMBER;
extern PyObject *const_str_plain_XL_NOTE;
static PyObject *const_int_pos_54;
static PyObject *const_int_pos_55;
extern PyObject *const_str_digest_4d1b2450b7a7643f798830120130cc35;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_sz;
static PyObject *const_tuple_908ca83fea928ec6d97ff1eeddbf3947_tuple;
extern PyObject *const_str_plain_XL_ARRAY;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_XL_MULRK;
static PyObject *const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple;
extern PyObject *const_str_plain_fout;
static PyObject *const_str_plain_biff_rec_name_dict;
extern PyObject *const_str_plain_print_function;
static PyObject *const_str_plain_WBKBLOBAL;
extern PyObject *const_str_plain_XL_FILEPASS;
extern PyObject *const_str_plain_XL_SCL;
extern PyObject *const_str_plain_lenlen;
static PyObject *const_dict_ff5080d8cdf9d4b5eabcea109a63e8c9;
static PyObject *const_tuple_c1402daa720d85b29c196951ec0f4aca_tuple;
extern PyObject *const_str_plain_BYTES_ORD;
extern PyObject *const_dict_empty;
static PyObject *const_tuple_2d7a42b9ddf265e370cf80ec4093139a_tuple;
static PyObject *const_str_plain_XL_BOUNDSHEET_VB_MODULE;
static PyObject *const_str_plain_XL_SHEETPR;
static PyObject *const_str_plain_count;
static PyObject *const_str_plain_setattr;
static PyObject *const_str_plain__name;
static PyObject *const_int_pos_440;
extern PyObject *const_str_plain_length;
static PyObject *const_int_pos_523;
static PyObject *const_int_pos_521;
static PyObject *const_int_pos_520;
static PyObject *const_str_plain_XL_PRINTHEADERS;
static PyObject *const_str_plain_richtext;
extern PyObject *const_str_plain_ofs;
static PyObject *const_str_digest_0245eef4613ec410273d22c8f101bc78;
extern PyObject *const_int_pos_237;
static PyObject *const_int_pos_236;
extern PyObject *const_int_pos_235;
extern PyObject *const_str_plain_base;
static PyObject *const_int_pos_10079;
extern PyObject *const_str_plain_XL_EXTERNSHEET;
extern PyObject *const_str_plain_XL_FEAT11;
extern PyObject *const_str_plain_XL_WORKBOOK_GLOBALS_4W;
static PyObject *const_str_plain_XL_HCENTER;
static PyObject *const_str_plain_mac_turkish;
extern PyObject *const_str_plain_XL_STRING_B2;
static PyObject *const_int_pos_2057;
extern PyObject *const_str_plain_split;
extern PyObject *const_str_plain_XL_BOUNDSHEET_WORKSHEET;
extern PyObject *const_str_plain_encoding_from_codepage;
static PyObject *const_str_plain_XL_TOPMARGIN;
extern PyObject *const_str_plain_XL_HLINK;
extern PyObject *const_str_plain_FUN;
extern PyObject *const_str_plain___module__;
static PyObject *const_str_plain_splitlines;
static PyObject *const_tuple_int_pos_6_int_pos_8_tuple;
static PyObject *const_str_plain_tally;
static PyObject *const_str_digest_96756a074b4136fc9b772237c96d84d0;
static PyObject *const_str_plain_list;
extern PyObject *const_int_pos_34;
extern PyObject *const_str_plain_error_text_from_code;
static PyObject *const_str_digest_f3465426630a3bcfed3eab224df00939;
extern PyObject *const_str_plain_hex_char_dump;
static PyObject *const_int_pos_659;
extern PyObject *const_str_plain_alist;
static PyObject *const_str_plain_XL_RIGHTMARGIN;
static PyObject *const_int_pos_10081;
extern PyObject *const_str_plain_XL_QUICKTIP;
static PyObject *const_str_plain_XL_BOF;
extern PyObject *const_str_plain_XL_SHRFMLA;
extern PyObject *const_str_plain_dump;
static PyObject *const_str_plain_pad;
extern PyObject *const_str_plain_XL_LABEL_B2;
extern PyObject *const_str_plain_dict;
extern PyObject *const_str_plain_XL_CELL_ERROR;
extern PyObject *const_str_plain_options;
static PyObject *const_int_pos_153;
extern PyObject *const_str_plain_XL_TXO;
static PyObject *const_str_plain_lensub;
extern PyObject *const_str_plain_XL_WRITEACCESS;
static PyObject *const_str_plain_XL_UNCALCED;
extern PyObject *const_str_plain_fmt;
extern PyObject *const_str_plain_XL_DATEMODE;
extern PyObject *const_str_plain_stream_len;
extern PyObject *const_str_plain_XL_CELL_BOOLEAN;
extern PyObject *const_str_space;
static PyObject *const_tuple_none_none_none_int_0_tuple;
static PyObject *const_int_pos_574;
extern PyObject *const_str_chr_63;
extern PyObject *const_str_chr_60;
static PyObject *const_str_plain_XL_MSO_DRAWING_SELECTION;
extern PyObject *const_str_plain_XL_FONT_B3B4;
extern PyObject *const_str_plain_mask;
extern PyObject *const_str_plain_XL_LABEL;
extern PyObject *const_str_plain_biff_text_from_num;
static PyObject *const_str_plain_hexd;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_XL_ROW;
static PyObject *const_int_pos_1212;
extern PyObject *const_str_plain_XL_FONT;
static PyObject *const_str_digest_4e68982272e8ae4779d3456c4ece1ba3;
extern PyObject *const_str_plain_XL_ROW_B2;
extern PyObject *const_str_plain_savpos;
extern PyObject *const_str_empty;
extern PyObject *const_str_plain_XL_HORIZONTALPAGEBREAKS;
extern PyObject *const_str_plain_XL_XF;
extern PyObject *const_int_pos_2162;
extern PyObject *const_int_pos_47;
static PyObject *const_int_pos_43;
static PyObject *const_int_pos_42;
static PyObject *const_int_pos_41;
extern PyObject *const_int_pos_40;
static PyObject *const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple;
extern PyObject *const_int_pos_49;
static PyObject *const_str_plain__cell_opcode;
static PyObject *module_filename_obj;

static void _initModuleConstants( void )
{
    const_str_plain_tgt_obj = UNSTREAM_STRING( &constant_bin[ 461301 ], 7, 1 );
    const_tuple_9fd131eedafe66de2cf3bd725fc3092c_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_9fd131eedafe66de2cf3bd725fc3092c_tuple, 0, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    PyTuple_SET_ITEM( const_tuple_9fd131eedafe66de2cf3bd725fc3092c_tuple, 1, const_str_plain_pos ); Py_INCREF( const_str_plain_pos );
    PyTuple_SET_ITEM( const_tuple_9fd131eedafe66de2cf3bd725fc3092c_tuple, 2, const_str_plain_encoding ); Py_INCREF( const_str_plain_encoding );
    PyTuple_SET_ITEM( const_tuple_9fd131eedafe66de2cf3bd725fc3092c_tuple, 3, const_str_plain_lenlen ); Py_INCREF( const_str_plain_lenlen );
    PyTuple_SET_ITEM( const_tuple_9fd131eedafe66de2cf3bd725fc3092c_tuple, 4, const_str_plain_nchars ); Py_INCREF( const_str_plain_nchars );
    const_int_pos_351 = PyInt_FromLong( 351l );
    const_str_digest_73bc75123e178cc9716a872958726225 = UNSTREAM_STRING( &constant_bin[ 461308 ], 6, 0 );
    const_int_pos_438 = PyInt_FromLong( 438l );
    const_int_pos_125 = PyInt_FromLong( 125l );
    const_int_pos_430 = PyInt_FromLong( 430l );
    const_int_pos_433 = PyInt_FromLong( 433l );
    const_int_pos_432 = PyInt_FromLong( 432l );
    const_dict_3ea0089e9e3dc196147eafc31e4c1ee9 = _PyDict_NewPresized( 9 );
    const_str_plain_mac_roman = UNSTREAM_STRING( &constant_bin[ 461314 ], 9, 1 );
    PyDict_SetItem( const_dict_3ea0089e9e3dc196147eafc31e4c1ee9, const_int_pos_32768, const_str_plain_mac_roman );
    const_int_pos_32769 = PyInt_FromLong( 32769l );
    const_str_plain_cp1252 = UNSTREAM_STRING( &constant_bin[ 461323 ], 6, 1 );
    PyDict_SetItem( const_dict_3ea0089e9e3dc196147eafc31e4c1ee9, const_int_pos_32769, const_str_plain_cp1252 );
    const_int_pos_10081 = PyInt_FromLong( 10081l );
    const_str_plain_mac_turkish = UNSTREAM_STRING( &constant_bin[ 461329 ], 11, 1 );
    PyDict_SetItem( const_dict_3ea0089e9e3dc196147eafc31e4c1ee9, const_int_pos_10081, const_str_plain_mac_turkish );
    PyDict_SetItem( const_dict_3ea0089e9e3dc196147eafc31e4c1ee9, const_int_pos_1200, const_str_plain_utf_16_le );
    const_int_pos_10029 = PyInt_FromLong( 10029l );
    const_str_plain_mac_latin2 = UNSTREAM_STRING( &constant_bin[ 461340 ], 10, 1 );
    PyDict_SetItem( const_dict_3ea0089e9e3dc196147eafc31e4c1ee9, const_int_pos_10029, const_str_plain_mac_latin2 );
    const_int_pos_10000 = PyInt_FromLong( 10000l );
    PyDict_SetItem( const_dict_3ea0089e9e3dc196147eafc31e4c1ee9, const_int_pos_10000, const_str_plain_mac_roman );
    const_int_pos_10006 = PyInt_FromLong( 10006l );
    const_str_plain_mac_greek = UNSTREAM_STRING( &constant_bin[ 461350 ], 9, 1 );
    PyDict_SetItem( const_dict_3ea0089e9e3dc196147eafc31e4c1ee9, const_int_pos_10006, const_str_plain_mac_greek );
    const_int_pos_10007 = PyInt_FromLong( 10007l );
    const_str_plain_mac_cyrillic = UNSTREAM_STRING( &constant_bin[ 461359 ], 12, 1 );
    PyDict_SetItem( const_dict_3ea0089e9e3dc196147eafc31e4c1ee9, const_int_pos_10007, const_str_plain_mac_cyrillic );
    const_int_pos_10079 = PyInt_FromLong( 10079l );
    const_str_plain_mac_iceland = UNSTREAM_STRING( &constant_bin[ 461371 ], 11, 1 );
    PyDict_SetItem( const_dict_3ea0089e9e3dc196147eafc31e4c1ee9, const_int_pos_10079, const_str_plain_mac_iceland );
    assert( PyDict_Size( const_dict_3ea0089e9e3dc196147eafc31e4c1ee9 ) == 9 );
    const_str_plain_numbered = UNSTREAM_STRING( &constant_bin[ 461382 ], 8, 1 );
    const_str_plain_manifest = UNSTREAM_STRING( &constant_bin[ 461390 ], 8, 1 );
    const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple = PyTuple_New( 16 );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 0, const_str_plain_strg ); Py_INCREF( const_str_plain_strg );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 1, const_str_plain_ofs ); Py_INCREF( const_str_plain_ofs );
    const_str_plain_dlen = UNSTREAM_STRING( &constant_bin[ 461398 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 2, const_str_plain_dlen ); Py_INCREF( const_str_plain_dlen );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 3, const_str_plain_base ); Py_INCREF( const_str_plain_base );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 4, const_str_plain_fout ); Py_INCREF( const_str_plain_fout );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 5, const_str_plain_unnumbered ); Py_INCREF( const_str_plain_unnumbered );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 6, const_str_plain_endpos ); Py_INCREF( const_str_plain_endpos );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 7, const_str_plain_pos ); Py_INCREF( const_str_plain_pos );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 8, const_str_plain_numbered ); Py_INCREF( const_str_plain_numbered );
    const_str_plain_num_prefix = UNSTREAM_STRING( &constant_bin[ 11157 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 9, const_str_plain_num_prefix ); Py_INCREF( const_str_plain_num_prefix );
    const_str_plain_endsub = UNSTREAM_STRING( &constant_bin[ 461402 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 10, const_str_plain_endsub ); Py_INCREF( const_str_plain_endsub );
    const_str_plain_substrg = UNSTREAM_STRING( &constant_bin[ 461408 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 11, const_str_plain_substrg ); Py_INCREF( const_str_plain_substrg );
    const_str_plain_lensub = UNSTREAM_STRING( &constant_bin[ 461415 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 12, const_str_plain_lensub ); Py_INCREF( const_str_plain_lensub );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 13, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    const_str_plain_hexd = UNSTREAM_STRING( &constant_bin[ 461421 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 14, const_str_plain_hexd ); Py_INCREF( const_str_plain_hexd );
    const_str_plain_chard = UNSTREAM_STRING( &constant_bin[ 396 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 15, const_str_plain_chard ); Py_INCREF( const_str_plain_chard );
    const_str_digest_c8684a7fbdb42f7b66b506461dffd581 = UNSTREAM_STRING( &constant_bin[ 461425 ], 5, 0 );
    const_int_pos_545 = PyInt_FromLong( 545l );
    const_str_plain_slist = UNSTREAM_STRING( &constant_bin[ 461430 ], 5, 1 );
    const_int_pos_549 = PyInt_FromLong( 549l );
    const_str_plain_XL_FORMULA4 = UNSTREAM_STRING( &constant_bin[ 9835 ], 11, 1 );
    const_str_plain_XL_FORMULA3 = UNSTREAM_STRING( &constant_bin[ 9802 ], 11, 1 );
    const_tuple_c5a46ceff3868bca5656e2faaa793860_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_c5a46ceff3868bca5656e2faaa793860_tuple, 0, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    PyTuple_SET_ITEM( const_tuple_c5a46ceff3868bca5656e2faaa793860_tuple, 1, const_str_plain_pos ); Py_INCREF( const_str_plain_pos );
    PyTuple_SET_ITEM( const_tuple_c5a46ceff3868bca5656e2faaa793860_tuple, 2, const_str_plain_lenlen ); Py_INCREF( const_str_plain_lenlen );
    PyTuple_SET_ITEM( const_tuple_c5a46ceff3868bca5656e2faaa793860_tuple, 3, const_str_plain_known_len ); Py_INCREF( const_str_plain_known_len );
    PyTuple_SET_ITEM( const_tuple_c5a46ceff3868bca5656e2faaa793860_tuple, 4, const_str_plain_nchars ); Py_INCREF( const_str_plain_nchars );
    PyTuple_SET_ITEM( const_tuple_c5a46ceff3868bca5656e2faaa793860_tuple, 5, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    const_str_plain_phonetic = UNSTREAM_STRING( &constant_bin[ 461435 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_c5a46ceff3868bca5656e2faaa793860_tuple, 6, const_str_plain_phonetic ); Py_INCREF( const_str_plain_phonetic );
    const_str_plain_richtext = UNSTREAM_STRING( &constant_bin[ 461443 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_c5a46ceff3868bca5656e2faaa793860_tuple, 7, const_str_plain_richtext ); Py_INCREF( const_str_plain_richtext );
    PyTuple_SET_ITEM( const_tuple_c5a46ceff3868bca5656e2faaa793860_tuple, 8, const_str_plain_rt ); Py_INCREF( const_str_plain_rt );
    PyTuple_SET_ITEM( const_tuple_c5a46ceff3868bca5656e2faaa793860_tuple, 9, const_str_plain_sz ); Py_INCREF( const_str_plain_sz );
    PyTuple_SET_ITEM( const_tuple_c5a46ceff3868bca5656e2faaa793860_tuple, 10, const_str_plain_strg ); Py_INCREF( const_str_plain_strg );
    const_tuple_int_pos_2_none_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_int_pos_2_none_tuple, 0, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    PyTuple_SET_ITEM( const_tuple_int_pos_2_none_tuple, 1, Py_None ); Py_INCREF( Py_None );
    const_tuple_int_pos_2057_int_pos_1033_int_pos_521_int_pos_9_tuple = PyTuple_New( 4 );
    const_int_pos_2057 = PyInt_FromLong( 2057l );
    PyTuple_SET_ITEM( const_tuple_int_pos_2057_int_pos_1033_int_pos_521_int_pos_9_tuple, 0, const_int_pos_2057 ); Py_INCREF( const_int_pos_2057 );
    const_int_pos_1033 = PyInt_FromLong( 1033l );
    PyTuple_SET_ITEM( const_tuple_int_pos_2057_int_pos_1033_int_pos_521_int_pos_9_tuple, 1, const_int_pos_1033 ); Py_INCREF( const_int_pos_1033 );
    const_int_pos_521 = PyInt_FromLong( 521l );
    PyTuple_SET_ITEM( const_tuple_int_pos_2057_int_pos_1033_int_pos_521_int_pos_9_tuple, 2, const_int_pos_521 ); Py_INCREF( const_int_pos_521 );
    PyTuple_SET_ITEM( const_tuple_int_pos_2057_int_pos_1033_int_pos_521_int_pos_9_tuple, 3, const_int_pos_9 ); Py_INCREF( const_int_pos_9 );
    const_str_digest_4d4f1eb43dba0133c343318478063cbc = UNSTREAM_STRING( &constant_bin[ 461451 ], 4, 0 );
    const_str_plain_recname = UNSTREAM_STRING( &constant_bin[ 11507 ], 7, 1 );
    const_int_pos_38 = PyInt_FromLong( 38l );
    const_int_pos_39 = PyInt_FromLong( 39l );
    const_int_pos_33 = PyInt_FromLong( 33l );
    const_str_digest_8a11c0c083f755e41606cc49dac48c2e = UNSTREAM_STRING( &constant_bin[ 461455 ], 7, 0 );
    const_int_pos_1030 = PyInt_FromLong( 1030l );
    const_str_plain_dummies = UNSTREAM_STRING( &constant_bin[ 11313 ], 7, 1 );
    const_str_plain_adj = UNSTREAM_STRING( &constant_bin[ 25404 ], 3, 1 );
    const_str_digest_035ddedf6744ab690912d979cd8804ae = UNSTREAM_STRING( &constant_bin[ 461462 ], 43, 0 );
    const_dict_6ca13ccd13dd5d5b66b3d9a86dbc0eb0 = _PyDict_NewPresized( 7 );
    const_str_digest_81a9a087be28ba712bbfe67d1d7a8bf1 = UNSTREAM_STRING( &constant_bin[ 461505 ], 6, 0 );
    PyDict_SetItem( const_dict_6ca13ccd13dd5d5b66b3d9a86dbc0eb0, const_int_0, const_str_digest_81a9a087be28ba712bbfe67d1d7a8bf1 );
    const_str_digest_9756e42be4b940a8f413b6a411e53934 = UNSTREAM_STRING( &constant_bin[ 461511 ], 5, 0 );
    PyDict_SetItem( const_dict_6ca13ccd13dd5d5b66b3d9a86dbc0eb0, const_int_pos_36, const_str_digest_9756e42be4b940a8f413b6a411e53934 );
    PyDict_SetItem( const_dict_6ca13ccd13dd5d5b66b3d9a86dbc0eb0, const_int_pos_7, const_str_digest_8a11c0c083f755e41606cc49dac48c2e );
    const_int_pos_42 = PyInt_FromLong( 42l );
    PyDict_SetItem( const_dict_6ca13ccd13dd5d5b66b3d9a86dbc0eb0, const_int_pos_42, const_str_digest_4d4f1eb43dba0133c343318478063cbc );
    const_str_digest_4468a499e275b96988b2c7c7fbfc545b = UNSTREAM_STRING( &constant_bin[ 461516 ], 7, 0 );
    PyDict_SetItem( const_dict_6ca13ccd13dd5d5b66b3d9a86dbc0eb0, const_int_pos_15, const_str_digest_4468a499e275b96988b2c7c7fbfc545b );
    const_str_digest_f794daa1e63029828ec58248549815d2 = UNSTREAM_STRING( &constant_bin[ 461523 ], 5, 0 );
    PyDict_SetItem( const_dict_6ca13ccd13dd5d5b66b3d9a86dbc0eb0, const_int_pos_23, const_str_digest_f794daa1e63029828ec58248549815d2 );
    const_str_digest_4e68982272e8ae4779d3456c4ece1ba3 = UNSTREAM_STRING( &constant_bin[ 461528 ], 6, 0 );
    PyDict_SetItem( const_dict_6ca13ccd13dd5d5b66b3d9a86dbc0eb0, const_int_pos_29, const_str_digest_4e68982272e8ae4779d3456c4ece1ba3 );
    assert( PyDict_Size( const_dict_6ca13ccd13dd5d5b66b3d9a86dbc0eb0 ) == 7 );
    const_str_digest_d30f5965de4596eef50172dfa5661286 = UNSTREAM_STRING( &constant_bin[ 461534 ], 17, 0 );
    const_str_plain_stream_end = UNSTREAM_STRING( &constant_bin[ 461551 ], 10, 1 );
    const_str_digest_b6139e4269dc05287b03c22a344c5df7 = UNSTREAM_STRING( &constant_bin[ 461561 ], 53, 0 );
    const_str_digest_fafacfd47cf13238a8c1faf6984b28af = UNSTREAM_STRING( &constant_bin[ 461614 ], 14, 0 );
    const_tuple_str_plain___module___str_plain__repr_these_str_plain_dump_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain___module___str_plain__repr_these_str_plain_dump_tuple, 0, const_str_plain___module__ ); Py_INCREF( const_str_plain___module__ );
    PyTuple_SET_ITEM( const_tuple_str_plain___module___str_plain__repr_these_str_plain_dump_tuple, 1, const_str_plain__repr_these ); Py_INCREF( const_str_plain__repr_these );
    PyTuple_SET_ITEM( const_tuple_str_plain___module___str_plain__repr_these_str_plain_dump_tuple, 2, const_str_plain_dump ); Py_INCREF( const_str_plain_dump );
    const_str_plain__cell_opcode_dict = UNSTREAM_STRING( &constant_bin[ 10090 ], 17, 1 );
    const_str_plain_XL_FORMULA = UNSTREAM_STRING( &constant_bin[ 9770 ], 10, 1 );
    const_tuple_0f3a1163ae378483907f1114e471f41f_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_0f3a1163ae378483907f1114e471f41f_tuple, 0, const_str_plain_tgt_obj ); Py_INCREF( const_str_plain_tgt_obj );
    const_str_plain_src = UNSTREAM_STRING( &constant_bin[ 461628 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_0f3a1163ae378483907f1114e471f41f_tuple, 1, const_str_plain_src ); Py_INCREF( const_str_plain_src );
    PyTuple_SET_ITEM( const_tuple_0f3a1163ae378483907f1114e471f41f_tuple, 2, const_str_plain_manifest ); Py_INCREF( const_str_plain_manifest );
    const_str_plain_local_setattr = UNSTREAM_STRING( &constant_bin[ 461631 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_0f3a1163ae378483907f1114e471f41f_tuple, 3, const_str_plain_local_setattr ); Py_INCREF( const_str_plain_local_setattr );
    const_str_plain_n = UNSTREAM_CHAR( 110, 1 );
    PyTuple_SET_ITEM( const_tuple_0f3a1163ae378483907f1114e471f41f_tuple, 4, const_str_plain_n ); Py_INCREF( const_str_plain_n );
    PyTuple_SET_ITEM( const_tuple_0f3a1163ae378483907f1114e471f41f_tuple, 5, const_str_plain_mask ); Py_INCREF( const_str_plain_mask );
    PyTuple_SET_ITEM( const_tuple_0f3a1163ae378483907f1114e471f41f_tuple, 6, const_str_plain_attr ); Py_INCREF( const_str_plain_attr );
    const_str_digest_279b5d5b7f24193f9814ec32a3d4e5c8 = UNSTREAM_STRING( &constant_bin[ 461644 ], 83, 0 );
    const_int_pos_214 = PyInt_FromLong( 214l );
    const_tuple_11afd00d8994dbdff702e5548795eb51_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_11afd00d8994dbdff702e5548795eb51_tuple, 0, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    PyTuple_SET_ITEM( const_tuple_11afd00d8994dbdff702e5548795eb51_tuple, 1, const_str_plain_pos ); Py_INCREF( const_str_plain_pos );
    PyTuple_SET_ITEM( const_tuple_11afd00d8994dbdff702e5548795eb51_tuple, 2, const_str_plain_lenlen ); Py_INCREF( const_str_plain_lenlen );
    PyTuple_SET_ITEM( const_tuple_11afd00d8994dbdff702e5548795eb51_tuple, 3, const_str_plain_nchars ); Py_INCREF( const_str_plain_nchars );
    PyTuple_SET_ITEM( const_tuple_11afd00d8994dbdff702e5548795eb51_tuple, 4, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    PyTuple_SET_ITEM( const_tuple_11afd00d8994dbdff702e5548795eb51_tuple, 5, const_str_plain_rawstrg ); Py_INCREF( const_str_plain_rawstrg );
    PyTuple_SET_ITEM( const_tuple_11afd00d8994dbdff702e5548795eb51_tuple, 6, const_str_plain_strg ); Py_INCREF( const_str_plain_strg );
    const_str_plain_DATEFORMAT = UNSTREAM_STRING( &constant_bin[ 461727 ], 10, 1 );
    const_int_pos_1054 = PyInt_FromLong( 1054l );
    const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple = PyTuple_New( 12 );
    PyTuple_SET_ITEM( const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple, 0, const_str_plain_mem ); Py_INCREF( const_str_plain_mem );
    const_str_plain_stream_offset = UNSTREAM_STRING( &constant_bin[ 461737 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple, 1, const_str_plain_stream_offset ); Py_INCREF( const_str_plain_stream_offset );
    PyTuple_SET_ITEM( const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple, 2, const_str_plain_stream_len ); Py_INCREF( const_str_plain_stream_len );
    PyTuple_SET_ITEM( const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple, 3, const_str_plain_fout ); Py_INCREF( const_str_plain_fout );
    PyTuple_SET_ITEM( const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple, 4, const_str_plain_pos ); Py_INCREF( const_str_plain_pos );
    PyTuple_SET_ITEM( const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple, 5, const_str_plain_stream_end ); Py_INCREF( const_str_plain_stream_end );
    const_str_plain_tally = UNSTREAM_STRING( &constant_bin[ 461750 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple, 6, const_str_plain_tally ); Py_INCREF( const_str_plain_tally );
    PyTuple_SET_ITEM( const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple, 7, const_str_plain_rc ); Py_INCREF( const_str_plain_rc );
    PyTuple_SET_ITEM( const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple, 8, const_str_plain_length ); Py_INCREF( const_str_plain_length );
    PyTuple_SET_ITEM( const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple, 9, const_str_plain_recname ); Py_INCREF( const_str_plain_recname );
    PyTuple_SET_ITEM( const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple, 10, const_str_plain_slist ); Py_INCREF( const_str_plain_slist );
    const_str_plain_count = UNSTREAM_STRING( &constant_bin[ 11560 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple, 11, const_str_plain_count ); Py_INCREF( const_str_plain_count );
    const_int_pos_189 = PyInt_FromLong( 189l );
    const_int_pos_1091 = PyInt_FromLong( 1091l );
    const_int_pos_92 = PyInt_FromLong( 92l );
    const_int_pos_93 = PyInt_FromLong( 93l );
    const_str_digest_14b1d9542392632c1dd58ad837d7d5e0 = UNSTREAM_STRING( &constant_bin[ 461755 ], 34, 0 );
    const_str_plain_ra = UNSTREAM_STRING( &constant_bin[ 1389 ], 2, 1 );
    const_str_plain_indent = UNSTREAM_STRING( &constant_bin[ 461789 ], 6, 1 );
    const_str_plain_is_cell_opcode = UNSTREAM_STRING( &constant_bin[ 461795 ], 14, 1 );
    const_str_digest_2dd278a1d486e1dec430092103f8a661 = UNSTREAM_STRING( &constant_bin[ 461809 ], 26, 0 );
    const_str_plain__brecstrg = UNSTREAM_STRING( &constant_bin[ 10163 ], 9, 1 );
    const_str_plain_XL_PAGESETUP = UNSTREAM_STRING( &constant_bin[ 461835 ], 12, 1 );
    const_str_digest_d1092b79338523976efa4188834ad524 = UNSTREAM_STRING( &constant_bin[ 461847 ], 30, 0 );
    const_int_pos_131 = PyInt_FromLong( 131l );
    const_int_pos_132 = PyInt_FromLong( 132l );
    const_int_pos_133 = PyInt_FromLong( 133l );
    const_int_pos_66 = PyInt_FromLong( 66l );
    const_str_plain__cell_opcode_list = UNSTREAM_STRING( &constant_bin[ 10051 ], 17, 1 );
    const_int_pos_253 = PyInt_FromLong( 253l );
    const_str_chr_126 = UNSTREAM_CHAR( 126, 0 );
    const_str_digest_9d755e04da81084ffe307f7654bf55a3 = UNSTREAM_STRING( &constant_bin[ 461877 ], 16, 0 );
    const_str_plain_XL_PRINTGRIDLINES = UNSTREAM_STRING( &constant_bin[ 461893 ], 17, 1 );
    const_str_digest_6d43b3c93240dbf6ee5b33f97f4493f7 = UNSTREAM_STRING( &constant_bin[ 461910 ], 9, 0 );
    const_str_plain_XL_INDEX = UNSTREAM_STRING( &constant_bin[ 461919 ], 8, 1 );
    const_str_plain_ca = UNSTREAM_STRING( &constant_bin[ 16 ], 2, 1 );
    const_str_digest_151a9c921b189f1612ad426f27c62dd9 = UNSTREAM_STRING( &constant_bin[ 461927 ], 14, 0 );
    const_str_plain_XL_BOTTOMMARGIN = UNSTREAM_STRING( &constant_bin[ 461941 ], 15, 1 );
    const_int_pos_171 = PyInt_FromLong( 171l );
    const_str_plain__numh = UNSTREAM_STRING( &constant_bin[ 10288 ], 5, 1 );
    const_str_plain_NUMBERFORMAT = UNSTREAM_STRING( &constant_bin[ 461956 ], 12, 1 );
    const_tuple_int_pos_2_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_int_pos_2_tuple, 0, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    const_str_plain_XL_HEADER = UNSTREAM_STRING( &constant_bin[ 461968 ], 9, 1 );
    const_int_pos_518 = PyInt_FromLong( 518l );
    const_int_pos_519 = PyInt_FromLong( 519l );
    const_int_pos_516 = PyInt_FromLong( 516l );
    const_int_pos_517 = PyInt_FromLong( 517l );
    const_int_pos_515 = PyInt_FromLong( 515l );
    const_int_pos_513 = PyInt_FromLong( 513l );
    const_tuple_52142698930a28821d51b9a6e51acf25_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_52142698930a28821d51b9a6e51acf25_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_52142698930a28821d51b9a6e51acf25_tuple, 1, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    PyTuple_SET_ITEM( const_tuple_52142698930a28821d51b9a6e51acf25_tuple, 2, const_str_plain_header ); Py_INCREF( const_str_plain_header );
    PyTuple_SET_ITEM( const_tuple_52142698930a28821d51b9a6e51acf25_tuple, 3, const_str_plain_footer ); Py_INCREF( const_str_plain_footer );
    PyTuple_SET_ITEM( const_tuple_52142698930a28821d51b9a6e51acf25_tuple, 4, const_str_plain_indent ); Py_INCREF( const_str_plain_indent );
    PyTuple_SET_ITEM( const_tuple_52142698930a28821d51b9a6e51acf25_tuple, 5, const_str_plain_alist ); Py_INCREF( const_str_plain_alist );
    PyTuple_SET_ITEM( const_tuple_52142698930a28821d51b9a6e51acf25_tuple, 6, const_str_plain_attr ); Py_INCREF( const_str_plain_attr );
    const_str_plain_pad = UNSTREAM_STRING( &constant_bin[ 461977 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_52142698930a28821d51b9a6e51acf25_tuple, 7, const_str_plain_pad ); Py_INCREF( const_str_plain_pad );
    const_str_plain_list_type = UNSTREAM_STRING( &constant_bin[ 461980 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_52142698930a28821d51b9a6e51acf25_tuple, 8, const_str_plain_list_type ); Py_INCREF( const_str_plain_list_type );
    const_str_plain_dict_type = UNSTREAM_STRING( &constant_bin[ 461989 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_52142698930a28821d51b9a6e51acf25_tuple, 9, const_str_plain_dict_type ); Py_INCREF( const_str_plain_dict_type );
    PyTuple_SET_ITEM( const_tuple_52142698930a28821d51b9a6e51acf25_tuple, 10, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    const_str_digest_8989e5819a70580cc3034b2f9b4fbe1b = UNSTREAM_STRING( &constant_bin[ 461998 ], 19, 0 );
    const_str_plain_XL_UNKNOWN = UNSTREAM_STRING( &constant_bin[ 462017 ], 10, 1 );
    const_int_pos_190 = PyInt_FromLong( 190l );
    const_int_pos_69 = PyInt_FromLong( 69l );
    const_int_pos_68 = PyInt_FromLong( 68l );
    const_str_plain_local_int = UNSTREAM_STRING( &constant_bin[ 462027 ], 9, 1 );
    const_dict_a3a2e5d3cfb5ecd8166a51f69633413e = _PyDict_NewPresized( 4 );
    PyDict_SetItem( const_dict_a3a2e5d3cfb5ecd8166a51f69633413e, const_int_pos_9, const_int_pos_4 );
    PyDict_SetItem( const_dict_a3a2e5d3cfb5ecd8166a51f69633413e, const_int_pos_1033, const_int_pos_6 );
    PyDict_SetItem( const_dict_a3a2e5d3cfb5ecd8166a51f69633413e, const_int_pos_2057, const_int_pos_8 );
    PyDict_SetItem( const_dict_a3a2e5d3cfb5ecd8166a51f69633413e, const_int_pos_521, const_int_pos_6 );
    assert( PyDict_Size( const_dict_a3a2e5d3cfb5ecd8166a51f69633413e ) == 4 );
    const_int_pos_229 = PyInt_FromLong( 229l );
    const_int_pos_86 = PyInt_FromLong( 86l );
    const_int_pos_85 = PyInt_FromLong( 85l );
    const_str_plain_WRKSHEET = UNSTREAM_STRING( &constant_bin[ 462036 ], 8, 1 );
    const_str_plain_XL_EXTSST = UNSTREAM_STRING( &constant_bin[ 462044 ], 9, 1 );
    const_str_digest_aecd01c0995302cd41850de69810d8f6 = UNSTREAM_STRING( &constant_bin[ 462053 ], 13, 0 );
    const_tuple_str_plain_c_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_c_tuple, 0, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    const_str_plain_XL_FOOTER = UNSTREAM_STRING( &constant_bin[ 462066 ], 9, 1 );
    const_int_pos_638 = PyInt_FromLong( 638l );
    const_tuple_int_pos_1_none_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_int_pos_1_none_tuple, 0, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    PyTuple_SET_ITEM( const_tuple_int_pos_1_none_tuple, 1, Py_None ); Py_INCREF( Py_None );
    const_int_pos_579 = PyInt_FromLong( 579l );
    const_int_pos_146 = PyInt_FromLong( 146l );
    const_tuple_int_pos_6_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_int_pos_6_tuple, 0, const_int_pos_6 ); Py_INCREF( const_int_pos_6 );
    const_int_pos_140 = PyInt_FromLong( 140l );
    const_str_plain_XL_MSO_DRAWING_GROUP = UNSTREAM_STRING( &constant_bin[ 462075 ], 20, 1 );
    const_str_plain_XL_BOUNDSHEET_CHART = UNSTREAM_STRING( &constant_bin[ 462095 ], 19, 1 );
    const_str_angle_UNKNOWN = UNSTREAM_STRING( &constant_bin[ 462114 ], 9, 0 );
    const_int_pos_566 = PyInt_FromLong( 566l );
    const_int_pos_561 = PyInt_FromLong( 561l );
    const_str_plain_output_list = UNSTREAM_STRING( &constant_bin[ 462123 ], 11, 1 );
    const_tuple_int_pos_6_int_pos_1030_int_pos_518_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_int_pos_6_int_pos_1030_int_pos_518_tuple, 0, const_int_pos_6 ); Py_INCREF( const_int_pos_6 );
    PyTuple_SET_ITEM( const_tuple_int_pos_6_int_pos_1030_int_pos_518_tuple, 1, const_int_pos_1030 ); Py_INCREF( const_int_pos_1030 );
    PyTuple_SET_ITEM( const_tuple_int_pos_6_int_pos_1030_int_pos_518_tuple, 2, const_int_pos_518 ); Py_INCREF( const_int_pos_518 );
    const_str_plain_XL_LEFTMARGIN = UNSTREAM_STRING( &constant_bin[ 462134 ], 13, 1 );
    const_str_plain_XL_VCENTER = UNSTREAM_STRING( &constant_bin[ 462147 ], 10, 1 );
    const_str_plain__buff = UNSTREAM_STRING( &constant_bin[ 10194 ], 5, 1 );
    const_str_digest_c8a0af0c0a1527d7b677e5153ac3cf44 = UNSTREAM_STRING( &constant_bin[ 462157 ], 10, 0 );
    const_str_plain_XL_WSBOOL = UNSTREAM_STRING( &constant_bin[ 462167 ], 9, 1 );
    const_int_pos_54 = PyInt_FromLong( 54l );
    const_int_pos_55 = PyInt_FromLong( 55l );
    const_tuple_908ca83fea928ec6d97ff1eeddbf3947_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_908ca83fea928ec6d97ff1eeddbf3947_tuple, 0, const_str_plain_tgt_obj ); Py_INCREF( const_str_plain_tgt_obj );
    PyTuple_SET_ITEM( const_tuple_908ca83fea928ec6d97ff1eeddbf3947_tuple, 1, const_str_plain_src ); Py_INCREF( const_str_plain_src );
    PyTuple_SET_ITEM( const_tuple_908ca83fea928ec6d97ff1eeddbf3947_tuple, 2, const_str_plain_manifest ); Py_INCREF( const_str_plain_manifest );
    PyTuple_SET_ITEM( const_tuple_908ca83fea928ec6d97ff1eeddbf3947_tuple, 3, const_str_plain_local_setattr ); Py_INCREF( const_str_plain_local_setattr );
    PyTuple_SET_ITEM( const_tuple_908ca83fea928ec6d97ff1eeddbf3947_tuple, 4, const_str_plain_local_int ); Py_INCREF( const_str_plain_local_int );
    PyTuple_SET_ITEM( const_tuple_908ca83fea928ec6d97ff1eeddbf3947_tuple, 5, const_str_plain_n ); Py_INCREF( const_str_plain_n );
    PyTuple_SET_ITEM( const_tuple_908ca83fea928ec6d97ff1eeddbf3947_tuple, 6, const_str_plain_mask ); Py_INCREF( const_str_plain_mask );
    PyTuple_SET_ITEM( const_tuple_908ca83fea928ec6d97ff1eeddbf3947_tuple, 7, const_str_plain_attr ); Py_INCREF( const_str_plain_attr );
    const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple = PyTuple_New( 16 );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 0, const_str_plain_mem ); Py_INCREF( const_str_plain_mem );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 1, const_str_plain_stream_offset ); Py_INCREF( const_str_plain_stream_offset );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 2, const_str_plain_stream_len ); Py_INCREF( const_str_plain_stream_len );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 3, const_str_plain_base ); Py_INCREF( const_str_plain_base );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 4, const_str_plain_fout ); Py_INCREF( const_str_plain_fout );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 5, const_str_plain_unnumbered ); Py_INCREF( const_str_plain_unnumbered );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 6, const_str_plain_pos ); Py_INCREF( const_str_plain_pos );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 7, const_str_plain_stream_end ); Py_INCREF( const_str_plain_stream_end );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 8, const_str_plain_adj ); Py_INCREF( const_str_plain_adj );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 9, const_str_plain_dummies ); Py_INCREF( const_str_plain_dummies );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 10, const_str_plain_numbered ); Py_INCREF( const_str_plain_numbered );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 11, const_str_plain_num_prefix ); Py_INCREF( const_str_plain_num_prefix );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 12, const_str_plain_rc ); Py_INCREF( const_str_plain_rc );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 13, const_str_plain_length ); Py_INCREF( const_str_plain_length );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 14, const_str_plain_savpos ); Py_INCREF( const_str_plain_savpos );
    PyTuple_SET_ITEM( const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 15, const_str_plain_recname ); Py_INCREF( const_str_plain_recname );
    const_str_plain_biff_rec_name_dict = UNSTREAM_STRING( &constant_bin[ 10248 ], 18, 1 );
    const_str_plain_WBKBLOBAL = UNSTREAM_STRING( &constant_bin[ 462176 ], 9, 1 );
    const_dict_ff5080d8cdf9d4b5eabcea109a63e8c9 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 462185 ], 128 );
    const_tuple_c1402daa720d85b29c196951ec0f4aca_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_c1402daa720d85b29c196951ec0f4aca_tuple, 0, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    PyTuple_SET_ITEM( const_tuple_c1402daa720d85b29c196951ec0f4aca_tuple, 1, const_str_plain_pos ); Py_INCREF( const_str_plain_pos );
    PyTuple_SET_ITEM( const_tuple_c1402daa720d85b29c196951ec0f4aca_tuple, 2, const_str_plain_encoding ); Py_INCREF( const_str_plain_encoding );
    PyTuple_SET_ITEM( const_tuple_c1402daa720d85b29c196951ec0f4aca_tuple, 3, const_str_plain_lenlen ); Py_INCREF( const_str_plain_lenlen );
    PyTuple_SET_ITEM( const_tuple_c1402daa720d85b29c196951ec0f4aca_tuple, 4, const_str_plain_known_len ); Py_INCREF( const_str_plain_known_len );
    PyTuple_SET_ITEM( const_tuple_c1402daa720d85b29c196951ec0f4aca_tuple, 5, const_str_plain_nchars ); Py_INCREF( const_str_plain_nchars );
    PyTuple_SET_ITEM( const_tuple_c1402daa720d85b29c196951ec0f4aca_tuple, 6, const_str_plain_newpos ); Py_INCREF( const_str_plain_newpos );
    const_tuple_2d7a42b9ddf265e370cf80ec4093139a_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_2d7a42b9ddf265e370cf80ec4093139a_tuple, 0, const_int_0 ); Py_INCREF( const_int_0 );
    PyTuple_SET_ITEM( const_tuple_2d7a42b9ddf265e370cf80ec4093139a_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    PyTuple_SET_ITEM( const_tuple_2d7a42b9ddf265e370cf80ec4093139a_tuple, 2, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    PyTuple_SET_ITEM( const_tuple_2d7a42b9ddf265e370cf80ec4093139a_tuple, 3, const_int_pos_3 ); Py_INCREF( const_int_pos_3 );
    PyTuple_SET_ITEM( const_tuple_2d7a42b9ddf265e370cf80ec4093139a_tuple, 4, const_int_pos_4 ); Py_INCREF( const_int_pos_4 );
    PyTuple_SET_ITEM( const_tuple_2d7a42b9ddf265e370cf80ec4093139a_tuple, 5, const_int_pos_5 ); Py_INCREF( const_int_pos_5 );
    PyTuple_SET_ITEM( const_tuple_2d7a42b9ddf265e370cf80ec4093139a_tuple, 6, const_int_pos_6 ); Py_INCREF( const_int_pos_6 );
    const_str_plain_XL_BOUNDSHEET_VB_MODULE = UNSTREAM_STRING( &constant_bin[ 462313 ], 23, 1 );
    const_str_plain_XL_SHEETPR = UNSTREAM_STRING( &constant_bin[ 9706 ], 10, 1 );
    const_str_plain_setattr = UNSTREAM_STRING( &constant_bin[ 461637 ], 7, 1 );
    const_str_plain__name = UNSTREAM_STRING( &constant_bin[ 10221 ], 5, 1 );
    const_int_pos_440 = PyInt_FromLong( 440l );
    const_int_pos_523 = PyInt_FromLong( 523l );
    const_int_pos_520 = PyInt_FromLong( 520l );
    const_str_plain_XL_PRINTHEADERS = UNSTREAM_STRING( &constant_bin[ 462336 ], 15, 1 );
    const_str_digest_0245eef4613ec410273d22c8f101bc78 = UNSTREAM_STRING( &constant_bin[ 462351 ], 5, 0 );
    const_int_pos_236 = PyInt_FromLong( 236l );
    const_str_plain_XL_HCENTER = UNSTREAM_STRING( &constant_bin[ 462356 ], 10, 1 );
    const_str_plain_XL_TOPMARGIN = UNSTREAM_STRING( &constant_bin[ 462366 ], 12, 1 );
    const_str_plain_splitlines = UNSTREAM_STRING( &constant_bin[ 462378 ], 10, 1 );
    const_tuple_int_pos_6_int_pos_8_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_int_pos_6_int_pos_8_tuple, 0, const_int_pos_6 ); Py_INCREF( const_int_pos_6 );
    PyTuple_SET_ITEM( const_tuple_int_pos_6_int_pos_8_tuple, 1, const_int_pos_8 ); Py_INCREF( const_int_pos_8 );
    const_str_digest_96756a074b4136fc9b772237c96d84d0 = UNSTREAM_STRING( &constant_bin[ 462388 ], 2320, 0 );
    const_str_plain_list = UNSTREAM_STRING( &constant_bin[ 117 ], 4, 1 );
    const_str_digest_f3465426630a3bcfed3eab224df00939 = UNSTREAM_STRING( &constant_bin[ 464708 ], 18, 0 );
    const_int_pos_659 = PyInt_FromLong( 659l );
    const_str_plain_XL_RIGHTMARGIN = UNSTREAM_STRING( &constant_bin[ 464726 ], 14, 1 );
    const_str_plain_XL_BOF = UNSTREAM_STRING( &constant_bin[ 464740 ], 6, 1 );
    const_int_pos_153 = PyInt_FromLong( 153l );
    const_str_plain_XL_UNCALCED = UNSTREAM_STRING( &constant_bin[ 464746 ], 11, 1 );
    const_tuple_none_none_none_int_0_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_none_none_none_int_0_tuple, 0, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_none_int_0_tuple, 1, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_none_int_0_tuple, 2, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_none_int_0_tuple, 3, const_int_0 ); Py_INCREF( const_int_0 );
    const_int_pos_574 = PyInt_FromLong( 574l );
    const_str_plain_XL_MSO_DRAWING_SELECTION = UNSTREAM_STRING( &constant_bin[ 464757 ], 24, 1 );
    const_int_pos_1212 = PyInt_FromLong( 1212l );
    const_int_pos_43 = PyInt_FromLong( 43l );
    const_int_pos_41 = PyInt_FromLong( 41l );
    const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple = PyTuple_New( 12 );
    PyTuple_SET_ITEM( const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple, 0, const_str_plain_output_list ); Py_INCREF( const_str_plain_output_list );
    PyTuple_SET_ITEM( const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple, 1, const_str_plain_data ); Py_INCREF( const_str_plain_data );
    PyTuple_SET_ITEM( const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple, 2, const_str_plain_pos ); Py_INCREF( const_str_plain_pos );
    PyTuple_SET_ITEM( const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple, 3, const_str_plain_biff_version ); Py_INCREF( const_str_plain_biff_version );
    PyTuple_SET_ITEM( const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple, 4, const_str_plain_addr_size ); Py_INCREF( const_str_plain_addr_size );
    PyTuple_SET_ITEM( const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple, 5, const_str_plain_n ); Py_INCREF( const_str_plain_n );
    PyTuple_SET_ITEM( const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple, 6, const_str_plain_fmt ); Py_INCREF( const_str_plain_fmt );
    PyTuple_SET_ITEM( const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple, 7, const_str_plain__unused ); Py_INCREF( const_str_plain__unused );
    PyTuple_SET_ITEM( const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple, 8, const_str_plain_ra ); Py_INCREF( const_str_plain_ra );
    PyTuple_SET_ITEM( const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple, 9, const_str_plain_rb ); Py_INCREF( const_str_plain_rb );
    PyTuple_SET_ITEM( const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple, 10, const_str_plain_ca ); Py_INCREF( const_str_plain_ca );
    PyTuple_SET_ITEM( const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple, 11, const_str_plain_cb ); Py_INCREF( const_str_plain_cb );
    const_str_plain__cell_opcode = UNSTREAM_STRING( &constant_bin[ 10051 ], 12, 1 );
}

#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_xlrd$biffh( void )
{

}
#endif

// The module code objects.
static PyCodeObject *codeobj_eaf6be3210a2a76ae592c8ec0bb46599;
static PyCodeObject *codeobj_e51d167ceb0c76b2ab1e73d7adefde31;
static PyCodeObject *codeobj_97e99faadaa018455f7decf994313044;
static PyCodeObject *codeobj_650207a5b7da7dc4f354aeda34b91d67;
static PyCodeObject *codeobj_4ebfadaef41f3d75b1e9d7c4b5e0583c;
static PyCodeObject *codeobj_a94fe701d8d9ee6a72feccc7707afaae;
static PyCodeObject *codeobj_5be5cdf2ab2da8906009df1bd7ebf3f1;
static PyCodeObject *codeobj_37d8e658270520bbbb78114daec154c0;
static PyCodeObject *codeobj_1388f9aeddaf16831e766a08cb3c6c0f;
static PyCodeObject *codeobj_94aeeee88a55179d7df58ca47aff0221;
static PyCodeObject *codeobj_f9afca4691fec58985424a48940d50f4;
static PyCodeObject *codeobj_c0775a082f861eac7fa3f350d3af47e1;
static PyCodeObject *codeobj_8fd68a4528e572be93ef97238a9050aa;
static PyCodeObject *codeobj_3e6beaad15eb3e64b04a7b643fa43b3a;

static void _initModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_aecd01c0995302cd41850de69810d8f6 );
    codeobj_eaf6be3210a2a76ae592c8ec0bb46599 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_BaseObject, 34, const_tuple_str_plain___module___str_plain__repr_these_str_plain_dump_tuple, 0, CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_PRINT_FUNCTION );
    codeobj_e51d167ceb0c76b2ab1e73d7adefde31 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_biff_count_records, 618, const_tuple_324a13cac91978ca5d7edbb79ad02780_tuple, 4, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE | CO_FUTURE_PRINT_FUNCTION );
    codeobj_97e99faadaa018455f7decf994313044 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_biff_dump, 572, const_tuple_85dfc1e3822b4ca3b0c5fa7be761b297_tuple, 6, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE | CO_FUTURE_PRINT_FUNCTION );
    codeobj_650207a5b7da7dc4f354aeda34b91d67 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_biffh, 0, const_tuple_empty, 0, CO_NOFREE | CO_FUTURE_PRINT_FUNCTION );
    codeobj_4ebfadaef41f3d75b1e9d7c4b5e0583c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_dump, 44, const_tuple_52142698930a28821d51b9a6e51acf25_tuple, 5, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE | CO_FUTURE_PRINT_FUNCTION );
    codeobj_a94fe701d8d9ee6a72feccc7707afaae = MAKE_CODEOBJ( module_filename_obj, const_str_plain_hex_char_dump, 541, const_tuple_f449474a076092a9ae89c54215b6d5e3_tuple, 6, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE | CO_FUTURE_PRINT_FUNCTION );
    codeobj_5be5cdf2ab2da8906009df1bd7ebf3f1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_is_cell_opcode, 255, const_tuple_str_plain_c_tuple, 1, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE | CO_FUTURE_PRINT_FUNCTION );
    codeobj_37d8e658270520bbbb78114daec154c0 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_ca7becc9d49589e000ad29ce1ad566e8, 355, const_tuple_afd88b0b5633bf8398246d16ecdf1295_tuple, 5, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE | CO_FUTURE_PRINT_FUNCTION );
    codeobj_1388f9aeddaf16831e766a08cb3c6c0f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_unpack_string, 266, const_tuple_9fd131eedafe66de2cf3bd725fc3092c_tuple, 4, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE | CO_FUTURE_PRINT_FUNCTION );
    codeobj_94aeeee88a55179d7df58ca47aff0221 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_unpack_string_update_pos, 271, const_tuple_c1402daa720d85b29c196951ec0f4aca_tuple, 5, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE | CO_FUTURE_PRINT_FUNCTION );
    codeobj_f9afca4691fec58985424a48940d50f4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_unpack_unicode, 281, const_tuple_11afd00d8994dbdff702e5548795eb51_tuple, 3, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE | CO_FUTURE_PRINT_FUNCTION );
    codeobj_c0775a082f861eac7fa3f350d3af47e1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_unpack_unicode_update_pos, 320, const_tuple_c5a46ceff3868bca5656e2faaa793860_tuple, 4, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE | CO_FUTURE_PRINT_FUNCTION );
    codeobj_8fd68a4528e572be93ef97238a9050aa = MAKE_CODEOBJ( module_filename_obj, const_str_plain_upkbits, 258, const_tuple_0f3a1163ae378483907f1114e471f41f_tuple, 4, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE | CO_FUTURE_PRINT_FUNCTION );
    codeobj_3e6beaad15eb3e64b04a7b643fa43b3a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_upkbitsL, 262, const_tuple_908ca83fea928ec6d97ff1eeddbf3947_tuple, 5, CO_NEWLOCALS | CO_OPTIMIZED | CO_NOFREE | CO_FUTURE_PRINT_FUNCTION );
}

// The module function declarations.
NUITKA_LOCAL_MODULE PyObject *impl_class_1_XLRDError_of_xlrd$biffh(  );


NUITKA_LOCAL_MODULE PyObject *impl_class_2_BaseObject_of_xlrd$biffh(  );


static PyObject *MAKE_FUNCTION_function_10_unpack_cell_range_address_list_update_pos_of_xlrd$biffh( PyObject *defaults );


static PyObject *MAKE_FUNCTION_function_11_hex_char_dump_of_xlrd$biffh( PyObject *defaults );


static PyObject *MAKE_FUNCTION_function_12_biff_dump_of_xlrd$biffh( PyObject *defaults );


static PyObject *MAKE_FUNCTION_function_13_biff_count_records_of_xlrd$biffh( PyObject *defaults );


static PyObject *MAKE_FUNCTION_function_1_dump_of_class_2_BaseObject_of_xlrd$biffh( PyObject *defaults );


static PyObject *MAKE_FUNCTION_function_3_is_cell_opcode_of_xlrd$biffh(  );


static PyObject *MAKE_FUNCTION_function_4_upkbits_of_xlrd$biffh( PyObject *defaults );


static PyObject *MAKE_FUNCTION_function_5_upkbitsL_of_xlrd$biffh( PyObject *defaults );


static PyObject *MAKE_FUNCTION_function_6_unpack_string_of_xlrd$biffh( PyObject *defaults );


static PyObject *MAKE_FUNCTION_function_7_unpack_string_update_pos_of_xlrd$biffh( PyObject *defaults );


static PyObject *MAKE_FUNCTION_function_8_unpack_unicode_of_xlrd$biffh( PyObject *defaults );


static PyObject *MAKE_FUNCTION_function_9_unpack_unicode_update_pos_of_xlrd$biffh( PyObject *defaults );


// The module function definitions.
NUITKA_LOCAL_MODULE PyObject *impl_class_1_XLRDError_of_xlrd$biffh(  )
{
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
    assert(!had_error); // Do not enter inlined functions with error set.
#endif

    // Local variable declarations.
    PyObject *var___module__ = NULL;
    PyObject *tmp_assign_source_1;
    int tmp_res;
    PyObject *tmp_return_value;
    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = const_str_digest_c8a0af0c0a1527d7b677e5153ac3cf44;
    assert( var___module__ == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var___module__ = tmp_assign_source_1;

    // Tried code
    tmp_return_value = PyDict_New();
    if ( var___module__ != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_return_value,
            const_str_plain___module__,
            var___module__
        );
        assert( tmp_res != -1 );

    }
    goto try_finally_handler_start_1;
    try_finally_handler_start_1:;
    CHECK_OBJECT( (PyObject *)var___module__ );
    Py_DECREF( var___module__ );
    var___module__ = NULL;

    // Re-raise as necessary after finally was executed.
    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_1;
    finally_end_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( class_1_XLRDError_of_xlrd$biffh );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


NUITKA_LOCAL_MODULE PyObject *impl_class_2_BaseObject_of_xlrd$biffh(  )
{
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
    assert(!had_error); // Do not enter inlined functions with error set.
#endif

    // Local variable declarations.
    PyObject *var___module__ = NULL;
    PyObject *var__repr_these = NULL;
    PyObject *var_dump = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_defaults_1;
    PyObject *tmp_frame_locals;
    int tmp_res;
    PyObject *tmp_return_value;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = const_str_digest_c8a0af0c0a1527d7b677e5153ac3cf44;
    assert( var___module__ == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var___module__ = tmp_assign_source_1;

    tmp_assign_source_2 = PyList_New( 0 );
    assert( var__repr_these == NULL );
    var__repr_these = tmp_assign_source_2;

    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_eaf6be3210a2a76ae592c8ec0bb46599, module_xlrd$biffh );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_defaults_1 = const_tuple_none_none_none_int_0_tuple;
    tmp_assign_source_3 = MAKE_FUNCTION_function_1_dump_of_class_2_BaseObject_of_xlrd$biffh( INCREASE_REFCOUNT( tmp_defaults_1 ) );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_3 );

        frame_function->f_lineno = 44;
        goto frame_exception_exit_1;
    }
    assert( var_dump == NULL );
    var_dump = tmp_assign_source_3;


#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var___module__ != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain___module__,
            var___module__
        );
        assert( tmp_res != -1 );

    }
    if ( var__repr_these != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain__repr_these,
            var__repr_these
        );
        assert( tmp_res != -1 );

    }
    if ( var_dump != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_dump,
            var_dump
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    tmp_return_value = PyDict_New();
    if ( var___module__ != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_return_value,
            const_str_plain___module__,
            var___module__
        );
        assert( tmp_res != -1 );

    }
    if ( var__repr_these != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_return_value,
            const_str_plain__repr_these,
            var__repr_these
        );
        assert( tmp_res != -1 );

    }
    if ( var_dump != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_return_value,
            const_str_plain_dump,
            var_dump
        );
        assert( tmp_res != -1 );

    }
    goto try_finally_handler_start_1;
    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)var___module__ );
    Py_DECREF( var___module__ );
    var___module__ = NULL;

    CHECK_OBJECT( (PyObject *)var__repr_these );
    Py_DECREF( var__repr_these );
    var__repr_these = NULL;

    Py_XDECREF( var_dump );
    var_dump = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_1;
    finally_end_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( class_2_BaseObject_of_xlrd$biffh );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}


static PyObject *impl_function_1_dump_of_class_2_BaseObject_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject *_python_par_self, PyObject *_python_par_f, PyObject *_python_par_header, PyObject *_python_par_footer, PyObject *_python_par_indent )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = _python_par_self;
    PyObject *par_f = _python_par_f;
    PyObject *par_header = _python_par_header;
    PyObject *par_footer = _python_par_footer;
    PyObject *par_indent = _python_par_indent;
    PyObject *var_alist = NULL;
    PyObject *var_attr = NULL;
    PyObject *var_pad = NULL;
    PyObject *var_list_type = NULL;
    PyObject *var_dict_type = NULL;
    PyObject *var_value = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_and_1__value_1 = NULL;
    PyObject *tmp_or_1__value_1 = NULL;
    PyObject *tmp_and_2__value_1 = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_args_element_name_7;
    PyObject *tmp_args_name_1;
    PyObject *tmp_args_name_2;
    PyObject *tmp_args_name_3;
    PyObject *tmp_args_name_4;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    PyObject *tmp_called_name_6;
    PyObject *tmp_called_name_7;
    PyObject *tmp_called_name_8;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    PyObject *tmp_compexpr_left_1;
    PyObject *tmp_compexpr_left_2;
    PyObject *tmp_compexpr_right_1;
    PyObject *tmp_compexpr_right_2;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    int tmp_cond_truth_3;
    int tmp_cond_truth_4;
    int tmp_cond_truth_5;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_cond_value_3;
    PyObject *tmp_cond_value_4;
    PyObject *tmp_cond_value_5;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_key_2;
    PyObject *tmp_dict_key_3;
    PyObject *tmp_dict_key_4;
    PyObject *tmp_dict_key_5;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_dict_value_2;
    PyObject *tmp_dict_value_3;
    PyObject *tmp_dict_value_4;
    PyObject *tmp_dict_value_5;
    PyObject *tmp_frame_locals;
    PyObject *tmp_getattr_attr_1;
    PyObject *tmp_getattr_attr_2;
    PyObject *tmp_getattr_default_1;
    PyObject *tmp_getattr_target_1;
    PyObject *tmp_getattr_target_2;
    PyObject *tmp_hasattr_attr_1;
    PyObject *tmp_hasattr_source_1;
    bool tmp_is_1;
    PyObject *tmp_isinstance_cls_1;
    PyObject *tmp_isinstance_cls_2;
    PyObject *tmp_isinstance_inst_1;
    PyObject *tmp_isinstance_inst_2;
    bool tmp_isnot_1;
    bool tmp_isnot_2;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iter_arg_2;
    PyObject *tmp_iter_arg_3;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_kw_name_2;
    PyObject *tmp_kw_name_3;
    PyObject *tmp_kw_name_4;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_left_name_4;
    PyObject *tmp_len_arg_1;
    PyObject *tmp_next_source_1;
    PyObject *tmp_next_source_2;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_right_name_4;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_source_name_6;
    PyObject *tmp_source_name_7;
    PyObject *tmp_source_name_8;
    PyObject *tmp_source_name_9;
    int tmp_tried_lineno_1;
    int tmp_tried_lineno_2;
    int tmp_tried_lineno_3;
    int tmp_tried_lineno_4;
    int tmp_tried_lineno_5;
    int tmp_tried_lineno_6;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    PyObject *tmp_tuple_element_3;
    PyObject *tmp_tuple_element_4;
    PyObject *tmp_tuple_element_5;
    PyObject *tmp_tuple_element_6;
    PyObject *tmp_tuple_element_7;
    PyObject *tmp_type_arg_1;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_4ebfadaef41f3d75b1e9d7c4b5e0583c, module_xlrd$biffh );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_compare_left_1 = par_f;

    tmp_compare_right_1 = Py_None;
    tmp_is_1 = ( tmp_compare_left_1 == tmp_compare_right_1 );
    if (tmp_is_1)
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 639 ], 32, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 46;
        goto frame_exception_exit_1;
    }

    tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_stderr );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 46;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = par_f;
        assert( old != NULL );
        par_f = tmp_assign_source_1;
        Py_DECREF( old );
    }

    branch_no_1:;
    tmp_hasattr_source_1 = par_self;

    tmp_hasattr_attr_1 = const_str_plain___slots__;
    tmp_res = PyObject_HasAttr( tmp_hasattr_source_1, tmp_hasattr_attr_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 47;
        goto frame_exception_exit_1;
    }
    if (tmp_res == 1)
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_assign_source_2 = PyList_New( 0 );
    assert( var_alist == NULL );
    var_alist = tmp_assign_source_2;

    tmp_source_name_2 = par_self;

    tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___slots__ );
    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 49;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 49;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_3;

    // Tried code
    loop_start_1:;
    tmp_next_source_1 = tmp_for_loop_1__for_iterator;

    tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_1;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 49;
            goto try_finally_handler_2;
        }
    }

    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_4;
        Py_XDECREF( old );
    }

    tmp_assign_source_5 = tmp_for_loop_1__iter_value;

    {
        PyObject *old = var_attr;
        var_attr = tmp_assign_source_5;
        Py_INCREF( var_attr );
        Py_XDECREF( old );
    }

    tmp_source_name_3 = var_alist;

    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_append );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 50;
        goto try_finally_handler_2;
    }
    tmp_args_element_name_1 = PyTuple_New( 2 );
    tmp_tuple_element_1 = var_attr;

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_element_name_1, 0, tmp_tuple_element_1 );
    tmp_getattr_target_1 = par_self;

    tmp_getattr_attr_1 = var_attr;

    tmp_tuple_element_1 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );

        frame_function->f_lineno = 50;
        goto try_finally_handler_2;
    }
    PyTuple_SET_ITEM( tmp_args_element_name_1, 1, tmp_tuple_element_1 );
    frame_function->f_lineno = 50;
    tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, tmp_args_element_name_1 );
    Py_DECREF( tmp_called_name_1 );
    Py_DECREF( tmp_args_element_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 50;
        goto try_finally_handler_2;
    }
    Py_DECREF( tmp_unused );
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 49;
        goto try_finally_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_1 = frame_function->f_lineno;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    frame_function->f_lineno = tmp_tried_lineno_1;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto frame_exception_exit_1;
    }

    goto finally_end_1;
    finally_end_1:;
    goto branch_end_2;
    branch_no_2:;
    tmp_source_name_5 = par_self;

    tmp_source_name_4 = LOOKUP_ATTRIBUTE_DICT_SLOT( tmp_source_name_5 );
    if ( tmp_source_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 52;
        goto frame_exception_exit_1;
    }
    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_items );
    Py_DECREF( tmp_source_name_4 );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 52;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 52;
    tmp_assign_source_6 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
    Py_DECREF( tmp_called_name_2 );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 52;
        goto frame_exception_exit_1;
    }
    assert( var_alist == NULL );
    var_alist = tmp_assign_source_6;

    branch_end_2:;
    tmp_called_name_3 = LOOKUP_BUILTIN( const_str_plain_sorted );
    if ( tmp_called_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 53;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_2 = var_alist;

    frame_function->f_lineno = 53;
    tmp_assign_source_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, tmp_args_element_name_2 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 53;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_alist;
        assert( old != NULL );
        var_alist = tmp_assign_source_7;
        Py_DECREF( old );
    }

    tmp_left_name_1 = const_str_space;
    tmp_right_name_1 = par_indent;

    tmp_assign_source_8 = BINARY_OPERATION_MUL( tmp_left_name_1, tmp_right_name_1 );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 54;
        goto frame_exception_exit_1;
    }
    assert( var_pad == NULL );
    var_pad = tmp_assign_source_8;

    tmp_compare_left_2 = par_header;

    tmp_compare_right_2 = Py_None;
    tmp_isnot_1 = ( tmp_compare_left_2 != tmp_compare_right_2 );
    if (tmp_isnot_1)
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_called_name_4 = LOOKUP_BUILTIN( const_str_plain_print );
    if ( tmp_called_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 55;
        goto frame_exception_exit_1;
    }
    tmp_args_name_1 = PyTuple_New( 1 );
    tmp_tuple_element_2 = par_header;

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
    tmp_kw_name_1 = _PyDict_NewPresized( 1 );
    tmp_dict_value_1 = par_f;

    tmp_dict_key_1 = const_str_plain_file;
    PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    frame_function->f_lineno = 55;
    tmp_unused = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 55;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    branch_no_3:;
    tmp_assign_source_9 = LOOKUP_BUILTIN( const_str_plain_list );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 56;
        goto frame_exception_exit_1;
    }
    assert( var_list_type == NULL );
    Py_INCREF( tmp_assign_source_9 );
    var_list_type = tmp_assign_source_9;

    tmp_assign_source_10 = LOOKUP_BUILTIN( const_str_plain_dict );
    if ( tmp_assign_source_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 57;
        goto frame_exception_exit_1;
    }
    assert( var_dict_type == NULL );
    Py_INCREF( tmp_assign_source_10 );
    var_dict_type = tmp_assign_source_10;

    tmp_iter_arg_2 = var_alist;

    tmp_assign_source_11 = MAKE_ITERATOR( tmp_iter_arg_2 );
    if ( tmp_assign_source_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 58;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_2__for_iterator == NULL );
    tmp_for_loop_2__for_iterator = tmp_assign_source_11;

    // Tried code
    loop_start_2:;
    tmp_next_source_2 = tmp_for_loop_2__for_iterator;

    tmp_assign_source_12 = ITERATOR_NEXT( tmp_next_source_2 );
    if ( tmp_assign_source_12 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_2;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 58;
            goto try_finally_handler_3;
        }
    }

    {
        PyObject *old = tmp_for_loop_2__iter_value;
        tmp_for_loop_2__iter_value = tmp_assign_source_12;
        Py_XDECREF( old );
    }

    // Tried code
    tmp_iter_arg_3 = tmp_for_loop_2__iter_value;

    tmp_assign_source_13 = MAKE_ITERATOR( tmp_iter_arg_3 );
    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 58;
        goto try_finally_handler_4;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__source_iter;
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_13;
        Py_XDECREF( old );
    }

    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_14 = UNPACK_NEXT( tmp_unpack_1, 0 );
    if ( tmp_assign_source_14 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 58;
        goto try_finally_handler_4;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__element_1;
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_14;
        Py_XDECREF( old );
    }

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_15 = UNPACK_NEXT( tmp_unpack_2, 1 );
    if ( tmp_assign_source_15 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 58;
        goto try_finally_handler_4;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__element_2;
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_15;
        Py_XDECREF( old );
    }

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                goto try_finally_handler_4;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        goto try_finally_handler_4;
    }
    tmp_assign_source_16 = tmp_tuple_unpack_1__element_1;

    {
        PyObject *old = var_attr;
        var_attr = tmp_assign_source_16;
        Py_INCREF( var_attr );
        Py_XDECREF( old );
    }

    tmp_assign_source_17 = tmp_tuple_unpack_1__element_2;

    {
        PyObject *old = var_value;
        var_value = tmp_assign_source_17;
        Py_INCREF( var_value );
        Py_XDECREF( old );
    }

    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_2 = frame_function->f_lineno;
    Py_XDECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_2;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_2 != NULL )
    {
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;

        goto try_finally_handler_3;
    }

    goto finally_end_2;
    finally_end_2:;
    // Tried code
    tmp_cond_value_1 = NULL;
    // Tried code
    tmp_getattr_target_2 = var_value;

    if ( tmp_getattr_target_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10309 ], 51, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 59;
        goto try_finally_handler_6;
    }

    tmp_getattr_attr_2 = const_str_plain_dump;
    tmp_getattr_default_1 = Py_None;
    tmp_assign_source_18 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_1 );
    if ( tmp_assign_source_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 59;
        goto try_finally_handler_6;
    }
    {
        PyObject *old = tmp_and_1__value_1;
        tmp_and_1__value_1 = tmp_assign_source_18;
        Py_XDECREF( old );
    }

    tmp_cond_value_2 = tmp_and_1__value_1;

    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 59;
        goto try_finally_handler_6;
    }
    if (tmp_cond_truth_2 == 1)
    {
        goto condexpr_true_1;
    }
    else
    {
        goto condexpr_false_1;
    }
    condexpr_true_1:;
    tmp_cond_value_1 = NULL;
    // Tried code
    tmp_result = tmp_and_1__value_1 != NULL;
    if ( tmp_result == true )
    {
        Py_DECREF( tmp_and_1__value_1 );
        tmp_and_1__value_1 = NULL;
    }

    assert( tmp_result != false );
    tmp_compexpr_left_1 = var_attr;

    if ( tmp_compexpr_left_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10360 ], 50, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 59;
        goto try_finally_handler_7;
    }

    tmp_compexpr_right_1 = const_str_plain_book;
    tmp_cond_value_1 = RICH_COMPARE_NE( tmp_compexpr_left_1, tmp_compexpr_right_1 );
    if ( tmp_cond_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 59;
        goto try_finally_handler_7;
    }
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_7:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_3 != NULL )
    {
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;

        goto try_finally_handler_6;
    }

    goto finally_end_3;
    finally_end_3:;
    goto condexpr_end_1;
    condexpr_false_1:;
    tmp_cond_value_1 = tmp_and_1__value_1;

    Py_INCREF( tmp_cond_value_1 );
    condexpr_end_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_6:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_4 != NULL )
    {
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;

        goto try_finally_handler_5;
    }

    goto finally_end_4;
    finally_end_4:;
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        frame_function->f_lineno = 59;
        goto try_finally_handler_5;
    }
    Py_DECREF( tmp_cond_value_1 );
    if (tmp_cond_truth_1 == 1)
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_source_name_6 = var_value;

    if ( tmp_source_name_6 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10309 ], 51, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 60;
        goto try_finally_handler_5;
    }

    tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_dump );
    if ( tmp_called_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 60;
        goto try_finally_handler_5;
    }
    tmp_args_name_2 = PyTuple_New( 1 );
    tmp_tuple_element_3 = par_f;

    Py_INCREF( tmp_tuple_element_3 );
    PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
    tmp_kw_name_2 = _PyDict_NewPresized( 2 );
    tmp_left_name_2 = const_str_digest_d30f5965de4596eef50172dfa5661286;
    tmp_right_name_2 = PyTuple_New( 3 );
    tmp_tuple_element_4 = var_pad;

    Py_INCREF( tmp_tuple_element_4 );
    PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_4 );
    tmp_tuple_element_4 = var_attr;

    if ( tmp_tuple_element_4 == NULL )
    {
        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        Py_DECREF( tmp_right_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10360 ], 50, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 61;
        goto try_finally_handler_5;
    }

    Py_INCREF( tmp_tuple_element_4 );
    PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_4 );
    tmp_source_name_8 = var_value;

    if ( tmp_source_name_8 == NULL )
    {
        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        Py_DECREF( tmp_right_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10309 ], 51, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 61;
        goto try_finally_handler_5;
    }

    tmp_source_name_7 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_8 );
    if ( tmp_source_name_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        Py_DECREF( tmp_right_name_2 );

        frame_function->f_lineno = 61;
        goto try_finally_handler_5;
    }
    tmp_tuple_element_4 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain___name__ );
    Py_DECREF( tmp_source_name_7 );
    if ( tmp_tuple_element_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        Py_DECREF( tmp_right_name_2 );

        frame_function->f_lineno = 61;
        goto try_finally_handler_5;
    }
    PyTuple_SET_ITEM( tmp_right_name_2, 2, tmp_tuple_element_4 );
    tmp_dict_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
    Py_DECREF( tmp_right_name_2 );
    if ( tmp_dict_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_2 );

        frame_function->f_lineno = 61;
        goto try_finally_handler_5;
    }
    tmp_dict_key_2 = const_str_plain_header;
    PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
    Py_DECREF( tmp_dict_value_2 );
    tmp_left_name_3 = par_indent;

    tmp_right_name_3 = const_int_pos_4;
    tmp_dict_value_3 = BINARY_OPERATION_ADD( tmp_left_name_3, tmp_right_name_3 );
    if ( tmp_dict_value_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_2 );

        frame_function->f_lineno = 62;
        goto try_finally_handler_5;
    }
    tmp_dict_key_3 = const_str_plain_indent;
    PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
    Py_DECREF( tmp_dict_value_3 );
    frame_function->f_lineno = 62;
    tmp_unused = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_2, tmp_kw_name_2 );
    Py_DECREF( tmp_called_name_5 );
    Py_DECREF( tmp_args_name_2 );
    Py_DECREF( tmp_kw_name_2 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 62;
        goto try_finally_handler_5;
    }
    Py_DECREF( tmp_unused );
    goto branch_end_4;
    branch_no_4:;
    // Tried code
    tmp_cond_value_3 = NULL;
    // Tried code
    tmp_compexpr_left_2 = var_attr;

    if ( tmp_compexpr_left_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10360 ], 50, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 63;
        goto try_finally_handler_9;
    }

    tmp_source_name_9 = par_self;

    tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain__repr_these );
    if ( tmp_compexpr_right_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 63;
        goto try_finally_handler_9;
    }
    tmp_assign_source_19 = SEQUENCE_CONTAINS_NOT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
    if ( tmp_assign_source_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compexpr_right_2 );

        frame_function->f_lineno = 63;
        goto try_finally_handler_9;
    }
    Py_DECREF( tmp_compexpr_right_2 );
    {
        PyObject *old = tmp_and_2__value_1;
        tmp_and_2__value_1 = tmp_assign_source_19;
        Py_INCREF( tmp_and_2__value_1 );
        Py_XDECREF( old );
    }

    tmp_cond_value_4 = tmp_and_2__value_1;

    tmp_cond_truth_4 = CHECK_IF_TRUE( tmp_cond_value_4 );
    if ( tmp_cond_truth_4 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 64;
        goto try_finally_handler_9;
    }
    if (tmp_cond_truth_4 == 1)
    {
        goto condexpr_true_2;
    }
    else
    {
        goto condexpr_false_2;
    }
    condexpr_true_2:;
    tmp_cond_value_3 = NULL;
    // Tried code
    tmp_cond_value_3 = NULL;
    // Tried code
    tmp_result = tmp_and_2__value_1 != NULL;
    if ( tmp_result == true )
    {
        Py_DECREF( tmp_and_2__value_1 );
        tmp_and_2__value_1 = NULL;
    }

    assert( tmp_result != false );
    tmp_cond_value_3 = NULL;
    // Tried code
    tmp_isinstance_inst_1 = var_value;

    if ( tmp_isinstance_inst_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10309 ], 51, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 64;
        goto try_finally_handler_12;
    }

    tmp_isinstance_cls_1 = LOOKUP_BUILTIN( const_str_plain_list );
    if ( tmp_isinstance_cls_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 56;
        goto try_finally_handler_12;
    }
    tmp_assign_source_20 = BUILTIN_ISINSTANCE( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
    if ( tmp_assign_source_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 64;
        goto try_finally_handler_12;
    }
    {
        PyObject *old = tmp_or_1__value_1;
        tmp_or_1__value_1 = tmp_assign_source_20;
        Py_INCREF( tmp_or_1__value_1 );
        Py_XDECREF( old );
    }

    tmp_cond_value_5 = tmp_or_1__value_1;

    tmp_cond_truth_5 = CHECK_IF_TRUE( tmp_cond_value_5 );
    if ( tmp_cond_truth_5 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 64;
        goto try_finally_handler_12;
    }
    if (tmp_cond_truth_5 == 1)
    {
        goto condexpr_true_3;
    }
    else
    {
        goto condexpr_false_3;
    }
    condexpr_true_3:;
    tmp_cond_value_3 = tmp_or_1__value_1;

    goto condexpr_end_3;
    condexpr_false_3:;
    tmp_cond_value_3 = NULL;
    // Tried code
    tmp_result = tmp_or_1__value_1 != NULL;
    if ( tmp_result == true )
    {
        Py_DECREF( tmp_or_1__value_1 );
        tmp_or_1__value_1 = NULL;
    }

    assert( tmp_result != false );
    tmp_isinstance_inst_2 = var_value;

    if ( tmp_isinstance_inst_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10309 ], 51, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 64;
        goto try_finally_handler_13;
    }

    tmp_isinstance_cls_2 = LOOKUP_BUILTIN( const_str_plain_dict );
    if ( tmp_isinstance_cls_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 57;
        goto try_finally_handler_13;
    }
    tmp_cond_value_3 = BUILTIN_ISINSTANCE( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
    if ( tmp_cond_value_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 64;
        goto try_finally_handler_13;
    }
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_13:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_5 != NULL )
    {
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;

        goto try_finally_handler_12;
    }

    goto finally_end_5;
    finally_end_5:;
    condexpr_end_3:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_12:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_6 != NULL )
    {
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;

        goto try_finally_handler_11;
    }

    goto finally_end_6;
    finally_end_6:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_11:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_7 != NULL )
    {
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;

        goto try_finally_handler_10;
    }

    goto finally_end_7;
    finally_end_7:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_10:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_3 = frame_function->f_lineno;
    Py_XDECREF( tmp_or_1__value_1 );
    tmp_or_1__value_1 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_3;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_8 != NULL )
    {
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;

        goto try_finally_handler_9;
    }

    goto finally_end_8;
    finally_end_8:;
    goto condexpr_end_2;
    condexpr_false_2:;
    tmp_cond_value_3 = tmp_and_2__value_1;

    condexpr_end_2:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_9:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_9 != NULL )
    {
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;

        goto try_finally_handler_8;
    }

    goto finally_end_9;
    finally_end_9:;
    tmp_cond_truth_3 = CHECK_IF_TRUE( tmp_cond_value_3 );
    if ( tmp_cond_truth_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 64;
        goto try_finally_handler_8;
    }
    if (tmp_cond_truth_3 == 1)
    {
        goto branch_yes_5;
    }
    else
    {
        goto branch_no_5;
    }
    branch_yes_5:;
    tmp_called_name_6 = LOOKUP_BUILTIN( const_str_plain_print );
    if ( tmp_called_name_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 66;
        goto try_finally_handler_8;
    }
    tmp_args_name_3 = PyTuple_New( 1 );
    tmp_left_name_4 = const_str_digest_f3465426630a3bcfed3eab224df00939;
    tmp_right_name_4 = PyTuple_New( 4 );
    tmp_tuple_element_6 = var_pad;

    Py_INCREF( tmp_tuple_element_6 );
    PyTuple_SET_ITEM( tmp_right_name_4, 0, tmp_tuple_element_6 );
    tmp_tuple_element_6 = var_attr;

    if ( tmp_tuple_element_6 == NULL )
    {
        Py_DECREF( tmp_args_name_3 );
        Py_DECREF( tmp_right_name_4 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10360 ], 50, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 66;
        goto try_finally_handler_8;
    }

    Py_INCREF( tmp_tuple_element_6 );
    PyTuple_SET_ITEM( tmp_right_name_4, 1, tmp_tuple_element_6 );
    tmp_type_arg_1 = var_value;

    if ( tmp_type_arg_1 == NULL )
    {
        Py_DECREF( tmp_args_name_3 );
        Py_DECREF( tmp_right_name_4 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10309 ], 51, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 66;
        goto try_finally_handler_8;
    }

    tmp_tuple_element_6 = BUILTIN_TYPE1( tmp_type_arg_1 );
    if ( tmp_tuple_element_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_name_3 );
        Py_DECREF( tmp_right_name_4 );

        frame_function->f_lineno = 66;
        goto try_finally_handler_8;
    }
    PyTuple_SET_ITEM( tmp_right_name_4, 2, tmp_tuple_element_6 );
    tmp_len_arg_1 = var_value;

    if ( tmp_len_arg_1 == NULL )
    {
        Py_DECREF( tmp_args_name_3 );
        Py_DECREF( tmp_right_name_4 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10309 ], 51, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 66;
        goto try_finally_handler_8;
    }

    tmp_tuple_element_6 = BUILTIN_LEN( tmp_len_arg_1 );
    if ( tmp_tuple_element_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_name_3 );
        Py_DECREF( tmp_right_name_4 );

        frame_function->f_lineno = 66;
        goto try_finally_handler_8;
    }
    PyTuple_SET_ITEM( tmp_right_name_4, 3, tmp_tuple_element_6 );
    tmp_tuple_element_5 = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
    Py_DECREF( tmp_right_name_4 );
    if ( tmp_tuple_element_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_name_3 );

        frame_function->f_lineno = 66;
        goto try_finally_handler_8;
    }
    PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_5 );
    tmp_kw_name_3 = _PyDict_NewPresized( 1 );
    tmp_dict_value_4 = par_f;

    tmp_dict_key_4 = const_str_plain_file;
    PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_4, tmp_dict_value_4 );
    frame_function->f_lineno = 66;
    tmp_unused = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_3, tmp_kw_name_3 );
    Py_DECREF( tmp_args_name_3 );
    Py_DECREF( tmp_kw_name_3 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 66;
        goto try_finally_handler_8;
    }
    Py_DECREF( tmp_unused );
    goto branch_end_5;
    branch_no_5:;
    tmp_called_name_7 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_fprintf );

    if (unlikely( tmp_called_name_7 == NULL ))
    {
        tmp_called_name_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fprintf );
    }

    if ( tmp_called_name_7 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10410 ], 36, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 68;
        goto try_finally_handler_8;
    }

    tmp_args_element_name_3 = par_f;

    tmp_args_element_name_4 = const_str_digest_6d43b3c93240dbf6ee5b33f97f4493f7;
    tmp_args_element_name_5 = var_pad;

    tmp_args_element_name_6 = var_attr;

    if ( tmp_args_element_name_6 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10360 ], 50, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 68;
        goto try_finally_handler_8;
    }

    tmp_args_element_name_7 = var_value;

    if ( tmp_args_element_name_7 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10309 ], 51, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 68;
        goto try_finally_handler_8;
    }

    frame_function->f_lineno = 68;
    tmp_unused = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_7, tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 68;
        goto try_finally_handler_8;
    }
    Py_DECREF( tmp_unused );
    branch_end_5:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_8:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_4 = frame_function->f_lineno;
    Py_XDECREF( tmp_and_2__value_1 );
    tmp_and_2__value_1 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_4;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_10 != NULL )
    {
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;

        goto try_finally_handler_5;
    }

    goto finally_end_10;
    finally_end_10:;
    branch_end_4:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_5:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_5 = frame_function->f_lineno;
    Py_XDECREF( tmp_and_1__value_1 );
    tmp_and_1__value_1 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_5;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_11 != NULL )
    {
        exception_type = exception_keeper_type_11;
        exception_value = exception_keeper_value_11;
        exception_tb = exception_keeper_tb_11;

        goto try_finally_handler_3;
    }

    goto finally_end_11;
    finally_end_11:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 58;
        goto try_finally_handler_3;
    }
    goto loop_start_2;
    loop_end_2:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_3:;
    exception_keeper_type_12 = exception_type;
    exception_keeper_value_12 = exception_value;
    exception_keeper_tb_12 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_6 = frame_function->f_lineno;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    frame_function->f_lineno = tmp_tried_lineno_6;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_12 != NULL )
    {
        exception_type = exception_keeper_type_12;
        exception_value = exception_keeper_value_12;
        exception_tb = exception_keeper_tb_12;

        goto frame_exception_exit_1;
    }

    goto finally_end_12;
    finally_end_12:;
    tmp_compare_left_3 = par_footer;

    tmp_compare_right_3 = Py_None;
    tmp_isnot_2 = ( tmp_compare_left_3 != tmp_compare_right_3 );
    if (tmp_isnot_2)
    {
        goto branch_yes_6;
    }
    else
    {
        goto branch_no_6;
    }
    branch_yes_6:;
    tmp_called_name_8 = LOOKUP_BUILTIN( const_str_plain_print );
    if ( tmp_called_name_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 69;
        goto frame_exception_exit_1;
    }
    tmp_args_name_4 = PyTuple_New( 1 );
    tmp_tuple_element_7 = par_footer;

    Py_INCREF( tmp_tuple_element_7 );
    PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_7 );
    tmp_kw_name_4 = _PyDict_NewPresized( 1 );
    tmp_dict_value_5 = par_f;

    tmp_dict_key_5 = const_str_plain_file;
    PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_5, tmp_dict_value_5 );
    frame_function->f_lineno = 69;
    tmp_unused = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_4, tmp_kw_name_4 );
    Py_DECREF( tmp_args_name_4 );
    Py_DECREF( tmp_kw_name_4 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 69;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    branch_no_6:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var_alist != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_alist,
            var_alist
        );
        assert( tmp_res != -1 );

    }
    if ( var_attr != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_attr,
            var_attr
        );
        assert( tmp_res != -1 );

    }
    if ( var_pad != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_pad,
            var_pad
        );
        assert( tmp_res != -1 );

    }
    if ( var_list_type != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_list_type,
            var_list_type
        );
        assert( tmp_res != -1 );

    }
    if ( var_dict_type != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_dict_type,
            var_dict_type
        );
        assert( tmp_res != -1 );

    }
    if ( var_value != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_value,
            var_value
        );
        assert( tmp_res != -1 );

    }
    if ( par_self != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_self,
            par_self
        );
        assert( tmp_res != -1 );

    }
    if ( par_f != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_f,
            par_f
        );
        assert( tmp_res != -1 );

    }
    if ( par_header != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_header,
            par_header
        );
        assert( tmp_res != -1 );

    }
    if ( par_footer != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_footer,
            par_footer
        );
        assert( tmp_res != -1 );

    }
    if ( par_indent != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_indent,
            par_indent
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_finally_handler_start_1;
    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_13 = exception_type;
    exception_keeper_value_13 = exception_value;
    exception_keeper_tb_13 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_f );
    par_f = NULL;

    CHECK_OBJECT( (PyObject *)par_header );
    Py_DECREF( par_header );
    par_header = NULL;

    CHECK_OBJECT( (PyObject *)par_footer );
    Py_DECREF( par_footer );
    par_footer = NULL;

    CHECK_OBJECT( (PyObject *)par_indent );
    Py_DECREF( par_indent );
    par_indent = NULL;

    Py_XDECREF( var_alist );
    var_alist = NULL;

    Py_XDECREF( var_attr );
    var_attr = NULL;

    Py_XDECREF( var_pad );
    var_pad = NULL;

    Py_XDECREF( var_list_type );
    var_list_type = NULL;

    Py_XDECREF( var_dict_type );
    var_dict_type = NULL;

    Py_XDECREF( var_value );
    var_value = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_13 != NULL )
    {
        exception_type = exception_keeper_type_13;
        exception_value = exception_keeper_value_13;
        exception_tb = exception_keeper_tb_13;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_13;
    finally_end_13:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_1_dump_of_class_2_BaseObject_of_xlrd$biffh );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_1_dump_of_class_2_BaseObject_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_self = NULL;
    PyObject *_python_par_f = NULL;
    PyObject *_python_par_header = NULL;
    PyObject *_python_par_footer = NULL;
    PyObject *_python_par_indent = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "dump() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_self == key )
            {
                assert( _python_par_self == NULL );
                _python_par_self = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_f == key )
            {
                assert( _python_par_f == NULL );
                _python_par_f = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_header == key )
            {
                assert( _python_par_header == NULL );
                _python_par_header = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_footer == key )
            {
                assert( _python_par_footer == NULL );
                _python_par_footer = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_indent == key )
            {
                assert( _python_par_indent == NULL );
                _python_par_indent = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_self, key ) == 1 )
            {
                assert( _python_par_self == NULL );
                _python_par_self = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_f, key ) == 1 )
            {
                assert( _python_par_f == NULL );
                _python_par_f = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_header, key ) == 1 )
            {
                assert( _python_par_header == NULL );
                _python_par_header = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_footer, key ) == 1 )
            {
                assert( _python_par_footer == NULL );
                _python_par_footer = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_indent, key ) == 1 )
            {
                assert( _python_par_indent == NULL );
                _python_par_indent = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "dump() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 5 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_self != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_self = args[ 0 ];
        Py_INCREF( _python_par_self );
    }
    else if ( _python_par_self == NULL )
    {
        if ( 0 + self->m_defaults_given >= 5  )
        {
            _python_par_self = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 5 );
            Py_INCREF( _python_par_self );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 1 < args_given ))
    {
         if (unlikely( _python_par_f != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 1 );
             goto error_exit;
         }

        _python_par_f = args[ 1 ];
        Py_INCREF( _python_par_f );
    }
    else if ( _python_par_f == NULL )
    {
        if ( 1 + self->m_defaults_given >= 5  )
        {
            _python_par_f = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 1 - 5 );
            Py_INCREF( _python_par_f );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 2 < args_given ))
    {
         if (unlikely( _python_par_header != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 2 );
             goto error_exit;
         }

        _python_par_header = args[ 2 ];
        Py_INCREF( _python_par_header );
    }
    else if ( _python_par_header == NULL )
    {
        if ( 2 + self->m_defaults_given >= 5  )
        {
            _python_par_header = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 2 - 5 );
            Py_INCREF( _python_par_header );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 3 < args_given ))
    {
         if (unlikely( _python_par_footer != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 3 );
             goto error_exit;
         }

        _python_par_footer = args[ 3 ];
        Py_INCREF( _python_par_footer );
    }
    else if ( _python_par_footer == NULL )
    {
        if ( 3 + self->m_defaults_given >= 5  )
        {
            _python_par_footer = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 3 - 5 );
            Py_INCREF( _python_par_footer );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 4 < args_given ))
    {
         if (unlikely( _python_par_indent != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 4 );
             goto error_exit;
         }

        _python_par_indent = args[ 4 ];
        Py_INCREF( _python_par_indent );
    }
    else if ( _python_par_indent == NULL )
    {
        if ( 4 + self->m_defaults_given >= 5  )
        {
            _python_par_indent = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 4 - 5 );
            Py_INCREF( _python_par_indent );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_self == NULL || _python_par_f == NULL || _python_par_header == NULL || _python_par_footer == NULL || _python_par_indent == NULL ))
    {
        PyObject *values[] = { _python_par_self, _python_par_f, _python_par_header, _python_par_footer, _python_par_indent };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_1_dump_of_class_2_BaseObject_of_xlrd$biffh( self, _python_par_self, _python_par_f, _python_par_header, _python_par_footer, _python_par_indent );

error_exit:;

    Py_XDECREF( _python_par_self );
    Py_XDECREF( _python_par_f );
    Py_XDECREF( _python_par_header );
    Py_XDECREF( _python_par_footer );
    Py_XDECREF( _python_par_indent );

    return NULL;
}

static PyObject *dparse_function_1_dump_of_class_2_BaseObject_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 5 )
    {
        return impl_function_1_dump_of_class_2_BaseObject_of_xlrd$biffh( self, INCREASE_REFCOUNT( args[ 0 ] ), INCREASE_REFCOUNT( args[ 1 ] ), INCREASE_REFCOUNT( args[ 2 ] ), INCREASE_REFCOUNT( args[ 3 ] ), INCREASE_REFCOUNT( args[ 4 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_1_dump_of_class_2_BaseObject_of_xlrd$biffh( self, args, size, NULL );
        return result;
    }

}



static PyObject *impl_function_3_is_cell_opcode_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject *_python_par_c )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_c = _python_par_c;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *tmp_compexpr_left_1;
    PyObject *tmp_compexpr_right_1;
    PyObject *tmp_frame_locals;
    int tmp_res;
    PyObject *tmp_return_value;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_5be5cdf2ab2da8906009df1bd7ebf3f1, module_xlrd$biffh );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_compexpr_left_1 = par_c;

    tmp_compexpr_right_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__cell_opcode_dict );

    if (unlikely( tmp_compexpr_right_1 == NULL ))
    {
        tmp_compexpr_right_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__cell_opcode_dict );
    }

    if ( tmp_compexpr_right_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10446 ], 46, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 256;
        goto frame_exception_exit_1;
    }

    tmp_return_value = SEQUENCE_CONTAINS( tmp_compexpr_left_1, tmp_compexpr_right_1 );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 256;
        goto frame_exception_exit_1;
    }
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto try_finally_handler_start_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( par_c != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_c,
            par_c
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_c );
    Py_DECREF( par_c );
    par_c = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_1;
    finally_end_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_3_is_cell_opcode_of_xlrd$biffh );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_3_is_cell_opcode_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_c = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "is_cell_opcode() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_c == key )
            {
                assert( _python_par_c == NULL );
                _python_par_c = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_c, key ) == 1 )
            {
                assert( _python_par_c == NULL );
                _python_par_c = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "is_cell_opcode() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 1 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_c != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_c = args[ 0 ];
        Py_INCREF( _python_par_c );
    }
    else if ( _python_par_c == NULL )
    {
        if ( 0 + self->m_defaults_given >= 1  )
        {
            _python_par_c = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 1 );
            Py_INCREF( _python_par_c );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_c == NULL ))
    {
        PyObject *values[] = { _python_par_c };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_3_is_cell_opcode_of_xlrd$biffh( self, _python_par_c );

error_exit:;

    Py_XDECREF( _python_par_c );

    return NULL;
}

static PyObject *dparse_function_3_is_cell_opcode_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 1 )
    {
        return impl_function_3_is_cell_opcode_of_xlrd$biffh( self, INCREASE_REFCOUNT( args[ 0 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_3_is_cell_opcode_of_xlrd$biffh( self, args, size, NULL );
        return result;
    }

}



static PyObject *impl_function_4_upkbits_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject *_python_par_tgt_obj, PyObject *_python_par_src, PyObject *_python_par_manifest, PyObject *_python_par_local_setattr )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_tgt_obj = _python_par_tgt_obj;
    PyObject *par_src = _python_par_src;
    PyObject *par_manifest = _python_par_manifest;
    PyObject *par_local_setattr = _python_par_local_setattr;
    PyObject *var_n = NULL;
    PyObject *var_mask = NULL;
    PyObject *var_attr = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_called_name_1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iter_arg_2;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_next_source_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    int tmp_tried_lineno_1;
    int tmp_tried_lineno_2;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    PyObject *tmp_unpack_3;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_8fd68a4528e572be93ef97238a9050aa, module_xlrd$biffh );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_iter_arg_1 = par_manifest;

    tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 259;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_1;

    // Tried code
    loop_start_1:;
    tmp_next_source_1 = tmp_for_loop_1__for_iterator;

    tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_1;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 259;
            goto try_finally_handler_2;
        }
    }

    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_2;
        Py_XDECREF( old );
    }

    // Tried code
    tmp_iter_arg_2 = tmp_for_loop_1__iter_value;

    tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_2 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 259;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__source_iter;
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_3;
        Py_XDECREF( old );
    }

    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_1, 0 );
    if ( tmp_assign_source_4 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 259;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__element_1;
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_4;
        Py_XDECREF( old );
    }

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_2, 1 );
    if ( tmp_assign_source_5 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 259;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__element_2;
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_5;
        Py_XDECREF( old );
    }

    tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_3, 2 );
    if ( tmp_assign_source_6 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 259;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__element_3;
        tmp_tuple_unpack_1__element_3 = tmp_assign_source_6;
        Py_XDECREF( old );
    }

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                goto try_finally_handler_3;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        goto try_finally_handler_3;
    }
    tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;

    {
        PyObject *old = var_n;
        var_n = tmp_assign_source_7;
        Py_INCREF( var_n );
        Py_XDECREF( old );
    }

    tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;

    {
        PyObject *old = var_mask;
        var_mask = tmp_assign_source_8;
        Py_INCREF( var_mask );
        Py_XDECREF( old );
    }

    tmp_assign_source_9 = tmp_tuple_unpack_1__element_3;

    {
        PyObject *old = var_attr;
        var_attr = tmp_assign_source_9;
        Py_INCREF( var_attr );
        Py_XDECREF( old );
    }

    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_1 = frame_function->f_lineno;
    Py_XDECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_1;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto try_finally_handler_2;
    }

    goto finally_end_1;
    finally_end_1:;
    tmp_called_name_1 = par_local_setattr;

    tmp_args_element_name_1 = par_tgt_obj;

    tmp_args_element_name_2 = var_attr;

    if ( tmp_args_element_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10360 ], 50, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 260;
        goto try_finally_handler_2;
    }

    tmp_left_name_2 = par_src;

    tmp_right_name_1 = var_mask;

    if ( tmp_right_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10492 ], 50, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 260;
        goto try_finally_handler_2;
    }

    tmp_left_name_1 = BINARY_OPERATION( PyNumber_And, tmp_left_name_2, tmp_right_name_1 );
    if ( tmp_left_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 260;
        goto try_finally_handler_2;
    }
    tmp_right_name_2 = var_n;

    if ( tmp_right_name_2 == NULL )
    {
        Py_DECREF( tmp_left_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10542 ], 47, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 260;
        goto try_finally_handler_2;
    }

    tmp_args_element_name_3 = BINARY_OPERATION( PyNumber_Rshift, tmp_left_name_1, tmp_right_name_2 );
    Py_DECREF( tmp_left_name_1 );
    if ( tmp_args_element_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 260;
        goto try_finally_handler_2;
    }
    frame_function->f_lineno = 260;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 );
    Py_DECREF( tmp_args_element_name_3 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 260;
        goto try_finally_handler_2;
    }
    Py_DECREF( tmp_unused );
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 259;
        goto try_finally_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_2 = frame_function->f_lineno;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    frame_function->f_lineno = tmp_tried_lineno_2;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_2 != NULL )
    {
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;

        goto frame_exception_exit_1;
    }

    goto finally_end_2;
    finally_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var_n != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_n,
            var_n
        );
        assert( tmp_res != -1 );

    }
    if ( var_mask != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_mask,
            var_mask
        );
        assert( tmp_res != -1 );

    }
    if ( var_attr != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_attr,
            var_attr
        );
        assert( tmp_res != -1 );

    }
    if ( par_tgt_obj != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_tgt_obj,
            par_tgt_obj
        );
        assert( tmp_res != -1 );

    }
    if ( par_src != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_src,
            par_src
        );
        assert( tmp_res != -1 );

    }
    if ( par_manifest != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_manifest,
            par_manifest
        );
        assert( tmp_res != -1 );

    }
    if ( par_local_setattr != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_local_setattr,
            par_local_setattr
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_finally_handler_start_1;
    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_tgt_obj );
    Py_DECREF( par_tgt_obj );
    par_tgt_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_src );
    Py_DECREF( par_src );
    par_src = NULL;

    CHECK_OBJECT( (PyObject *)par_manifest );
    Py_DECREF( par_manifest );
    par_manifest = NULL;

    CHECK_OBJECT( (PyObject *)par_local_setattr );
    Py_DECREF( par_local_setattr );
    par_local_setattr = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    Py_XDECREF( var_mask );
    var_mask = NULL;

    Py_XDECREF( var_attr );
    var_attr = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_3 != NULL )
    {
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_3;
    finally_end_3:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_4_upkbits_of_xlrd$biffh );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_4_upkbits_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_tgt_obj = NULL;
    PyObject *_python_par_src = NULL;
    PyObject *_python_par_manifest = NULL;
    PyObject *_python_par_local_setattr = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "upkbits() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_tgt_obj == key )
            {
                assert( _python_par_tgt_obj == NULL );
                _python_par_tgt_obj = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_src == key )
            {
                assert( _python_par_src == NULL );
                _python_par_src = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_manifest == key )
            {
                assert( _python_par_manifest == NULL );
                _python_par_manifest = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_local_setattr == key )
            {
                assert( _python_par_local_setattr == NULL );
                _python_par_local_setattr = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_tgt_obj, key ) == 1 )
            {
                assert( _python_par_tgt_obj == NULL );
                _python_par_tgt_obj = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_src, key ) == 1 )
            {
                assert( _python_par_src == NULL );
                _python_par_src = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_manifest, key ) == 1 )
            {
                assert( _python_par_manifest == NULL );
                _python_par_manifest = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_local_setattr, key ) == 1 )
            {
                assert( _python_par_local_setattr == NULL );
                _python_par_local_setattr = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "upkbits() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 4 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_tgt_obj != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_tgt_obj = args[ 0 ];
        Py_INCREF( _python_par_tgt_obj );
    }
    else if ( _python_par_tgt_obj == NULL )
    {
        if ( 0 + self->m_defaults_given >= 4  )
        {
            _python_par_tgt_obj = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 4 );
            Py_INCREF( _python_par_tgt_obj );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 1 < args_given ))
    {
         if (unlikely( _python_par_src != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 1 );
             goto error_exit;
         }

        _python_par_src = args[ 1 ];
        Py_INCREF( _python_par_src );
    }
    else if ( _python_par_src == NULL )
    {
        if ( 1 + self->m_defaults_given >= 4  )
        {
            _python_par_src = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 1 - 4 );
            Py_INCREF( _python_par_src );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 2 < args_given ))
    {
         if (unlikely( _python_par_manifest != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 2 );
             goto error_exit;
         }

        _python_par_manifest = args[ 2 ];
        Py_INCREF( _python_par_manifest );
    }
    else if ( _python_par_manifest == NULL )
    {
        if ( 2 + self->m_defaults_given >= 4  )
        {
            _python_par_manifest = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 2 - 4 );
            Py_INCREF( _python_par_manifest );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 3 < args_given ))
    {
         if (unlikely( _python_par_local_setattr != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 3 );
             goto error_exit;
         }

        _python_par_local_setattr = args[ 3 ];
        Py_INCREF( _python_par_local_setattr );
    }
    else if ( _python_par_local_setattr == NULL )
    {
        if ( 3 + self->m_defaults_given >= 4  )
        {
            _python_par_local_setattr = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 3 - 4 );
            Py_INCREF( _python_par_local_setattr );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_tgt_obj == NULL || _python_par_src == NULL || _python_par_manifest == NULL || _python_par_local_setattr == NULL ))
    {
        PyObject *values[] = { _python_par_tgt_obj, _python_par_src, _python_par_manifest, _python_par_local_setattr };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_4_upkbits_of_xlrd$biffh( self, _python_par_tgt_obj, _python_par_src, _python_par_manifest, _python_par_local_setattr );

error_exit:;

    Py_XDECREF( _python_par_tgt_obj );
    Py_XDECREF( _python_par_src );
    Py_XDECREF( _python_par_manifest );
    Py_XDECREF( _python_par_local_setattr );

    return NULL;
}

static PyObject *dparse_function_4_upkbits_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 4 )
    {
        return impl_function_4_upkbits_of_xlrd$biffh( self, INCREASE_REFCOUNT( args[ 0 ] ), INCREASE_REFCOUNT( args[ 1 ] ), INCREASE_REFCOUNT( args[ 2 ] ), INCREASE_REFCOUNT( args[ 3 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_4_upkbits_of_xlrd$biffh( self, args, size, NULL );
        return result;
    }

}



static PyObject *impl_function_5_upkbitsL_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject *_python_par_tgt_obj, PyObject *_python_par_src, PyObject *_python_par_manifest, PyObject *_python_par_local_setattr, PyObject *_python_par_local_int )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_tgt_obj = _python_par_tgt_obj;
    PyObject *par_src = _python_par_src;
    PyObject *par_manifest = _python_par_manifest;
    PyObject *par_local_setattr = _python_par_local_setattr;
    PyObject *par_local_int = _python_par_local_int;
    PyObject *var_n = NULL;
    PyObject *var_mask = NULL;
    PyObject *var_attr = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_frame_locals;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iter_arg_2;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_next_source_1;
    int tmp_res;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    int tmp_tried_lineno_1;
    int tmp_tried_lineno_2;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    PyObject *tmp_unpack_3;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_3e6beaad15eb3e64b04a7b643fa43b3a, module_xlrd$biffh );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_iter_arg_1 = par_manifest;

    tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 263;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_1;

    // Tried code
    loop_start_1:;
    tmp_next_source_1 = tmp_for_loop_1__for_iterator;

    tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_1;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 263;
            goto try_finally_handler_2;
        }
    }

    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_2;
        Py_XDECREF( old );
    }

    // Tried code
    tmp_iter_arg_2 = tmp_for_loop_1__iter_value;

    tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_2 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 263;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__source_iter;
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_3;
        Py_XDECREF( old );
    }

    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_1, 0 );
    if ( tmp_assign_source_4 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 263;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__element_1;
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_4;
        Py_XDECREF( old );
    }

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_2, 1 );
    if ( tmp_assign_source_5 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 263;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__element_2;
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_5;
        Py_XDECREF( old );
    }

    tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_3, 2 );
    if ( tmp_assign_source_6 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 263;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__element_3;
        tmp_tuple_unpack_1__element_3 = tmp_assign_source_6;
        Py_XDECREF( old );
    }

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                goto try_finally_handler_3;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        goto try_finally_handler_3;
    }
    tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;

    {
        PyObject *old = var_n;
        var_n = tmp_assign_source_7;
        Py_INCREF( var_n );
        Py_XDECREF( old );
    }

    tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;

    {
        PyObject *old = var_mask;
        var_mask = tmp_assign_source_8;
        Py_INCREF( var_mask );
        Py_XDECREF( old );
    }

    tmp_assign_source_9 = tmp_tuple_unpack_1__element_3;

    {
        PyObject *old = var_attr;
        var_attr = tmp_assign_source_9;
        Py_INCREF( var_attr );
        Py_XDECREF( old );
    }

    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_1 = frame_function->f_lineno;
    Py_XDECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_1;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto try_finally_handler_2;
    }

    goto finally_end_1;
    finally_end_1:;
    tmp_called_name_1 = par_local_setattr;

    tmp_args_element_name_1 = par_tgt_obj;

    tmp_args_element_name_2 = var_attr;

    if ( tmp_args_element_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10360 ], 50, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 264;
        goto try_finally_handler_2;
    }

    tmp_called_name_2 = par_local_int;

    tmp_left_name_2 = par_src;

    tmp_right_name_1 = var_mask;

    if ( tmp_right_name_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10492 ], 50, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 264;
        goto try_finally_handler_2;
    }

    tmp_left_name_1 = BINARY_OPERATION( PyNumber_And, tmp_left_name_2, tmp_right_name_1 );
    if ( tmp_left_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 264;
        goto try_finally_handler_2;
    }
    tmp_right_name_2 = var_n;

    if ( tmp_right_name_2 == NULL )
    {
        Py_DECREF( tmp_left_name_1 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10542 ], 47, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 264;
        goto try_finally_handler_2;
    }

    tmp_args_element_name_4 = BINARY_OPERATION( PyNumber_Rshift, tmp_left_name_1, tmp_right_name_2 );
    Py_DECREF( tmp_left_name_1 );
    if ( tmp_args_element_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 264;
        goto try_finally_handler_2;
    }
    frame_function->f_lineno = 264;
    tmp_args_element_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, tmp_args_element_name_4 );
    Py_DECREF( tmp_args_element_name_4 );
    if ( tmp_args_element_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 264;
        goto try_finally_handler_2;
    }
    frame_function->f_lineno = 264;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 );
    Py_DECREF( tmp_args_element_name_3 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 264;
        goto try_finally_handler_2;
    }
    Py_DECREF( tmp_unused );
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 263;
        goto try_finally_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_2 = frame_function->f_lineno;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    frame_function->f_lineno = tmp_tried_lineno_2;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_2 != NULL )
    {
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;

        goto frame_exception_exit_1;
    }

    goto finally_end_2;
    finally_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var_n != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_n,
            var_n
        );
        assert( tmp_res != -1 );

    }
    if ( var_mask != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_mask,
            var_mask
        );
        assert( tmp_res != -1 );

    }
    if ( var_attr != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_attr,
            var_attr
        );
        assert( tmp_res != -1 );

    }
    if ( par_tgt_obj != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_tgt_obj,
            par_tgt_obj
        );
        assert( tmp_res != -1 );

    }
    if ( par_src != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_src,
            par_src
        );
        assert( tmp_res != -1 );

    }
    if ( par_manifest != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_manifest,
            par_manifest
        );
        assert( tmp_res != -1 );

    }
    if ( par_local_setattr != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_local_setattr,
            par_local_setattr
        );
        assert( tmp_res != -1 );

    }
    if ( par_local_int != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_local_int,
            par_local_int
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_finally_handler_start_1;
    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_tgt_obj );
    Py_DECREF( par_tgt_obj );
    par_tgt_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_src );
    Py_DECREF( par_src );
    par_src = NULL;

    CHECK_OBJECT( (PyObject *)par_manifest );
    Py_DECREF( par_manifest );
    par_manifest = NULL;

    CHECK_OBJECT( (PyObject *)par_local_setattr );
    Py_DECREF( par_local_setattr );
    par_local_setattr = NULL;

    CHECK_OBJECT( (PyObject *)par_local_int );
    Py_DECREF( par_local_int );
    par_local_int = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    Py_XDECREF( var_mask );
    var_mask = NULL;

    Py_XDECREF( var_attr );
    var_attr = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_3 != NULL )
    {
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_3;
    finally_end_3:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_5_upkbitsL_of_xlrd$biffh );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_5_upkbitsL_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_tgt_obj = NULL;
    PyObject *_python_par_src = NULL;
    PyObject *_python_par_manifest = NULL;
    PyObject *_python_par_local_setattr = NULL;
    PyObject *_python_par_local_int = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "upkbitsL() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_tgt_obj == key )
            {
                assert( _python_par_tgt_obj == NULL );
                _python_par_tgt_obj = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_src == key )
            {
                assert( _python_par_src == NULL );
                _python_par_src = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_manifest == key )
            {
                assert( _python_par_manifest == NULL );
                _python_par_manifest = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_local_setattr == key )
            {
                assert( _python_par_local_setattr == NULL );
                _python_par_local_setattr = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_local_int == key )
            {
                assert( _python_par_local_int == NULL );
                _python_par_local_int = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_tgt_obj, key ) == 1 )
            {
                assert( _python_par_tgt_obj == NULL );
                _python_par_tgt_obj = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_src, key ) == 1 )
            {
                assert( _python_par_src == NULL );
                _python_par_src = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_manifest, key ) == 1 )
            {
                assert( _python_par_manifest == NULL );
                _python_par_manifest = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_local_setattr, key ) == 1 )
            {
                assert( _python_par_local_setattr == NULL );
                _python_par_local_setattr = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_local_int, key ) == 1 )
            {
                assert( _python_par_local_int == NULL );
                _python_par_local_int = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "upkbitsL() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 5 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_tgt_obj != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_tgt_obj = args[ 0 ];
        Py_INCREF( _python_par_tgt_obj );
    }
    else if ( _python_par_tgt_obj == NULL )
    {
        if ( 0 + self->m_defaults_given >= 5  )
        {
            _python_par_tgt_obj = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 5 );
            Py_INCREF( _python_par_tgt_obj );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 1 < args_given ))
    {
         if (unlikely( _python_par_src != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 1 );
             goto error_exit;
         }

        _python_par_src = args[ 1 ];
        Py_INCREF( _python_par_src );
    }
    else if ( _python_par_src == NULL )
    {
        if ( 1 + self->m_defaults_given >= 5  )
        {
            _python_par_src = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 1 - 5 );
            Py_INCREF( _python_par_src );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 2 < args_given ))
    {
         if (unlikely( _python_par_manifest != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 2 );
             goto error_exit;
         }

        _python_par_manifest = args[ 2 ];
        Py_INCREF( _python_par_manifest );
    }
    else if ( _python_par_manifest == NULL )
    {
        if ( 2 + self->m_defaults_given >= 5  )
        {
            _python_par_manifest = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 2 - 5 );
            Py_INCREF( _python_par_manifest );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 3 < args_given ))
    {
         if (unlikely( _python_par_local_setattr != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 3 );
             goto error_exit;
         }

        _python_par_local_setattr = args[ 3 ];
        Py_INCREF( _python_par_local_setattr );
    }
    else if ( _python_par_local_setattr == NULL )
    {
        if ( 3 + self->m_defaults_given >= 5  )
        {
            _python_par_local_setattr = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 3 - 5 );
            Py_INCREF( _python_par_local_setattr );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 4 < args_given ))
    {
         if (unlikely( _python_par_local_int != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 4 );
             goto error_exit;
         }

        _python_par_local_int = args[ 4 ];
        Py_INCREF( _python_par_local_int );
    }
    else if ( _python_par_local_int == NULL )
    {
        if ( 4 + self->m_defaults_given >= 5  )
        {
            _python_par_local_int = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 4 - 5 );
            Py_INCREF( _python_par_local_int );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_tgt_obj == NULL || _python_par_src == NULL || _python_par_manifest == NULL || _python_par_local_setattr == NULL || _python_par_local_int == NULL ))
    {
        PyObject *values[] = { _python_par_tgt_obj, _python_par_src, _python_par_manifest, _python_par_local_setattr, _python_par_local_int };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_5_upkbitsL_of_xlrd$biffh( self, _python_par_tgt_obj, _python_par_src, _python_par_manifest, _python_par_local_setattr, _python_par_local_int );

error_exit:;

    Py_XDECREF( _python_par_tgt_obj );
    Py_XDECREF( _python_par_src );
    Py_XDECREF( _python_par_manifest );
    Py_XDECREF( _python_par_local_setattr );
    Py_XDECREF( _python_par_local_int );

    return NULL;
}

static PyObject *dparse_function_5_upkbitsL_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 5 )
    {
        return impl_function_5_upkbitsL_of_xlrd$biffh( self, INCREASE_REFCOUNT( args[ 0 ] ), INCREASE_REFCOUNT( args[ 1 ] ), INCREASE_REFCOUNT( args[ 2 ] ), INCREASE_REFCOUNT( args[ 3 ] ), INCREASE_REFCOUNT( args[ 4 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_5_upkbitsL_of_xlrd$biffh( self, args, size, NULL );
        return result;
    }

}



static PyObject *impl_function_6_unpack_string_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject *_python_par_data, PyObject *_python_par_pos, PyObject *_python_par_encoding, PyObject *_python_par_lenlen )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_data = _python_par_data;
    PyObject *par_pos = _python_par_pos;
    PyObject *par_encoding = _python_par_encoding;
    PyObject *par_lenlen = _python_par_lenlen;
    PyObject *var_nchars = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_called_name_1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_left_name_4;
    PyObject *tmp_left_name_5;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_right_name_4;
    PyObject *tmp_right_name_5;
    PyObject *tmp_slice_lower_1;
    PyObject *tmp_slice_lower_2;
    PyObject *tmp_slice_source_1;
    PyObject *tmp_slice_source_2;
    PyObject *tmp_slice_upper_1;
    PyObject *tmp_slice_upper_2;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscribed_name_2;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_subscript_name_2;
    PyObject *tmp_unicode_arg_1;
    PyObject *tmp_unicode_encoding_1;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_1388f9aeddaf16831e766a08cb3c6c0f, module_xlrd$biffh );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unpack );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10589 ], 35, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 267;
        goto frame_exception_exit_1;
    }

    tmp_left_name_1 = const_str_chr_60;
    tmp_subscribed_name_2 = const_str_plain_BH;
    tmp_left_name_2 = par_lenlen;

    tmp_right_name_2 = const_int_pos_1;
    tmp_subscript_name_1 = BINARY_OPERATION_SUB( tmp_left_name_2, tmp_right_name_2 );
    if ( tmp_subscript_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 267;
        goto frame_exception_exit_1;
    }
    tmp_right_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_1 );
    Py_DECREF( tmp_subscript_name_1 );
    if ( tmp_right_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 267;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_1 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_right_name_1 );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 267;
        goto frame_exception_exit_1;
    }
    tmp_slice_source_1 = par_data;

    tmp_slice_lower_1 = par_pos;

    tmp_left_name_3 = par_pos;

    tmp_right_name_3 = par_lenlen;

    tmp_slice_upper_1 = BINARY_OPERATION_ADD( tmp_left_name_3, tmp_right_name_3 );
    if ( tmp_slice_upper_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_element_name_1 );

        frame_function->f_lineno = 267;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_2 = LOOKUP_SLICE( tmp_slice_source_1, tmp_slice_lower_1, tmp_slice_upper_1 );
    Py_DECREF( tmp_slice_upper_1 );
    if ( tmp_args_element_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_element_name_1 );

        frame_function->f_lineno = 267;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 267;
    tmp_subscribed_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, tmp_args_element_name_1, tmp_args_element_name_2 );
    Py_DECREF( tmp_args_element_name_1 );
    Py_DECREF( tmp_args_element_name_2 );
    if ( tmp_subscribed_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 267;
        goto frame_exception_exit_1;
    }
    tmp_subscript_name_2 = const_int_0;
    tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_2 );
    Py_DECREF( tmp_subscribed_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 267;
        goto frame_exception_exit_1;
    }
    assert( var_nchars == NULL );
    var_nchars = tmp_assign_source_1;

    tmp_left_name_4 = par_pos;

    tmp_right_name_4 = par_lenlen;

    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_4, tmp_right_name_4 );
    tmp_assign_source_2 = tmp_left_name_4;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 268;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_2;

    tmp_slice_source_2 = par_data;

    tmp_slice_lower_2 = par_pos;

    tmp_left_name_5 = par_pos;

    tmp_right_name_5 = var_nchars;

    tmp_slice_upper_2 = BINARY_OPERATION_ADD( tmp_left_name_5, tmp_right_name_5 );
    if ( tmp_slice_upper_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 269;
        goto frame_exception_exit_1;
    }
    tmp_unicode_arg_1 = LOOKUP_SLICE( tmp_slice_source_2, tmp_slice_lower_2, tmp_slice_upper_2 );
    Py_DECREF( tmp_slice_upper_2 );
    if ( tmp_unicode_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 269;
        goto frame_exception_exit_1;
    }
    tmp_unicode_encoding_1 = par_encoding;

    tmp_return_value = TO_UNICODE3( tmp_unicode_arg_1, tmp_unicode_encoding_1, NULL );
    Py_DECREF( tmp_unicode_arg_1 );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 269;
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto try_finally_handler_start_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var_nchars != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_nchars,
            var_nchars
        );
        assert( tmp_res != -1 );

    }
    if ( par_data != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_data,
            par_data
        );
        assert( tmp_res != -1 );

    }
    if ( par_pos != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_pos,
            par_pos
        );
        assert( tmp_res != -1 );

    }
    if ( par_encoding != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_encoding,
            par_encoding
        );
        assert( tmp_res != -1 );

    }
    if ( par_lenlen != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_lenlen,
            par_lenlen
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;

    Py_XDECREF( par_pos );
    par_pos = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_lenlen );
    Py_DECREF( par_lenlen );
    par_lenlen = NULL;

    Py_XDECREF( var_nchars );
    var_nchars = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_1;
    finally_end_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_6_unpack_string_of_xlrd$biffh );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_6_unpack_string_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_data = NULL;
    PyObject *_python_par_pos = NULL;
    PyObject *_python_par_encoding = NULL;
    PyObject *_python_par_lenlen = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "unpack_string() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_data == key )
            {
                assert( _python_par_data == NULL );
                _python_par_data = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_pos == key )
            {
                assert( _python_par_pos == NULL );
                _python_par_pos = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_encoding == key )
            {
                assert( _python_par_encoding == NULL );
                _python_par_encoding = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_lenlen == key )
            {
                assert( _python_par_lenlen == NULL );
                _python_par_lenlen = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_data, key ) == 1 )
            {
                assert( _python_par_data == NULL );
                _python_par_data = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_pos, key ) == 1 )
            {
                assert( _python_par_pos == NULL );
                _python_par_pos = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_encoding, key ) == 1 )
            {
                assert( _python_par_encoding == NULL );
                _python_par_encoding = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_lenlen, key ) == 1 )
            {
                assert( _python_par_lenlen == NULL );
                _python_par_lenlen = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "unpack_string() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 4 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_data != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_data = args[ 0 ];
        Py_INCREF( _python_par_data );
    }
    else if ( _python_par_data == NULL )
    {
        if ( 0 + self->m_defaults_given >= 4  )
        {
            _python_par_data = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 4 );
            Py_INCREF( _python_par_data );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 1 < args_given ))
    {
         if (unlikely( _python_par_pos != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 1 );
             goto error_exit;
         }

        _python_par_pos = args[ 1 ];
        Py_INCREF( _python_par_pos );
    }
    else if ( _python_par_pos == NULL )
    {
        if ( 1 + self->m_defaults_given >= 4  )
        {
            _python_par_pos = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 1 - 4 );
            Py_INCREF( _python_par_pos );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 2 < args_given ))
    {
         if (unlikely( _python_par_encoding != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 2 );
             goto error_exit;
         }

        _python_par_encoding = args[ 2 ];
        Py_INCREF( _python_par_encoding );
    }
    else if ( _python_par_encoding == NULL )
    {
        if ( 2 + self->m_defaults_given >= 4  )
        {
            _python_par_encoding = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 2 - 4 );
            Py_INCREF( _python_par_encoding );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 3 < args_given ))
    {
         if (unlikely( _python_par_lenlen != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 3 );
             goto error_exit;
         }

        _python_par_lenlen = args[ 3 ];
        Py_INCREF( _python_par_lenlen );
    }
    else if ( _python_par_lenlen == NULL )
    {
        if ( 3 + self->m_defaults_given >= 4  )
        {
            _python_par_lenlen = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 3 - 4 );
            Py_INCREF( _python_par_lenlen );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_data == NULL || _python_par_pos == NULL || _python_par_encoding == NULL || _python_par_lenlen == NULL ))
    {
        PyObject *values[] = { _python_par_data, _python_par_pos, _python_par_encoding, _python_par_lenlen };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_6_unpack_string_of_xlrd$biffh( self, _python_par_data, _python_par_pos, _python_par_encoding, _python_par_lenlen );

error_exit:;

    Py_XDECREF( _python_par_data );
    Py_XDECREF( _python_par_pos );
    Py_XDECREF( _python_par_encoding );
    Py_XDECREF( _python_par_lenlen );

    return NULL;
}

static PyObject *dparse_function_6_unpack_string_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 4 )
    {
        return impl_function_6_unpack_string_of_xlrd$biffh( self, INCREASE_REFCOUNT( args[ 0 ] ), INCREASE_REFCOUNT( args[ 1 ] ), INCREASE_REFCOUNT( args[ 2 ] ), INCREASE_REFCOUNT( args[ 3 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_6_unpack_string_of_xlrd$biffh( self, args, size, NULL );
        return result;
    }

}



static PyObject *impl_function_7_unpack_string_update_pos_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject *_python_par_data, PyObject *_python_par_pos, PyObject *_python_par_encoding, PyObject *_python_par_lenlen, PyObject *_python_par_known_len )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_data = _python_par_data;
    PyObject *par_pos = _python_par_pos;
    PyObject *par_encoding = _python_par_encoding;
    PyObject *par_lenlen = _python_par_lenlen;
    PyObject *par_known_len = _python_par_known_len;
    PyObject *var_nchars = NULL;
    PyObject *var_newpos = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_called_name_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_frame_locals;
    bool tmp_isnot_1;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_left_name_4;
    PyObject *tmp_left_name_5;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_right_name_4;
    PyObject *tmp_right_name_5;
    PyObject *tmp_slice_lower_1;
    PyObject *tmp_slice_lower_2;
    PyObject *tmp_slice_source_1;
    PyObject *tmp_slice_source_2;
    PyObject *tmp_slice_upper_1;
    PyObject *tmp_slice_upper_2;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscribed_name_2;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_subscript_name_2;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_unicode_arg_1;
    PyObject *tmp_unicode_encoding_1;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_94aeeee88a55179d7df58ca47aff0221, module_xlrd$biffh );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_compare_left_1 = par_known_len;

    tmp_compare_right_1 = Py_None;
    tmp_isnot_1 = ( tmp_compare_left_1 != tmp_compare_right_1 );
    if (tmp_isnot_1)
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_assign_source_1 = par_known_len;

    assert( var_nchars == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var_nchars = tmp_assign_source_1;

    goto branch_end_1;
    branch_no_1:;
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unpack );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10589 ], 35, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 276;
        goto frame_exception_exit_1;
    }

    tmp_left_name_1 = const_str_chr_60;
    tmp_subscribed_name_2 = const_str_plain_BH;
    tmp_left_name_2 = par_lenlen;

    tmp_right_name_2 = const_int_pos_1;
    tmp_subscript_name_1 = BINARY_OPERATION_SUB( tmp_left_name_2, tmp_right_name_2 );
    if ( tmp_subscript_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 276;
        goto frame_exception_exit_1;
    }
    tmp_right_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_1 );
    Py_DECREF( tmp_subscript_name_1 );
    if ( tmp_right_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 276;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_1 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_right_name_1 );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 276;
        goto frame_exception_exit_1;
    }
    tmp_slice_source_1 = par_data;

    tmp_slice_lower_1 = par_pos;

    tmp_left_name_3 = par_pos;

    tmp_right_name_3 = par_lenlen;

    tmp_slice_upper_1 = BINARY_OPERATION_ADD( tmp_left_name_3, tmp_right_name_3 );
    if ( tmp_slice_upper_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_element_name_1 );

        frame_function->f_lineno = 276;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_2 = LOOKUP_SLICE( tmp_slice_source_1, tmp_slice_lower_1, tmp_slice_upper_1 );
    Py_DECREF( tmp_slice_upper_1 );
    if ( tmp_args_element_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_element_name_1 );

        frame_function->f_lineno = 276;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 276;
    tmp_subscribed_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, tmp_args_element_name_1, tmp_args_element_name_2 );
    Py_DECREF( tmp_args_element_name_1 );
    Py_DECREF( tmp_args_element_name_2 );
    if ( tmp_subscribed_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 276;
        goto frame_exception_exit_1;
    }
    tmp_subscript_name_2 = const_int_0;
    tmp_assign_source_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_2 );
    Py_DECREF( tmp_subscribed_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 276;
        goto frame_exception_exit_1;
    }
    assert( var_nchars == NULL );
    var_nchars = tmp_assign_source_2;

    tmp_left_name_4 = par_pos;

    tmp_right_name_4 = par_lenlen;

    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_4, tmp_right_name_4 );
    tmp_assign_source_3 = tmp_left_name_4;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 277;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_3;

    branch_end_1:;
    tmp_left_name_5 = par_pos;

    tmp_right_name_5 = var_nchars;

    tmp_assign_source_4 = BINARY_OPERATION_ADD( tmp_left_name_5, tmp_right_name_5 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 278;
        goto frame_exception_exit_1;
    }
    assert( var_newpos == NULL );
    var_newpos = tmp_assign_source_4;

    tmp_return_value = PyTuple_New( 2 );
    tmp_slice_source_2 = par_data;

    tmp_slice_lower_2 = par_pos;

    tmp_slice_upper_2 = var_newpos;

    tmp_unicode_arg_1 = LOOKUP_SLICE( tmp_slice_source_2, tmp_slice_lower_2, tmp_slice_upper_2 );
    if ( tmp_unicode_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );

        frame_function->f_lineno = 279;
        goto frame_exception_exit_1;
    }
    tmp_unicode_encoding_1 = par_encoding;

    tmp_tuple_element_1 = TO_UNICODE3( tmp_unicode_arg_1, tmp_unicode_encoding_1, NULL );
    Py_DECREF( tmp_unicode_arg_1 );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );

        frame_function->f_lineno = 279;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = var_newpos;

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto try_finally_handler_start_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var_nchars != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_nchars,
            var_nchars
        );
        assert( tmp_res != -1 );

    }
    if ( var_newpos != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_newpos,
            var_newpos
        );
        assert( tmp_res != -1 );

    }
    if ( par_data != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_data,
            par_data
        );
        assert( tmp_res != -1 );

    }
    if ( par_pos != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_pos,
            par_pos
        );
        assert( tmp_res != -1 );

    }
    if ( par_encoding != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_encoding,
            par_encoding
        );
        assert( tmp_res != -1 );

    }
    if ( par_lenlen != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_lenlen,
            par_lenlen
        );
        assert( tmp_res != -1 );

    }
    if ( par_known_len != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_known_len,
            par_known_len
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;

    Py_XDECREF( par_pos );
    par_pos = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_lenlen );
    Py_DECREF( par_lenlen );
    par_lenlen = NULL;

    CHECK_OBJECT( (PyObject *)par_known_len );
    Py_DECREF( par_known_len );
    par_known_len = NULL;

    Py_XDECREF( var_nchars );
    var_nchars = NULL;

    Py_XDECREF( var_newpos );
    var_newpos = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_1;
    finally_end_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_7_unpack_string_update_pos_of_xlrd$biffh );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_7_unpack_string_update_pos_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_data = NULL;
    PyObject *_python_par_pos = NULL;
    PyObject *_python_par_encoding = NULL;
    PyObject *_python_par_lenlen = NULL;
    PyObject *_python_par_known_len = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "unpack_string_update_pos() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_data == key )
            {
                assert( _python_par_data == NULL );
                _python_par_data = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_pos == key )
            {
                assert( _python_par_pos == NULL );
                _python_par_pos = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_encoding == key )
            {
                assert( _python_par_encoding == NULL );
                _python_par_encoding = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_lenlen == key )
            {
                assert( _python_par_lenlen == NULL );
                _python_par_lenlen = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_known_len == key )
            {
                assert( _python_par_known_len == NULL );
                _python_par_known_len = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_data, key ) == 1 )
            {
                assert( _python_par_data == NULL );
                _python_par_data = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_pos, key ) == 1 )
            {
                assert( _python_par_pos == NULL );
                _python_par_pos = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_encoding, key ) == 1 )
            {
                assert( _python_par_encoding == NULL );
                _python_par_encoding = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_lenlen, key ) == 1 )
            {
                assert( _python_par_lenlen == NULL );
                _python_par_lenlen = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_known_len, key ) == 1 )
            {
                assert( _python_par_known_len == NULL );
                _python_par_known_len = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "unpack_string_update_pos() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 5 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_data != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_data = args[ 0 ];
        Py_INCREF( _python_par_data );
    }
    else if ( _python_par_data == NULL )
    {
        if ( 0 + self->m_defaults_given >= 5  )
        {
            _python_par_data = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 5 );
            Py_INCREF( _python_par_data );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 1 < args_given ))
    {
         if (unlikely( _python_par_pos != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 1 );
             goto error_exit;
         }

        _python_par_pos = args[ 1 ];
        Py_INCREF( _python_par_pos );
    }
    else if ( _python_par_pos == NULL )
    {
        if ( 1 + self->m_defaults_given >= 5  )
        {
            _python_par_pos = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 1 - 5 );
            Py_INCREF( _python_par_pos );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 2 < args_given ))
    {
         if (unlikely( _python_par_encoding != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 2 );
             goto error_exit;
         }

        _python_par_encoding = args[ 2 ];
        Py_INCREF( _python_par_encoding );
    }
    else if ( _python_par_encoding == NULL )
    {
        if ( 2 + self->m_defaults_given >= 5  )
        {
            _python_par_encoding = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 2 - 5 );
            Py_INCREF( _python_par_encoding );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 3 < args_given ))
    {
         if (unlikely( _python_par_lenlen != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 3 );
             goto error_exit;
         }

        _python_par_lenlen = args[ 3 ];
        Py_INCREF( _python_par_lenlen );
    }
    else if ( _python_par_lenlen == NULL )
    {
        if ( 3 + self->m_defaults_given >= 5  )
        {
            _python_par_lenlen = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 3 - 5 );
            Py_INCREF( _python_par_lenlen );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 4 < args_given ))
    {
         if (unlikely( _python_par_known_len != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 4 );
             goto error_exit;
         }

        _python_par_known_len = args[ 4 ];
        Py_INCREF( _python_par_known_len );
    }
    else if ( _python_par_known_len == NULL )
    {
        if ( 4 + self->m_defaults_given >= 5  )
        {
            _python_par_known_len = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 4 - 5 );
            Py_INCREF( _python_par_known_len );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_data == NULL || _python_par_pos == NULL || _python_par_encoding == NULL || _python_par_lenlen == NULL || _python_par_known_len == NULL ))
    {
        PyObject *values[] = { _python_par_data, _python_par_pos, _python_par_encoding, _python_par_lenlen, _python_par_known_len };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_7_unpack_string_update_pos_of_xlrd$biffh( self, _python_par_data, _python_par_pos, _python_par_encoding, _python_par_lenlen, _python_par_known_len );

error_exit:;

    Py_XDECREF( _python_par_data );
    Py_XDECREF( _python_par_pos );
    Py_XDECREF( _python_par_encoding );
    Py_XDECREF( _python_par_lenlen );
    Py_XDECREF( _python_par_known_len );

    return NULL;
}

static PyObject *dparse_function_7_unpack_string_update_pos_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 5 )
    {
        return impl_function_7_unpack_string_update_pos_of_xlrd$biffh( self, INCREASE_REFCOUNT( args[ 0 ] ), INCREASE_REFCOUNT( args[ 1 ] ), INCREASE_REFCOUNT( args[ 2 ] ), INCREASE_REFCOUNT( args[ 3 ] ), INCREASE_REFCOUNT( args[ 4 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_7_unpack_string_update_pos_of_xlrd$biffh( self, args, size, NULL );
        return result;
    }

}



static PyObject *impl_function_8_unpack_unicode_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject *_python_par_data, PyObject *_python_par_pos, PyObject *_python_par_lenlen )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_data = _python_par_data;
    PyObject *par_pos = _python_par_pos;
    PyObject *par_lenlen = _python_par_lenlen;
    PyObject *var_nchars = NULL;
    PyObject *var_options = NULL;
    PyObject *var_rawstrg = NULL;
    PyObject *var_strg = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_call_arg_element_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    int tmp_cond_truth_3;
    int tmp_cond_truth_4;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_cond_value_3;
    PyObject *tmp_cond_value_4;
    PyObject *tmp_frame_locals;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_left_name_4;
    PyObject *tmp_left_name_5;
    PyObject *tmp_left_name_6;
    PyObject *tmp_left_name_7;
    PyObject *tmp_left_name_8;
    PyObject *tmp_left_name_9;
    PyObject *tmp_left_name_10;
    PyObject *tmp_left_name_11;
    PyObject *tmp_left_name_12;
    PyObject *tmp_left_name_13;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_right_name_4;
    PyObject *tmp_right_name_5;
    PyObject *tmp_right_name_6;
    PyObject *tmp_right_name_7;
    PyObject *tmp_right_name_8;
    PyObject *tmp_right_name_9;
    PyObject *tmp_right_name_10;
    PyObject *tmp_right_name_11;
    PyObject *tmp_right_name_12;
    PyObject *tmp_right_name_13;
    PyObject *tmp_slice_lower_1;
    PyObject *tmp_slice_lower_2;
    PyObject *tmp_slice_lower_3;
    PyObject *tmp_slice_source_1;
    PyObject *tmp_slice_source_2;
    PyObject *tmp_slice_source_3;
    PyObject *tmp_slice_upper_1;
    PyObject *tmp_slice_upper_2;
    PyObject *tmp_slice_upper_3;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscribed_name_2;
    PyObject *tmp_subscribed_name_3;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_subscript_name_2;
    PyObject *tmp_subscript_name_3;
    PyObject *tmp_unicode_arg_1;
    PyObject *tmp_unicode_arg_2;
    PyObject *tmp_unicode_encoding_1;
    PyObject *tmp_unicode_encoding_2;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_f9afca4691fec58985424a48940d50f4, module_xlrd$biffh );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unpack );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10589 ], 35, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 283;
        goto frame_exception_exit_1;
    }

    tmp_left_name_1 = const_str_chr_60;
    tmp_subscribed_name_2 = const_str_plain_BH;
    tmp_left_name_2 = par_lenlen;

    tmp_right_name_2 = const_int_pos_1;
    tmp_subscript_name_1 = BINARY_OPERATION_SUB( tmp_left_name_2, tmp_right_name_2 );
    if ( tmp_subscript_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 283;
        goto frame_exception_exit_1;
    }
    tmp_right_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_1 );
    Py_DECREF( tmp_subscript_name_1 );
    if ( tmp_right_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 283;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_1 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_right_name_1 );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 283;
        goto frame_exception_exit_1;
    }
    tmp_slice_source_1 = par_data;

    tmp_slice_lower_1 = par_pos;

    tmp_left_name_3 = par_pos;

    tmp_right_name_3 = par_lenlen;

    tmp_slice_upper_1 = BINARY_OPERATION_ADD( tmp_left_name_3, tmp_right_name_3 );
    if ( tmp_slice_upper_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_element_name_1 );

        frame_function->f_lineno = 283;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_2 = LOOKUP_SLICE( tmp_slice_source_1, tmp_slice_lower_1, tmp_slice_upper_1 );
    Py_DECREF( tmp_slice_upper_1 );
    if ( tmp_args_element_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_element_name_1 );

        frame_function->f_lineno = 283;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 283;
    tmp_subscribed_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, tmp_args_element_name_1, tmp_args_element_name_2 );
    Py_DECREF( tmp_args_element_name_1 );
    Py_DECREF( tmp_args_element_name_2 );
    if ( tmp_subscribed_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 283;
        goto frame_exception_exit_1;
    }
    tmp_subscript_name_2 = const_int_0;
    tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_2 );
    Py_DECREF( tmp_subscribed_name_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 283;
        goto frame_exception_exit_1;
    }
    assert( var_nchars == NULL );
    var_nchars = tmp_assign_source_1;

    tmp_cond_value_1 = var_nchars;

    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 284;
        goto frame_exception_exit_1;
    }
    if (tmp_cond_truth_1 == 1)
    {
        goto branch_no_1;
    }
    else
    {
        goto branch_yes_1;
    }
    branch_yes_1:;
    tmp_called_name_2 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_UNICODE_LITERAL );

    if (unlikely( tmp_called_name_2 == NULL ))
    {
        tmp_called_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UNICODE_LITERAL );
    }

    if ( tmp_called_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10624 ], 44, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 287;
        goto frame_exception_exit_1;
    }

    tmp_call_arg_element_1 = const_str_empty;
    frame_function->f_lineno = 287;
    tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, tmp_call_arg_element_1 );
    if ( tmp_return_value == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 287;
        goto frame_exception_exit_1;
    }
    goto frame_return_exit_1;
    branch_no_1:;
    tmp_left_name_4 = par_pos;

    tmp_right_name_4 = par_lenlen;

    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_4, tmp_right_name_4 );
    tmp_assign_source_2 = tmp_left_name_4;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 288;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_2;

    tmp_called_name_3 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_BYTES_ORD );

    if (unlikely( tmp_called_name_3 == NULL ))
    {
        tmp_called_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BYTES_ORD );
    }

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10668 ], 38, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 289;
        goto frame_exception_exit_1;
    }

    tmp_subscribed_name_3 = par_data;

    tmp_subscript_name_3 = par_pos;

    tmp_args_element_name_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
    if ( tmp_args_element_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 289;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 289;
    tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, tmp_args_element_name_3 );
    Py_DECREF( tmp_args_element_name_3 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 289;
        goto frame_exception_exit_1;
    }
    assert( var_options == NULL );
    var_options = tmp_assign_source_3;

    tmp_left_name_5 = par_pos;

    tmp_right_name_5 = const_int_pos_1;
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_5, tmp_right_name_5 );
    tmp_assign_source_4 = tmp_left_name_5;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 290;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_4;

    tmp_left_name_6 = var_options;

    tmp_right_name_6 = const_int_pos_8;
    tmp_cond_value_2 = BINARY_OPERATION( PyNumber_And, tmp_left_name_6, tmp_right_name_6 );
    if ( tmp_cond_value_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 293;
        goto frame_exception_exit_1;
    }
    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_2 );

        frame_function->f_lineno = 293;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_2 );
    if (tmp_cond_truth_2 == 1)
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_left_name_7 = par_pos;

    tmp_right_name_7 = const_int_pos_2;
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_7, tmp_right_name_7 );
    tmp_assign_source_5 = tmp_left_name_7;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 295;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_5;

    branch_no_2:;
    tmp_left_name_8 = var_options;

    tmp_right_name_8 = const_int_pos_4;
    tmp_cond_value_3 = BINARY_OPERATION( PyNumber_And, tmp_left_name_8, tmp_right_name_8 );
    if ( tmp_cond_value_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 296;
        goto frame_exception_exit_1;
    }
    tmp_cond_truth_3 = CHECK_IF_TRUE( tmp_cond_value_3 );
    if ( tmp_cond_truth_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_3 );

        frame_function->f_lineno = 296;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_3 );
    if (tmp_cond_truth_3 == 1)
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_left_name_9 = par_pos;

    tmp_right_name_9 = const_int_pos_4;
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_9, tmp_right_name_9 );
    tmp_assign_source_6 = tmp_left_name_9;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 298;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_6;

    branch_no_3:;
    tmp_left_name_10 = var_options;

    tmp_right_name_10 = const_int_pos_1;
    tmp_cond_value_4 = BINARY_OPERATION( PyNumber_And, tmp_left_name_10, tmp_right_name_10 );
    if ( tmp_cond_value_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 299;
        goto frame_exception_exit_1;
    }
    tmp_cond_truth_4 = CHECK_IF_TRUE( tmp_cond_value_4 );
    if ( tmp_cond_truth_4 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_4 );

        frame_function->f_lineno = 299;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_4 );
    if (tmp_cond_truth_4 == 1)
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_slice_source_2 = par_data;

    tmp_slice_lower_2 = par_pos;

    if ( tmp_slice_lower_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 301;
        goto frame_exception_exit_1;
    }

    tmp_left_name_11 = par_pos;

    if ( tmp_left_name_11 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 301;
        goto frame_exception_exit_1;
    }

    tmp_left_name_12 = const_int_pos_2;
    tmp_right_name_12 = var_nchars;

    tmp_right_name_11 = BINARY_OPERATION_MUL( tmp_left_name_12, tmp_right_name_12 );
    if ( tmp_right_name_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 301;
        goto frame_exception_exit_1;
    }
    tmp_slice_upper_2 = BINARY_OPERATION_ADD( tmp_left_name_11, tmp_right_name_11 );
    Py_DECREF( tmp_right_name_11 );
    if ( tmp_slice_upper_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 301;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_7 = LOOKUP_SLICE( tmp_slice_source_2, tmp_slice_lower_2, tmp_slice_upper_2 );
    Py_DECREF( tmp_slice_upper_2 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 301;
        goto frame_exception_exit_1;
    }
    assert( var_rawstrg == NULL );
    var_rawstrg = tmp_assign_source_7;

    tmp_unicode_arg_1 = var_rawstrg;

    tmp_unicode_encoding_1 = const_str_plain_utf_16_le;
    tmp_assign_source_8 = TO_UNICODE3( tmp_unicode_arg_1, tmp_unicode_encoding_1, NULL );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 303;
        goto frame_exception_exit_1;
    }
    assert( var_strg == NULL );
    var_strg = tmp_assign_source_8;

    goto branch_end_4;
    branch_no_4:;
    tmp_slice_source_3 = par_data;

    tmp_slice_lower_3 = par_pos;

    if ( tmp_slice_lower_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 311;
        goto frame_exception_exit_1;
    }

    tmp_left_name_13 = par_pos;

    if ( tmp_left_name_13 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 311;
        goto frame_exception_exit_1;
    }

    tmp_right_name_13 = var_nchars;

    tmp_slice_upper_3 = BINARY_OPERATION_ADD( tmp_left_name_13, tmp_right_name_13 );
    if ( tmp_slice_upper_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 311;
        goto frame_exception_exit_1;
    }
    tmp_unicode_arg_2 = LOOKUP_SLICE( tmp_slice_source_3, tmp_slice_lower_3, tmp_slice_upper_3 );
    Py_DECREF( tmp_slice_upper_3 );
    if ( tmp_unicode_arg_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 311;
        goto frame_exception_exit_1;
    }
    tmp_unicode_encoding_2 = const_str_plain_latin_1;
    tmp_assign_source_9 = TO_UNICODE3( tmp_unicode_arg_2, tmp_unicode_encoding_2, NULL );
    Py_DECREF( tmp_unicode_arg_2 );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 311;
        goto frame_exception_exit_1;
    }
    assert( var_strg == NULL );
    var_strg = tmp_assign_source_9;

    branch_end_4:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto try_finally_handler_start_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var_nchars != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_nchars,
            var_nchars
        );
        assert( tmp_res != -1 );

    }
    if ( var_options != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_options,
            var_options
        );
        assert( tmp_res != -1 );

    }
    if ( var_rawstrg != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_rawstrg,
            var_rawstrg
        );
        assert( tmp_res != -1 );

    }
    if ( var_strg != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_strg,
            var_strg
        );
        assert( tmp_res != -1 );

    }
    if ( par_data != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_data,
            par_data
        );
        assert( tmp_res != -1 );

    }
    if ( par_pos != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_pos,
            par_pos
        );
        assert( tmp_res != -1 );

    }
    if ( par_lenlen != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_lenlen,
            par_lenlen
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    tmp_return_value = var_strg;

    Py_INCREF( tmp_return_value );
    goto try_finally_handler_start_1;
    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;

    Py_XDECREF( par_pos );
    par_pos = NULL;

    CHECK_OBJECT( (PyObject *)par_lenlen );
    Py_DECREF( par_lenlen );
    par_lenlen = NULL;

    Py_XDECREF( var_nchars );
    var_nchars = NULL;

    Py_XDECREF( var_options );
    var_options = NULL;

    Py_XDECREF( var_rawstrg );
    var_rawstrg = NULL;

    Py_XDECREF( var_strg );
    var_strg = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_1;
    finally_end_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_8_unpack_unicode_of_xlrd$biffh );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_8_unpack_unicode_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_data = NULL;
    PyObject *_python_par_pos = NULL;
    PyObject *_python_par_lenlen = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "unpack_unicode() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_data == key )
            {
                assert( _python_par_data == NULL );
                _python_par_data = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_pos == key )
            {
                assert( _python_par_pos == NULL );
                _python_par_pos = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_lenlen == key )
            {
                assert( _python_par_lenlen == NULL );
                _python_par_lenlen = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_data, key ) == 1 )
            {
                assert( _python_par_data == NULL );
                _python_par_data = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_pos, key ) == 1 )
            {
                assert( _python_par_pos == NULL );
                _python_par_pos = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_lenlen, key ) == 1 )
            {
                assert( _python_par_lenlen == NULL );
                _python_par_lenlen = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "unpack_unicode() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 3 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_data != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_data = args[ 0 ];
        Py_INCREF( _python_par_data );
    }
    else if ( _python_par_data == NULL )
    {
        if ( 0 + self->m_defaults_given >= 3  )
        {
            _python_par_data = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 3 );
            Py_INCREF( _python_par_data );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 1 < args_given ))
    {
         if (unlikely( _python_par_pos != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 1 );
             goto error_exit;
         }

        _python_par_pos = args[ 1 ];
        Py_INCREF( _python_par_pos );
    }
    else if ( _python_par_pos == NULL )
    {
        if ( 1 + self->m_defaults_given >= 3  )
        {
            _python_par_pos = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 1 - 3 );
            Py_INCREF( _python_par_pos );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 2 < args_given ))
    {
         if (unlikely( _python_par_lenlen != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 2 );
             goto error_exit;
         }

        _python_par_lenlen = args[ 2 ];
        Py_INCREF( _python_par_lenlen );
    }
    else if ( _python_par_lenlen == NULL )
    {
        if ( 2 + self->m_defaults_given >= 3  )
        {
            _python_par_lenlen = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 2 - 3 );
            Py_INCREF( _python_par_lenlen );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_data == NULL || _python_par_pos == NULL || _python_par_lenlen == NULL ))
    {
        PyObject *values[] = { _python_par_data, _python_par_pos, _python_par_lenlen };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_8_unpack_unicode_of_xlrd$biffh( self, _python_par_data, _python_par_pos, _python_par_lenlen );

error_exit:;

    Py_XDECREF( _python_par_data );
    Py_XDECREF( _python_par_pos );
    Py_XDECREF( _python_par_lenlen );

    return NULL;
}

static PyObject *dparse_function_8_unpack_unicode_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 3 )
    {
        return impl_function_8_unpack_unicode_of_xlrd$biffh( self, INCREASE_REFCOUNT( args[ 0 ] ), INCREASE_REFCOUNT( args[ 1 ] ), INCREASE_REFCOUNT( args[ 2 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_8_unpack_unicode_of_xlrd$biffh( self, args, size, NULL );
        return result;
    }

}



static PyObject *impl_function_9_unpack_unicode_update_pos_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject *_python_par_data, PyObject *_python_par_pos, PyObject *_python_par_lenlen, PyObject *_python_par_known_len )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_data = _python_par_data;
    PyObject *par_pos = _python_par_pos;
    PyObject *par_lenlen = _python_par_lenlen;
    PyObject *par_known_len = _python_par_known_len;
    PyObject *var_nchars = NULL;
    PyObject *var_options = NULL;
    PyObject *var_phonetic = NULL;
    PyObject *var_richtext = NULL;
    PyObject *var_rt = NULL;
    PyObject *var_sz = NULL;
    PyObject *var_strg = NULL;
    PyObject *tmp_and_1__value_1 = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_args_element_name_7;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_call_arg_element_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_right_1;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    int tmp_cond_truth_3;
    int tmp_cond_truth_4;
    int tmp_cond_truth_5;
    int tmp_cond_truth_6;
    int tmp_cond_truth_7;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_cond_value_3;
    PyObject *tmp_cond_value_4;
    PyObject *tmp_cond_value_5;
    PyObject *tmp_cond_value_6;
    PyObject *tmp_cond_value_7;
    PyObject *tmp_frame_locals;
    bool tmp_isnot_1;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_left_name_4;
    PyObject *tmp_left_name_5;
    PyObject *tmp_left_name_6;
    PyObject *tmp_left_name_7;
    PyObject *tmp_left_name_8;
    PyObject *tmp_left_name_9;
    PyObject *tmp_left_name_10;
    PyObject *tmp_left_name_11;
    PyObject *tmp_left_name_12;
    PyObject *tmp_left_name_13;
    PyObject *tmp_left_name_14;
    PyObject *tmp_left_name_15;
    PyObject *tmp_left_name_16;
    PyObject *tmp_left_name_17;
    PyObject *tmp_left_name_18;
    PyObject *tmp_left_name_19;
    PyObject *tmp_left_name_20;
    PyObject *tmp_left_name_21;
    PyObject *tmp_operand_name_1;
    PyObject *tmp_operand_name_2;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_right_name_4;
    PyObject *tmp_right_name_5;
    PyObject *tmp_right_name_6;
    PyObject *tmp_right_name_7;
    PyObject *tmp_right_name_8;
    PyObject *tmp_right_name_9;
    PyObject *tmp_right_name_10;
    PyObject *tmp_right_name_11;
    PyObject *tmp_right_name_12;
    PyObject *tmp_right_name_13;
    PyObject *tmp_right_name_14;
    PyObject *tmp_right_name_15;
    PyObject *tmp_right_name_16;
    PyObject *tmp_right_name_17;
    PyObject *tmp_right_name_18;
    PyObject *tmp_right_name_19;
    PyObject *tmp_right_name_20;
    PyObject *tmp_right_name_21;
    PyObject *tmp_slice_lower_1;
    PyObject *tmp_slice_lower_2;
    PyObject *tmp_slice_lower_3;
    PyObject *tmp_slice_lower_4;
    PyObject *tmp_slice_lower_5;
    PyObject *tmp_slice_lower_6;
    PyObject *tmp_slice_source_1;
    PyObject *tmp_slice_source_2;
    PyObject *tmp_slice_source_3;
    PyObject *tmp_slice_source_4;
    PyObject *tmp_slice_source_5;
    PyObject *tmp_slice_source_6;
    PyObject *tmp_slice_upper_1;
    PyObject *tmp_slice_upper_2;
    PyObject *tmp_slice_upper_3;
    PyObject *tmp_slice_upper_4;
    PyObject *tmp_slice_upper_5;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscribed_name_2;
    PyObject *tmp_subscribed_name_3;
    PyObject *tmp_subscribed_name_4;
    PyObject *tmp_subscribed_name_5;
    PyObject *tmp_subscript_name_1;
    PyObject *tmp_subscript_name_2;
    PyObject *tmp_subscript_name_3;
    PyObject *tmp_subscript_name_4;
    PyObject *tmp_subscript_name_5;
    int tmp_tried_lineno_1;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    PyObject *tmp_unicode_arg_1;
    PyObject *tmp_unicode_arg_2;
    PyObject *tmp_unicode_encoding_1;
    PyObject *tmp_unicode_encoding_2;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_c0775a082f861eac7fa3f350d3af47e1, module_xlrd$biffh );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_compare_left_1 = par_known_len;

    tmp_compare_right_1 = Py_None;
    tmp_isnot_1 = ( tmp_compare_left_1 != tmp_compare_right_1 );
    if (tmp_isnot_1)
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_assign_source_1 = par_known_len;

    assert( var_nchars == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var_nchars = tmp_assign_source_1;

    goto branch_end_1;
    branch_no_1:;
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unpack );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10589 ], 35, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 326;
        goto frame_exception_exit_1;
    }

    tmp_left_name_1 = const_str_chr_60;
    tmp_subscribed_name_2 = const_str_plain_BH;
    tmp_left_name_2 = par_lenlen;

    tmp_right_name_2 = const_int_pos_1;
    tmp_subscript_name_1 = BINARY_OPERATION_SUB( tmp_left_name_2, tmp_right_name_2 );
    if ( tmp_subscript_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 326;
        goto frame_exception_exit_1;
    }
    tmp_right_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_1 );
    Py_DECREF( tmp_subscript_name_1 );
    if ( tmp_right_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 326;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_1 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    Py_DECREF( tmp_right_name_1 );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 326;
        goto frame_exception_exit_1;
    }
    tmp_slice_source_1 = par_data;

    tmp_slice_lower_1 = par_pos;

    tmp_left_name_3 = par_pos;

    tmp_right_name_3 = par_lenlen;

    tmp_slice_upper_1 = BINARY_OPERATION_ADD( tmp_left_name_3, tmp_right_name_3 );
    if ( tmp_slice_upper_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_element_name_1 );

        frame_function->f_lineno = 326;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_2 = LOOKUP_SLICE( tmp_slice_source_1, tmp_slice_lower_1, tmp_slice_upper_1 );
    Py_DECREF( tmp_slice_upper_1 );
    if ( tmp_args_element_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_element_name_1 );

        frame_function->f_lineno = 326;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 326;
    tmp_subscribed_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, tmp_args_element_name_1, tmp_args_element_name_2 );
    Py_DECREF( tmp_args_element_name_1 );
    Py_DECREF( tmp_args_element_name_2 );
    if ( tmp_subscribed_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 326;
        goto frame_exception_exit_1;
    }
    tmp_subscript_name_2 = const_int_0;
    tmp_assign_source_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_2 );
    Py_DECREF( tmp_subscribed_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 326;
        goto frame_exception_exit_1;
    }
    assert( var_nchars == NULL );
    var_nchars = tmp_assign_source_2;

    tmp_left_name_4 = par_pos;

    tmp_right_name_4 = par_lenlen;

    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_4, tmp_right_name_4 );
    tmp_assign_source_3 = tmp_left_name_4;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 327;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_3;

    branch_end_1:;
    // Tried code
    tmp_cond_value_1 = NULL;
    // Tried code
    tmp_operand_name_1 = var_nchars;

    tmp_assign_source_4 = UNARY_OPERATION( UNARY_NOT, tmp_operand_name_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 328;
        goto try_finally_handler_3;
    }
    assert( tmp_and_1__value_1 == NULL );
    Py_INCREF( tmp_assign_source_4 );
    tmp_and_1__value_1 = tmp_assign_source_4;

    tmp_cond_value_2 = tmp_and_1__value_1;

    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 328;
        goto try_finally_handler_3;
    }
    if (tmp_cond_truth_2 == 1)
    {
        goto condexpr_true_1;
    }
    else
    {
        goto condexpr_false_1;
    }
    condexpr_true_1:;
    tmp_cond_value_1 = NULL;
    // Tried code
    tmp_result = tmp_and_1__value_1 != NULL;
    if ( tmp_result == true )
    {
        Py_DECREF( tmp_and_1__value_1 );
        tmp_and_1__value_1 = NULL;
    }

    assert( tmp_result != false );
    tmp_slice_source_2 = par_data;

    tmp_slice_lower_2 = par_pos;

    tmp_operand_name_2 = LOOKUP_SLICE( tmp_slice_source_2, tmp_slice_lower_2, Py_None );
    if ( tmp_operand_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 328;
        goto try_finally_handler_4;
    }
    tmp_cond_value_1 = UNARY_OPERATION( UNARY_NOT, tmp_operand_name_2 );
    Py_DECREF( tmp_operand_name_2 );
    if ( tmp_cond_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 328;
        goto try_finally_handler_4;
    }
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto try_finally_handler_3;
    }

    goto finally_end_1;
    finally_end_1:;
    goto condexpr_end_1;
    condexpr_false_1:;
    tmp_cond_value_1 = tmp_and_1__value_1;

    condexpr_end_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_2 != NULL )
    {
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;

        goto try_finally_handler_2;
    }

    goto finally_end_2;
    finally_end_2:;
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 328;
        goto try_finally_handler_2;
    }
    if (tmp_cond_truth_1 == 1)
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_return_value = PyTuple_New( 2 );
    tmp_called_name_2 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_UNICODE_LITERAL );

    if (unlikely( tmp_called_name_2 == NULL ))
    {
        tmp_called_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UNICODE_LITERAL );
    }

    if ( tmp_called_name_2 == NULL )
    {
        Py_DECREF( tmp_return_value );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10624 ], 44, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 330;
        goto try_finally_handler_2;
    }

    tmp_call_arg_element_1 = const_str_empty;
    frame_function->f_lineno = 330;
    tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, tmp_call_arg_element_1 );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_return_value );

        frame_function->f_lineno = 330;
        goto try_finally_handler_2;
    }
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_1 );
    tmp_tuple_element_1 = par_pos;

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_1 );
    goto try_finally_handler_start_2;
    branch_no_2:;
    try_finally_handler_start_2:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_1 = frame_function->f_lineno;
    Py_XDECREF( tmp_and_1__value_1 );
    tmp_and_1__value_1 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_1;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_3 != NULL )
    {
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;

        goto frame_exception_exit_1;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto frame_return_exit_1;
    }

    goto finally_end_3;
    finally_end_3:;
    tmp_called_name_3 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_BYTES_ORD );

    if (unlikely( tmp_called_name_3 == NULL ))
    {
        tmp_called_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BYTES_ORD );
    }

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10668 ], 38, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 331;
        goto frame_exception_exit_1;
    }

    tmp_subscribed_name_3 = par_data;

    tmp_subscript_name_3 = par_pos;

    tmp_args_element_name_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
    if ( tmp_args_element_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 331;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 331;
    tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, tmp_args_element_name_3 );
    Py_DECREF( tmp_args_element_name_3 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 331;
        goto frame_exception_exit_1;
    }
    assert( var_options == NULL );
    var_options = tmp_assign_source_5;

    tmp_left_name_5 = par_pos;

    tmp_right_name_5 = const_int_pos_1;
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_5, tmp_right_name_5 );
    tmp_assign_source_6 = tmp_left_name_5;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 332;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_6;

    tmp_left_name_6 = var_options;

    tmp_right_name_6 = const_int_pos_4;
    tmp_assign_source_7 = BINARY_OPERATION( PyNumber_And, tmp_left_name_6, tmp_right_name_6 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 333;
        goto frame_exception_exit_1;
    }
    assert( var_phonetic == NULL );
    var_phonetic = tmp_assign_source_7;

    tmp_left_name_7 = var_options;

    tmp_right_name_7 = const_int_pos_8;
    tmp_assign_source_8 = BINARY_OPERATION( PyNumber_And, tmp_left_name_7, tmp_right_name_7 );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 334;
        goto frame_exception_exit_1;
    }
    assert( var_richtext == NULL );
    var_richtext = tmp_assign_source_8;

    tmp_cond_value_3 = var_richtext;

    tmp_cond_truth_3 = CHECK_IF_TRUE( tmp_cond_value_3 );
    if ( tmp_cond_truth_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 335;
        goto frame_exception_exit_1;
    }
    if (tmp_cond_truth_3 == 1)
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_called_name_4 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack );

    if (unlikely( tmp_called_name_4 == NULL ))
    {
        tmp_called_name_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unpack );
    }

    if ( tmp_called_name_4 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10589 ], 35, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 336;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_4 = const_str_digest_ce040a5a2e567cbd88761767ab4cb887;
    tmp_slice_source_3 = par_data;

    tmp_slice_lower_3 = par_pos;

    tmp_left_name_8 = par_pos;

    tmp_right_name_8 = const_int_pos_2;
    tmp_slice_upper_2 = BINARY_OPERATION_ADD( tmp_left_name_8, tmp_right_name_8 );
    if ( tmp_slice_upper_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 336;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_5 = LOOKUP_SLICE( tmp_slice_source_3, tmp_slice_lower_3, tmp_slice_upper_2 );
    Py_DECREF( tmp_slice_upper_2 );
    if ( tmp_args_element_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 336;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 336;
    tmp_subscribed_name_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, tmp_args_element_name_4, tmp_args_element_name_5 );
    Py_DECREF( tmp_args_element_name_5 );
    if ( tmp_subscribed_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 336;
        goto frame_exception_exit_1;
    }
    tmp_subscript_name_4 = const_int_0;
    tmp_assign_source_9 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
    Py_DECREF( tmp_subscribed_name_4 );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 336;
        goto frame_exception_exit_1;
    }
    assert( var_rt == NULL );
    var_rt = tmp_assign_source_9;

    tmp_left_name_9 = par_pos;

    tmp_right_name_9 = const_int_pos_2;
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_9, tmp_right_name_9 );
    tmp_assign_source_10 = tmp_left_name_9;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 337;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_10;

    branch_no_3:;
    tmp_cond_value_4 = var_phonetic;

    tmp_cond_truth_4 = CHECK_IF_TRUE( tmp_cond_value_4 );
    if ( tmp_cond_truth_4 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 338;
        goto frame_exception_exit_1;
    }
    if (tmp_cond_truth_4 == 1)
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_called_name_5 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack );

    if (unlikely( tmp_called_name_5 == NULL ))
    {
        tmp_called_name_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unpack );
    }

    if ( tmp_called_name_5 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10589 ], 35, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 339;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_6 = const_str_digest_c789ca952478ee28f98e94d83dd276b2;
    tmp_slice_source_4 = par_data;

    tmp_slice_lower_4 = par_pos;

    tmp_left_name_10 = par_pos;

    tmp_right_name_10 = const_int_pos_4;
    tmp_slice_upper_3 = BINARY_OPERATION_ADD( tmp_left_name_10, tmp_right_name_10 );
    if ( tmp_slice_upper_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 339;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_7 = LOOKUP_SLICE( tmp_slice_source_4, tmp_slice_lower_4, tmp_slice_upper_3 );
    Py_DECREF( tmp_slice_upper_3 );
    if ( tmp_args_element_name_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 339;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 339;
    tmp_subscribed_name_5 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, tmp_args_element_name_6, tmp_args_element_name_7 );
    Py_DECREF( tmp_args_element_name_7 );
    if ( tmp_subscribed_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 339;
        goto frame_exception_exit_1;
    }
    tmp_subscript_name_5 = const_int_0;
    tmp_assign_source_11 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
    Py_DECREF( tmp_subscribed_name_5 );
    if ( tmp_assign_source_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 339;
        goto frame_exception_exit_1;
    }
    assert( var_sz == NULL );
    var_sz = tmp_assign_source_11;

    tmp_left_name_11 = par_pos;

    tmp_right_name_11 = const_int_pos_4;
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_11, tmp_right_name_11 );
    tmp_assign_source_12 = tmp_left_name_11;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 340;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_12;

    branch_no_4:;
    tmp_left_name_12 = var_options;

    tmp_right_name_12 = const_int_pos_1;
    tmp_cond_value_5 = BINARY_OPERATION( PyNumber_And, tmp_left_name_12, tmp_right_name_12 );
    if ( tmp_cond_value_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 341;
        goto frame_exception_exit_1;
    }
    tmp_cond_truth_5 = CHECK_IF_TRUE( tmp_cond_value_5 );
    if ( tmp_cond_truth_5 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_5 );

        frame_function->f_lineno = 341;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_cond_value_5 );
    if (tmp_cond_truth_5 == 1)
    {
        goto branch_yes_5;
    }
    else
    {
        goto branch_no_5;
    }
    branch_yes_5:;
    tmp_slice_source_5 = par_data;

    tmp_slice_lower_5 = par_pos;

    if ( tmp_slice_lower_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 343;
        goto frame_exception_exit_1;
    }

    tmp_left_name_13 = par_pos;

    if ( tmp_left_name_13 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 343;
        goto frame_exception_exit_1;
    }

    tmp_left_name_14 = const_int_pos_2;
    tmp_right_name_14 = var_nchars;

    tmp_right_name_13 = BINARY_OPERATION_MUL( tmp_left_name_14, tmp_right_name_14 );
    if ( tmp_right_name_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 343;
        goto frame_exception_exit_1;
    }
    tmp_slice_upper_4 = BINARY_OPERATION_ADD( tmp_left_name_13, tmp_right_name_13 );
    Py_DECREF( tmp_right_name_13 );
    if ( tmp_slice_upper_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 343;
        goto frame_exception_exit_1;
    }
    tmp_unicode_arg_1 = LOOKUP_SLICE( tmp_slice_source_5, tmp_slice_lower_5, tmp_slice_upper_4 );
    Py_DECREF( tmp_slice_upper_4 );
    if ( tmp_unicode_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 343;
        goto frame_exception_exit_1;
    }
    tmp_unicode_encoding_1 = const_str_plain_utf_16_le;
    tmp_assign_source_13 = TO_UNICODE3( tmp_unicode_arg_1, tmp_unicode_encoding_1, NULL );
    Py_DECREF( tmp_unicode_arg_1 );
    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 343;
        goto frame_exception_exit_1;
    }
    assert( var_strg == NULL );
    var_strg = tmp_assign_source_13;

    tmp_left_name_15 = par_pos;

    if ( tmp_left_name_15 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 344;
        goto frame_exception_exit_1;
    }

    tmp_left_name_16 = const_int_pos_2;
    tmp_right_name_16 = var_nchars;

    tmp_right_name_15 = BINARY_OPERATION_MUL( tmp_left_name_16, tmp_right_name_16 );
    if ( tmp_right_name_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 344;
        goto frame_exception_exit_1;
    }
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_15, tmp_right_name_15 );
    tmp_assign_source_14 = tmp_left_name_15;
    Py_DECREF( tmp_right_name_15 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 344;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_14;

    goto branch_end_5;
    branch_no_5:;
    tmp_slice_source_6 = par_data;

    tmp_slice_lower_6 = par_pos;

    if ( tmp_slice_lower_6 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 347;
        goto frame_exception_exit_1;
    }

    tmp_left_name_17 = par_pos;

    if ( tmp_left_name_17 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 347;
        goto frame_exception_exit_1;
    }

    tmp_right_name_17 = var_nchars;

    tmp_slice_upper_5 = BINARY_OPERATION_ADD( tmp_left_name_17, tmp_right_name_17 );
    if ( tmp_slice_upper_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 347;
        goto frame_exception_exit_1;
    }
    tmp_unicode_arg_2 = LOOKUP_SLICE( tmp_slice_source_6, tmp_slice_lower_6, tmp_slice_upper_5 );
    Py_DECREF( tmp_slice_upper_5 );
    if ( tmp_unicode_arg_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 347;
        goto frame_exception_exit_1;
    }
    tmp_unicode_encoding_2 = const_str_plain_latin_1;
    tmp_assign_source_15 = TO_UNICODE3( tmp_unicode_arg_2, tmp_unicode_encoding_2, NULL );
    Py_DECREF( tmp_unicode_arg_2 );
    if ( tmp_assign_source_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 347;
        goto frame_exception_exit_1;
    }
    assert( var_strg == NULL );
    var_strg = tmp_assign_source_15;

    tmp_left_name_18 = par_pos;

    if ( tmp_left_name_18 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 348;
        goto frame_exception_exit_1;
    }

    tmp_right_name_18 = var_nchars;

    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_18, tmp_right_name_18 );
    tmp_assign_source_16 = tmp_left_name_18;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 348;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_16;

    branch_end_5:;
    tmp_cond_value_6 = var_richtext;

    tmp_cond_truth_6 = CHECK_IF_TRUE( tmp_cond_value_6 );
    if ( tmp_cond_truth_6 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 349;
        goto frame_exception_exit_1;
    }
    if (tmp_cond_truth_6 == 1)
    {
        goto branch_yes_6;
    }
    else
    {
        goto branch_no_6;
    }
    branch_yes_6:;
    tmp_left_name_19 = par_pos;

    tmp_left_name_20 = const_int_pos_4;
    tmp_right_name_20 = var_rt;

    if ( tmp_right_name_20 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10755 ], 48, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 350;
        goto frame_exception_exit_1;
    }

    tmp_right_name_19 = BINARY_OPERATION_MUL( tmp_left_name_20, tmp_right_name_20 );
    if ( tmp_right_name_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 350;
        goto frame_exception_exit_1;
    }
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_19, tmp_right_name_19 );
    tmp_assign_source_17 = tmp_left_name_19;
    Py_DECREF( tmp_right_name_19 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 350;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_17;

    branch_no_6:;
    tmp_cond_value_7 = var_phonetic;

    tmp_cond_truth_7 = CHECK_IF_TRUE( tmp_cond_value_7 );
    if ( tmp_cond_truth_7 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 351;
        goto frame_exception_exit_1;
    }
    if (tmp_cond_truth_7 == 1)
    {
        goto branch_yes_7;
    }
    else
    {
        goto branch_no_7;
    }
    branch_yes_7:;
    tmp_left_name_21 = par_pos;

    if ( tmp_left_name_21 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 352;
        goto frame_exception_exit_1;
    }

    tmp_right_name_21 = var_sz;

    if ( tmp_right_name_21 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10803 ], 48, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 352;
        goto frame_exception_exit_1;
    }

    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_21, tmp_right_name_21 );
    tmp_assign_source_18 = tmp_left_name_21;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 352;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_18;

    branch_no_7:;
    tmp_return_value = PyTuple_New( 2 );
    tmp_tuple_element_2 = var_strg;

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_2 );
    tmp_tuple_element_2 = par_pos;

    if ( tmp_tuple_element_2 == NULL )
    {
        Py_DECREF( tmp_return_value );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 353;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_2 );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto try_finally_handler_start_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var_nchars != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_nchars,
            var_nchars
        );
        assert( tmp_res != -1 );

    }
    if ( var_options != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_options,
            var_options
        );
        assert( tmp_res != -1 );

    }
    if ( var_phonetic != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_phonetic,
            var_phonetic
        );
        assert( tmp_res != -1 );

    }
    if ( var_richtext != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_richtext,
            var_richtext
        );
        assert( tmp_res != -1 );

    }
    if ( var_rt != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_rt,
            var_rt
        );
        assert( tmp_res != -1 );

    }
    if ( var_sz != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_sz,
            var_sz
        );
        assert( tmp_res != -1 );

    }
    if ( var_strg != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_strg,
            var_strg
        );
        assert( tmp_res != -1 );

    }
    if ( par_data != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_data,
            par_data
        );
        assert( tmp_res != -1 );

    }
    if ( par_pos != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_pos,
            par_pos
        );
        assert( tmp_res != -1 );

    }
    if ( par_lenlen != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_lenlen,
            par_lenlen
        );
        assert( tmp_res != -1 );

    }
    if ( par_known_len != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_known_len,
            par_known_len
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;

    Py_XDECREF( par_pos );
    par_pos = NULL;

    CHECK_OBJECT( (PyObject *)par_lenlen );
    Py_DECREF( par_lenlen );
    par_lenlen = NULL;

    CHECK_OBJECT( (PyObject *)par_known_len );
    Py_DECREF( par_known_len );
    par_known_len = NULL;

    Py_XDECREF( var_nchars );
    var_nchars = NULL;

    Py_XDECREF( var_options );
    var_options = NULL;

    Py_XDECREF( var_phonetic );
    var_phonetic = NULL;

    Py_XDECREF( var_richtext );
    var_richtext = NULL;

    Py_XDECREF( var_rt );
    var_rt = NULL;

    Py_XDECREF( var_sz );
    var_sz = NULL;

    Py_XDECREF( var_strg );
    var_strg = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_4 != NULL )
    {
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_4;
    finally_end_4:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_9_unpack_unicode_update_pos_of_xlrd$biffh );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_9_unpack_unicode_update_pos_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_data = NULL;
    PyObject *_python_par_pos = NULL;
    PyObject *_python_par_lenlen = NULL;
    PyObject *_python_par_known_len = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "unpack_unicode_update_pos() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_data == key )
            {
                assert( _python_par_data == NULL );
                _python_par_data = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_pos == key )
            {
                assert( _python_par_pos == NULL );
                _python_par_pos = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_lenlen == key )
            {
                assert( _python_par_lenlen == NULL );
                _python_par_lenlen = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_known_len == key )
            {
                assert( _python_par_known_len == NULL );
                _python_par_known_len = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_data, key ) == 1 )
            {
                assert( _python_par_data == NULL );
                _python_par_data = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_pos, key ) == 1 )
            {
                assert( _python_par_pos == NULL );
                _python_par_pos = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_lenlen, key ) == 1 )
            {
                assert( _python_par_lenlen == NULL );
                _python_par_lenlen = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_known_len, key ) == 1 )
            {
                assert( _python_par_known_len == NULL );
                _python_par_known_len = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "unpack_unicode_update_pos() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 4 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_data != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_data = args[ 0 ];
        Py_INCREF( _python_par_data );
    }
    else if ( _python_par_data == NULL )
    {
        if ( 0 + self->m_defaults_given >= 4  )
        {
            _python_par_data = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 4 );
            Py_INCREF( _python_par_data );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 1 < args_given ))
    {
         if (unlikely( _python_par_pos != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 1 );
             goto error_exit;
         }

        _python_par_pos = args[ 1 ];
        Py_INCREF( _python_par_pos );
    }
    else if ( _python_par_pos == NULL )
    {
        if ( 1 + self->m_defaults_given >= 4  )
        {
            _python_par_pos = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 1 - 4 );
            Py_INCREF( _python_par_pos );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 2 < args_given ))
    {
         if (unlikely( _python_par_lenlen != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 2 );
             goto error_exit;
         }

        _python_par_lenlen = args[ 2 ];
        Py_INCREF( _python_par_lenlen );
    }
    else if ( _python_par_lenlen == NULL )
    {
        if ( 2 + self->m_defaults_given >= 4  )
        {
            _python_par_lenlen = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 2 - 4 );
            Py_INCREF( _python_par_lenlen );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 3 < args_given ))
    {
         if (unlikely( _python_par_known_len != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 3 );
             goto error_exit;
         }

        _python_par_known_len = args[ 3 ];
        Py_INCREF( _python_par_known_len );
    }
    else if ( _python_par_known_len == NULL )
    {
        if ( 3 + self->m_defaults_given >= 4  )
        {
            _python_par_known_len = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 3 - 4 );
            Py_INCREF( _python_par_known_len );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_data == NULL || _python_par_pos == NULL || _python_par_lenlen == NULL || _python_par_known_len == NULL ))
    {
        PyObject *values[] = { _python_par_data, _python_par_pos, _python_par_lenlen, _python_par_known_len };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_9_unpack_unicode_update_pos_of_xlrd$biffh( self, _python_par_data, _python_par_pos, _python_par_lenlen, _python_par_known_len );

error_exit:;

    Py_XDECREF( _python_par_data );
    Py_XDECREF( _python_par_pos );
    Py_XDECREF( _python_par_lenlen );
    Py_XDECREF( _python_par_known_len );

    return NULL;
}

static PyObject *dparse_function_9_unpack_unicode_update_pos_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 4 )
    {
        return impl_function_9_unpack_unicode_update_pos_of_xlrd$biffh( self, INCREASE_REFCOUNT( args[ 0 ] ), INCREASE_REFCOUNT( args[ 1 ] ), INCREASE_REFCOUNT( args[ 2 ] ), INCREASE_REFCOUNT( args[ 3 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_9_unpack_unicode_update_pos_of_xlrd$biffh( self, args, size, NULL );
        return result;
    }

}



static PyObject *impl_function_10_unpack_cell_range_address_list_update_pos_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject *_python_par_output_list, PyObject *_python_par_data, PyObject *_python_par_pos, PyObject *_python_par_biff_version, PyObject *_python_par_addr_size )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_output_list = _python_par_output_list;
    PyObject *par_data = _python_par_data;
    PyObject *par_pos = _python_par_pos;
    PyObject *par_biff_version = _python_par_biff_version;
    PyObject *par_addr_size = _python_par_addr_size;
    PyObject *var_n = NULL;
    PyObject *var_fmt = NULL;
    PyObject *var__unused = NULL;
    PyObject *var_ra = NULL;
    PyObject *var_rb = NULL;
    PyObject *var_ca = NULL;
    PyObject *var_cb = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__element_3 = NULL;
    PyObject *tmp_tuple_unpack_2__element_4 = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    int tmp_cmp_Eq_1;
    int tmp_cmp_NotIn_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    int tmp_cond_truth_1;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_frame_locals;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iter_arg_2;
    PyObject *tmp_iter_arg_3;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_iterator_name_2;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_left_name_4;
    PyObject *tmp_left_name_5;
    PyObject *tmp_left_name_6;
    PyObject *tmp_next_source_1;
    PyObject *tmp_raise_type_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_right_name_4;
    PyObject *tmp_right_name_5;
    PyObject *tmp_right_name_6;
    PyObject *tmp_slice_lower_1;
    PyObject *tmp_slice_lower_2;
    PyObject *tmp_slice_source_1;
    PyObject *tmp_slice_source_2;
    PyObject *tmp_slice_upper_1;
    PyObject *tmp_slice_upper_2;
    PyObject *tmp_source_name_1;
    int tmp_tried_lineno_1;
    int tmp_tried_lineno_2;
    int tmp_tried_lineno_3;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    PyObject *tmp_unpack_3;
    PyObject *tmp_unpack_4;
    PyObject *tmp_unpack_5;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_37d8e658270520bbbb78114daec154c0, module_xlrd$biffh );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_compare_left_1 = par_addr_size;

    tmp_compare_right_1 = const_tuple_int_pos_6_int_pos_8_tuple;
    tmp_cmp_NotIn_1 = PySequence_Contains( tmp_compare_right_1, tmp_compare_left_1 );
    if ( tmp_cmp_NotIn_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 358;
        goto frame_exception_exit_1;
    }
    if (tmp_cmp_NotIn_1 == 0)
    {
        goto branch_yes_1;
    }
    else
    {
        goto branch_no_1;
    }
    branch_yes_1:;
    tmp_raise_type_1 = PyExc_AssertionError;
    exception_type = INCREASE_REFCOUNT( tmp_raise_type_1 );
    frame_function->f_lineno = 358;
    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
    goto frame_exception_exit_1;
    branch_no_1:;
    // Tried code
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unpack );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10589 ], 35, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 361;
        goto try_finally_handler_2;
    }

    tmp_args_element_name_1 = const_str_digest_ce040a5a2e567cbd88761767ab4cb887;
    tmp_slice_source_1 = par_data;

    tmp_slice_lower_1 = par_pos;

    tmp_left_name_1 = par_pos;

    tmp_right_name_1 = const_int_pos_2;
    tmp_slice_upper_1 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    if ( tmp_slice_upper_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 361;
        goto try_finally_handler_2;
    }
    tmp_args_element_name_2 = LOOKUP_SLICE( tmp_slice_source_1, tmp_slice_lower_1, tmp_slice_upper_1 );
    Py_DECREF( tmp_slice_upper_1 );
    if ( tmp_args_element_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 361;
        goto try_finally_handler_2;
    }
    frame_function->f_lineno = 361;
    tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, tmp_args_element_name_1, tmp_args_element_name_2 );
    Py_DECREF( tmp_args_element_name_2 );
    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 361;
        goto try_finally_handler_2;
    }
    tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 361;
        goto try_finally_handler_2;
    }
    assert( tmp_tuple_unpack_1__source_iter == NULL );
    tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;

    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0 );
    if ( tmp_assign_source_2 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 361;
        goto try_finally_handler_2;
    }
    assert( tmp_tuple_unpack_1__element_1 == NULL );
    tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                goto try_finally_handler_2;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 1)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        goto try_finally_handler_2;
    }
    tmp_assign_source_3 = tmp_tuple_unpack_1__element_1;

    assert( var_n == NULL );
    Py_INCREF( tmp_assign_source_3 );
    var_n = tmp_assign_source_3;

    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_1 = frame_function->f_lineno;
    Py_XDECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_1;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto frame_exception_exit_1;
    }

    goto finally_end_1;
    finally_end_1:;
    tmp_left_name_2 = par_pos;

    tmp_right_name_2 = const_int_pos_2;
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_2, tmp_right_name_2 );
    tmp_assign_source_4 = tmp_left_name_2;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 362;
        goto frame_exception_exit_1;
    }
    par_pos = tmp_assign_source_4;

    tmp_cond_value_1 = var_n;

    if ( tmp_cond_value_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10542 ], 47, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 363;
        goto frame_exception_exit_1;
    }

    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 363;
        goto frame_exception_exit_1;
    }
    if (tmp_cond_truth_1 == 1)
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_compare_left_2 = par_addr_size;

    tmp_compare_right_2 = const_int_pos_6;
    tmp_cmp_Eq_1 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_2, tmp_compare_right_2 );
    if ( tmp_cmp_Eq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 364;
        goto frame_exception_exit_1;
    }
    if (tmp_cmp_Eq_1 == 1)
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_assign_source_5 = const_str_digest_525193cd98a1d97690ef9daa0960a42a;
    assert( var_fmt == NULL );
    Py_INCREF( tmp_assign_source_5 );
    var_fmt = tmp_assign_source_5;

    goto branch_end_3;
    branch_no_3:;
    tmp_assign_source_6 = const_str_digest_4d1b2450b7a7643f798830120130cc35;
    assert( var_fmt == NULL );
    Py_INCREF( tmp_assign_source_6 );
    var_fmt = tmp_assign_source_6;

    branch_end_3:;
    tmp_called_name_2 = LOOKUP_BUILTIN( const_str_plain_xrange );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 368;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_3 = var_n;

    if ( tmp_args_element_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10542 ], 47, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 368;
        goto frame_exception_exit_1;
    }

    frame_function->f_lineno = 368;
    tmp_iter_arg_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, tmp_args_element_name_3 );
    if ( tmp_iter_arg_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 368;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_7 = MAKE_ITERATOR( tmp_iter_arg_2 );
    Py_DECREF( tmp_iter_arg_2 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 368;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_7;

    // Tried code
    loop_start_1:;
    tmp_next_source_1 = tmp_for_loop_1__for_iterator;

    tmp_assign_source_8 = ITERATOR_NEXT( tmp_next_source_1 );
    if ( tmp_assign_source_8 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_1;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 368;
            goto try_finally_handler_3;
        }
    }

    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_8;
        Py_XDECREF( old );
    }

    tmp_assign_source_9 = tmp_for_loop_1__iter_value;

    {
        PyObject *old = var__unused;
        var__unused = tmp_assign_source_9;
        Py_INCREF( var__unused );
        Py_XDECREF( old );
    }

    // Tried code
    tmp_called_name_3 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack );

    if (unlikely( tmp_called_name_3 == NULL ))
    {
        tmp_called_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unpack );
    }

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10589 ], 35, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 369;
        goto try_finally_handler_4;
    }

    tmp_args_element_name_4 = var_fmt;

    tmp_slice_source_2 = par_data;

    tmp_slice_lower_2 = par_pos;

    if ( tmp_slice_lower_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 369;
        goto try_finally_handler_4;
    }

    tmp_left_name_3 = par_pos;

    if ( tmp_left_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 369;
        goto try_finally_handler_4;
    }

    tmp_right_name_3 = par_addr_size;

    tmp_slice_upper_2 = BINARY_OPERATION_ADD( tmp_left_name_3, tmp_right_name_3 );
    if ( tmp_slice_upper_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 369;
        goto try_finally_handler_4;
    }
    tmp_args_element_name_5 = LOOKUP_SLICE( tmp_slice_source_2, tmp_slice_lower_2, tmp_slice_upper_2 );
    Py_DECREF( tmp_slice_upper_2 );
    if ( tmp_args_element_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 369;
        goto try_finally_handler_4;
    }
    frame_function->f_lineno = 369;
    tmp_iter_arg_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, tmp_args_element_name_4, tmp_args_element_name_5 );
    Py_DECREF( tmp_args_element_name_5 );
    if ( tmp_iter_arg_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 369;
        goto try_finally_handler_4;
    }
    tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_3 );
    Py_DECREF( tmp_iter_arg_3 );
    if ( tmp_assign_source_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 369;
        goto try_finally_handler_4;
    }
    {
        PyObject *old = tmp_tuple_unpack_2__source_iter;
        tmp_tuple_unpack_2__source_iter = tmp_assign_source_10;
        Py_XDECREF( old );
    }

    tmp_unpack_2 = tmp_tuple_unpack_2__source_iter;

    tmp_assign_source_11 = UNPACK_NEXT( tmp_unpack_2, 0 );
    if ( tmp_assign_source_11 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 369;
        goto try_finally_handler_4;
    }
    {
        PyObject *old = tmp_tuple_unpack_2__element_1;
        tmp_tuple_unpack_2__element_1 = tmp_assign_source_11;
        Py_XDECREF( old );
    }

    tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;

    tmp_assign_source_12 = UNPACK_NEXT( tmp_unpack_3, 1 );
    if ( tmp_assign_source_12 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 369;
        goto try_finally_handler_4;
    }
    {
        PyObject *old = tmp_tuple_unpack_2__element_2;
        tmp_tuple_unpack_2__element_2 = tmp_assign_source_12;
        Py_XDECREF( old );
    }

    tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;

    tmp_assign_source_13 = UNPACK_NEXT( tmp_unpack_4, 2 );
    if ( tmp_assign_source_13 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 369;
        goto try_finally_handler_4;
    }
    {
        PyObject *old = tmp_tuple_unpack_2__element_3;
        tmp_tuple_unpack_2__element_3 = tmp_assign_source_13;
        Py_XDECREF( old );
    }

    tmp_unpack_5 = tmp_tuple_unpack_2__source_iter;

    tmp_assign_source_14 = UNPACK_NEXT( tmp_unpack_5, 3 );
    if ( tmp_assign_source_14 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 369;
        goto try_finally_handler_4;
    }
    {
        PyObject *old = tmp_tuple_unpack_2__element_4;
        tmp_tuple_unpack_2__element_4 = tmp_assign_source_14;
        Py_XDECREF( old );
    }

    tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;

    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                goto try_finally_handler_4;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 4)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        goto try_finally_handler_4;
    }
    tmp_assign_source_15 = tmp_tuple_unpack_2__element_1;

    {
        PyObject *old = var_ra;
        var_ra = tmp_assign_source_15;
        Py_INCREF( var_ra );
        Py_XDECREF( old );
    }

    tmp_assign_source_16 = tmp_tuple_unpack_2__element_2;

    {
        PyObject *old = var_rb;
        var_rb = tmp_assign_source_16;
        Py_INCREF( var_rb );
        Py_XDECREF( old );
    }

    tmp_assign_source_17 = tmp_tuple_unpack_2__element_3;

    {
        PyObject *old = var_ca;
        var_ca = tmp_assign_source_17;
        Py_INCREF( var_ca );
        Py_XDECREF( old );
    }

    tmp_assign_source_18 = tmp_tuple_unpack_2__element_4;

    {
        PyObject *old = var_cb;
        var_cb = tmp_assign_source_18;
        Py_INCREF( var_cb );
        Py_XDECREF( old );
    }

    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_2 = frame_function->f_lineno;
    Py_XDECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_3 );
    tmp_tuple_unpack_2__element_3 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_4 );
    tmp_tuple_unpack_2__element_4 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_2;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_2 != NULL )
    {
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;

        goto try_finally_handler_3;
    }

    goto finally_end_2;
    finally_end_2:;
    tmp_source_name_1 = par_output_list;

    tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
    if ( tmp_called_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 370;
        goto try_finally_handler_3;
    }
    tmp_args_element_name_6 = PyTuple_New( 4 );
    tmp_tuple_element_1 = var_ra;

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_6 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10851 ], 48, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 370;
        goto try_finally_handler_3;
    }

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_element_name_6, 0, tmp_tuple_element_1 );
    tmp_left_name_4 = var_rb;

    if ( tmp_left_name_4 == NULL )
    {
        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_6 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10899 ], 48, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 370;
        goto try_finally_handler_3;
    }

    tmp_right_name_4 = const_int_pos_1;
    tmp_tuple_element_1 = BINARY_OPERATION_ADD( tmp_left_name_4, tmp_right_name_4 );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_6 );

        frame_function->f_lineno = 370;
        goto try_finally_handler_3;
    }
    PyTuple_SET_ITEM( tmp_args_element_name_6, 1, tmp_tuple_element_1 );
    tmp_tuple_element_1 = var_ca;

    if ( tmp_tuple_element_1 == NULL )
    {
        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_6 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10947 ], 48, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 370;
        goto try_finally_handler_3;
    }

    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_element_name_6, 2, tmp_tuple_element_1 );
    tmp_left_name_5 = var_cb;

    if ( tmp_left_name_5 == NULL )
    {
        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_6 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10995 ], 48, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 370;
        goto try_finally_handler_3;
    }

    tmp_right_name_5 = const_int_pos_1;
    tmp_tuple_element_1 = BINARY_OPERATION_ADD( tmp_left_name_5, tmp_right_name_5 );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_6 );

        frame_function->f_lineno = 370;
        goto try_finally_handler_3;
    }
    PyTuple_SET_ITEM( tmp_args_element_name_6, 3, tmp_tuple_element_1 );
    frame_function->f_lineno = 370;
    tmp_unused = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, tmp_args_element_name_6 );
    Py_DECREF( tmp_called_name_4 );
    Py_DECREF( tmp_args_element_name_6 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 370;
        goto try_finally_handler_3;
    }
    Py_DECREF( tmp_unused );
    tmp_left_name_6 = par_pos;

    if ( tmp_left_name_6 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 371;
        goto try_finally_handler_3;
    }

    tmp_right_name_6 = par_addr_size;

    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_6, tmp_right_name_6 );
    tmp_assign_source_19 = tmp_left_name_6;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 371;
        goto try_finally_handler_3;
    }
    par_pos = tmp_assign_source_19;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 368;
        goto try_finally_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_3 = frame_function->f_lineno;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    frame_function->f_lineno = tmp_tried_lineno_3;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_3 != NULL )
    {
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;

        goto frame_exception_exit_1;
    }

    goto finally_end_3;
    finally_end_3:;
    branch_no_2:;
    tmp_return_value = par_pos;

    if ( tmp_return_value == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 372;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto try_finally_handler_start_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var_n != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_n,
            var_n
        );
        assert( tmp_res != -1 );

    }
    if ( var_fmt != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_fmt,
            var_fmt
        );
        assert( tmp_res != -1 );

    }
    if ( var__unused != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain__unused,
            var__unused
        );
        assert( tmp_res != -1 );

    }
    if ( var_ra != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_ra,
            var_ra
        );
        assert( tmp_res != -1 );

    }
    if ( var_rb != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_rb,
            var_rb
        );
        assert( tmp_res != -1 );

    }
    if ( var_ca != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_ca,
            var_ca
        );
        assert( tmp_res != -1 );

    }
    if ( var_cb != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_cb,
            var_cb
        );
        assert( tmp_res != -1 );

    }
    if ( par_output_list != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_output_list,
            par_output_list
        );
        assert( tmp_res != -1 );

    }
    if ( par_data != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_data,
            par_data
        );
        assert( tmp_res != -1 );

    }
    if ( par_pos != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_pos,
            par_pos
        );
        assert( tmp_res != -1 );

    }
    if ( par_biff_version != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_biff_version,
            par_biff_version
        );
        assert( tmp_res != -1 );

    }
    if ( par_addr_size != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_addr_size,
            par_addr_size
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_output_list );
    Py_DECREF( par_output_list );
    par_output_list = NULL;

    CHECK_OBJECT( (PyObject *)par_data );
    Py_DECREF( par_data );
    par_data = NULL;

    Py_XDECREF( par_pos );
    par_pos = NULL;

    CHECK_OBJECT( (PyObject *)par_biff_version );
    Py_DECREF( par_biff_version );
    par_biff_version = NULL;

    CHECK_OBJECT( (PyObject *)par_addr_size );
    Py_DECREF( par_addr_size );
    par_addr_size = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    Py_XDECREF( var_fmt );
    var_fmt = NULL;

    Py_XDECREF( var__unused );
    var__unused = NULL;

    Py_XDECREF( var_ra );
    var_ra = NULL;

    Py_XDECREF( var_rb );
    var_rb = NULL;

    Py_XDECREF( var_ca );
    var_ca = NULL;

    Py_XDECREF( var_cb );
    var_cb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_4 != NULL )
    {
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_4;
    finally_end_4:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_10_unpack_cell_range_address_list_update_pos_of_xlrd$biffh );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_10_unpack_cell_range_address_list_update_pos_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_output_list = NULL;
    PyObject *_python_par_data = NULL;
    PyObject *_python_par_pos = NULL;
    PyObject *_python_par_biff_version = NULL;
    PyObject *_python_par_addr_size = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "unpack_cell_range_address_list_update_pos() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_output_list == key )
            {
                assert( _python_par_output_list == NULL );
                _python_par_output_list = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_data == key )
            {
                assert( _python_par_data == NULL );
                _python_par_data = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_pos == key )
            {
                assert( _python_par_pos == NULL );
                _python_par_pos = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_biff_version == key )
            {
                assert( _python_par_biff_version == NULL );
                _python_par_biff_version = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_addr_size == key )
            {
                assert( _python_par_addr_size == NULL );
                _python_par_addr_size = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_output_list, key ) == 1 )
            {
                assert( _python_par_output_list == NULL );
                _python_par_output_list = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_data, key ) == 1 )
            {
                assert( _python_par_data == NULL );
                _python_par_data = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_pos, key ) == 1 )
            {
                assert( _python_par_pos == NULL );
                _python_par_pos = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_biff_version, key ) == 1 )
            {
                assert( _python_par_biff_version == NULL );
                _python_par_biff_version = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_addr_size, key ) == 1 )
            {
                assert( _python_par_addr_size == NULL );
                _python_par_addr_size = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "unpack_cell_range_address_list_update_pos() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 5 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_output_list != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_output_list = args[ 0 ];
        Py_INCREF( _python_par_output_list );
    }
    else if ( _python_par_output_list == NULL )
    {
        if ( 0 + self->m_defaults_given >= 5  )
        {
            _python_par_output_list = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 5 );
            Py_INCREF( _python_par_output_list );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 1 < args_given ))
    {
         if (unlikely( _python_par_data != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 1 );
             goto error_exit;
         }

        _python_par_data = args[ 1 ];
        Py_INCREF( _python_par_data );
    }
    else if ( _python_par_data == NULL )
    {
        if ( 1 + self->m_defaults_given >= 5  )
        {
            _python_par_data = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 1 - 5 );
            Py_INCREF( _python_par_data );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 2 < args_given ))
    {
         if (unlikely( _python_par_pos != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 2 );
             goto error_exit;
         }

        _python_par_pos = args[ 2 ];
        Py_INCREF( _python_par_pos );
    }
    else if ( _python_par_pos == NULL )
    {
        if ( 2 + self->m_defaults_given >= 5  )
        {
            _python_par_pos = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 2 - 5 );
            Py_INCREF( _python_par_pos );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 3 < args_given ))
    {
         if (unlikely( _python_par_biff_version != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 3 );
             goto error_exit;
         }

        _python_par_biff_version = args[ 3 ];
        Py_INCREF( _python_par_biff_version );
    }
    else if ( _python_par_biff_version == NULL )
    {
        if ( 3 + self->m_defaults_given >= 5  )
        {
            _python_par_biff_version = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 3 - 5 );
            Py_INCREF( _python_par_biff_version );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 4 < args_given ))
    {
         if (unlikely( _python_par_addr_size != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 4 );
             goto error_exit;
         }

        _python_par_addr_size = args[ 4 ];
        Py_INCREF( _python_par_addr_size );
    }
    else if ( _python_par_addr_size == NULL )
    {
        if ( 4 + self->m_defaults_given >= 5  )
        {
            _python_par_addr_size = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 4 - 5 );
            Py_INCREF( _python_par_addr_size );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_output_list == NULL || _python_par_data == NULL || _python_par_pos == NULL || _python_par_biff_version == NULL || _python_par_addr_size == NULL ))
    {
        PyObject *values[] = { _python_par_output_list, _python_par_data, _python_par_pos, _python_par_biff_version, _python_par_addr_size };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_10_unpack_cell_range_address_list_update_pos_of_xlrd$biffh( self, _python_par_output_list, _python_par_data, _python_par_pos, _python_par_biff_version, _python_par_addr_size );

error_exit:;

    Py_XDECREF( _python_par_output_list );
    Py_XDECREF( _python_par_data );
    Py_XDECREF( _python_par_pos );
    Py_XDECREF( _python_par_biff_version );
    Py_XDECREF( _python_par_addr_size );

    return NULL;
}

static PyObject *dparse_function_10_unpack_cell_range_address_list_update_pos_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 5 )
    {
        return impl_function_10_unpack_cell_range_address_list_update_pos_of_xlrd$biffh( self, INCREASE_REFCOUNT( args[ 0 ] ), INCREASE_REFCOUNT( args[ 1 ] ), INCREASE_REFCOUNT( args[ 2 ] ), INCREASE_REFCOUNT( args[ 3 ] ), INCREASE_REFCOUNT( args[ 4 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_10_unpack_cell_range_address_list_update_pos_of_xlrd$biffh( self, args, size, NULL );
        return result;
    }

}



static PyObject *impl_function_11_hex_char_dump_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject *_python_par_strg, PyObject *_python_par_ofs, PyObject *_python_par_dlen, PyObject *_python_par_base, PyObject *_python_par_fout, PyObject *_python_par_unnumbered )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_strg = _python_par_strg;
    PyObject *par_ofs = _python_par_ofs;
    PyObject *par_dlen = _python_par_dlen;
    PyObject *par_base = _python_par_base;
    PyObject *par_fout = _python_par_fout;
    PyObject *par_unnumbered = _python_par_unnumbered;
    PyObject *var_endpos = NULL;
    PyObject *var_pos = NULL;
    PyObject *var_numbered = NULL;
    PyObject *var_num_prefix = NULL;
    PyObject *var_endsub = NULL;
    PyObject *var_substrg = NULL;
    PyObject *var_lensub = NULL;
    PyObject *var_c = NULL;
    PyObject *var_hexd = NULL;
    PyObject *var_chard = NULL;
    PyObject *tmp_or_1__value_1 = NULL;
    PyObject *tmp_listcontr_1__listcontr_iter = NULL;
    PyObject *tmp_listcontr_1__listcontr_result = NULL;
    PyObject *tmp_listcontr_1__iter_value_0 = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_comparison_1__value_2 = NULL;
    PyObject *tmp_and_1__value_1 = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    PyObject *tmp_append_to_1;
    PyObject *tmp_append_value_1;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_args_element_name_7;
    PyObject *tmp_args_element_name_8;
    PyObject *tmp_args_element_name_9;
    PyObject *tmp_args_element_name_10;
    PyObject *tmp_args_element_name_11;
    PyObject *tmp_args_element_name_12;
    PyObject *tmp_args_element_name_13;
    PyObject *tmp_args_element_name_14;
    PyObject *tmp_args_element_name_15;
    PyObject *tmp_args_element_name_16;
    PyObject *tmp_args_element_name_17;
    PyObject *tmp_args_element_name_18;
    PyObject *tmp_args_element_name_19;
    PyObject *tmp_args_element_name_20;
    PyObject *tmp_args_name_1;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    bool tmp_break_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    PyObject *tmp_called_name_6;
    PyObject *tmp_called_name_7;
    PyObject *tmp_chr_arg_1;
    int tmp_cmp_Eq_1;
    int tmp_cmp_Lt_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compexpr_left_1;
    PyObject *tmp_compexpr_left_2;
    PyObject *tmp_compexpr_left_3;
    PyObject *tmp_compexpr_left_4;
    PyObject *tmp_compexpr_right_1;
    PyObject *tmp_compexpr_right_2;
    PyObject *tmp_compexpr_right_3;
    PyObject *tmp_compexpr_right_4;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    int tmp_cond_truth_3;
    int tmp_cond_truth_4;
    int tmp_cond_truth_5;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_cond_value_3;
    PyObject *tmp_cond_value_4;
    PyObject *tmp_cond_value_5;
    PyObject *tmp_frame_locals;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iter_arg_2;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_left_name_4;
    PyObject *tmp_left_name_5;
    PyObject *tmp_left_name_6;
    PyObject *tmp_left_name_7;
    PyObject *tmp_left_name_8;
    PyObject *tmp_len_arg_1;
    PyObject *tmp_len_arg_2;
    PyObject *tmp_next_source_1;
    PyObject *tmp_next_source_2;
    PyObject *tmp_operand_name_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_right_name_4;
    PyObject *tmp_right_name_5;
    PyObject *tmp_right_name_6;
    PyObject *tmp_right_name_7;
    PyObject *tmp_right_name_8;
    PyObject *tmp_slice_lower_1;
    PyObject *tmp_slice_source_1;
    PyObject *tmp_slice_upper_1;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    int tmp_tried_lineno_1;
    int tmp_tried_lineno_2;
    int tmp_tried_lineno_3;
    int tmp_tried_lineno_4;
    int tmp_tried_lineno_5;
    int tmp_tried_lineno_6;
    PyObject *tmp_tuple_element_1;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_a94fe701d8d9ee6a72feccc7707afaae, module_xlrd$biffh );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_min );
    if ( tmp_called_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 542;
        goto frame_exception_exit_1;
    }
    tmp_left_name_1 = par_ofs;

    tmp_right_name_1 = par_dlen;

    tmp_args_element_name_1 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    if ( tmp_args_element_name_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 542;
        goto frame_exception_exit_1;
    }
    tmp_len_arg_1 = par_strg;

    tmp_args_element_name_2 = BUILTIN_LEN( tmp_len_arg_1 );
    if ( tmp_args_element_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_element_name_1 );

        frame_function->f_lineno = 542;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 542;
    tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, tmp_args_element_name_1, tmp_args_element_name_2 );
    Py_DECREF( tmp_args_element_name_1 );
    Py_DECREF( tmp_args_element_name_2 );
    if ( tmp_assign_source_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 542;
        goto frame_exception_exit_1;
    }
    assert( var_endpos == NULL );
    var_endpos = tmp_assign_source_1;

    tmp_assign_source_2 = par_ofs;

    assert( var_pos == NULL );
    Py_INCREF( tmp_assign_source_2 );
    var_pos = tmp_assign_source_2;

    tmp_operand_name_1 = par_unnumbered;

    tmp_assign_source_3 = UNARY_OPERATION( UNARY_NOT, tmp_operand_name_1 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 544;
        goto frame_exception_exit_1;
    }
    assert( var_numbered == NULL );
    Py_INCREF( tmp_assign_source_3 );
    var_numbered = tmp_assign_source_3;

    tmp_assign_source_4 = const_str_empty;
    assert( var_num_prefix == NULL );
    Py_INCREF( tmp_assign_source_4 );
    var_num_prefix = tmp_assign_source_4;

    loop_start_1:;
    tmp_compare_left_1 = var_pos;

    if ( tmp_compare_left_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 546;
        goto frame_exception_exit_1;
    }

    tmp_compare_right_1 = var_endpos;

    tmp_cmp_Lt_1 = RICH_COMPARE_BOOL_LT( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_cmp_Lt_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 546;
        goto frame_exception_exit_1;
    }
    if (tmp_cmp_Lt_1 == 1)
    {
        goto branch_no_1;
    }
    else
    {
        goto branch_yes_1;
    }
    branch_yes_1:;
    goto loop_end_1;
    branch_no_1:;
    tmp_called_name_2 = LOOKUP_BUILTIN( const_str_plain_min );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 547;
        goto frame_exception_exit_1;
    }
    tmp_left_name_2 = var_pos;

    if ( tmp_left_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 547;
        goto frame_exception_exit_1;
    }

    tmp_right_name_2 = const_int_pos_16;
    tmp_args_element_name_3 = BINARY_OPERATION_ADD( tmp_left_name_2, tmp_right_name_2 );
    if ( tmp_args_element_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 547;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_4 = var_endpos;

    frame_function->f_lineno = 547;
    tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, tmp_args_element_name_3, tmp_args_element_name_4 );
    Py_DECREF( tmp_args_element_name_3 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 547;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_endsub;
        var_endsub = tmp_assign_source_5;
        Py_XDECREF( old );
    }

    tmp_slice_source_1 = par_strg;

    tmp_slice_lower_1 = var_pos;

    if ( tmp_slice_lower_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 548;
        goto frame_exception_exit_1;
    }

    tmp_slice_upper_1 = var_endsub;

    tmp_assign_source_6 = LOOKUP_SLICE( tmp_slice_source_1, tmp_slice_lower_1, tmp_slice_upper_1 );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 548;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_substrg;
        var_substrg = tmp_assign_source_6;
        Py_XDECREF( old );
    }

    tmp_left_name_3 = var_endsub;

    tmp_right_name_3 = var_pos;

    if ( tmp_right_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 549;
        goto frame_exception_exit_1;
    }

    tmp_assign_source_7 = BINARY_OPERATION_SUB( tmp_left_name_3, tmp_right_name_3 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 549;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_lensub;
        var_lensub = tmp_assign_source_7;
        Py_XDECREF( old );
    }

    tmp_break_1 = false;
    // Tried code
    tmp_cond_value_1 = NULL;
    // Tried code
    tmp_compexpr_left_1 = var_lensub;

    tmp_compexpr_right_1 = const_int_0;
    tmp_assign_source_8 = RICH_COMPARE_LE( tmp_compexpr_left_1, tmp_compexpr_right_1 );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 550;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = tmp_or_1__value_1;
        tmp_or_1__value_1 = tmp_assign_source_8;
        Py_XDECREF( old );
    }

    tmp_cond_value_2 = tmp_or_1__value_1;

    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 550;
        goto try_finally_handler_3;
    }
    if (tmp_cond_truth_2 == 1)
    {
        goto condexpr_true_1;
    }
    else
    {
        goto condexpr_false_1;
    }
    condexpr_true_1:;
    tmp_cond_value_1 = tmp_or_1__value_1;

    Py_INCREF( tmp_cond_value_1 );
    goto condexpr_end_1;
    condexpr_false_1:;
    tmp_cond_value_1 = NULL;
    // Tried code
    tmp_result = tmp_or_1__value_1 != NULL;
    if ( tmp_result == true )
    {
        Py_DECREF( tmp_or_1__value_1 );
        tmp_or_1__value_1 = NULL;
    }

    assert( tmp_result != false );
    tmp_compexpr_left_2 = var_lensub;

    tmp_len_arg_2 = var_substrg;

    tmp_compexpr_right_2 = BUILTIN_LEN( tmp_len_arg_2 );
    if ( tmp_compexpr_right_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 550;
        goto try_finally_handler_4;
    }
    tmp_cond_value_1 = RICH_COMPARE_NE( tmp_compexpr_left_2, tmp_compexpr_right_2 );
    Py_DECREF( tmp_compexpr_right_2 );
    if ( tmp_cond_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 550;
        goto try_finally_handler_4;
    }
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto try_finally_handler_3;
    }

    goto finally_end_1;
    finally_end_1:;
    condexpr_end_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_2 != NULL )
    {
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;

        goto try_finally_handler_2;
    }

    goto finally_end_2;
    finally_end_2:;
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        frame_function->f_lineno = 550;
        goto try_finally_handler_2;
    }
    Py_DECREF( tmp_cond_value_1 );
    if (tmp_cond_truth_1 == 1)
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_called_name_3 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_fprintf );

    if (unlikely( tmp_called_name_3 == NULL ))
    {
        tmp_called_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fprintf );
    }

    if ( tmp_called_name_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10410 ], 36, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 551;
        goto try_finally_handler_2;
    }

    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 639 ], 32, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 552;
        goto try_finally_handler_2;
    }

    tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_stdout );
    if ( tmp_args_element_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 552;
        goto try_finally_handler_2;
    }
    tmp_args_element_name_6 = const_str_digest_279b5d5b7f24193f9814ec32a3d4e5c8;
    tmp_args_element_name_7 = par_ofs;

    tmp_args_element_name_8 = par_dlen;

    tmp_args_element_name_9 = par_base;

    tmp_args_element_name_10 = var_endpos;

    tmp_args_element_name_11 = var_pos;

    if ( tmp_args_element_name_11 == NULL )
    {
        Py_DECREF( tmp_args_element_name_5 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 554;
        goto try_finally_handler_2;
    }

    tmp_args_element_name_12 = var_endsub;

    tmp_args_element_name_13 = var_substrg;

    frame_function->f_lineno = 554;
    tmp_unused = CALL_FUNCTION_WITH_ARGS9( tmp_called_name_3, tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9, tmp_args_element_name_10, tmp_args_element_name_11, tmp_args_element_name_12, tmp_args_element_name_13 );
    Py_DECREF( tmp_args_element_name_5 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 554;
        goto try_finally_handler_2;
    }
    Py_DECREF( tmp_unused );
    tmp_break_1 = true;
    goto try_finally_handler_start_2;
    branch_no_2:;
    try_finally_handler_start_2:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_1 = frame_function->f_lineno;
    Py_XDECREF( tmp_or_1__value_1 );
    tmp_or_1__value_1 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_1;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_3 != NULL )
    {
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;

        goto frame_exception_exit_1;
    }

    // Break if entered via break.
    if ( tmp_break_1 )
    {

    goto loop_end_1;
    }
    goto finally_end_3;
    finally_end_3:;
    tmp_source_name_2 = const_str_empty;
    tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_join );
    if ( tmp_called_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 556;
        goto frame_exception_exit_1;
    }
    tmp_args_name_1 = NULL;
    // Tried code
    tmp_args_name_1 = PyTuple_New( 1 );
    tmp_tuple_element_1 = NULL;
    // Tried code
    tmp_iter_arg_1 = var_substrg;

    tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_1 );
    if ( tmp_assign_source_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 556;
        goto try_finally_handler_6;
    }
    {
        PyObject *old = tmp_listcontr_1__listcontr_iter;
        tmp_listcontr_1__listcontr_iter = tmp_assign_source_10;
        Py_XDECREF( old );
    }

    tmp_assign_source_11 = PyList_New( 0 );
    {
        PyObject *old = tmp_listcontr_1__listcontr_result;
        tmp_listcontr_1__listcontr_result = tmp_assign_source_11;
        Py_XDECREF( old );
    }

    loop_start_2:;
    tmp_next_source_1 = tmp_listcontr_1__listcontr_iter;

    tmp_assign_source_12 = ITERATOR_NEXT( tmp_next_source_1 );
    if ( tmp_assign_source_12 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_2;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 556;
            goto try_finally_handler_6;
        }
    }

    {
        PyObject *old = tmp_listcontr_1__iter_value_0;
        tmp_listcontr_1__iter_value_0 = tmp_assign_source_12;
        Py_XDECREF( old );
    }

    tmp_assign_source_13 = tmp_listcontr_1__iter_value_0;

    {
        PyObject *old = var_c;
        var_c = tmp_assign_source_13;
        Py_INCREF( var_c );
        Py_XDECREF( old );
    }

    tmp_append_to_1 = tmp_listcontr_1__listcontr_result;

    tmp_left_name_4 = const_str_digest_c8684a7fbdb42f7b66b506461dffd581;
    tmp_called_name_5 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_BYTES_ORD );

    if (unlikely( tmp_called_name_5 == NULL ))
    {
        tmp_called_name_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BYTES_ORD );
    }

    if ( tmp_called_name_5 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10668 ], 38, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 556;
        goto try_finally_handler_6;
    }

    tmp_args_element_name_14 = var_c;

    frame_function->f_lineno = 556;
    tmp_right_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, tmp_args_element_name_14 );
    if ( tmp_right_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 556;
        goto try_finally_handler_6;
    }
    tmp_append_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
    Py_DECREF( tmp_right_name_4 );
    if ( tmp_append_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 556;
        goto try_finally_handler_6;
    }
    tmp_res = PyList_Append( tmp_append_to_1, tmp_append_value_1 );
    Py_DECREF( tmp_append_value_1 );
    if ( tmp_res == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 556;
        goto try_finally_handler_6;
    }
    tmp_unused = Py_None;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 556;
        goto try_finally_handler_6;
    }
    goto loop_start_2;
    loop_end_2:;
    tmp_tuple_element_1 = tmp_listcontr_1__listcontr_result;

    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_6:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_2 = frame_function->f_lineno;
    Py_XDECREF( tmp_listcontr_1__iter_value_0 );
    tmp_listcontr_1__iter_value_0 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_2;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_4 != NULL )
    {
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;

        goto try_finally_handler_5;
    }

    goto finally_end_4;
    finally_end_4:;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_3 = frame_function->f_lineno;
    Py_XDECREF( tmp_listcontr_1__listcontr_result );
    tmp_listcontr_1__listcontr_result = NULL;

    Py_XDECREF( tmp_listcontr_1__listcontr_iter );
    tmp_listcontr_1__listcontr_iter = NULL;

    frame_function->f_lineno = tmp_tried_lineno_3;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_5 != NULL )
    {
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;

        goto frame_exception_exit_1;
    }

    goto finally_end_5;
    finally_end_5:;
    frame_function->f_lineno = 556;
    tmp_assign_source_9 = CALL_FUNCTION_WITH_POSARGS( tmp_called_name_4, tmp_args_name_1 );
    Py_DECREF( tmp_called_name_4 );
    Py_DECREF( tmp_args_name_1 );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 556;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_hexd;
        var_hexd = tmp_assign_source_9;
        Py_XDECREF( old );
    }

    tmp_assign_source_14 = const_str_empty;
    {
        PyObject *old = var_chard;
        var_chard = tmp_assign_source_14;
        Py_INCREF( var_chard );
        Py_XDECREF( old );
    }

    tmp_iter_arg_2 = var_substrg;

    tmp_assign_source_15 = MAKE_ITERATOR( tmp_iter_arg_2 );
    if ( tmp_assign_source_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 559;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = tmp_for_loop_1__for_iterator;
        tmp_for_loop_1__for_iterator = tmp_assign_source_15;
        Py_XDECREF( old );
    }

    // Tried code
    loop_start_3:;
    tmp_next_source_2 = tmp_for_loop_1__for_iterator;

    tmp_assign_source_16 = ITERATOR_NEXT( tmp_next_source_2 );
    if ( tmp_assign_source_16 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_3;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 559;
            goto try_finally_handler_7;
        }
    }

    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_16;
        Py_XDECREF( old );
    }

    tmp_assign_source_17 = tmp_for_loop_1__iter_value;

    {
        PyObject *old = var_c;
        var_c = tmp_assign_source_17;
        Py_INCREF( var_c );
        Py_XDECREF( old );
    }

    tmp_called_name_6 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_BYTES_ORD );

    if (unlikely( tmp_called_name_6 == NULL ))
    {
        tmp_called_name_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BYTES_ORD );
    }

    if ( tmp_called_name_6 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10668 ], 38, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 560;
        goto try_finally_handler_7;
    }

    tmp_args_element_name_15 = var_c;

    frame_function->f_lineno = 560;
    tmp_chr_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, tmp_args_element_name_15 );
    if ( tmp_chr_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 560;
        goto try_finally_handler_7;
    }
    tmp_assign_source_18 = BUILTIN_CHR( tmp_chr_arg_1 );
    Py_DECREF( tmp_chr_arg_1 );
    if ( tmp_assign_source_18 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 560;
        goto try_finally_handler_7;
    }
    {
        PyObject *old = var_c;
        assert( old != NULL );
        var_c = tmp_assign_source_18;
        Py_DECREF( old );
    }

    tmp_compare_left_2 = var_c;

    tmp_compare_right_2 = const_str_chr_0;
    tmp_cmp_Eq_1 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_2, tmp_compare_right_2 );
    if ( tmp_cmp_Eq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 561;
        goto try_finally_handler_7;
    }
    if (tmp_cmp_Eq_1 == 1)
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_assign_source_19 = const_str_chr_126;
    {
        PyObject *old = var_c;
        assert( old != NULL );
        var_c = tmp_assign_source_19;
        Py_INCREF( var_c );
        Py_DECREF( old );
    }

    goto branch_end_3;
    branch_no_3:;
    tmp_cond_value_3 = NULL;
    // Tried code
    tmp_cond_value_3 = NULL;
    // Tried code
    tmp_cond_value_3 = NULL;
    // Tried code
    tmp_compexpr_left_3 = const_str_space;
    tmp_compexpr_right_3 = NULL;
    // Tried code
    tmp_assign_source_21 = var_c;

    {
        PyObject *old = tmp_comparison_1__value_2;
        tmp_comparison_1__value_2 = tmp_assign_source_21;
        Py_INCREF( tmp_comparison_1__value_2 );
        Py_XDECREF( old );
    }

    tmp_compexpr_right_3 = tmp_comparison_1__value_2;

    // Re-raise as necessary after finally was executed.
    goto finally_end_6;
    finally_end_6:;
    tmp_assign_source_20 = RICH_COMPARE_LE( tmp_compexpr_left_3, tmp_compexpr_right_3 );
    if ( tmp_assign_source_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 563;
        goto try_finally_handler_10;
    }
    {
        PyObject *old = tmp_and_1__value_1;
        tmp_and_1__value_1 = tmp_assign_source_20;
        Py_XDECREF( old );
    }

    tmp_cond_value_4 = tmp_and_1__value_1;

    tmp_cond_truth_4 = CHECK_IF_TRUE( tmp_cond_value_4 );
    if ( tmp_cond_truth_4 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 563;
        goto try_finally_handler_10;
    }
    if (tmp_cond_truth_4 == 1)
    {
        goto condexpr_true_2;
    }
    else
    {
        goto condexpr_false_2;
    }
    condexpr_true_2:;
    tmp_cond_value_3 = NULL;
    // Tried code
    tmp_result = tmp_and_1__value_1 != NULL;
    if ( tmp_result == true )
    {
        Py_DECREF( tmp_and_1__value_1 );
        tmp_and_1__value_1 = NULL;
    }

    assert( tmp_result != false );
    tmp_compexpr_left_4 = tmp_comparison_1__value_2;

    tmp_compexpr_right_4 = const_str_chr_126;
    tmp_cond_value_3 = RICH_COMPARE_LE( tmp_compexpr_left_4, tmp_compexpr_right_4 );
    if ( tmp_cond_value_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 563;
        goto try_finally_handler_12;
    }
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_12:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_6 != NULL )
    {
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;

        goto try_finally_handler_10;
    }

    goto finally_end_7;
    finally_end_7:;
    goto condexpr_end_2;
    condexpr_false_2:;
    tmp_cond_value_3 = tmp_and_1__value_1;

    Py_INCREF( tmp_cond_value_3 );
    condexpr_end_2:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_10:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_7 != NULL )
    {
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;

        goto try_finally_handler_9;
    }

    goto finally_end_8;
    finally_end_8:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_9:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_4 = frame_function->f_lineno;
    Py_XDECREF( tmp_comparison_1__value_2 );
    tmp_comparison_1__value_2 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_4;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_8 != NULL )
    {
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;

        goto try_finally_handler_8;
    }

    goto finally_end_9;
    finally_end_9:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_8:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_5 = frame_function->f_lineno;
    Py_XDECREF( tmp_and_1__value_1 );
    tmp_and_1__value_1 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_5;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_9 != NULL )
    {
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;

        goto try_finally_handler_7;
    }

    goto finally_end_10;
    finally_end_10:;
    tmp_cond_truth_3 = CHECK_IF_TRUE( tmp_cond_value_3 );
    if ( tmp_cond_truth_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_3 );

        frame_function->f_lineno = 563;
        goto try_finally_handler_7;
    }
    Py_DECREF( tmp_cond_value_3 );
    if (tmp_cond_truth_3 == 1)
    {
        goto branch_no_4;
    }
    else
    {
        goto branch_yes_4;
    }
    branch_yes_4:;
    tmp_assign_source_22 = const_str_chr_63;
    {
        PyObject *old = var_c;
        assert( old != NULL );
        var_c = tmp_assign_source_22;
        Py_INCREF( var_c );
        Py_DECREF( old );
    }

    branch_no_4:;
    branch_end_3:;
    tmp_left_name_5 = var_chard;

    if ( tmp_left_name_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11043 ], 51, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 565;
        goto try_finally_handler_7;
    }

    tmp_right_name_5 = var_c;

    if ( tmp_right_name_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11094 ], 47, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 565;
        goto try_finally_handler_7;
    }

    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_5, tmp_right_name_5 );
    tmp_assign_source_23 = tmp_left_name_5;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 565;
        goto try_finally_handler_7;
    }
    var_chard = tmp_assign_source_23;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 559;
        goto try_finally_handler_7;
    }
    goto loop_start_3;
    loop_end_3:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_7:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_6 = frame_function->f_lineno;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    frame_function->f_lineno = tmp_tried_lineno_6;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_10 != NULL )
    {
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;

        goto frame_exception_exit_1;
    }

    goto finally_end_11;
    finally_end_11:;
    tmp_cond_value_5 = var_numbered;

    tmp_cond_truth_5 = CHECK_IF_TRUE( tmp_cond_value_5 );
    if ( tmp_cond_truth_5 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 566;
        goto frame_exception_exit_1;
    }
    if (tmp_cond_truth_5 == 1)
    {
        goto branch_yes_5;
    }
    else
    {
        goto branch_no_5;
    }
    branch_yes_5:;
    tmp_left_name_6 = const_str_digest_0245eef4613ec410273d22c8f101bc78;
    tmp_left_name_8 = par_base;

    tmp_right_name_7 = var_pos;

    if ( tmp_right_name_7 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 567;
        goto frame_exception_exit_1;
    }

    tmp_left_name_7 = BINARY_OPERATION_ADD( tmp_left_name_8, tmp_right_name_7 );
    if ( tmp_left_name_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 567;
        goto frame_exception_exit_1;
    }
    tmp_right_name_8 = par_ofs;

    tmp_right_name_6 = BINARY_OPERATION_SUB( tmp_left_name_7, tmp_right_name_8 );
    Py_DECREF( tmp_left_name_7 );
    if ( tmp_right_name_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 567;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_24 = BINARY_OPERATION_REMAINDER( tmp_left_name_6, tmp_right_name_6 );
    Py_DECREF( tmp_right_name_6 );
    if ( tmp_assign_source_24 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 567;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_num_prefix;
        var_num_prefix = tmp_assign_source_24;
        Py_XDECREF( old );
    }

    branch_no_5:;
    tmp_called_name_7 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_fprintf );

    if (unlikely( tmp_called_name_7 == NULL ))
    {
        tmp_called_name_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fprintf );
    }

    if ( tmp_called_name_7 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10410 ], 36, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 569;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_16 = par_fout;

    tmp_args_element_name_17 = const_str_digest_9d755e04da81084ffe307f7654bf55a3;
    tmp_args_element_name_18 = var_num_prefix;

    if ( tmp_args_element_name_18 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11141 ], 56, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 569;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_19 = var_hexd;

    tmp_args_element_name_20 = var_chard;

    if ( tmp_args_element_name_20 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11043 ], 51, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 569;
        goto frame_exception_exit_1;
    }

    frame_function->f_lineno = 569;
    tmp_unused = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_7, tmp_args_element_name_16, tmp_args_element_name_17, tmp_args_element_name_18, tmp_args_element_name_19, tmp_args_element_name_20 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 569;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_assign_source_25 = var_endsub;

    {
        PyObject *old = var_pos;
        var_pos = tmp_assign_source_25;
        Py_INCREF( var_pos );
        Py_XDECREF( old );
    }

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 546;
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var_endpos != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_endpos,
            var_endpos
        );
        assert( tmp_res != -1 );

    }
    if ( var_pos != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_pos,
            var_pos
        );
        assert( tmp_res != -1 );

    }
    if ( var_numbered != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_numbered,
            var_numbered
        );
        assert( tmp_res != -1 );

    }
    if ( var_num_prefix != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_num_prefix,
            var_num_prefix
        );
        assert( tmp_res != -1 );

    }
    if ( var_endsub != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_endsub,
            var_endsub
        );
        assert( tmp_res != -1 );

    }
    if ( var_substrg != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_substrg,
            var_substrg
        );
        assert( tmp_res != -1 );

    }
    if ( var_lensub != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_lensub,
            var_lensub
        );
        assert( tmp_res != -1 );

    }
    if ( var_c != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_c,
            var_c
        );
        assert( tmp_res != -1 );

    }
    if ( var_hexd != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_hexd,
            var_hexd
        );
        assert( tmp_res != -1 );

    }
    if ( var_chard != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_chard,
            var_chard
        );
        assert( tmp_res != -1 );

    }
    if ( par_strg != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_strg,
            par_strg
        );
        assert( tmp_res != -1 );

    }
    if ( par_ofs != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_ofs,
            par_ofs
        );
        assert( tmp_res != -1 );

    }
    if ( par_dlen != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_dlen,
            par_dlen
        );
        assert( tmp_res != -1 );

    }
    if ( par_base != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_base,
            par_base
        );
        assert( tmp_res != -1 );

    }
    if ( par_fout != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_fout,
            par_fout
        );
        assert( tmp_res != -1 );

    }
    if ( par_unnumbered != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_unnumbered,
            par_unnumbered
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_finally_handler_start_1;
    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_strg );
    Py_DECREF( par_strg );
    par_strg = NULL;

    CHECK_OBJECT( (PyObject *)par_ofs );
    Py_DECREF( par_ofs );
    par_ofs = NULL;

    CHECK_OBJECT( (PyObject *)par_dlen );
    Py_DECREF( par_dlen );
    par_dlen = NULL;

    CHECK_OBJECT( (PyObject *)par_base );
    Py_DECREF( par_base );
    par_base = NULL;

    CHECK_OBJECT( (PyObject *)par_fout );
    Py_DECREF( par_fout );
    par_fout = NULL;

    CHECK_OBJECT( (PyObject *)par_unnumbered );
    Py_DECREF( par_unnumbered );
    par_unnumbered = NULL;

    Py_XDECREF( var_endpos );
    var_endpos = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var_numbered );
    var_numbered = NULL;

    Py_XDECREF( var_num_prefix );
    var_num_prefix = NULL;

    Py_XDECREF( var_endsub );
    var_endsub = NULL;

    Py_XDECREF( var_substrg );
    var_substrg = NULL;

    Py_XDECREF( var_lensub );
    var_lensub = NULL;

    Py_XDECREF( var_c );
    var_c = NULL;

    Py_XDECREF( var_hexd );
    var_hexd = NULL;

    Py_XDECREF( var_chard );
    var_chard = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_11 != NULL )
    {
        exception_type = exception_keeper_type_11;
        exception_value = exception_keeper_value_11;
        exception_tb = exception_keeper_tb_11;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_12;
    finally_end_12:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_11_hex_char_dump_of_xlrd$biffh );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_11_hex_char_dump_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_strg = NULL;
    PyObject *_python_par_ofs = NULL;
    PyObject *_python_par_dlen = NULL;
    PyObject *_python_par_base = NULL;
    PyObject *_python_par_fout = NULL;
    PyObject *_python_par_unnumbered = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "hex_char_dump() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_strg == key )
            {
                assert( _python_par_strg == NULL );
                _python_par_strg = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_ofs == key )
            {
                assert( _python_par_ofs == NULL );
                _python_par_ofs = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_dlen == key )
            {
                assert( _python_par_dlen == NULL );
                _python_par_dlen = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_base == key )
            {
                assert( _python_par_base == NULL );
                _python_par_base = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_fout == key )
            {
                assert( _python_par_fout == NULL );
                _python_par_fout = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_unnumbered == key )
            {
                assert( _python_par_unnumbered == NULL );
                _python_par_unnumbered = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_strg, key ) == 1 )
            {
                assert( _python_par_strg == NULL );
                _python_par_strg = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_ofs, key ) == 1 )
            {
                assert( _python_par_ofs == NULL );
                _python_par_ofs = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_dlen, key ) == 1 )
            {
                assert( _python_par_dlen == NULL );
                _python_par_dlen = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_base, key ) == 1 )
            {
                assert( _python_par_base == NULL );
                _python_par_base = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_fout, key ) == 1 )
            {
                assert( _python_par_fout == NULL );
                _python_par_fout = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_unnumbered, key ) == 1 )
            {
                assert( _python_par_unnumbered == NULL );
                _python_par_unnumbered = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "hex_char_dump() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 6 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_strg != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_strg = args[ 0 ];
        Py_INCREF( _python_par_strg );
    }
    else if ( _python_par_strg == NULL )
    {
        if ( 0 + self->m_defaults_given >= 6  )
        {
            _python_par_strg = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 6 );
            Py_INCREF( _python_par_strg );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 1 < args_given ))
    {
         if (unlikely( _python_par_ofs != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 1 );
             goto error_exit;
         }

        _python_par_ofs = args[ 1 ];
        Py_INCREF( _python_par_ofs );
    }
    else if ( _python_par_ofs == NULL )
    {
        if ( 1 + self->m_defaults_given >= 6  )
        {
            _python_par_ofs = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 1 - 6 );
            Py_INCREF( _python_par_ofs );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 2 < args_given ))
    {
         if (unlikely( _python_par_dlen != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 2 );
             goto error_exit;
         }

        _python_par_dlen = args[ 2 ];
        Py_INCREF( _python_par_dlen );
    }
    else if ( _python_par_dlen == NULL )
    {
        if ( 2 + self->m_defaults_given >= 6  )
        {
            _python_par_dlen = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 2 - 6 );
            Py_INCREF( _python_par_dlen );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 3 < args_given ))
    {
         if (unlikely( _python_par_base != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 3 );
             goto error_exit;
         }

        _python_par_base = args[ 3 ];
        Py_INCREF( _python_par_base );
    }
    else if ( _python_par_base == NULL )
    {
        if ( 3 + self->m_defaults_given >= 6  )
        {
            _python_par_base = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 3 - 6 );
            Py_INCREF( _python_par_base );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 4 < args_given ))
    {
         if (unlikely( _python_par_fout != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 4 );
             goto error_exit;
         }

        _python_par_fout = args[ 4 ];
        Py_INCREF( _python_par_fout );
    }
    else if ( _python_par_fout == NULL )
    {
        if ( 4 + self->m_defaults_given >= 6  )
        {
            _python_par_fout = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 4 - 6 );
            Py_INCREF( _python_par_fout );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 5 < args_given ))
    {
         if (unlikely( _python_par_unnumbered != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 5 );
             goto error_exit;
         }

        _python_par_unnumbered = args[ 5 ];
        Py_INCREF( _python_par_unnumbered );
    }
    else if ( _python_par_unnumbered == NULL )
    {
        if ( 5 + self->m_defaults_given >= 6  )
        {
            _python_par_unnumbered = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 5 - 6 );
            Py_INCREF( _python_par_unnumbered );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_strg == NULL || _python_par_ofs == NULL || _python_par_dlen == NULL || _python_par_base == NULL || _python_par_fout == NULL || _python_par_unnumbered == NULL ))
    {
        PyObject *values[] = { _python_par_strg, _python_par_ofs, _python_par_dlen, _python_par_base, _python_par_fout, _python_par_unnumbered };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_11_hex_char_dump_of_xlrd$biffh( self, _python_par_strg, _python_par_ofs, _python_par_dlen, _python_par_base, _python_par_fout, _python_par_unnumbered );

error_exit:;

    Py_XDECREF( _python_par_strg );
    Py_XDECREF( _python_par_ofs );
    Py_XDECREF( _python_par_dlen );
    Py_XDECREF( _python_par_base );
    Py_XDECREF( _python_par_fout );
    Py_XDECREF( _python_par_unnumbered );

    return NULL;
}

static PyObject *dparse_function_11_hex_char_dump_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 6 )
    {
        return impl_function_11_hex_char_dump_of_xlrd$biffh( self, INCREASE_REFCOUNT( args[ 0 ] ), INCREASE_REFCOUNT( args[ 1 ] ), INCREASE_REFCOUNT( args[ 2 ] ), INCREASE_REFCOUNT( args[ 3 ] ), INCREASE_REFCOUNT( args[ 4 ] ), INCREASE_REFCOUNT( args[ 5 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_11_hex_char_dump_of_xlrd$biffh( self, args, size, NULL );
        return result;
    }

}



static PyObject *impl_function_12_biff_dump_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject *_python_par_mem, PyObject *_python_par_stream_offset, PyObject *_python_par_stream_len, PyObject *_python_par_base, PyObject *_python_par_fout, PyObject *_python_par_unnumbered )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mem = _python_par_mem;
    PyObject *par_stream_offset = _python_par_stream_offset;
    PyObject *par_stream_len = _python_par_stream_len;
    PyObject *par_base = _python_par_base;
    PyObject *par_fout = _python_par_fout;
    PyObject *par_unnumbered = _python_par_unnumbered;
    PyObject *var_pos = NULL;
    PyObject *var_stream_end = NULL;
    PyObject *var_adj = NULL;
    PyObject *var_dummies = NULL;
    PyObject *var_numbered = NULL;
    PyObject *var_num_prefix = NULL;
    PyObject *var_rc = NULL;
    PyObject *var_length = NULL;
    PyObject *var_savpos = NULL;
    PyObject *var_recname = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_and_1__value_1 = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_args_element_name_7;
    PyObject *tmp_args_element_name_8;
    PyObject *tmp_args_element_name_9;
    PyObject *tmp_args_element_name_10;
    PyObject *tmp_args_element_name_11;
    PyObject *tmp_args_element_name_12;
    PyObject *tmp_args_element_name_13;
    PyObject *tmp_args_element_name_14;
    PyObject *tmp_args_element_name_15;
    PyObject *tmp_args_element_name_16;
    PyObject *tmp_args_element_name_17;
    PyObject *tmp_args_element_name_18;
    PyObject *tmp_args_element_name_19;
    PyObject *tmp_args_element_name_20;
    PyObject *tmp_args_element_name_21;
    PyObject *tmp_args_element_name_22;
    PyObject *tmp_args_element_name_23;
    PyObject *tmp_args_element_name_24;
    PyObject *tmp_args_element_name_25;
    PyObject *tmp_args_element_name_26;
    PyObject *tmp_args_element_name_27;
    PyObject *tmp_args_element_name_28;
    PyObject *tmp_args_element_name_29;
    PyObject *tmp_args_element_name_30;
    PyObject *tmp_args_element_name_31;
    PyObject *tmp_args_element_name_32;
    PyObject *tmp_args_element_name_33;
    PyObject *tmp_args_element_name_34;
    PyObject *tmp_args_element_name_35;
    PyObject *tmp_args_element_name_36;
    PyObject *tmp_args_element_name_37;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    bool tmp_break_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    PyObject *tmp_called_name_6;
    PyObject *tmp_called_name_7;
    PyObject *tmp_called_name_8;
    PyObject *tmp_called_name_9;
    int tmp_cmp_Eq_1;
    int tmp_cmp_Gt_1;
    int tmp_cmp_GtE_1;
    int tmp_cmp_Lt_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_left_4;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    PyObject *tmp_compare_right_4;
    PyObject *tmp_compexpr_left_1;
    PyObject *tmp_compexpr_left_2;
    PyObject *tmp_compexpr_right_1;
    PyObject *tmp_compexpr_right_2;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    int tmp_cond_truth_3;
    int tmp_cond_truth_4;
    int tmp_cond_truth_5;
    int tmp_cond_truth_6;
    int tmp_cond_truth_7;
    int tmp_cond_truth_8;
    int tmp_cond_truth_9;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_cond_value_3;
    PyObject *tmp_cond_value_4;
    PyObject *tmp_cond_value_5;
    PyObject *tmp_cond_value_6;
    PyObject *tmp_cond_value_7;
    PyObject *tmp_cond_value_8;
    PyObject *tmp_cond_value_9;
    PyObject *tmp_frame_locals;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_left_name_4;
    PyObject *tmp_left_name_5;
    PyObject *tmp_left_name_6;
    PyObject *tmp_left_name_7;
    PyObject *tmp_left_name_8;
    PyObject *tmp_left_name_9;
    PyObject *tmp_left_name_10;
    PyObject *tmp_left_name_11;
    PyObject *tmp_left_name_12;
    PyObject *tmp_left_name_13;
    PyObject *tmp_left_name_14;
    PyObject *tmp_left_name_15;
    PyObject *tmp_left_name_16;
    PyObject *tmp_left_name_17;
    PyObject *tmp_left_name_18;
    PyObject *tmp_left_name_19;
    PyObject *tmp_left_name_20;
    PyObject *tmp_left_name_21;
    PyObject *tmp_left_name_22;
    PyObject *tmp_operand_name_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_right_name_4;
    PyObject *tmp_right_name_5;
    PyObject *tmp_right_name_6;
    PyObject *tmp_right_name_7;
    PyObject *tmp_right_name_8;
    PyObject *tmp_right_name_9;
    PyObject *tmp_right_name_10;
    PyObject *tmp_right_name_11;
    PyObject *tmp_right_name_12;
    PyObject *tmp_right_name_13;
    PyObject *tmp_right_name_14;
    PyObject *tmp_right_name_15;
    PyObject *tmp_right_name_16;
    PyObject *tmp_right_name_17;
    PyObject *tmp_right_name_18;
    PyObject *tmp_right_name_19;
    PyObject *tmp_right_name_20;
    PyObject *tmp_right_name_21;
    PyObject *tmp_right_name_22;
    PyObject *tmp_slice_lower_1;
    PyObject *tmp_slice_lower_2;
    PyObject *tmp_slice_source_1;
    PyObject *tmp_slice_source_2;
    PyObject *tmp_slice_upper_1;
    PyObject *tmp_source_name_1;
    int tmp_tried_lineno_1;
    int tmp_tried_lineno_2;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = par_stream_offset;

    assert( var_pos == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var_pos = tmp_assign_source_1;

    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_97e99faadaa018455f7decf994313044, module_xlrd$biffh );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_left_name_1 = par_stream_offset;

    tmp_right_name_1 = par_stream_len;

    tmp_assign_source_2 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 574;
        goto frame_exception_exit_1;
    }
    assert( var_stream_end == NULL );
    var_stream_end = tmp_assign_source_2;

    tmp_left_name_2 = par_base;

    tmp_right_name_2 = par_stream_offset;

    tmp_assign_source_3 = BINARY_OPERATION_SUB( tmp_left_name_2, tmp_right_name_2 );
    if ( tmp_assign_source_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 575;
        goto frame_exception_exit_1;
    }
    assert( var_adj == NULL );
    var_adj = tmp_assign_source_3;

    tmp_assign_source_4 = const_int_0;
    assert( var_dummies == NULL );
    Py_INCREF( tmp_assign_source_4 );
    var_dummies = tmp_assign_source_4;

    tmp_operand_name_1 = par_unnumbered;

    tmp_assign_source_5 = UNARY_OPERATION( UNARY_NOT, tmp_operand_name_1 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 577;
        goto frame_exception_exit_1;
    }
    assert( var_numbered == NULL );
    Py_INCREF( tmp_assign_source_5 );
    var_numbered = tmp_assign_source_5;

    tmp_assign_source_6 = const_str_empty;
    assert( var_num_prefix == NULL );
    Py_INCREF( tmp_assign_source_6 );
    var_num_prefix = tmp_assign_source_6;

    loop_start_1:;
    tmp_left_name_3 = var_stream_end;

    tmp_right_name_3 = var_pos;

    if ( tmp_right_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 579;
        goto frame_exception_exit_1;
    }

    tmp_compare_left_1 = BINARY_OPERATION_SUB( tmp_left_name_3, tmp_right_name_3 );
    if ( tmp_compare_left_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 579;
        goto frame_exception_exit_1;
    }
    tmp_compare_right_1 = const_int_pos_4;
    tmp_cmp_GtE_1 = RICH_COMPARE_BOOL_GE( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_cmp_GtE_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_1 );

        frame_function->f_lineno = 579;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_left_1 );
    if (tmp_cmp_GtE_1 == 1)
    {
        goto branch_no_1;
    }
    else
    {
        goto branch_yes_1;
    }
    branch_yes_1:;
    goto loop_end_1;
    branch_no_1:;
    // Tried code
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unpack );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10589 ], 35, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 580;
        goto try_finally_handler_2;
    }

    tmp_args_element_name_1 = const_str_digest_b8d2060bb898fb3742d05b0e0db99007;
    tmp_slice_source_1 = par_mem;

    tmp_slice_lower_1 = var_pos;

    if ( tmp_slice_lower_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 580;
        goto try_finally_handler_2;
    }

    tmp_left_name_4 = var_pos;

    if ( tmp_left_name_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 580;
        goto try_finally_handler_2;
    }

    tmp_right_name_4 = const_int_pos_4;
    tmp_slice_upper_1 = BINARY_OPERATION_ADD( tmp_left_name_4, tmp_right_name_4 );
    if ( tmp_slice_upper_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 580;
        goto try_finally_handler_2;
    }
    tmp_args_element_name_2 = LOOKUP_SLICE( tmp_slice_source_1, tmp_slice_lower_1, tmp_slice_upper_1 );
    Py_DECREF( tmp_slice_upper_1 );
    if ( tmp_args_element_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 580;
        goto try_finally_handler_2;
    }
    frame_function->f_lineno = 580;
    tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, tmp_args_element_name_1, tmp_args_element_name_2 );
    Py_DECREF( tmp_args_element_name_2 );
    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 580;
        goto try_finally_handler_2;
    }
    tmp_assign_source_7 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 580;
        goto try_finally_handler_2;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__source_iter;
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_7;
        Py_XDECREF( old );
    }

    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_1, 0 );
    if ( tmp_assign_source_8 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 580;
        goto try_finally_handler_2;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__element_1;
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_8;
        Py_XDECREF( old );
    }

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_9 = UNPACK_NEXT( tmp_unpack_2, 1 );
    if ( tmp_assign_source_9 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 580;
        goto try_finally_handler_2;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__element_2;
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_9;
        Py_XDECREF( old );
    }

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                goto try_finally_handler_2;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        goto try_finally_handler_2;
    }
    tmp_assign_source_10 = tmp_tuple_unpack_1__element_1;

    {
        PyObject *old = var_rc;
        var_rc = tmp_assign_source_10;
        Py_INCREF( var_rc );
        Py_XDECREF( old );
    }

    tmp_assign_source_11 = tmp_tuple_unpack_1__element_2;

    {
        PyObject *old = var_length;
        var_length = tmp_assign_source_11;
        Py_INCREF( var_length );
        Py_XDECREF( old );
    }

    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_1 = frame_function->f_lineno;
    Py_XDECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_1;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto frame_exception_exit_1;
    }

    goto finally_end_1;
    finally_end_1:;
    tmp_break_1 = false;
    // Tried code
    tmp_cond_value_1 = NULL;
    // Tried code
    tmp_compexpr_left_1 = var_rc;

    if ( tmp_compexpr_left_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11197 ], 48, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 581;
        goto try_finally_handler_4;
    }

    tmp_compexpr_right_1 = const_int_0;
    tmp_assign_source_12 = RICH_COMPARE_EQ( tmp_compexpr_left_1, tmp_compexpr_right_1 );
    if ( tmp_assign_source_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 581;
        goto try_finally_handler_4;
    }
    {
        PyObject *old = tmp_and_1__value_1;
        tmp_and_1__value_1 = tmp_assign_source_12;
        Py_XDECREF( old );
    }

    tmp_cond_value_2 = tmp_and_1__value_1;

    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 581;
        goto try_finally_handler_4;
    }
    if (tmp_cond_truth_2 == 1)
    {
        goto condexpr_true_1;
    }
    else
    {
        goto condexpr_false_1;
    }
    condexpr_true_1:;
    tmp_cond_value_1 = NULL;
    // Tried code
    tmp_result = tmp_and_1__value_1 != NULL;
    if ( tmp_result == true )
    {
        Py_DECREF( tmp_and_1__value_1 );
        tmp_and_1__value_1 = NULL;
    }

    assert( tmp_result != false );
    tmp_compexpr_left_2 = var_length;

    if ( tmp_compexpr_left_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11245 ], 52, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 581;
        goto try_finally_handler_5;
    }

    tmp_compexpr_right_2 = const_int_0;
    tmp_cond_value_1 = RICH_COMPARE_EQ( tmp_compexpr_left_2, tmp_compexpr_right_2 );
    if ( tmp_cond_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 581;
        goto try_finally_handler_5;
    }
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_5:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_2 != NULL )
    {
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;

        goto try_finally_handler_4;
    }

    goto finally_end_2;
    finally_end_2:;
    goto condexpr_end_1;
    condexpr_false_1:;
    tmp_cond_value_1 = tmp_and_1__value_1;

    Py_INCREF( tmp_cond_value_1 );
    condexpr_end_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_3 != NULL )
    {
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;

        goto try_finally_handler_3;
    }

    goto finally_end_3;
    finally_end_3:;
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        frame_function->f_lineno = 581;
        goto try_finally_handler_3;
    }
    Py_DECREF( tmp_cond_value_1 );
    if (tmp_cond_truth_1 == 1)
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_slice_source_2 = par_mem;

    tmp_slice_lower_2 = var_pos;

    if ( tmp_slice_lower_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 582;
        goto try_finally_handler_3;
    }

    tmp_compare_left_2 = LOOKUP_SLICE( tmp_slice_source_2, tmp_slice_lower_2, Py_None );
    if ( tmp_compare_left_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 582;
        goto try_finally_handler_3;
    }
    tmp_left_name_5 = const_str_chr_0;
    tmp_left_name_6 = var_stream_end;

    tmp_right_name_6 = var_pos;

    if ( tmp_right_name_6 == NULL )
    {
        Py_DECREF( tmp_compare_left_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 582;
        goto try_finally_handler_3;
    }

    tmp_right_name_5 = BINARY_OPERATION_SUB( tmp_left_name_6, tmp_right_name_6 );
    if ( tmp_right_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_2 );

        frame_function->f_lineno = 582;
        goto try_finally_handler_3;
    }
    tmp_compare_right_2 = BINARY_OPERATION_MUL( tmp_left_name_5, tmp_right_name_5 );
    Py_DECREF( tmp_right_name_5 );
    if ( tmp_compare_right_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_2 );

        frame_function->f_lineno = 582;
        goto try_finally_handler_3;
    }
    tmp_cmp_Eq_1 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_2, tmp_compare_right_2 );
    if ( tmp_cmp_Eq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_2 );
        Py_DECREF( tmp_compare_right_2 );

        frame_function->f_lineno = 582;
        goto try_finally_handler_3;
    }
    Py_DECREF( tmp_compare_left_2 );
    Py_DECREF( tmp_compare_right_2 );
    if (tmp_cmp_Eq_1 == 1)
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_left_name_7 = var_stream_end;

    tmp_right_name_7 = var_pos;

    if ( tmp_right_name_7 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 583;
        goto try_finally_handler_3;
    }

    tmp_assign_source_13 = BINARY_OPERATION_SUB( tmp_left_name_7, tmp_right_name_7 );
    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 583;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = var_dummies;
        var_dummies = tmp_assign_source_13;
        Py_XDECREF( old );
    }

    tmp_assign_source_14 = var_pos;

    if ( tmp_assign_source_14 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 584;
        goto try_finally_handler_3;
    }

    {
        PyObject *old = var_savpos;
        var_savpos = tmp_assign_source_14;
        Py_INCREF( var_savpos );
        Py_XDECREF( old );
    }

    tmp_assign_source_15 = var_stream_end;

    {
        PyObject *old = var_pos;
        var_pos = tmp_assign_source_15;
        Py_INCREF( var_pos );
        Py_XDECREF( old );
    }

    tmp_break_1 = true;
    goto try_finally_handler_start_2;
    branch_no_3:;
    tmp_cond_value_3 = var_dummies;

    if ( tmp_cond_value_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11297 ], 53, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 587;
        goto try_finally_handler_3;
    }

    tmp_cond_truth_3 = CHECK_IF_TRUE( tmp_cond_value_3 );
    if ( tmp_cond_truth_3 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 587;
        goto try_finally_handler_3;
    }
    if (tmp_cond_truth_3 == 1)
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_left_name_8 = var_dummies;

    if ( tmp_left_name_8 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11297 ], 53, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 588;
        goto try_finally_handler_3;
    }

    tmp_right_name_8 = const_int_pos_4;
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_8, tmp_right_name_8 );
    tmp_assign_source_16 = tmp_left_name_8;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 588;
        goto try_finally_handler_3;
    }
    var_dummies = tmp_assign_source_16;

    goto branch_end_4;
    branch_no_4:;
    tmp_assign_source_17 = var_pos;

    if ( tmp_assign_source_17 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 590;
        goto try_finally_handler_3;
    }

    {
        PyObject *old = var_savpos;
        var_savpos = tmp_assign_source_17;
        Py_INCREF( var_savpos );
        Py_XDECREF( old );
    }

    tmp_assign_source_18 = const_int_pos_4;
    {
        PyObject *old = var_dummies;
        var_dummies = tmp_assign_source_18;
        Py_INCREF( var_dummies );
        Py_XDECREF( old );
    }

    branch_end_4:;
    tmp_left_name_9 = var_pos;

    if ( tmp_left_name_9 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 592;
        goto try_finally_handler_3;
    }

    tmp_right_name_9 = const_int_pos_4;
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_9, tmp_right_name_9 );
    tmp_assign_source_19 = tmp_left_name_9;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 592;
        goto try_finally_handler_3;
    }
    var_pos = tmp_assign_source_19;

    goto branch_end_2;
    branch_no_2:;
    tmp_cond_value_4 = var_dummies;

    if ( tmp_cond_value_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11297 ], 53, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 594;
        goto try_finally_handler_3;
    }

    tmp_cond_truth_4 = CHECK_IF_TRUE( tmp_cond_value_4 );
    if ( tmp_cond_truth_4 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 594;
        goto try_finally_handler_3;
    }
    if (tmp_cond_truth_4 == 1)
    {
        goto branch_yes_5;
    }
    else
    {
        goto branch_no_5;
    }
    branch_yes_5:;
    tmp_cond_value_5 = var_numbered;

    tmp_cond_truth_5 = CHECK_IF_TRUE( tmp_cond_value_5 );
    if ( tmp_cond_truth_5 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 595;
        goto try_finally_handler_3;
    }
    if (tmp_cond_truth_5 == 1)
    {
        goto branch_yes_6;
    }
    else
    {
        goto branch_no_6;
    }
    branch_yes_6:;
    tmp_left_name_10 = const_str_digest_0245eef4613ec410273d22c8f101bc78;
    tmp_left_name_11 = var_adj;

    tmp_right_name_11 = var_savpos;

    if ( tmp_right_name_11 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11350 ], 52, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 596;
        goto try_finally_handler_3;
    }

    tmp_right_name_10 = BINARY_OPERATION_ADD( tmp_left_name_11, tmp_right_name_11 );
    if ( tmp_right_name_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 596;
        goto try_finally_handler_3;
    }
    tmp_assign_source_20 = BINARY_OPERATION_REMAINDER( tmp_left_name_10, tmp_right_name_10 );
    Py_DECREF( tmp_right_name_10 );
    if ( tmp_assign_source_20 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 596;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = var_num_prefix;
        var_num_prefix = tmp_assign_source_20;
        Py_XDECREF( old );
    }

    branch_no_6:;
    tmp_called_name_2 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_fprintf );

    if (unlikely( tmp_called_name_2 == NULL ))
    {
        tmp_called_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fprintf );
    }

    if ( tmp_called_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10410 ], 36, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 597;
        goto try_finally_handler_3;
    }

    tmp_args_element_name_3 = par_fout;

    tmp_args_element_name_4 = const_str_digest_14b1d9542392632c1dd58ad837d7d5e0;
    tmp_args_element_name_5 = var_num_prefix;

    if ( tmp_args_element_name_5 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11141 ], 56, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 597;
        goto try_finally_handler_3;
    }

    tmp_args_element_name_6 = var_dummies;

    if ( tmp_args_element_name_6 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11297 ], 53, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 597;
        goto try_finally_handler_3;
    }

    frame_function->f_lineno = 597;
    tmp_unused = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_2, tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 597;
        goto try_finally_handler_3;
    }
    Py_DECREF( tmp_unused );
    tmp_assign_source_21 = const_int_0;
    {
        PyObject *old = var_dummies;
        var_dummies = tmp_assign_source_21;
        Py_INCREF( var_dummies );
        Py_XDECREF( old );
    }

    branch_no_5:;
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_biff_rec_name_dict );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_biff_rec_name_dict );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11402 ], 47, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 599;
        goto try_finally_handler_3;
    }

    tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get );
    if ( tmp_called_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 599;
        goto try_finally_handler_3;
    }
    tmp_args_element_name_7 = var_rc;

    if ( tmp_args_element_name_7 == NULL )
    {
        Py_DECREF( tmp_called_name_3 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11197 ], 48, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 599;
        goto try_finally_handler_3;
    }

    tmp_args_element_name_8 = const_str_angle_UNKNOWN;
    frame_function->f_lineno = 599;
    tmp_assign_source_22 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, tmp_args_element_name_7, tmp_args_element_name_8 );
    Py_DECREF( tmp_called_name_3 );
    if ( tmp_assign_source_22 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 599;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = var_recname;
        var_recname = tmp_assign_source_22;
        Py_XDECREF( old );
    }

    tmp_cond_value_6 = var_numbered;

    tmp_cond_truth_6 = CHECK_IF_TRUE( tmp_cond_value_6 );
    if ( tmp_cond_truth_6 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 600;
        goto try_finally_handler_3;
    }
    if (tmp_cond_truth_6 == 1)
    {
        goto branch_yes_7;
    }
    else
    {
        goto branch_no_7;
    }
    branch_yes_7:;
    tmp_left_name_12 = const_str_digest_0245eef4613ec410273d22c8f101bc78;
    tmp_left_name_13 = var_adj;

    tmp_right_name_13 = var_pos;

    if ( tmp_right_name_13 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 601;
        goto try_finally_handler_3;
    }

    tmp_right_name_12 = BINARY_OPERATION_ADD( tmp_left_name_13, tmp_right_name_13 );
    if ( tmp_right_name_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 601;
        goto try_finally_handler_3;
    }
    tmp_assign_source_23 = BINARY_OPERATION_REMAINDER( tmp_left_name_12, tmp_right_name_12 );
    Py_DECREF( tmp_right_name_12 );
    if ( tmp_assign_source_23 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 601;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = var_num_prefix;
        var_num_prefix = tmp_assign_source_23;
        Py_XDECREF( old );
    }

    branch_no_7:;
    tmp_called_name_4 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_fprintf );

    if (unlikely( tmp_called_name_4 == NULL ))
    {
        tmp_called_name_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fprintf );
    }

    if ( tmp_called_name_4 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10410 ], 36, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 602;
        goto try_finally_handler_3;
    }

    tmp_args_element_name_9 = par_fout;

    tmp_args_element_name_10 = const_str_digest_2dd278a1d486e1dec430092103f8a661;
    tmp_args_element_name_11 = var_num_prefix;

    if ( tmp_args_element_name_11 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11141 ], 56, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 602;
        goto try_finally_handler_3;
    }

    tmp_args_element_name_12 = var_rc;

    if ( tmp_args_element_name_12 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11197 ], 48, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 602;
        goto try_finally_handler_3;
    }

    tmp_args_element_name_13 = var_recname;

    tmp_args_element_name_14 = var_length;

    if ( tmp_args_element_name_14 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11245 ], 52, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 602;
        goto try_finally_handler_3;
    }

    tmp_args_element_name_15 = var_length;

    if ( tmp_args_element_name_15 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11245 ], 52, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 602;
        goto try_finally_handler_3;
    }

    frame_function->f_lineno = 602;
    tmp_unused = CALL_FUNCTION_WITH_ARGS7( tmp_called_name_4, tmp_args_element_name_9, tmp_args_element_name_10, tmp_args_element_name_11, tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_14, tmp_args_element_name_15 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 602;
        goto try_finally_handler_3;
    }
    Py_DECREF( tmp_unused );
    tmp_left_name_14 = var_pos;

    if ( tmp_left_name_14 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 603;
        goto try_finally_handler_3;
    }

    tmp_right_name_14 = const_int_pos_4;
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_14, tmp_right_name_14 );
    tmp_assign_source_24 = tmp_left_name_14;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 603;
        goto try_finally_handler_3;
    }
    var_pos = tmp_assign_source_24;

    tmp_called_name_5 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_hex_char_dump );

    if (unlikely( tmp_called_name_5 == NULL ))
    {
        tmp_called_name_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_hex_char_dump );
    }

    if ( tmp_called_name_5 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11449 ], 42, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 604;
        goto try_finally_handler_3;
    }

    tmp_args_element_name_16 = par_mem;

    tmp_args_element_name_17 = var_pos;

    tmp_args_element_name_18 = var_length;

    if ( tmp_args_element_name_18 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11245 ], 52, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 604;
        goto try_finally_handler_3;
    }

    tmp_left_name_15 = var_adj;

    tmp_right_name_15 = var_pos;

    tmp_args_element_name_19 = BINARY_OPERATION_ADD( tmp_left_name_15, tmp_right_name_15 );
    if ( tmp_args_element_name_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 604;
        goto try_finally_handler_3;
    }
    tmp_args_element_name_20 = par_fout;

    tmp_args_element_name_21 = par_unnumbered;

    frame_function->f_lineno = 604;
    tmp_unused = CALL_FUNCTION_WITH_ARGS6( tmp_called_name_5, tmp_args_element_name_16, tmp_args_element_name_17, tmp_args_element_name_18, tmp_args_element_name_19, tmp_args_element_name_20, tmp_args_element_name_21 );
    Py_DECREF( tmp_args_element_name_19 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 604;
        goto try_finally_handler_3;
    }
    Py_DECREF( tmp_unused );
    tmp_left_name_16 = var_pos;

    tmp_right_name_16 = var_length;

    if ( tmp_right_name_16 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11245 ], 52, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 605;
        goto try_finally_handler_3;
    }

    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_16, tmp_right_name_16 );
    tmp_assign_source_25 = tmp_left_name_16;
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 605;
        goto try_finally_handler_3;
    }
    var_pos = tmp_assign_source_25;

    branch_end_2:;
    try_finally_handler_start_2:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_3:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_2 = frame_function->f_lineno;
    Py_XDECREF( tmp_and_1__value_1 );
    tmp_and_1__value_1 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_2;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_4 != NULL )
    {
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;

        goto frame_exception_exit_1;
    }

    // Break if entered via break.
    if ( tmp_break_1 )
    {

    goto loop_end_1;
    }
    goto finally_end_4;
    finally_end_4:;
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 579;
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;
    tmp_cond_value_7 = var_dummies;

    if ( tmp_cond_value_7 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11297 ], 53, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 606;
        goto frame_exception_exit_1;
    }

    tmp_cond_truth_7 = CHECK_IF_TRUE( tmp_cond_value_7 );
    if ( tmp_cond_truth_7 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 606;
        goto frame_exception_exit_1;
    }
    if (tmp_cond_truth_7 == 1)
    {
        goto branch_yes_8;
    }
    else
    {
        goto branch_no_8;
    }
    branch_yes_8:;
    tmp_cond_value_8 = var_numbered;

    tmp_cond_truth_8 = CHECK_IF_TRUE( tmp_cond_value_8 );
    if ( tmp_cond_truth_8 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 607;
        goto frame_exception_exit_1;
    }
    if (tmp_cond_truth_8 == 1)
    {
        goto branch_yes_9;
    }
    else
    {
        goto branch_no_9;
    }
    branch_yes_9:;
    tmp_left_name_17 = const_str_digest_0245eef4613ec410273d22c8f101bc78;
    tmp_left_name_18 = var_adj;

    tmp_right_name_18 = var_savpos;

    if ( tmp_right_name_18 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11350 ], 52, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 608;
        goto frame_exception_exit_1;
    }

    tmp_right_name_17 = BINARY_OPERATION_ADD( tmp_left_name_18, tmp_right_name_18 );
    if ( tmp_right_name_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 608;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_26 = BINARY_OPERATION_REMAINDER( tmp_left_name_17, tmp_right_name_17 );
    Py_DECREF( tmp_right_name_17 );
    if ( tmp_assign_source_26 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 608;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_num_prefix;
        var_num_prefix = tmp_assign_source_26;
        Py_XDECREF( old );
    }

    branch_no_9:;
    tmp_called_name_6 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_fprintf );

    if (unlikely( tmp_called_name_6 == NULL ))
    {
        tmp_called_name_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fprintf );
    }

    if ( tmp_called_name_6 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10410 ], 36, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 609;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_22 = par_fout;

    tmp_args_element_name_23 = const_str_digest_14b1d9542392632c1dd58ad837d7d5e0;
    tmp_args_element_name_24 = var_num_prefix;

    if ( tmp_args_element_name_24 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11141 ], 56, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 609;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_25 = var_dummies;

    if ( tmp_args_element_name_25 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11297 ], 53, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 609;
        goto frame_exception_exit_1;
    }

    frame_function->f_lineno = 609;
    tmp_unused = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_6, tmp_args_element_name_22, tmp_args_element_name_23, tmp_args_element_name_24, tmp_args_element_name_25 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 609;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    branch_no_8:;
    tmp_compare_left_3 = var_pos;

    if ( tmp_compare_left_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 610;
        goto frame_exception_exit_1;
    }

    tmp_compare_right_3 = var_stream_end;

    tmp_cmp_Lt_1 = RICH_COMPARE_BOOL_LT( tmp_compare_left_3, tmp_compare_right_3 );
    if ( tmp_cmp_Lt_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 610;
        goto frame_exception_exit_1;
    }
    if (tmp_cmp_Lt_1 == 1)
    {
        goto branch_yes_10;
    }
    else
    {
        goto branch_no_10;
    }
    branch_yes_10:;
    tmp_cond_value_9 = var_numbered;

    tmp_cond_truth_9 = CHECK_IF_TRUE( tmp_cond_value_9 );
    if ( tmp_cond_truth_9 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 611;
        goto frame_exception_exit_1;
    }
    if (tmp_cond_truth_9 == 1)
    {
        goto branch_yes_11;
    }
    else
    {
        goto branch_no_11;
    }
    branch_yes_11:;
    tmp_left_name_19 = const_str_digest_0245eef4613ec410273d22c8f101bc78;
    tmp_left_name_20 = var_adj;

    tmp_right_name_20 = var_pos;

    if ( tmp_right_name_20 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 612;
        goto frame_exception_exit_1;
    }

    tmp_right_name_19 = BINARY_OPERATION_ADD( tmp_left_name_20, tmp_right_name_20 );
    if ( tmp_right_name_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 612;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_27 = BINARY_OPERATION_REMAINDER( tmp_left_name_19, tmp_right_name_19 );
    Py_DECREF( tmp_right_name_19 );
    if ( tmp_assign_source_27 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 612;
        goto frame_exception_exit_1;
    }
    {
        PyObject *old = var_num_prefix;
        var_num_prefix = tmp_assign_source_27;
        Py_XDECREF( old );
    }

    branch_no_11:;
    tmp_called_name_7 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_fprintf );

    if (unlikely( tmp_called_name_7 == NULL ))
    {
        tmp_called_name_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fprintf );
    }

    if ( tmp_called_name_7 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10410 ], 36, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 613;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_26 = par_fout;

    tmp_args_element_name_27 = const_str_digest_d1092b79338523976efa4188834ad524;
    tmp_args_element_name_28 = var_num_prefix;

    if ( tmp_args_element_name_28 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11141 ], 56, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 613;
        goto frame_exception_exit_1;
    }

    frame_function->f_lineno = 613;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, tmp_args_element_name_26, tmp_args_element_name_27, tmp_args_element_name_28 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 613;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    tmp_called_name_8 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_hex_char_dump );

    if (unlikely( tmp_called_name_8 == NULL ))
    {
        tmp_called_name_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_hex_char_dump );
    }

    if ( tmp_called_name_8 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11449 ], 42, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 614;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_29 = par_mem;

    tmp_args_element_name_30 = var_pos;

    if ( tmp_args_element_name_30 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 614;
        goto frame_exception_exit_1;
    }

    tmp_left_name_21 = var_stream_end;

    tmp_right_name_21 = var_pos;

    if ( tmp_right_name_21 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 614;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_31 = BINARY_OPERATION_SUB( tmp_left_name_21, tmp_right_name_21 );
    if ( tmp_args_element_name_31 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 614;
        goto frame_exception_exit_1;
    }
    tmp_left_name_22 = var_adj;

    tmp_right_name_22 = var_pos;

    if ( tmp_right_name_22 == NULL )
    {
        Py_DECREF( tmp_args_element_name_31 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 614;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_32 = BINARY_OPERATION_ADD( tmp_left_name_22, tmp_right_name_22 );
    if ( tmp_args_element_name_32 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_element_name_31 );

        frame_function->f_lineno = 614;
        goto frame_exception_exit_1;
    }
    tmp_args_element_name_33 = par_fout;

    tmp_args_element_name_34 = par_unnumbered;

    frame_function->f_lineno = 614;
    tmp_unused = CALL_FUNCTION_WITH_ARGS6( tmp_called_name_8, tmp_args_element_name_29, tmp_args_element_name_30, tmp_args_element_name_31, tmp_args_element_name_32, tmp_args_element_name_33, tmp_args_element_name_34 );
    Py_DECREF( tmp_args_element_name_31 );
    Py_DECREF( tmp_args_element_name_32 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 614;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    goto branch_end_10;
    branch_no_10:;
    tmp_compare_left_4 = var_pos;

    if ( tmp_compare_left_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 615;
        goto frame_exception_exit_1;
    }

    tmp_compare_right_4 = var_stream_end;

    tmp_cmp_Gt_1 = RICH_COMPARE_BOOL_GT( tmp_compare_left_4, tmp_compare_right_4 );
    if ( tmp_cmp_Gt_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 615;
        goto frame_exception_exit_1;
    }
    if (tmp_cmp_Gt_1 == 1)
    {
        goto branch_yes_12;
    }
    else
    {
        goto branch_no_12;
    }
    branch_yes_12:;
    tmp_called_name_9 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_fprintf );

    if (unlikely( tmp_called_name_9 == NULL ))
    {
        tmp_called_name_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fprintf );
    }

    if ( tmp_called_name_9 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10410 ], 36, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 616;
        goto frame_exception_exit_1;
    }

    tmp_args_element_name_35 = par_fout;

    tmp_args_element_name_36 = const_str_digest_b6139e4269dc05287b03c22a344c5df7;
    tmp_args_element_name_37 = var_length;

    if ( tmp_args_element_name_37 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11245 ], 52, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 616;
        goto frame_exception_exit_1;
    }

    frame_function->f_lineno = 616;
    tmp_unused = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_9, tmp_args_element_name_35, tmp_args_element_name_36, tmp_args_element_name_37 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 616;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_unused );
    branch_no_12:;
    branch_end_10:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var_pos != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_pos,
            var_pos
        );
        assert( tmp_res != -1 );

    }
    if ( var_stream_end != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_stream_end,
            var_stream_end
        );
        assert( tmp_res != -1 );

    }
    if ( var_adj != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_adj,
            var_adj
        );
        assert( tmp_res != -1 );

    }
    if ( var_dummies != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_dummies,
            var_dummies
        );
        assert( tmp_res != -1 );

    }
    if ( var_numbered != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_numbered,
            var_numbered
        );
        assert( tmp_res != -1 );

    }
    if ( var_num_prefix != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_num_prefix,
            var_num_prefix
        );
        assert( tmp_res != -1 );

    }
    if ( var_rc != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_rc,
            var_rc
        );
        assert( tmp_res != -1 );

    }
    if ( var_length != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_length,
            var_length
        );
        assert( tmp_res != -1 );

    }
    if ( var_savpos != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_savpos,
            var_savpos
        );
        assert( tmp_res != -1 );

    }
    if ( var_recname != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_recname,
            var_recname
        );
        assert( tmp_res != -1 );

    }
    if ( par_mem != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_mem,
            par_mem
        );
        assert( tmp_res != -1 );

    }
    if ( par_stream_offset != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_stream_offset,
            par_stream_offset
        );
        assert( tmp_res != -1 );

    }
    if ( par_stream_len != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_stream_len,
            par_stream_len
        );
        assert( tmp_res != -1 );

    }
    if ( par_base != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_base,
            par_base
        );
        assert( tmp_res != -1 );

    }
    if ( par_fout != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_fout,
            par_fout
        );
        assert( tmp_res != -1 );

    }
    if ( par_unnumbered != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_unnumbered,
            par_unnumbered
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_finally_handler_start_1;
    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_mem );
    Py_DECREF( par_mem );
    par_mem = NULL;

    CHECK_OBJECT( (PyObject *)par_stream_offset );
    Py_DECREF( par_stream_offset );
    par_stream_offset = NULL;

    CHECK_OBJECT( (PyObject *)par_stream_len );
    Py_DECREF( par_stream_len );
    par_stream_len = NULL;

    CHECK_OBJECT( (PyObject *)par_base );
    Py_DECREF( par_base );
    par_base = NULL;

    CHECK_OBJECT( (PyObject *)par_fout );
    Py_DECREF( par_fout );
    par_fout = NULL;

    CHECK_OBJECT( (PyObject *)par_unnumbered );
    Py_DECREF( par_unnumbered );
    par_unnumbered = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var_stream_end );
    var_stream_end = NULL;

    Py_XDECREF( var_adj );
    var_adj = NULL;

    Py_XDECREF( var_dummies );
    var_dummies = NULL;

    Py_XDECREF( var_numbered );
    var_numbered = NULL;

    Py_XDECREF( var_num_prefix );
    var_num_prefix = NULL;

    Py_XDECREF( var_rc );
    var_rc = NULL;

    Py_XDECREF( var_length );
    var_length = NULL;

    Py_XDECREF( var_savpos );
    var_savpos = NULL;

    Py_XDECREF( var_recname );
    var_recname = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_5 != NULL )
    {
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_5;
    finally_end_5:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_12_biff_dump_of_xlrd$biffh );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_12_biff_dump_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_mem = NULL;
    PyObject *_python_par_stream_offset = NULL;
    PyObject *_python_par_stream_len = NULL;
    PyObject *_python_par_base = NULL;
    PyObject *_python_par_fout = NULL;
    PyObject *_python_par_unnumbered = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "biff_dump() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_mem == key )
            {
                assert( _python_par_mem == NULL );
                _python_par_mem = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_stream_offset == key )
            {
                assert( _python_par_stream_offset == NULL );
                _python_par_stream_offset = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_stream_len == key )
            {
                assert( _python_par_stream_len == NULL );
                _python_par_stream_len = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_base == key )
            {
                assert( _python_par_base == NULL );
                _python_par_base = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_fout == key )
            {
                assert( _python_par_fout == NULL );
                _python_par_fout = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_unnumbered == key )
            {
                assert( _python_par_unnumbered == NULL );
                _python_par_unnumbered = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_mem, key ) == 1 )
            {
                assert( _python_par_mem == NULL );
                _python_par_mem = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_stream_offset, key ) == 1 )
            {
                assert( _python_par_stream_offset == NULL );
                _python_par_stream_offset = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_stream_len, key ) == 1 )
            {
                assert( _python_par_stream_len == NULL );
                _python_par_stream_len = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_base, key ) == 1 )
            {
                assert( _python_par_base == NULL );
                _python_par_base = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_fout, key ) == 1 )
            {
                assert( _python_par_fout == NULL );
                _python_par_fout = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_unnumbered, key ) == 1 )
            {
                assert( _python_par_unnumbered == NULL );
                _python_par_unnumbered = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "biff_dump() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 6 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_mem != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_mem = args[ 0 ];
        Py_INCREF( _python_par_mem );
    }
    else if ( _python_par_mem == NULL )
    {
        if ( 0 + self->m_defaults_given >= 6  )
        {
            _python_par_mem = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 6 );
            Py_INCREF( _python_par_mem );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 1 < args_given ))
    {
         if (unlikely( _python_par_stream_offset != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 1 );
             goto error_exit;
         }

        _python_par_stream_offset = args[ 1 ];
        Py_INCREF( _python_par_stream_offset );
    }
    else if ( _python_par_stream_offset == NULL )
    {
        if ( 1 + self->m_defaults_given >= 6  )
        {
            _python_par_stream_offset = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 1 - 6 );
            Py_INCREF( _python_par_stream_offset );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 2 < args_given ))
    {
         if (unlikely( _python_par_stream_len != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 2 );
             goto error_exit;
         }

        _python_par_stream_len = args[ 2 ];
        Py_INCREF( _python_par_stream_len );
    }
    else if ( _python_par_stream_len == NULL )
    {
        if ( 2 + self->m_defaults_given >= 6  )
        {
            _python_par_stream_len = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 2 - 6 );
            Py_INCREF( _python_par_stream_len );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 3 < args_given ))
    {
         if (unlikely( _python_par_base != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 3 );
             goto error_exit;
         }

        _python_par_base = args[ 3 ];
        Py_INCREF( _python_par_base );
    }
    else if ( _python_par_base == NULL )
    {
        if ( 3 + self->m_defaults_given >= 6  )
        {
            _python_par_base = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 3 - 6 );
            Py_INCREF( _python_par_base );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 4 < args_given ))
    {
         if (unlikely( _python_par_fout != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 4 );
             goto error_exit;
         }

        _python_par_fout = args[ 4 ];
        Py_INCREF( _python_par_fout );
    }
    else if ( _python_par_fout == NULL )
    {
        if ( 4 + self->m_defaults_given >= 6  )
        {
            _python_par_fout = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 4 - 6 );
            Py_INCREF( _python_par_fout );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 5 < args_given ))
    {
         if (unlikely( _python_par_unnumbered != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 5 );
             goto error_exit;
         }

        _python_par_unnumbered = args[ 5 ];
        Py_INCREF( _python_par_unnumbered );
    }
    else if ( _python_par_unnumbered == NULL )
    {
        if ( 5 + self->m_defaults_given >= 6  )
        {
            _python_par_unnumbered = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 5 - 6 );
            Py_INCREF( _python_par_unnumbered );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_mem == NULL || _python_par_stream_offset == NULL || _python_par_stream_len == NULL || _python_par_base == NULL || _python_par_fout == NULL || _python_par_unnumbered == NULL ))
    {
        PyObject *values[] = { _python_par_mem, _python_par_stream_offset, _python_par_stream_len, _python_par_base, _python_par_fout, _python_par_unnumbered };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_12_biff_dump_of_xlrd$biffh( self, _python_par_mem, _python_par_stream_offset, _python_par_stream_len, _python_par_base, _python_par_fout, _python_par_unnumbered );

error_exit:;

    Py_XDECREF( _python_par_mem );
    Py_XDECREF( _python_par_stream_offset );
    Py_XDECREF( _python_par_stream_len );
    Py_XDECREF( _python_par_base );
    Py_XDECREF( _python_par_fout );
    Py_XDECREF( _python_par_unnumbered );

    return NULL;
}

static PyObject *dparse_function_12_biff_dump_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 6 )
    {
        return impl_function_12_biff_dump_of_xlrd$biffh( self, INCREASE_REFCOUNT( args[ 0 ] ), INCREASE_REFCOUNT( args[ 1 ] ), INCREASE_REFCOUNT( args[ 2 ] ), INCREASE_REFCOUNT( args[ 3 ] ), INCREASE_REFCOUNT( args[ 4 ] ), INCREASE_REFCOUNT( args[ 5 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_12_biff_dump_of_xlrd$biffh( self, args, size, NULL );
        return result;
    }

}



static PyObject *impl_function_13_biff_count_records_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject *_python_par_mem, PyObject *_python_par_stream_offset, PyObject *_python_par_stream_len, PyObject *_python_par_fout )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_mem = _python_par_mem;
    PyObject *par_stream_offset = _python_par_stream_offset;
    PyObject *par_stream_len = _python_par_stream_len;
    PyObject *par_fout = _python_par_fout;
    PyObject *var_pos = NULL;
    PyObject *var_stream_end = NULL;
    PyObject *var_tally = NULL;
    PyObject *var_rc = NULL;
    PyObject *var_length = NULL;
    PyObject *var_recname = NULL;
    PyObject *var_slist = NULL;
    PyObject *var_count = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_and_1__value_1 = NULL;
    PyObject *tmp_inplace_assign_subscr_1__target = NULL;
    PyObject *tmp_inplace_assign_subscr_1__subscript = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *exception_type = NULL, *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_name_1;
    PyObject *tmp_ass_subscribed_1;
    PyObject *tmp_ass_subscribed_2;
    PyObject *tmp_ass_subscript_1;
    PyObject *tmp_ass_subscript_2;
    PyObject *tmp_ass_subvalue_1;
    PyObject *tmp_ass_subvalue_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    bool tmp_break_1;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_called_name_5;
    int tmp_cmp_Eq_1;
    int tmp_cmp_GtE_1;
    int tmp_cmp_In_1;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_left_3;
    PyObject *tmp_compare_left_4;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_compare_right_3;
    PyObject *tmp_compare_right_4;
    PyObject *tmp_compexpr_left_1;
    PyObject *tmp_compexpr_left_2;
    PyObject *tmp_compexpr_right_1;
    PyObject *tmp_compexpr_right_2;
    int tmp_cond_truth_1;
    int tmp_cond_truth_2;
    PyObject *tmp_cond_value_1;
    PyObject *tmp_cond_value_2;
    PyObject *tmp_dict_key_1;
    PyObject *tmp_dict_value_1;
    PyObject *tmp_frame_locals;
    bool tmp_is_1;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iter_arg_2;
    PyObject *tmp_iter_arg_3;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_iterator_name_2;
    PyObject *tmp_kw_name_1;
    PyObject *tmp_left_name_1;
    PyObject *tmp_left_name_2;
    PyObject *tmp_left_name_3;
    PyObject *tmp_left_name_4;
    PyObject *tmp_left_name_5;
    PyObject *tmp_left_name_6;
    PyObject *tmp_left_name_7;
    PyObject *tmp_left_name_8;
    PyObject *tmp_left_name_9;
    PyObject *tmp_left_name_10;
    PyObject *tmp_next_source_1;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value;
    PyObject *tmp_right_name_1;
    PyObject *tmp_right_name_2;
    PyObject *tmp_right_name_3;
    PyObject *tmp_right_name_4;
    PyObject *tmp_right_name_5;
    PyObject *tmp_right_name_6;
    PyObject *tmp_right_name_7;
    PyObject *tmp_right_name_8;
    PyObject *tmp_right_name_9;
    PyObject *tmp_right_name_10;
    PyObject *tmp_slice_lower_1;
    PyObject *tmp_slice_lower_2;
    PyObject *tmp_slice_source_1;
    PyObject *tmp_slice_source_2;
    PyObject *tmp_slice_upper_1;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_subscribed_name_1;
    PyObject *tmp_subscript_name_1;
    int tmp_tried_lineno_1;
    int tmp_tried_lineno_2;
    int tmp_tried_lineno_3;
    int tmp_tried_lineno_4;
    int tmp_tried_lineno_5;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    PyObject *tmp_unpack_3;
    PyObject *tmp_unpack_4;
    NUITKA_MAY_BE_UNUSED PyObject *tmp_unused;
    static PyFrameObject *cache_frame_function = NULL;

    PyFrameObject *frame_function;

    tmp_return_value = NULL;

    // Actual function code.
    tmp_assign_source_1 = par_stream_offset;

    assert( var_pos == NULL );
    Py_INCREF( tmp_assign_source_1 );
    var_pos = tmp_assign_source_1;

    // Tried code
    MAKE_OR_REUSE_FRAME( cache_frame_function, codeobj_e51d167ceb0c76b2ab1e73d7adefde31, module_xlrd$biffh );
    frame_function = cache_frame_function;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_function );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( frame_function );
    assert( Py_REFCNT( frame_function ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    frame_function->f_executing += 1;
#endif

    // Framed code:
    tmp_left_name_1 = par_stream_offset;

    tmp_right_name_1 = par_stream_len;

    tmp_assign_source_2 = BINARY_OPERATION_ADD( tmp_left_name_1, tmp_right_name_1 );
    if ( tmp_assign_source_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 620;
        goto frame_exception_exit_1;
    }
    assert( var_stream_end == NULL );
    var_stream_end = tmp_assign_source_2;

    tmp_assign_source_3 = PyDict_New();
    assert( var_tally == NULL );
    var_tally = tmp_assign_source_3;

    loop_start_1:;
    tmp_left_name_2 = var_stream_end;

    tmp_right_name_2 = var_pos;

    if ( tmp_right_name_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 622;
        goto frame_exception_exit_1;
    }

    tmp_compare_left_1 = BINARY_OPERATION_SUB( tmp_left_name_2, tmp_right_name_2 );
    if ( tmp_compare_left_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 622;
        goto frame_exception_exit_1;
    }
    tmp_compare_right_1 = const_int_pos_4;
    tmp_cmp_GtE_1 = RICH_COMPARE_BOOL_GE( tmp_compare_left_1, tmp_compare_right_1 );
    if ( tmp_cmp_GtE_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_1 );

        frame_function->f_lineno = 622;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_compare_left_1 );
    if (tmp_cmp_GtE_1 == 1)
    {
        goto branch_no_1;
    }
    else
    {
        goto branch_yes_1;
    }
    branch_yes_1:;
    goto loop_end_1;
    branch_no_1:;
    // Tried code
    tmp_called_name_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack );

    if (unlikely( tmp_called_name_1 == NULL ))
    {
        tmp_called_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unpack );
    }

    if ( tmp_called_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10589 ], 35, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 623;
        goto try_finally_handler_2;
    }

    tmp_args_element_name_1 = const_str_digest_b8d2060bb898fb3742d05b0e0db99007;
    tmp_slice_source_1 = par_mem;

    tmp_slice_lower_1 = var_pos;

    if ( tmp_slice_lower_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 623;
        goto try_finally_handler_2;
    }

    tmp_left_name_3 = var_pos;

    if ( tmp_left_name_3 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 623;
        goto try_finally_handler_2;
    }

    tmp_right_name_3 = const_int_pos_4;
    tmp_slice_upper_1 = BINARY_OPERATION_ADD( tmp_left_name_3, tmp_right_name_3 );
    if ( tmp_slice_upper_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 623;
        goto try_finally_handler_2;
    }
    tmp_args_element_name_2 = LOOKUP_SLICE( tmp_slice_source_1, tmp_slice_lower_1, tmp_slice_upper_1 );
    Py_DECREF( tmp_slice_upper_1 );
    if ( tmp_args_element_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 623;
        goto try_finally_handler_2;
    }
    frame_function->f_lineno = 623;
    tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, tmp_args_element_name_1, tmp_args_element_name_2 );
    Py_DECREF( tmp_args_element_name_2 );
    if ( tmp_iter_arg_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 623;
        goto try_finally_handler_2;
    }
    tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
    Py_DECREF( tmp_iter_arg_1 );
    if ( tmp_assign_source_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 623;
        goto try_finally_handler_2;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__source_iter;
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_4;
        Py_XDECREF( old );
    }

    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_1, 0 );
    if ( tmp_assign_source_5 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 623;
        goto try_finally_handler_2;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__element_1;
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_5;
        Py_XDECREF( old );
    }

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_2, 1 );
    if ( tmp_assign_source_6 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 623;
        goto try_finally_handler_2;
    }
    {
        PyObject *old = tmp_tuple_unpack_1__element_2;
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_6;
        Py_XDECREF( old );
    }

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                goto try_finally_handler_2;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        goto try_finally_handler_2;
    }
    tmp_assign_source_7 = tmp_tuple_unpack_1__element_1;

    {
        PyObject *old = var_rc;
        var_rc = tmp_assign_source_7;
        Py_INCREF( var_rc );
        Py_XDECREF( old );
    }

    tmp_assign_source_8 = tmp_tuple_unpack_1__element_2;

    {
        PyObject *old = var_length;
        var_length = tmp_assign_source_8;
        Py_INCREF( var_length );
        Py_XDECREF( old );
    }

    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_1 = frame_function->f_lineno;
    Py_XDECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_1;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto frame_exception_exit_1;
    }

    goto finally_end_1;
    finally_end_1:;
    tmp_break_1 = false;
    // Tried code
    tmp_cond_value_1 = NULL;
    // Tried code
    tmp_compexpr_left_1 = var_rc;

    if ( tmp_compexpr_left_1 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11197 ], 48, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 624;
        goto try_finally_handler_4;
    }

    tmp_compexpr_right_1 = const_int_0;
    tmp_assign_source_9 = RICH_COMPARE_EQ( tmp_compexpr_left_1, tmp_compexpr_right_1 );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 624;
        goto try_finally_handler_4;
    }
    {
        PyObject *old = tmp_and_1__value_1;
        tmp_and_1__value_1 = tmp_assign_source_9;
        Py_XDECREF( old );
    }

    tmp_cond_value_2 = tmp_and_1__value_1;

    tmp_cond_truth_2 = CHECK_IF_TRUE( tmp_cond_value_2 );
    if ( tmp_cond_truth_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 624;
        goto try_finally_handler_4;
    }
    if (tmp_cond_truth_2 == 1)
    {
        goto condexpr_true_1;
    }
    else
    {
        goto condexpr_false_1;
    }
    condexpr_true_1:;
    tmp_cond_value_1 = NULL;
    // Tried code
    tmp_result = tmp_and_1__value_1 != NULL;
    if ( tmp_result == true )
    {
        Py_DECREF( tmp_and_1__value_1 );
        tmp_and_1__value_1 = NULL;
    }

    assert( tmp_result != false );
    tmp_compexpr_left_2 = var_length;

    if ( tmp_compexpr_left_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11245 ], 52, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 624;
        goto try_finally_handler_5;
    }

    tmp_compexpr_right_2 = const_int_0;
    tmp_cond_value_1 = RICH_COMPARE_EQ( tmp_compexpr_left_2, tmp_compexpr_right_2 );
    if ( tmp_cond_value_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 624;
        goto try_finally_handler_5;
    }
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_5:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_2 != NULL )
    {
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;

        goto try_finally_handler_4;
    }

    goto finally_end_2;
    finally_end_2:;
    goto condexpr_end_1;
    condexpr_false_1:;
    tmp_cond_value_1 = tmp_and_1__value_1;

    Py_INCREF( tmp_cond_value_1 );
    condexpr_end_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_3 != NULL )
    {
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;

        goto try_finally_handler_3;
    }

    goto finally_end_3;
    finally_end_3:;
    tmp_cond_truth_1 = CHECK_IF_TRUE( tmp_cond_value_1 );
    if ( tmp_cond_truth_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_cond_value_1 );

        frame_function->f_lineno = 624;
        goto try_finally_handler_3;
    }
    Py_DECREF( tmp_cond_value_1 );
    if (tmp_cond_truth_1 == 1)
    {
        goto branch_yes_2;
    }
    else
    {
        goto branch_no_2;
    }
    branch_yes_2:;
    tmp_slice_source_2 = par_mem;

    tmp_slice_lower_2 = var_pos;

    if ( tmp_slice_lower_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 625;
        goto try_finally_handler_3;
    }

    tmp_compare_left_2 = LOOKUP_SLICE( tmp_slice_source_2, tmp_slice_lower_2, Py_None );
    if ( tmp_compare_left_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 625;
        goto try_finally_handler_3;
    }
    tmp_left_name_4 = const_str_chr_0;
    tmp_left_name_5 = var_stream_end;

    tmp_right_name_5 = var_pos;

    if ( tmp_right_name_5 == NULL )
    {
        Py_DECREF( tmp_compare_left_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 625;
        goto try_finally_handler_3;
    }

    tmp_right_name_4 = BINARY_OPERATION_SUB( tmp_left_name_5, tmp_right_name_5 );
    if ( tmp_right_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_2 );

        frame_function->f_lineno = 625;
        goto try_finally_handler_3;
    }
    tmp_compare_right_2 = BINARY_OPERATION_MUL( tmp_left_name_4, tmp_right_name_4 );
    Py_DECREF( tmp_right_name_4 );
    if ( tmp_compare_right_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_2 );

        frame_function->f_lineno = 625;
        goto try_finally_handler_3;
    }
    tmp_cmp_Eq_1 = RICH_COMPARE_BOOL_EQ( tmp_compare_left_2, tmp_compare_right_2 );
    if ( tmp_cmp_Eq_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_compare_left_2 );
        Py_DECREF( tmp_compare_right_2 );

        frame_function->f_lineno = 625;
        goto try_finally_handler_3;
    }
    Py_DECREF( tmp_compare_left_2 );
    Py_DECREF( tmp_compare_right_2 );
    if (tmp_cmp_Eq_1 == 1)
    {
        goto branch_yes_3;
    }
    else
    {
        goto branch_no_3;
    }
    branch_yes_3:;
    tmp_break_1 = true;
    goto try_finally_handler_start_2;
    branch_no_3:;
    tmp_assign_source_10 = const_str_digest_fafacfd47cf13238a8c1faf6984b28af;
    {
        PyObject *old = var_recname;
        var_recname = tmp_assign_source_10;
        Py_INCREF( var_recname );
        Py_XDECREF( old );
    }

    goto branch_end_2;
    branch_no_2:;
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_biff_rec_name_dict );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_biff_rec_name_dict );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11402 ], 47, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 629;
        goto try_finally_handler_3;
    }

    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get );
    if ( tmp_called_name_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 629;
        goto try_finally_handler_3;
    }
    tmp_args_element_name_3 = var_rc;

    if ( tmp_args_element_name_3 == NULL )
    {
        Py_DECREF( tmp_called_name_2 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11197 ], 48, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 629;
        goto try_finally_handler_3;
    }

    tmp_args_element_name_4 = Py_None;
    frame_function->f_lineno = 629;
    tmp_assign_source_11 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, tmp_args_element_name_3, tmp_args_element_name_4 );
    Py_DECREF( tmp_called_name_2 );
    if ( tmp_assign_source_11 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 629;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = var_recname;
        var_recname = tmp_assign_source_11;
        Py_XDECREF( old );
    }

    tmp_compare_left_3 = var_recname;

    tmp_compare_right_3 = Py_None;
    tmp_is_1 = ( tmp_compare_left_3 == tmp_compare_right_3 );
    if (tmp_is_1)
    {
        goto branch_yes_4;
    }
    else
    {
        goto branch_no_4;
    }
    branch_yes_4:;
    tmp_left_name_6 = const_str_digest_151a9c921b189f1612ad426f27c62dd9;
    tmp_right_name_6 = var_rc;

    if ( tmp_right_name_6 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11197 ], 48, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 631;
        goto try_finally_handler_3;
    }

    tmp_assign_source_12 = BINARY_OPERATION_REMAINDER( tmp_left_name_6, tmp_right_name_6 );
    if ( tmp_assign_source_12 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 631;
        goto try_finally_handler_3;
    }
    {
        PyObject *old = var_recname;
        assert( old != NULL );
        var_recname = tmp_assign_source_12;
        Py_DECREF( old );
    }

    branch_no_4:;
    branch_end_2:;
    try_finally_handler_start_2:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_3:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_2 = frame_function->f_lineno;
    Py_XDECREF( tmp_and_1__value_1 );
    tmp_and_1__value_1 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_2;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_4 != NULL )
    {
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;

        goto frame_exception_exit_1;
    }

    // Break if entered via break.
    if ( tmp_break_1 )
    {

    goto loop_end_1;
    }
    goto finally_end_4;
    finally_end_4:;
    tmp_compare_left_4 = var_recname;

    if ( tmp_compare_left_4 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11491 ], 53, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 632;
        goto frame_exception_exit_1;
    }

    tmp_compare_right_4 = var_tally;

    tmp_cmp_In_1 = PySequence_Contains( tmp_compare_right_4, tmp_compare_left_4 );
    if ( tmp_cmp_In_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 632;
        goto frame_exception_exit_1;
    }
    if (tmp_cmp_In_1 == 1)
    {
        goto branch_yes_5;
    }
    else
    {
        goto branch_no_5;
    }
    branch_yes_5:;
    tmp_assign_source_13 = var_tally;

    {
        PyObject *old = tmp_inplace_assign_subscr_1__target;
        tmp_inplace_assign_subscr_1__target = tmp_assign_source_13;
        Py_INCREF( tmp_inplace_assign_subscr_1__target );
        Py_XDECREF( old );
    }

    // Tried code
    tmp_assign_source_14 = var_recname;

    if ( tmp_assign_source_14 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11491 ], 53, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 633;
        goto try_finally_handler_6;
    }

    {
        PyObject *old = tmp_inplace_assign_subscr_1__subscript;
        tmp_inplace_assign_subscr_1__subscript = tmp_assign_source_14;
        Py_INCREF( tmp_inplace_assign_subscr_1__subscript );
        Py_XDECREF( old );
    }

    tmp_subscribed_name_1 = tmp_inplace_assign_subscr_1__target;

    tmp_subscript_name_1 = tmp_inplace_assign_subscr_1__subscript;

    tmp_left_name_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
    if ( tmp_left_name_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 633;
        goto try_finally_handler_6;
    }
    tmp_right_name_7 = const_int_pos_1;
    tmp_ass_subvalue_1 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_7, tmp_right_name_7 );
    Py_DECREF( tmp_left_name_7 );
    if ( tmp_ass_subvalue_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 633;
        goto try_finally_handler_6;
    }
    tmp_ass_subscribed_1 = tmp_inplace_assign_subscr_1__target;

    tmp_ass_subscript_1 = tmp_inplace_assign_subscr_1__subscript;

    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
    Py_DECREF( tmp_ass_subvalue_1 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 633;
        goto try_finally_handler_6;
    }
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_6:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_3 = frame_function->f_lineno;
    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_subscr_1__target );
    Py_DECREF( tmp_inplace_assign_subscr_1__target );
    tmp_inplace_assign_subscr_1__target = NULL;

    Py_XDECREF( tmp_inplace_assign_subscr_1__subscript );
    tmp_inplace_assign_subscr_1__subscript = NULL;

    frame_function->f_lineno = tmp_tried_lineno_3;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_5 != NULL )
    {
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;

        goto frame_exception_exit_1;
    }

    goto finally_end_5;
    finally_end_5:;
    goto branch_end_5;
    branch_no_5:;
    tmp_ass_subvalue_2 = const_int_pos_1;
    tmp_ass_subscribed_2 = var_tally;

    tmp_ass_subscript_2 = var_recname;

    if ( tmp_ass_subscript_2 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11491 ], 53, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 635;
        goto frame_exception_exit_1;
    }

    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 635;
        goto frame_exception_exit_1;
    }
    branch_end_5:;
    tmp_left_name_8 = var_pos;

    if ( tmp_left_name_8 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10706 ], 49, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 636;
        goto frame_exception_exit_1;
    }

    tmp_left_name_9 = var_length;

    if ( tmp_left_name_9 == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11245 ], 52, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 636;
        goto frame_exception_exit_1;
    }

    tmp_right_name_9 = const_int_pos_4;
    tmp_right_name_8 = BINARY_OPERATION_ADD( tmp_left_name_9, tmp_right_name_9 );
    if ( tmp_right_name_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 636;
        goto frame_exception_exit_1;
    }
    tmp_result = BINARY_OPERATION_ADD_INPLACE( &tmp_left_name_8, tmp_right_name_8 );
    tmp_assign_source_15 = tmp_left_name_8;
    Py_DECREF( tmp_right_name_8 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 636;
        goto frame_exception_exit_1;
    }
    var_pos = tmp_assign_source_15;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 622;
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;
    tmp_called_name_3 = LOOKUP_BUILTIN( const_str_plain_sorted );
    if ( tmp_called_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 637;
        goto frame_exception_exit_1;
    }
    tmp_source_name_2 = var_tally;

    tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_items );
    if ( tmp_called_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 637;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 637;
    tmp_args_element_name_5 = CALL_FUNCTION_NO_ARGS( tmp_called_name_4 );
    Py_DECREF( tmp_called_name_4 );
    if ( tmp_args_element_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 637;
        goto frame_exception_exit_1;
    }
    frame_function->f_lineno = 637;
    tmp_assign_source_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, tmp_args_element_name_5 );
    Py_DECREF( tmp_args_element_name_5 );
    if ( tmp_assign_source_16 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 637;
        goto frame_exception_exit_1;
    }
    assert( var_slist == NULL );
    var_slist = tmp_assign_source_16;

    tmp_iter_arg_2 = var_slist;

    tmp_assign_source_17 = MAKE_ITERATOR( tmp_iter_arg_2 );
    if ( tmp_assign_source_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 638;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_17;

    // Tried code
    loop_start_2:;
    tmp_next_source_1 = tmp_for_loop_1__for_iterator;

    tmp_assign_source_18 = ITERATOR_NEXT( tmp_next_source_1 );
    if ( tmp_assign_source_18 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_2;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_function->f_lineno = 638;
            goto try_finally_handler_7;
        }
    }

    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_18;
        Py_XDECREF( old );
    }

    // Tried code
    tmp_iter_arg_3 = tmp_for_loop_1__iter_value;

    tmp_assign_source_19 = MAKE_ITERATOR( tmp_iter_arg_3 );
    if ( tmp_assign_source_19 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 638;
        goto try_finally_handler_8;
    }
    {
        PyObject *old = tmp_tuple_unpack_2__source_iter;
        tmp_tuple_unpack_2__source_iter = tmp_assign_source_19;
        Py_XDECREF( old );
    }

    tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;

    tmp_assign_source_20 = UNPACK_NEXT( tmp_unpack_3, 0 );
    if ( tmp_assign_source_20 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 638;
        goto try_finally_handler_8;
    }
    {
        PyObject *old = tmp_tuple_unpack_2__element_1;
        tmp_tuple_unpack_2__element_1 = tmp_assign_source_20;
        Py_XDECREF( old );
    }

    tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;

    tmp_assign_source_21 = UNPACK_NEXT( tmp_unpack_4, 1 );
    if ( tmp_assign_source_21 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_function->f_lineno = 638;
        goto try_finally_handler_8;
    }
    {
        PyObject *old = tmp_tuple_unpack_2__element_2;
        tmp_tuple_unpack_2__element_2 = tmp_assign_source_21;
        Py_XDECREF( old );
    }

    tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;

    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                goto try_finally_handler_8;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        goto try_finally_handler_8;
    }
    tmp_assign_source_22 = tmp_tuple_unpack_2__element_1;

    {
        PyObject *old = var_recname;
        var_recname = tmp_assign_source_22;
        Py_INCREF( var_recname );
        Py_XDECREF( old );
    }

    tmp_assign_source_23 = tmp_tuple_unpack_2__element_2;

    {
        PyObject *old = var_count;
        var_count = tmp_assign_source_23;
        Py_INCREF( var_count );
        Py_XDECREF( old );
    }

    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_8:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_4 = frame_function->f_lineno;
    Py_XDECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    frame_function->f_lineno = tmp_tried_lineno_4;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_6 != NULL )
    {
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;

        goto try_finally_handler_7;
    }

    goto finally_end_6;
    finally_end_6:;
    tmp_called_name_5 = LOOKUP_BUILTIN( const_str_plain_print );
    if ( tmp_called_name_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 639;
        goto try_finally_handler_7;
    }
    tmp_args_name_1 = PyTuple_New( 1 );
    tmp_left_name_10 = const_str_digest_73bc75123e178cc9716a872958726225;
    tmp_right_name_10 = PyTuple_New( 2 );
    tmp_tuple_element_2 = var_count;

    if ( tmp_tuple_element_2 == NULL )
    {
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_right_name_10 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11544 ], 51, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 639;
        goto try_finally_handler_7;
    }

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_right_name_10, 0, tmp_tuple_element_2 );
    tmp_tuple_element_2 = var_recname;

    if ( tmp_tuple_element_2 == NULL )
    {
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_right_name_10 );
        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 11491 ], 53, 0 );
        exception_tb = NULL;

        frame_function->f_lineno = 639;
        goto try_finally_handler_7;
    }

    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_right_name_10, 1, tmp_tuple_element_2 );
    tmp_tuple_element_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_10, tmp_right_name_10 );
    Py_DECREF( tmp_right_name_10 );
    if ( tmp_tuple_element_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_args_name_1 );

        frame_function->f_lineno = 639;
        goto try_finally_handler_7;
    }
    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
    tmp_kw_name_1 = _PyDict_NewPresized( 1 );
    tmp_dict_value_1 = par_fout;

    tmp_dict_key_1 = const_str_plain_file;
    PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
    frame_function->f_lineno = 639;
    tmp_unused = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_1, tmp_kw_name_1 );
    Py_DECREF( tmp_args_name_1 );
    Py_DECREF( tmp_kw_name_1 );
    if ( tmp_unused == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 639;
        goto try_finally_handler_7;
    }
    Py_DECREF( tmp_unused );
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_function->f_lineno = 638;
        goto try_finally_handler_7;
    }
    goto loop_start_2;
    loop_end_2:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_7:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_5 = frame_function->f_lineno;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    frame_function->f_lineno = tmp_tried_lineno_5;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_7 != NULL )
    {
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;

        goto frame_exception_exit_1;
    }

    goto finally_end_7;
    finally_end_7:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif
    // Put the previous frame back on top.
    popFrameStack();
#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );
    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_function );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_function );
    }
    else if ( exception_tb->tb_frame != frame_function )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_function );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }


    tmp_frame_locals = PyDict_New();
    if ( var_pos != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_pos,
            var_pos
        );
        assert( tmp_res != -1 );

    }
    if ( var_stream_end != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_stream_end,
            var_stream_end
        );
        assert( tmp_res != -1 );

    }
    if ( var_tally != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_tally,
            var_tally
        );
        assert( tmp_res != -1 );

    }
    if ( var_rc != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_rc,
            var_rc
        );
        assert( tmp_res != -1 );

    }
    if ( var_length != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_length,
            var_length
        );
        assert( tmp_res != -1 );

    }
    if ( var_recname != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_recname,
            var_recname
        );
        assert( tmp_res != -1 );

    }
    if ( var_slist != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_slist,
            var_slist
        );
        assert( tmp_res != -1 );

    }
    if ( var_count != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_count,
            var_count
        );
        assert( tmp_res != -1 );

    }
    if ( par_mem != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_mem,
            par_mem
        );
        assert( tmp_res != -1 );

    }
    if ( par_stream_offset != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_stream_offset,
            par_stream_offset
        );
        assert( tmp_res != -1 );

    }
    if ( par_stream_len != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_stream_len,
            par_stream_len
        );
        assert( tmp_res != -1 );

    }
    if ( par_fout != NULL )
    {
        tmp_res = PyDict_SetItem(
            tmp_frame_locals,
            const_str_plain_fout,
            par_fout
        );
        assert( tmp_res != -1 );

    }
    detachFrame( exception_tb, tmp_frame_locals );


    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_function->f_executing -= 1;
#endif
    Py_DECREF( frame_function );

    // Return the error.
    goto try_finally_handler_1;
    frame_no_exception_1:;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_finally_handler_start_1;
    try_finally_handler_start_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    CHECK_OBJECT( (PyObject *)par_mem );
    Py_DECREF( par_mem );
    par_mem = NULL;

    CHECK_OBJECT( (PyObject *)par_stream_offset );
    Py_DECREF( par_stream_offset );
    par_stream_offset = NULL;

    CHECK_OBJECT( (PyObject *)par_stream_len );
    Py_DECREF( par_stream_len );
    par_stream_len = NULL;

    CHECK_OBJECT( (PyObject *)par_fout );
    Py_DECREF( par_fout );
    par_fout = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var_stream_end );
    var_stream_end = NULL;

    Py_XDECREF( var_tally );
    var_tally = NULL;

    Py_XDECREF( var_rc );
    var_rc = NULL;

    Py_XDECREF( var_length );
    var_length = NULL;

    Py_XDECREF( var_recname );
    var_recname = NULL;

    Py_XDECREF( var_slist );
    var_slist = NULL;

    Py_XDECREF( var_count );
    var_count = NULL;

    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_8 != NULL )
    {
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;

        goto function_exception_exit;
    }

    // Return value if any.
    if ( tmp_return_value != NULL )
    {
        goto function_return_exit;
    }

    goto finally_end_8;
    finally_end_8:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( function_13_biff_count_records_of_xlrd$biffh );

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;
    function_return_exit:
    CHECK_OBJECT( tmp_return_value );
    assert( had_error || !ERROR_OCCURRED() );
    return tmp_return_value;

}
static PyObject *fparse_function_13_biff_count_records_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, Py_ssize_t args_size, PyObject *kw )
{
    assert( kw == NULL || PyDict_Check( kw ) );

    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_size = kw ? PyDict_Size( kw ) : 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_found = 0;
    NUITKA_MAY_BE_UNUSED Py_ssize_t kw_only_found = 0;
    Py_ssize_t args_given = args_size;
    PyObject *_python_par_mem = NULL;
    PyObject *_python_par_stream_offset = NULL;
    PyObject *_python_par_stream_len = NULL;
    PyObject *_python_par_fout = NULL;
    // Copy given dictionary values to the the respective variables:
    if ( kw_size > 0 )
    {
        Py_ssize_t ppos = 0;
        PyObject *key, *value;

        while( PyDict_Next( kw, &ppos, &key, &value ) )
        {
#if PYTHON_VERSION < 300
            if (unlikely( !PyString_Check( key ) && !PyUnicode_Check( key ) ))
#else
            if (unlikely( !PyUnicode_Check( key ) ))
#endif
            {
                PyErr_Format( PyExc_TypeError, "biff_count_records() keywords must be strings" );
                goto error_exit;
            }

            NUITKA_MAY_BE_UNUSED bool found = false;

            Py_INCREF( key );
            Py_INCREF( value );

            // Quick path, could be our value.
            if ( found == false && const_str_plain_mem == key )
            {
                assert( _python_par_mem == NULL );
                _python_par_mem = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_stream_offset == key )
            {
                assert( _python_par_stream_offset == NULL );
                _python_par_stream_offset = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_stream_len == key )
            {
                assert( _python_par_stream_len == NULL );
                _python_par_stream_len = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && const_str_plain_fout == key )
            {
                assert( _python_par_fout == NULL );
                _python_par_fout = value;

                found = true;
                kw_found += 1;
            }

            // Slow path, compare against all parameter names.
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_mem, key ) == 1 )
            {
                assert( _python_par_mem == NULL );
                _python_par_mem = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_stream_offset, key ) == 1 )
            {
                assert( _python_par_stream_offset == NULL );
                _python_par_stream_offset = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_stream_len, key ) == 1 )
            {
                assert( _python_par_stream_len == NULL );
                _python_par_stream_len = value;

                found = true;
                kw_found += 1;
            }
            if ( found == false && RICH_COMPARE_BOOL_EQ( const_str_plain_fout, key ) == 1 )
            {
                assert( _python_par_fout == NULL );
                _python_par_fout = value;

                found = true;
                kw_found += 1;
            }


            Py_DECREF( key );

            if ( found == false )
            {
               Py_DECREF( value );

               PyErr_Format(
                   PyExc_TypeError,
                   "biff_count_records() got an unexpected keyword argument '%s'",
                   Nuitka_String_Check( key ) ? Nuitka_String_AsString( key ) : "<non-string>"
               );

               goto error_exit;
            }
        }

#if PYTHON_VERSION < 300
        assert( kw_found == kw_size );
        assert( kw_only_found == 0 );
#endif
    }

    // Check if too many arguments were given in case of non star args
    if (unlikely( args_given > 4 ))
    {
#if PYTHON_VERSION < 270
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_size );
#elif PYTHON_VERSION < 330
        ERROR_TOO_MANY_ARGUMENTS( self, args_given + kw_found );
#else
        ERROR_TOO_MANY_ARGUMENTS( self, args_given, kw_only_found );
#endif
        goto error_exit;
    }


    // Copy normal parameter values given as part of the argument list to the
    // respective variables:

    if (likely( 0 < args_given ))
    {
         if (unlikely( _python_par_mem != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 0 );
             goto error_exit;
         }

        _python_par_mem = args[ 0 ];
        Py_INCREF( _python_par_mem );
    }
    else if ( _python_par_mem == NULL )
    {
        if ( 0 + self->m_defaults_given >= 4  )
        {
            _python_par_mem = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 0 - 4 );
            Py_INCREF( _python_par_mem );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 1 < args_given ))
    {
         if (unlikely( _python_par_stream_offset != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 1 );
             goto error_exit;
         }

        _python_par_stream_offset = args[ 1 ];
        Py_INCREF( _python_par_stream_offset );
    }
    else if ( _python_par_stream_offset == NULL )
    {
        if ( 1 + self->m_defaults_given >= 4  )
        {
            _python_par_stream_offset = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 1 - 4 );
            Py_INCREF( _python_par_stream_offset );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 2 < args_given ))
    {
         if (unlikely( _python_par_stream_len != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 2 );
             goto error_exit;
         }

        _python_par_stream_len = args[ 2 ];
        Py_INCREF( _python_par_stream_len );
    }
    else if ( _python_par_stream_len == NULL )
    {
        if ( 2 + self->m_defaults_given >= 4  )
        {
            _python_par_stream_len = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 2 - 4 );
            Py_INCREF( _python_par_stream_len );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }
    if (likely( 3 < args_given ))
    {
         if (unlikely( _python_par_fout != NULL ))
         {
             ERROR_MULTIPLE_VALUES( self, 3 );
             goto error_exit;
         }

        _python_par_fout = args[ 3 ];
        Py_INCREF( _python_par_fout );
    }
    else if ( _python_par_fout == NULL )
    {
        if ( 3 + self->m_defaults_given >= 4  )
        {
            _python_par_fout = PyTuple_GET_ITEM( self->m_defaults, self->m_defaults_given + 3 - 4 );
            Py_INCREF( _python_par_fout );
        }
#if PYTHON_VERSION < 330
        else
        {
#if PYTHON_VERSION < 270
            ERROR_TOO_FEW_ARGUMENTS( self, kw_size, args_given + kw_found );
#elif PYTHON_VERSION < 300
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found );
#else
            ERROR_TOO_FEW_ARGUMENTS( self, args_given + kw_found - kw_only_found );
#endif

            goto error_exit;
        }
#endif
    }

#if PYTHON_VERSION >= 330
    if (unlikely( _python_par_mem == NULL || _python_par_stream_offset == NULL || _python_par_stream_len == NULL || _python_par_fout == NULL ))
    {
        PyObject *values[] = { _python_par_mem, _python_par_stream_offset, _python_par_stream_len, _python_par_fout };
        ERROR_TOO_FEW_ARGUMENTS( self, values );

        goto error_exit;
    }
#endif


    return impl_function_13_biff_count_records_of_xlrd$biffh( self, _python_par_mem, _python_par_stream_offset, _python_par_stream_len, _python_par_fout );

error_exit:;

    Py_XDECREF( _python_par_mem );
    Py_XDECREF( _python_par_stream_offset );
    Py_XDECREF( _python_par_stream_len );
    Py_XDECREF( _python_par_fout );

    return NULL;
}

static PyObject *dparse_function_13_biff_count_records_of_xlrd$biffh( Nuitka_FunctionObject *self, PyObject **args, int size )
{
    if ( size == 4 )
    {
        return impl_function_13_biff_count_records_of_xlrd$biffh( self, INCREASE_REFCOUNT( args[ 0 ] ), INCREASE_REFCOUNT( args[ 1 ] ), INCREASE_REFCOUNT( args[ 2 ] ), INCREASE_REFCOUNT( args[ 3 ] ) );
    }
    else
    {
        PyObject *result = fparse_function_13_biff_count_records_of_xlrd$biffh( self, args, size, NULL );
        return result;
    }

}




static PyObject *MAKE_FUNCTION_function_10_unpack_cell_range_address_list_update_pos_of_xlrd$biffh( PyObject *defaults )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_10_unpack_cell_range_address_list_update_pos_of_xlrd$biffh,
        dparse_function_10_unpack_cell_range_address_list_update_pos_of_xlrd$biffh,
        const_str_digest_ca7becc9d49589e000ad29ce1ad566e8,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_37d8e658270520bbbb78114daec154c0,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd$biffh,
        Py_None
    );

    return result;
}



static PyObject *MAKE_FUNCTION_function_11_hex_char_dump_of_xlrd$biffh( PyObject *defaults )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_11_hex_char_dump_of_xlrd$biffh,
        dparse_function_11_hex_char_dump_of_xlrd$biffh,
        const_str_plain_hex_char_dump,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_a94fe701d8d9ee6a72feccc7707afaae,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd$biffh,
        Py_None
    );

    return result;
}



static PyObject *MAKE_FUNCTION_function_12_biff_dump_of_xlrd$biffh( PyObject *defaults )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_12_biff_dump_of_xlrd$biffh,
        dparse_function_12_biff_dump_of_xlrd$biffh,
        const_str_plain_biff_dump,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_97e99faadaa018455f7decf994313044,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd$biffh,
        Py_None
    );

    return result;
}



static PyObject *MAKE_FUNCTION_function_13_biff_count_records_of_xlrd$biffh( PyObject *defaults )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_13_biff_count_records_of_xlrd$biffh,
        dparse_function_13_biff_count_records_of_xlrd$biffh,
        const_str_plain_biff_count_records,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_e51d167ceb0c76b2ab1e73d7adefde31,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd$biffh,
        Py_None
    );

    return result;
}



static PyObject *MAKE_FUNCTION_function_1_dump_of_class_2_BaseObject_of_xlrd$biffh( PyObject *defaults )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_1_dump_of_class_2_BaseObject_of_xlrd$biffh,
        dparse_function_1_dump_of_class_2_BaseObject_of_xlrd$biffh,
        const_str_plain_dump,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_4ebfadaef41f3d75b1e9d7c4b5e0583c,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd$biffh,
        Py_None
    );

    return result;
}



static PyObject *MAKE_FUNCTION_function_3_is_cell_opcode_of_xlrd$biffh(  )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_3_is_cell_opcode_of_xlrd$biffh,
        dparse_function_3_is_cell_opcode_of_xlrd$biffh,
        const_str_plain_is_cell_opcode,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_5be5cdf2ab2da8906009df1bd7ebf3f1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd$biffh,
        Py_None
    );

    return result;
}



static PyObject *MAKE_FUNCTION_function_4_upkbits_of_xlrd$biffh( PyObject *defaults )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_4_upkbits_of_xlrd$biffh,
        dparse_function_4_upkbits_of_xlrd$biffh,
        const_str_plain_upkbits,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_8fd68a4528e572be93ef97238a9050aa,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd$biffh,
        Py_None
    );

    return result;
}



static PyObject *MAKE_FUNCTION_function_5_upkbitsL_of_xlrd$biffh( PyObject *defaults )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_5_upkbitsL_of_xlrd$biffh,
        dparse_function_5_upkbitsL_of_xlrd$biffh,
        const_str_plain_upkbitsL,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_3e6beaad15eb3e64b04a7b643fa43b3a,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd$biffh,
        Py_None
    );

    return result;
}



static PyObject *MAKE_FUNCTION_function_6_unpack_string_of_xlrd$biffh( PyObject *defaults )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_6_unpack_string_of_xlrd$biffh,
        dparse_function_6_unpack_string_of_xlrd$biffh,
        const_str_plain_unpack_string,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_1388f9aeddaf16831e766a08cb3c6c0f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd$biffh,
        Py_None
    );

    return result;
}



static PyObject *MAKE_FUNCTION_function_7_unpack_string_update_pos_of_xlrd$biffh( PyObject *defaults )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_7_unpack_string_update_pos_of_xlrd$biffh,
        dparse_function_7_unpack_string_update_pos_of_xlrd$biffh,
        const_str_plain_unpack_string_update_pos,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_94aeeee88a55179d7df58ca47aff0221,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd$biffh,
        Py_None
    );

    return result;
}



static PyObject *MAKE_FUNCTION_function_8_unpack_unicode_of_xlrd$biffh( PyObject *defaults )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_8_unpack_unicode_of_xlrd$biffh,
        dparse_function_8_unpack_unicode_of_xlrd$biffh,
        const_str_plain_unpack_unicode,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_f9afca4691fec58985424a48940d50f4,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd$biffh,
        const_str_digest_8989e5819a70580cc3034b2f9b4fbe1b
    );

    return result;
}



static PyObject *MAKE_FUNCTION_function_9_unpack_unicode_update_pos_of_xlrd$biffh( PyObject *defaults )
{
    PyObject *result = Nuitka_Function_New(
        fparse_function_9_unpack_unicode_update_pos_of_xlrd$biffh,
        dparse_function_9_unpack_unicode_update_pos_of_xlrd$biffh,
        const_str_plain_unpack_unicode_update_pos,
#if PYTHON_VERSION >= 330
        NULL,
#endif
        codeobj_c0775a082f861eac7fa3f350d3af47e1,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        const_dict_empty,
#endif
        module_xlrd$biffh,
        const_str_digest_035ddedf6744ab690912d979cd8804ae
    );

    return result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_xlrd$biffh =
{
    PyModuleDef_HEAD_INIT,
    "xlrd.biffh",   /* m_name */
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

#define _MODULE_UNFREEZER 0

#if _MODULE_UNFREEZER

#include "nuitka/unfreezing.hpp"

// Table for lookup to find "frozen" modules or DLLs, i.e. the ones included in
// or along this binary.

static struct Nuitka_MetaPathBasedLoaderEntry meta_path_loader_entries[] =
{

    { NULL, NULL, 0 }
};

#endif

// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL.

MOD_INIT_DECL( xlrd$biffh )
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_xlrd$biffh );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    // Initialize the compiled types of Nuitka.
    PyType_Ready( &Nuitka_Generator_Type );
    PyType_Ready( &Nuitka_Function_Type );
    PyType_Ready( &Nuitka_Method_Type );
    PyType_Ready( &Nuitka_Frame_Type );
#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

#endif

#if _MODULE_UNFREEZER
    registerMetaPathBasedUnfreezer( meta_path_loader_entries );
#endif

    _initModuleConstants();
    _initModuleCodeObjects();

    // puts( "in initxlrd$biffh" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_xlrd$biffh = Py_InitModule4(
        "xlrd.biffh",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No __doc__ is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else
    module_xlrd$biffh = PyModule_Create( &mdef_xlrd$biffh );
#endif

    moduledict_xlrd$biffh = (PyDictObject *)((PyModuleObject *)module_xlrd$biffh)->md_dict;

    CHECK_OBJECT( module_xlrd$biffh );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PySys_GetObject( (char *)"modules" ), const_str_digest_c8a0af0c0a1527d7b677e5153ac3cf44, module_xlrd$biffh );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    PyObject *module_dict = PyModule_GetDict( module_xlrd$biffh );

    if ( PyDict_GetItem( module_dict, const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

#ifndef __NUITKA_NO_ASSERT__
        int res =
#endif
            PyDict_SetItem( module_dict, const_str_plain___builtins__, value );

        assert( res == 0 );
    }

#if PYTHON_VERSION >= 330
#if _MODULE_UNFREEZER
    PyDict_SetItem( module_dict, const_str_plain___loader__, metapath_based_loader );
#else
    PyDict_SetItem( module_dict, const_str_plain___loader__, Py_None );
#endif
#endif

    // Temp variables if any
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__class = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__class_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__class = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *tmp_tuple_unpack_1__element_4 = NULL;
    PyObject *tmp_tuple_unpack_1__element_5 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__element_3 = NULL;
    PyObject *tmp_tuple_unpack_2__element_4 = NULL;
    PyObject *tmp_tuple_unpack_2__element_5 = NULL;
    PyObject *tmp_tuple_unpack_2__element_6 = NULL;
    PyObject *tmp_tuple_unpack_2__element_7 = NULL;
    PyObject *tmp_assign_unpack_1__assign_source = NULL;
    PyObject *tmp_assign_unpack_2__assign_source = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_tuple_unpack_3__source_iter = NULL;
    PyObject *tmp_tuple_unpack_3__element_1 = NULL;
    PyObject *tmp_tuple_unpack_3__element_2 = NULL;
    PyObject *exception_type, *exception_value;
    PyTracebackObject *exception_tb;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    PyObject *tmp_args_element_name_1;
    PyObject *tmp_args_element_name_2;
    PyObject *tmp_args_element_name_3;
    PyObject *tmp_args_element_name_4;
    PyObject *tmp_args_element_name_5;
    PyObject *tmp_args_element_name_6;
    PyObject *tmp_ass_subscribed_1;
    PyObject *tmp_ass_subscribed_2;
    PyObject *tmp_ass_subscript_1;
    PyObject *tmp_ass_subscript_2;
    PyObject *tmp_ass_subvalue_1;
    PyObject *tmp_ass_subvalue_2;
    PyObject *tmp_assign_source_1;
    PyObject *tmp_assign_source_2;
    PyObject *tmp_assign_source_3;
    PyObject *tmp_assign_source_4;
    PyObject *tmp_assign_source_5;
    PyObject *tmp_assign_source_6;
    PyObject *tmp_assign_source_7;
    PyObject *tmp_assign_source_8;
    PyObject *tmp_assign_source_9;
    PyObject *tmp_assign_source_10;
    PyObject *tmp_assign_source_11;
    PyObject *tmp_assign_source_12;
    PyObject *tmp_assign_source_13;
    PyObject *tmp_assign_source_14;
    PyObject *tmp_assign_source_15;
    PyObject *tmp_assign_source_16;
    PyObject *tmp_assign_source_17;
    PyObject *tmp_assign_source_18;
    PyObject *tmp_assign_source_19;
    PyObject *tmp_assign_source_20;
    PyObject *tmp_assign_source_21;
    PyObject *tmp_assign_source_22;
    PyObject *tmp_assign_source_23;
    PyObject *tmp_assign_source_24;
    PyObject *tmp_assign_source_25;
    PyObject *tmp_assign_source_26;
    PyObject *tmp_assign_source_27;
    PyObject *tmp_assign_source_28;
    PyObject *tmp_assign_source_29;
    PyObject *tmp_assign_source_30;
    PyObject *tmp_assign_source_31;
    PyObject *tmp_assign_source_32;
    PyObject *tmp_assign_source_33;
    PyObject *tmp_assign_source_34;
    PyObject *tmp_assign_source_35;
    PyObject *tmp_assign_source_36;
    PyObject *tmp_assign_source_37;
    PyObject *tmp_assign_source_38;
    PyObject *tmp_assign_source_39;
    PyObject *tmp_assign_source_40;
    PyObject *tmp_assign_source_41;
    PyObject *tmp_assign_source_42;
    PyObject *tmp_assign_source_43;
    PyObject *tmp_assign_source_44;
    PyObject *tmp_assign_source_45;
    PyObject *tmp_assign_source_46;
    PyObject *tmp_assign_source_47;
    PyObject *tmp_assign_source_48;
    PyObject *tmp_assign_source_49;
    PyObject *tmp_assign_source_50;
    PyObject *tmp_assign_source_51;
    PyObject *tmp_assign_source_52;
    PyObject *tmp_assign_source_53;
    PyObject *tmp_assign_source_54;
    PyObject *tmp_assign_source_55;
    PyObject *tmp_assign_source_56;
    PyObject *tmp_assign_source_57;
    PyObject *tmp_assign_source_58;
    PyObject *tmp_assign_source_59;
    PyObject *tmp_assign_source_60;
    PyObject *tmp_assign_source_61;
    PyObject *tmp_assign_source_62;
    PyObject *tmp_assign_source_63;
    PyObject *tmp_assign_source_64;
    PyObject *tmp_assign_source_65;
    PyObject *tmp_assign_source_66;
    PyObject *tmp_assign_source_67;
    PyObject *tmp_assign_source_68;
    PyObject *tmp_assign_source_69;
    PyObject *tmp_assign_source_70;
    PyObject *tmp_assign_source_71;
    PyObject *tmp_assign_source_72;
    PyObject *tmp_assign_source_73;
    PyObject *tmp_assign_source_74;
    PyObject *tmp_assign_source_75;
    PyObject *tmp_assign_source_76;
    PyObject *tmp_assign_source_77;
    PyObject *tmp_assign_source_78;
    PyObject *tmp_assign_source_79;
    PyObject *tmp_assign_source_80;
    PyObject *tmp_assign_source_81;
    PyObject *tmp_assign_source_82;
    PyObject *tmp_assign_source_83;
    PyObject *tmp_assign_source_84;
    PyObject *tmp_assign_source_85;
    PyObject *tmp_assign_source_86;
    PyObject *tmp_assign_source_87;
    PyObject *tmp_assign_source_88;
    PyObject *tmp_assign_source_89;
    PyObject *tmp_assign_source_90;
    PyObject *tmp_assign_source_91;
    PyObject *tmp_assign_source_92;
    PyObject *tmp_assign_source_93;
    PyObject *tmp_assign_source_94;
    PyObject *tmp_assign_source_95;
    PyObject *tmp_assign_source_96;
    PyObject *tmp_assign_source_97;
    PyObject *tmp_assign_source_98;
    PyObject *tmp_assign_source_99;
    PyObject *tmp_assign_source_100;
    PyObject *tmp_assign_source_101;
    PyObject *tmp_assign_source_102;
    PyObject *tmp_assign_source_103;
    PyObject *tmp_assign_source_104;
    PyObject *tmp_assign_source_105;
    PyObject *tmp_assign_source_106;
    PyObject *tmp_assign_source_107;
    PyObject *tmp_assign_source_108;
    PyObject *tmp_assign_source_109;
    PyObject *tmp_assign_source_110;
    PyObject *tmp_assign_source_111;
    PyObject *tmp_assign_source_112;
    PyObject *tmp_assign_source_113;
    PyObject *tmp_assign_source_114;
    PyObject *tmp_assign_source_115;
    PyObject *tmp_assign_source_116;
    PyObject *tmp_assign_source_117;
    PyObject *tmp_assign_source_118;
    PyObject *tmp_assign_source_119;
    PyObject *tmp_assign_source_120;
    PyObject *tmp_assign_source_121;
    PyObject *tmp_assign_source_122;
    PyObject *tmp_assign_source_123;
    PyObject *tmp_assign_source_124;
    PyObject *tmp_assign_source_125;
    PyObject *tmp_assign_source_126;
    PyObject *tmp_assign_source_127;
    PyObject *tmp_assign_source_128;
    PyObject *tmp_assign_source_129;
    PyObject *tmp_assign_source_130;
    PyObject *tmp_assign_source_131;
    PyObject *tmp_assign_source_132;
    PyObject *tmp_assign_source_133;
    PyObject *tmp_assign_source_134;
    PyObject *tmp_assign_source_135;
    PyObject *tmp_assign_source_136;
    PyObject *tmp_assign_source_137;
    PyObject *tmp_assign_source_138;
    PyObject *tmp_assign_source_139;
    PyObject *tmp_assign_source_140;
    PyObject *tmp_assign_source_141;
    PyObject *tmp_assign_source_142;
    PyObject *tmp_assign_source_143;
    PyObject *tmp_assign_source_144;
    PyObject *tmp_assign_source_145;
    PyObject *tmp_assign_source_146;
    PyObject *tmp_assign_source_147;
    PyObject *tmp_assign_source_148;
    PyObject *tmp_assign_source_149;
    PyObject *tmp_assign_source_150;
    PyObject *tmp_assign_source_151;
    PyObject *tmp_assign_source_152;
    PyObject *tmp_assign_source_153;
    PyObject *tmp_assign_source_154;
    PyObject *tmp_assign_source_155;
    PyObject *tmp_assign_source_156;
    PyObject *tmp_assign_source_157;
    PyObject *tmp_assign_source_158;
    PyObject *tmp_assign_source_159;
    PyObject *tmp_assign_source_160;
    PyObject *tmp_assign_source_161;
    PyObject *tmp_assign_source_162;
    PyObject *tmp_assign_source_163;
    PyObject *tmp_assign_source_164;
    PyObject *tmp_assign_source_165;
    PyObject *tmp_assign_source_166;
    PyObject *tmp_assign_source_167;
    PyObject *tmp_assign_source_168;
    PyObject *tmp_assign_source_169;
    PyObject *tmp_assign_source_170;
    PyObject *tmp_assign_source_171;
    PyObject *tmp_assign_source_172;
    PyObject *tmp_assign_source_173;
    PyObject *tmp_assign_source_174;
    PyObject *tmp_assign_source_175;
    PyObject *tmp_assign_source_176;
    PyObject *tmp_assign_source_177;
    PyObject *tmp_assign_source_178;
    PyObject *tmp_assign_source_179;
    PyObject *tmp_assign_source_180;
    PyObject *tmp_assign_source_181;
    PyObject *tmp_assign_source_182;
    PyObject *tmp_assign_source_183;
    PyObject *tmp_assign_source_184;
    PyObject *tmp_assign_source_185;
    PyObject *tmp_assign_source_186;
    PyObject *tmp_assign_source_187;
    PyObject *tmp_assign_source_188;
    PyObject *tmp_called_name_1;
    PyObject *tmp_called_name_2;
    PyObject *tmp_called_name_3;
    PyObject *tmp_called_name_4;
    PyObject *tmp_class_bases_1;
    PyObject *tmp_class_bases_2;
    int tmp_cmp_In_1;
    int tmp_cmp_In_2;
    PyObject *tmp_compare_left_1;
    PyObject *tmp_compare_left_2;
    PyObject *tmp_compare_right_1;
    PyObject *tmp_compare_right_2;
    PyObject *tmp_defaults_1;
    PyObject *tmp_defaults_2;
    PyObject *tmp_defaults_3;
    PyObject *tmp_defaults_4;
    PyObject *tmp_defaults_5;
    PyObject *tmp_defaults_6;
    PyObject *tmp_defaults_7;
    PyObject *tmp_defaults_8;
    PyObject *tmp_defaults_9;
    PyObject *tmp_defaults_10;
    PyObject *tmp_dget_dict_1;
    PyObject *tmp_dget_dict_2;
    PyObject *tmp_dget_key_1;
    PyObject *tmp_dget_key_2;
    PyObject *tmp_import_globals_1;
    PyObject *tmp_import_globals_2;
    PyObject *tmp_import_globals_3;
    PyObject *tmp_import_name_from_1;
    PyObject *tmp_int_base_1;
    PyObject *tmp_int_value_1;
    PyObject *tmp_iter_arg_1;
    PyObject *tmp_iter_arg_2;
    PyObject *tmp_iter_arg_3;
    PyObject *tmp_iter_arg_4;
    PyObject *tmp_iter_arg_5;
    PyObject *tmp_iterator_attempt;
    PyObject *tmp_iterator_name_1;
    PyObject *tmp_iterator_name_2;
    PyObject *tmp_iterator_name_3;
    PyObject *tmp_list_element_1;
    PyObject *tmp_next_source_1;
    PyObject *tmp_next_source_2;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_source_name_1;
    PyObject *tmp_source_name_2;
    PyObject *tmp_source_name_3;
    PyObject *tmp_source_name_4;
    PyObject *tmp_source_name_5;
    PyObject *tmp_star_imported_1;
    int tmp_tried_lineno_1;
    int tmp_tried_lineno_2;
    int tmp_tried_lineno_3;
    int tmp_tried_lineno_4;
    int tmp_tried_lineno_5;
    int tmp_tried_lineno_6;
    int tmp_tried_lineno_7;
    PyObject *tmp_tuple_element_1;
    PyObject *tmp_tuple_element_2;
    PyObject *tmp_tuple_element_3;
    PyObject *tmp_tuple_element_4;
    PyObject *tmp_tuple_element_5;
    PyObject *tmp_tuple_element_6;
    PyObject *tmp_tuple_element_7;
    PyObject *tmp_unpack_1;
    PyObject *tmp_unpack_2;
    PyObject *tmp_unpack_3;
    PyObject *tmp_unpack_4;
    PyObject *tmp_unpack_5;
    PyObject *tmp_unpack_6;
    PyObject *tmp_unpack_7;
    PyObject *tmp_unpack_8;
    PyObject *tmp_unpack_9;
    PyObject *tmp_unpack_10;
    PyObject *tmp_unpack_11;
    PyObject *tmp_unpack_12;
    PyObject *tmp_unpack_13;
    PyObject *tmp_unpack_14;
    PyFrameObject *frame_module;


    // Module code.
    tmp_assign_source_1 = Py_None;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    tmp_assign_source_2 = module_filename_obj;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    tmp_assign_source_3 = PyObject_GetAttrString(PyImport_ImportModule("__future__"), "print_function");
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_print_function, tmp_assign_source_3 );
    tmp_assign_source_4 = const_int_0;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_DEBUG, tmp_assign_source_4 );
    // Frame without reuse.
    frame_module = MAKE_FRAME( codeobj_650207a5b7da7dc4f354aeda34b91d67, module_xlrd$biffh );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_module );
    assert( Py_REFCNT( frame_module ) == 1 );

#if PYTHON_VERSION >= 340
    frame_module->f_executing += 1;
#endif

    // Framed code:
    tmp_import_globals_1 = ((PyModuleObject *)module_xlrd$biffh)->md_dict;
    frame_module->f_lineno = 23;
    tmp_import_name_from_1 = IMPORT_MODULE( const_str_plain_struct, tmp_import_globals_1, tmp_import_globals_1, const_tuple_str_plain_unpack_tuple, const_int_neg_1 );
    if ( tmp_import_name_from_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 23;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unpack );
    Py_DECREF( tmp_import_name_from_1 );
    if ( tmp_assign_source_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 23;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack, tmp_assign_source_5 );
    tmp_import_globals_2 = ((PyModuleObject *)module_xlrd$biffh)->md_dict;
    frame_module->f_lineno = 24;
    tmp_assign_source_6 = IMPORT_MODULE( const_str_plain_sys, tmp_import_globals_2, tmp_import_globals_2, Py_None, const_int_neg_1 );
    if ( tmp_assign_source_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 24;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_6 );
    tmp_import_globals_3 = ((PyModuleObject *)module_xlrd$biffh)->md_dict;
    frame_module->f_lineno = 25;
    tmp_star_imported_1 = IMPORT_MODULE( const_str_plain_timemachine, tmp_import_globals_3, tmp_import_globals_3, const_tuple_str_chr_42_tuple, const_int_pos_1 );
    if ( tmp_star_imported_1 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 25;
        goto frame_exception_exit_1;
    }
    tmp_result = IMPORT_MODULE_STAR( module_xlrd$biffh, true, tmp_star_imported_1 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_star_imported_1 );

        frame_module->f_lineno = 25;
        goto frame_exception_exit_1;
    }
    Py_DECREF( tmp_star_imported_1 );
    tmp_assign_source_7 = PyTuple_New( 1 );
    tmp_tuple_element_1 = PyExc_Exception;
    Py_INCREF( tmp_tuple_element_1 );
    PyTuple_SET_ITEM( tmp_assign_source_7, 0, tmp_tuple_element_1 );
    assert( tmp_class_creation_1__bases == NULL );
    tmp_class_creation_1__bases = tmp_assign_source_7;

    // Tried code
    tmp_assign_source_8 = impl_class_1_XLRDError_of_xlrd$biffh(  );
    if ( tmp_assign_source_8 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 27;
        goto try_finally_handler_1;
    }
    assert( tmp_class_creation_1__class_dict == NULL );
    tmp_class_creation_1__class_dict = tmp_assign_source_8;

    tmp_compare_left_1 = const_str_plain___metaclass__;
    tmp_compare_right_1 = tmp_class_creation_1__class_dict;

    tmp_cmp_In_1 = PySequence_Contains( tmp_compare_right_1, tmp_compare_left_1 );
    if ( tmp_cmp_In_1 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 27;
        goto try_finally_handler_1;
    }
    if (tmp_cmp_In_1 == 1)
    {
        goto condexpr_true_1;
    }
    else
    {
        goto condexpr_false_1;
    }
    condexpr_true_1:;
    tmp_dget_dict_1 = tmp_class_creation_1__class_dict;

    tmp_dget_key_1 = const_str_plain___metaclass__;
    tmp_assign_source_9 = DICT_GET_ITEM( tmp_dget_dict_1, tmp_dget_key_1 );
    if ( tmp_assign_source_9 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 27;
        goto try_finally_handler_1;
    }
    goto condexpr_end_1;
    condexpr_false_1:;
    tmp_class_bases_1 = tmp_class_creation_1__bases;

    tmp_assign_source_9 = SELECT_METACLASS( tmp_class_bases_1, GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain___metaclass__ ) );
    condexpr_end_1:;
    assert( tmp_class_creation_1__metaclass == NULL );
    tmp_class_creation_1__metaclass = tmp_assign_source_9;

    tmp_called_name_1 = tmp_class_creation_1__metaclass;

    tmp_args_element_name_1 = const_str_plain_XLRDError;
    tmp_args_element_name_2 = tmp_class_creation_1__bases;

    tmp_args_element_name_3 = tmp_class_creation_1__class_dict;

    frame_module->f_lineno = 27;
    tmp_assign_source_10 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 );
    if ( tmp_assign_source_10 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 27;
        goto try_finally_handler_1;
    }
    assert( tmp_class_creation_1__class == NULL );
    tmp_class_creation_1__class = tmp_assign_source_10;

    tmp_assign_source_11 = tmp_class_creation_1__class;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XLRDError, tmp_assign_source_11 );
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_1 = frame_module->f_lineno;
    Py_XDECREF( tmp_class_creation_1__class );
    tmp_class_creation_1__class = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_dict );
    tmp_class_creation_1__class_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    frame_module->f_lineno = tmp_tried_lineno_1;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_1 != NULL )
    {
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;

        goto frame_exception_exit_1;
    }

    goto finally_end_1;
    finally_end_1:;
    // Tried code
    tmp_assign_source_12 = PyTuple_New( 1 );
    tmp_tuple_element_2 = LOOKUP_BUILTIN( const_str_plain_object );
    if ( tmp_tuple_element_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_12 );

        frame_module->f_lineno = 34;
        goto try_finally_handler_2;
    }
    Py_INCREF( tmp_tuple_element_2 );
    PyTuple_SET_ITEM( tmp_assign_source_12, 0, tmp_tuple_element_2 );
    assert( tmp_class_creation_2__bases == NULL );
    tmp_class_creation_2__bases = tmp_assign_source_12;

    tmp_assign_source_13 = impl_class_2_BaseObject_of_xlrd$biffh(  );
    if ( tmp_assign_source_13 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 34;
        goto try_finally_handler_2;
    }
    assert( tmp_class_creation_2__class_dict == NULL );
    tmp_class_creation_2__class_dict = tmp_assign_source_13;

    tmp_compare_left_2 = const_str_plain___metaclass__;
    tmp_compare_right_2 = tmp_class_creation_2__class_dict;

    tmp_cmp_In_2 = PySequence_Contains( tmp_compare_right_2, tmp_compare_left_2 );
    if ( tmp_cmp_In_2 == -1 )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 34;
        goto try_finally_handler_2;
    }
    if (tmp_cmp_In_2 == 1)
    {
        goto condexpr_true_2;
    }
    else
    {
        goto condexpr_false_2;
    }
    condexpr_true_2:;
    tmp_dget_dict_2 = tmp_class_creation_2__class_dict;

    tmp_dget_key_2 = const_str_plain___metaclass__;
    tmp_assign_source_14 = DICT_GET_ITEM( tmp_dget_dict_2, tmp_dget_key_2 );
    if ( tmp_assign_source_14 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 34;
        goto try_finally_handler_2;
    }
    goto condexpr_end_2;
    condexpr_false_2:;
    tmp_class_bases_2 = tmp_class_creation_2__bases;

    tmp_assign_source_14 = SELECT_METACLASS( tmp_class_bases_2, GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain___metaclass__ ) );
    condexpr_end_2:;
    assert( tmp_class_creation_2__metaclass == NULL );
    tmp_class_creation_2__metaclass = tmp_assign_source_14;

    tmp_called_name_2 = tmp_class_creation_2__metaclass;

    tmp_args_element_name_4 = const_str_plain_BaseObject;
    tmp_args_element_name_5 = tmp_class_creation_2__bases;

    tmp_args_element_name_6 = tmp_class_creation_2__class_dict;

    frame_module->f_lineno = 34;
    tmp_assign_source_15 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 );
    if ( tmp_assign_source_15 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 34;
        goto try_finally_handler_2;
    }
    assert( tmp_class_creation_2__class == NULL );
    tmp_class_creation_2__class = tmp_assign_source_15;

    tmp_assign_source_16 = tmp_class_creation_2__class;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_BaseObject, tmp_assign_source_16 );
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_2 = frame_module->f_lineno;
    Py_XDECREF( tmp_class_creation_2__class );
    tmp_class_creation_2__class = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_dict );
    tmp_class_creation_2__class_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    frame_module->f_lineno = tmp_tried_lineno_2;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_2 != NULL )
    {
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;

        goto frame_exception_exit_1;
    }

    goto finally_end_2;
    finally_end_2:;
    // Tried code
    tmp_iter_arg_1 = const_tuple_int_0_int_pos_1_int_pos_2_int_pos_3_int_pos_4_tuple;
    tmp_assign_source_17 = MAKE_ITERATOR( tmp_iter_arg_1 );
    if ( tmp_assign_source_17 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 71;
        goto try_finally_handler_3;
    }
    assert( tmp_tuple_unpack_1__source_iter == NULL );
    tmp_tuple_unpack_1__source_iter = tmp_assign_source_17;

    tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_18 = UNPACK_NEXT( tmp_unpack_1, 0 );
    if ( tmp_assign_source_18 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_module->f_lineno = 71;
        goto try_finally_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_1 == NULL );
    tmp_tuple_unpack_1__element_1 = tmp_assign_source_18;

    tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_19 = UNPACK_NEXT( tmp_unpack_2, 1 );
    if ( tmp_assign_source_19 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_module->f_lineno = 71;
        goto try_finally_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_2 == NULL );
    tmp_tuple_unpack_1__element_2 = tmp_assign_source_19;

    tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_20 = UNPACK_NEXT( tmp_unpack_3, 2 );
    if ( tmp_assign_source_20 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_module->f_lineno = 71;
        goto try_finally_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_3 == NULL );
    tmp_tuple_unpack_1__element_3 = tmp_assign_source_20;

    tmp_unpack_4 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_21 = UNPACK_NEXT( tmp_unpack_4, 3 );
    if ( tmp_assign_source_21 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_module->f_lineno = 71;
        goto try_finally_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_4 == NULL );
    tmp_tuple_unpack_1__element_4 = tmp_assign_source_21;

    tmp_unpack_5 = tmp_tuple_unpack_1__source_iter;

    tmp_assign_source_22 = UNPACK_NEXT( tmp_unpack_5, 4 );
    if ( tmp_assign_source_22 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_module->f_lineno = 71;
        goto try_finally_handler_3;
    }
    assert( tmp_tuple_unpack_1__element_5 == NULL );
    tmp_tuple_unpack_1__element_5 = tmp_assign_source_22;

    tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;

    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                goto try_finally_handler_3;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 5)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        goto try_finally_handler_3;
    }
    tmp_assign_source_23 = tmp_tuple_unpack_1__element_1;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_FUN, tmp_assign_source_23 );
    tmp_assign_source_24 = tmp_tuple_unpack_1__element_2;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_FDT, tmp_assign_source_24 );
    tmp_assign_source_25 = tmp_tuple_unpack_1__element_3;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_FNU, tmp_assign_source_25 );
    tmp_assign_source_26 = tmp_tuple_unpack_1__element_4;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_FGE, tmp_assign_source_26 );
    tmp_assign_source_27 = tmp_tuple_unpack_1__element_5;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_FTX, tmp_assign_source_27 );
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_3 = frame_module->f_lineno;
    Py_XDECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_4 );
    tmp_tuple_unpack_1__element_4 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_5 );
    tmp_tuple_unpack_1__element_5 = NULL;

    frame_module->f_lineno = tmp_tried_lineno_3;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_3 != NULL )
    {
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;

        goto frame_exception_exit_1;
    }

    goto finally_end_3;
    finally_end_3:;
    tmp_assign_source_28 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_FDT );

    if (unlikely( tmp_assign_source_28 == NULL ))
    {
        tmp_assign_source_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FDT );
    }

    if ( tmp_assign_source_28 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9650 ], 25, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 72;
        goto frame_exception_exit_1;
    }

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_DATEFORMAT, tmp_assign_source_28 );
    tmp_assign_source_29 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_FNU );

    if (unlikely( tmp_assign_source_29 == NULL ))
    {
        tmp_assign_source_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FNU );
    }

    if ( tmp_assign_source_29 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9675 ], 25, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 73;
        goto frame_exception_exit_1;
    }

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_NUMBERFORMAT, tmp_assign_source_29 );
    // Tried code
    tmp_iter_arg_2 = const_tuple_2d7a42b9ddf265e370cf80ec4093139a_tuple;
    tmp_assign_source_30 = MAKE_ITERATOR( tmp_iter_arg_2 );
    if ( tmp_assign_source_30 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 75;
        goto try_finally_handler_4;
    }
    assert( tmp_tuple_unpack_2__source_iter == NULL );
    tmp_tuple_unpack_2__source_iter = tmp_assign_source_30;

    tmp_unpack_6 = tmp_tuple_unpack_2__source_iter;

    tmp_assign_source_31 = UNPACK_NEXT( tmp_unpack_6, 0 );
    if ( tmp_assign_source_31 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_module->f_lineno = 75;
        goto try_finally_handler_4;
    }
    assert( tmp_tuple_unpack_2__element_1 == NULL );
    tmp_tuple_unpack_2__element_1 = tmp_assign_source_31;

    tmp_unpack_7 = tmp_tuple_unpack_2__source_iter;

    tmp_assign_source_32 = UNPACK_NEXT( tmp_unpack_7, 1 );
    if ( tmp_assign_source_32 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_module->f_lineno = 75;
        goto try_finally_handler_4;
    }
    assert( tmp_tuple_unpack_2__element_2 == NULL );
    tmp_tuple_unpack_2__element_2 = tmp_assign_source_32;

    tmp_unpack_8 = tmp_tuple_unpack_2__source_iter;

    tmp_assign_source_33 = UNPACK_NEXT( tmp_unpack_8, 2 );
    if ( tmp_assign_source_33 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_module->f_lineno = 75;
        goto try_finally_handler_4;
    }
    assert( tmp_tuple_unpack_2__element_3 == NULL );
    tmp_tuple_unpack_2__element_3 = tmp_assign_source_33;

    tmp_unpack_9 = tmp_tuple_unpack_2__source_iter;

    tmp_assign_source_34 = UNPACK_NEXT( tmp_unpack_9, 3 );
    if ( tmp_assign_source_34 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_module->f_lineno = 75;
        goto try_finally_handler_4;
    }
    assert( tmp_tuple_unpack_2__element_4 == NULL );
    tmp_tuple_unpack_2__element_4 = tmp_assign_source_34;

    tmp_unpack_10 = tmp_tuple_unpack_2__source_iter;

    tmp_assign_source_35 = UNPACK_NEXT( tmp_unpack_10, 4 );
    if ( tmp_assign_source_35 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_module->f_lineno = 75;
        goto try_finally_handler_4;
    }
    assert( tmp_tuple_unpack_2__element_5 == NULL );
    tmp_tuple_unpack_2__element_5 = tmp_assign_source_35;

    tmp_unpack_11 = tmp_tuple_unpack_2__source_iter;

    tmp_assign_source_36 = UNPACK_NEXT( tmp_unpack_11, 5 );
    if ( tmp_assign_source_36 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_module->f_lineno = 75;
        goto try_finally_handler_4;
    }
    assert( tmp_tuple_unpack_2__element_6 == NULL );
    tmp_tuple_unpack_2__element_6 = tmp_assign_source_36;

    tmp_unpack_12 = tmp_tuple_unpack_2__source_iter;

    tmp_assign_source_37 = UNPACK_NEXT( tmp_unpack_12, 6 );
    if ( tmp_assign_source_37 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_module->f_lineno = 75;
        goto try_finally_handler_4;
    }
    assert( tmp_tuple_unpack_2__element_7 == NULL );
    tmp_tuple_unpack_2__element_7 = tmp_assign_source_37;

    tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;

    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                goto try_finally_handler_4;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 7)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        goto try_finally_handler_4;
    }
    tmp_assign_source_38 = tmp_tuple_unpack_2__element_1;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_CELL_EMPTY, tmp_assign_source_38 );
    tmp_assign_source_39 = tmp_tuple_unpack_2__element_2;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_CELL_TEXT, tmp_assign_source_39 );
    tmp_assign_source_40 = tmp_tuple_unpack_2__element_3;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_CELL_NUMBER, tmp_assign_source_40 );
    tmp_assign_source_41 = tmp_tuple_unpack_2__element_4;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_CELL_DATE, tmp_assign_source_41 );
    tmp_assign_source_42 = tmp_tuple_unpack_2__element_5;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_CELL_BOOLEAN, tmp_assign_source_42 );
    tmp_assign_source_43 = tmp_tuple_unpack_2__element_6;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_CELL_ERROR, tmp_assign_source_43 );
    tmp_assign_source_44 = tmp_tuple_unpack_2__element_7;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_CELL_BLANK, tmp_assign_source_44 );
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_4 = frame_module->f_lineno;
    Py_XDECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_3 );
    tmp_tuple_unpack_2__element_3 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_4 );
    tmp_tuple_unpack_2__element_4 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_5 );
    tmp_tuple_unpack_2__element_5 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_6 );
    tmp_tuple_unpack_2__element_6 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_7 );
    tmp_tuple_unpack_2__element_7 = NULL;

    frame_module->f_lineno = tmp_tried_lineno_4;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_4 != NULL )
    {
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;

        goto frame_exception_exit_1;
    }

    goto finally_end_4;
    finally_end_4:;
    tmp_assign_source_45 = PyDict_Copy( const_dict_ff5080d8cdf9d4b5eabcea109a63e8c9 );
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_biff_text_from_num, tmp_assign_source_45 );
    tmp_assign_source_46 = PyDict_Copy( const_dict_6ca13ccd13dd5d5b66b3d9a86dbc0eb0 );
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_error_text_from_code, tmp_assign_source_46 );
    tmp_assign_source_47 = const_int_pos_80;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_BIFF_FIRST_UNICODE, tmp_assign_source_47 );
    tmp_assign_source_48 = const_int_pos_5;
    assert( tmp_assign_unpack_1__assign_source == NULL );
    Py_INCREF( tmp_assign_source_48 );
    tmp_assign_unpack_1__assign_source = tmp_assign_source_48;

    tmp_assign_source_49 = const_int_pos_5;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_WORKBOOK_GLOBALS, tmp_assign_source_49 );
    tmp_assign_source_50 = const_int_pos_5;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_WBKBLOBAL, tmp_assign_source_50 );
    CHECK_OBJECT( (PyObject *)tmp_assign_unpack_1__assign_source );
    Py_DECREF( tmp_assign_unpack_1__assign_source );
    tmp_assign_unpack_1__assign_source = NULL;

    tmp_assign_source_51 = const_int_pos_256;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_WORKBOOK_GLOBALS_4W, tmp_assign_source_51 );
    tmp_assign_source_52 = const_int_pos_16;
    assert( tmp_assign_unpack_2__assign_source == NULL );
    Py_INCREF( tmp_assign_source_52 );
    tmp_assign_unpack_2__assign_source = tmp_assign_source_52;

    tmp_assign_source_53 = const_int_pos_16;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_WORKSHEET, tmp_assign_source_53 );
    tmp_assign_source_54 = const_int_pos_16;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_WRKSHEET, tmp_assign_source_54 );
    CHECK_OBJECT( (PyObject *)tmp_assign_unpack_2__assign_source );
    Py_DECREF( tmp_assign_unpack_2__assign_source );
    tmp_assign_unpack_2__assign_source = NULL;

    tmp_assign_source_55 = const_int_0;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_BOUNDSHEET_WORKSHEET, tmp_assign_source_55 );
    tmp_assign_source_56 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_BOUNDSHEET_CHART, tmp_assign_source_56 );
    tmp_assign_source_57 = const_int_pos_6;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_BOUNDSHEET_VB_MODULE, tmp_assign_source_57 );
    tmp_assign_source_58 = const_int_pos_545;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_ARRAY, tmp_assign_source_58 );
    tmp_assign_source_59 = const_int_pos_33;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_ARRAY2, tmp_assign_source_59 );
    tmp_assign_source_60 = const_int_pos_513;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_BLANK, tmp_assign_source_60 );
    tmp_assign_source_61 = const_int_pos_1;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_BLANK_B2, tmp_assign_source_61 );
    tmp_assign_source_62 = const_int_pos_2057;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_BOF, tmp_assign_source_62 );
    tmp_assign_source_63 = const_int_pos_517;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_BOOLERR, tmp_assign_source_63 );
    tmp_assign_source_64 = const_int_pos_5;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_BOOLERR_B2, tmp_assign_source_64 );
    tmp_assign_source_65 = const_int_pos_133;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_BOUNDSHEET, tmp_assign_source_65 );
    tmp_assign_source_66 = const_int_pos_86;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_BUILTINFMTCOUNT, tmp_assign_source_66 );
    tmp_assign_source_67 = const_int_pos_433;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_CF, tmp_assign_source_67 );
    tmp_assign_source_68 = const_int_pos_66;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_CODEPAGE, tmp_assign_source_68 );
    tmp_assign_source_69 = const_int_pos_125;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_COLINFO, tmp_assign_source_69 );
    tmp_assign_source_70 = const_int_pos_32;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_COLUMNDEFAULT, tmp_assign_source_70 );
    tmp_assign_source_71 = const_int_pos_36;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_COLWIDTH, tmp_assign_source_71 );
    tmp_assign_source_72 = const_int_pos_432;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_CONDFMT, tmp_assign_source_72 );
    tmp_assign_source_73 = const_int_pos_60;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_CONTINUE, tmp_assign_source_73 );
    tmp_assign_source_74 = const_int_pos_140;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_COUNTRY, tmp_assign_source_74 );
    tmp_assign_source_75 = const_int_pos_34;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_DATEMODE, tmp_assign_source_75 );
    tmp_assign_source_76 = const_int_pos_549;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_DEFAULTROWHEIGHT, tmp_assign_source_76 );
    tmp_assign_source_77 = const_int_pos_85;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_DEFCOLWIDTH, tmp_assign_source_77 );
    tmp_assign_source_78 = const_int_pos_512;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_DIMENSION, tmp_assign_source_78 );
    tmp_assign_source_79 = const_int_0;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_DIMENSION2, tmp_assign_source_79 );
    tmp_assign_source_80 = const_int_pos_69;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_EFONT, tmp_assign_source_80 );
    tmp_assign_source_81 = const_int_pos_10;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_EOF, tmp_assign_source_81 );
    tmp_assign_source_82 = const_int_pos_35;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_EXTERNNAME, tmp_assign_source_82 );
    tmp_assign_source_83 = const_int_pos_23;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_EXTERNSHEET, tmp_assign_source_83 );
    tmp_assign_source_84 = const_int_pos_255;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_EXTSST, tmp_assign_source_84 );
    tmp_assign_source_85 = const_int_pos_2162;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_FEAT11, tmp_assign_source_85 );
    tmp_assign_source_86 = const_int_pos_47;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_FILEPASS, tmp_assign_source_86 );
    tmp_assign_source_87 = const_int_pos_49;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_FONT, tmp_assign_source_87 );
    tmp_assign_source_88 = const_int_pos_561;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_FONT_B3B4, tmp_assign_source_88 );
    tmp_assign_source_89 = const_int_pos_1054;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_FORMAT, tmp_assign_source_89 );
    tmp_assign_source_90 = const_int_pos_30;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_FORMAT2, tmp_assign_source_90 );
    tmp_assign_source_91 = const_int_pos_6;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_FORMULA, tmp_assign_source_91 );
    tmp_assign_source_92 = const_int_pos_518;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_FORMULA3, tmp_assign_source_92 );
    tmp_assign_source_93 = const_int_pos_1030;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_FORMULA4, tmp_assign_source_93 );
    tmp_assign_source_94 = const_int_pos_171;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_GCW, tmp_assign_source_94 );
    tmp_assign_source_95 = const_int_pos_440;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_HLINK, tmp_assign_source_95 );
    tmp_assign_source_96 = const_int_pos_2048;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_QUICKTIP, tmp_assign_source_96 );
    tmp_assign_source_97 = const_int_pos_27;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_HORIZONTALPAGEBREAKS, tmp_assign_source_97 );
    tmp_assign_source_98 = const_int_pos_523;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_INDEX, tmp_assign_source_98 );
    tmp_assign_source_99 = const_int_pos_2;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_INTEGER, tmp_assign_source_99 );
    tmp_assign_source_100 = const_int_pos_68;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_IXFE, tmp_assign_source_100 );
    tmp_assign_source_101 = const_int_pos_516;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_LABEL, tmp_assign_source_101 );
    tmp_assign_source_102 = const_int_pos_4;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_LABEL_B2, tmp_assign_source_102 );
    tmp_assign_source_103 = const_int_pos_351;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_LABELRANGES, tmp_assign_source_103 );
    tmp_assign_source_104 = const_int_pos_253;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_LABELSST, tmp_assign_source_104 );
    tmp_assign_source_105 = const_int_pos_38;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_LEFTMARGIN, tmp_assign_source_105 );
    tmp_assign_source_106 = const_int_pos_40;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_TOPMARGIN, tmp_assign_source_106 );
    tmp_assign_source_107 = const_int_pos_39;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_RIGHTMARGIN, tmp_assign_source_107 );
    tmp_assign_source_108 = const_int_pos_41;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_BOTTOMMARGIN, tmp_assign_source_108 );
    tmp_assign_source_109 = const_int_pos_20;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_HEADER, tmp_assign_source_109 );
    tmp_assign_source_110 = const_int_pos_21;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_FOOTER, tmp_assign_source_110 );
    tmp_assign_source_111 = const_int_pos_131;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_HCENTER, tmp_assign_source_111 );
    tmp_assign_source_112 = const_int_pos_132;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_VCENTER, tmp_assign_source_112 );
    tmp_assign_source_113 = const_int_pos_229;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_MERGEDCELLS, tmp_assign_source_113 );
    tmp_assign_source_114 = const_int_pos_236;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_MSO_DRAWING, tmp_assign_source_114 );
    tmp_assign_source_115 = const_int_pos_235;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_MSO_DRAWING_GROUP, tmp_assign_source_115 );
    tmp_assign_source_116 = const_int_pos_237;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_MSO_DRAWING_SELECTION, tmp_assign_source_116 );
    tmp_assign_source_117 = const_int_pos_189;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_MULRK, tmp_assign_source_117 );
    tmp_assign_source_118 = const_int_pos_190;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_MULBLANK, tmp_assign_source_118 );
    tmp_assign_source_119 = const_int_pos_24;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_NAME, tmp_assign_source_119 );
    tmp_assign_source_120 = const_int_pos_28;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_NOTE, tmp_assign_source_120 );
    tmp_assign_source_121 = const_int_pos_515;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_NUMBER, tmp_assign_source_121 );
    tmp_assign_source_122 = const_int_pos_3;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_NUMBER_B2, tmp_assign_source_122 );
    tmp_assign_source_123 = const_int_pos_93;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_OBJ, tmp_assign_source_123 );
    tmp_assign_source_124 = const_int_pos_161;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_PAGESETUP, tmp_assign_source_124 );
    tmp_assign_source_125 = const_int_pos_146;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_PALETTE, tmp_assign_source_125 );
    tmp_assign_source_126 = const_int_pos_65;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_PANE, tmp_assign_source_126 );
    tmp_assign_source_127 = const_int_pos_43;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_PRINTGRIDLINES, tmp_assign_source_127 );
    tmp_assign_source_128 = const_int_pos_42;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_PRINTHEADERS, tmp_assign_source_128 );
    tmp_assign_source_129 = const_int_pos_638;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_RK, tmp_assign_source_129 );
    tmp_assign_source_130 = const_int_pos_520;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_ROW, tmp_assign_source_130 );
    tmp_assign_source_131 = const_int_pos_8;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_ROW_B2, tmp_assign_source_131 );
    tmp_assign_source_132 = const_int_pos_214;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_RSTRING, tmp_assign_source_132 );
    tmp_assign_source_133 = const_int_pos_160;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_SCL, tmp_assign_source_133 );
    tmp_assign_source_134 = const_int_pos_143;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_SHEETHDR, tmp_assign_source_134 );
    tmp_assign_source_135 = const_int_pos_129;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_SHEETPR, tmp_assign_source_135 );
    tmp_assign_source_136 = const_int_pos_142;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_SHEETSOFFSET, tmp_assign_source_136 );
    tmp_assign_source_137 = const_int_pos_1212;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_SHRFMLA, tmp_assign_source_137 );
    tmp_assign_source_138 = const_int_pos_252;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_SST, tmp_assign_source_138 );
    tmp_assign_source_139 = const_int_pos_153;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_STANDARDWIDTH, tmp_assign_source_139 );
    tmp_assign_source_140 = const_int_pos_519;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_STRING, tmp_assign_source_140 );
    tmp_assign_source_141 = const_int_pos_7;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_STRING_B2, tmp_assign_source_141 );
    tmp_assign_source_142 = const_int_pos_659;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_STYLE, tmp_assign_source_142 );
    tmp_assign_source_143 = const_int_pos_430;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_SUPBOOK, tmp_assign_source_143 );
    tmp_assign_source_144 = const_int_pos_566;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_TABLEOP, tmp_assign_source_144 );
    tmp_assign_source_145 = const_int_pos_55;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_TABLEOP2, tmp_assign_source_145 );
    tmp_assign_source_146 = const_int_pos_54;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_TABLEOP_B2, tmp_assign_source_146 );
    tmp_assign_source_147 = const_int_pos_438;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_TXO, tmp_assign_source_147 );
    tmp_assign_source_148 = const_int_pos_94;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_UNCALCED, tmp_assign_source_148 );
    tmp_assign_source_149 = const_int_pos_65535;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_UNKNOWN, tmp_assign_source_149 );
    tmp_assign_source_150 = const_int_pos_26;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_VERTICALPAGEBREAKS, tmp_assign_source_150 );
    tmp_assign_source_151 = const_int_pos_574;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_WINDOW2, tmp_assign_source_151 );
    tmp_assign_source_152 = const_int_pos_62;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_WINDOW2_B2, tmp_assign_source_152 );
    tmp_assign_source_153 = const_int_pos_92;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_WRITEACCESS, tmp_assign_source_153 );
    tmp_assign_source_154 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_SHEETPR );

    if (unlikely( tmp_assign_source_154 == NULL ))
    {
        tmp_assign_source_154 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XL_SHEETPR );
    }

    if ( tmp_assign_source_154 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9700 ], 32, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 228;
        goto frame_exception_exit_1;
    }

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_WSBOOL, tmp_assign_source_154 );
    tmp_assign_source_155 = const_int_pos_224;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_XF, tmp_assign_source_155 );
    tmp_assign_source_156 = const_int_pos_67;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_XF2, tmp_assign_source_156 );
    tmp_assign_source_157 = const_int_pos_579;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_XF3, tmp_assign_source_157 );
    tmp_assign_source_158 = const_int_pos_1091;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_XF4, tmp_assign_source_158 );
    tmp_assign_source_159 = PyDict_Copy( const_dict_a3a2e5d3cfb5ecd8166a51f69633413e );
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_boflen, tmp_assign_source_159 );
    tmp_assign_source_160 = const_tuple_int_pos_2057_int_pos_1033_int_pos_521_int_pos_9_tuple;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_bofcodes, tmp_assign_source_160 );
    tmp_assign_source_161 = const_tuple_int_pos_6_int_pos_1030_int_pos_518_tuple;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_FORMULA_OPCODES, tmp_assign_source_161 );
    tmp_assign_source_162 = PyList_New( 10 );
    tmp_list_element_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_BOOLERR );

    if (unlikely( tmp_list_element_1 == NULL ))
    {
        tmp_list_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XL_BOOLERR );
    }

    if ( tmp_list_element_1 == NULL )
    {
        Py_DECREF( tmp_assign_source_162 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9732 ], 32, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 240;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_assign_source_162, 0, tmp_list_element_1 );
    tmp_list_element_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_FORMULA );

    if (unlikely( tmp_list_element_1 == NULL ))
    {
        tmp_list_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XL_FORMULA );
    }

    if ( tmp_list_element_1 == NULL )
    {
        Py_DECREF( tmp_assign_source_162 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9764 ], 32, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 241;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_assign_source_162, 1, tmp_list_element_1 );
    tmp_list_element_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_FORMULA3 );

    if (unlikely( tmp_list_element_1 == NULL ))
    {
        tmp_list_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XL_FORMULA3 );
    }

    if ( tmp_list_element_1 == NULL )
    {
        Py_DECREF( tmp_assign_source_162 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9796 ], 33, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 242;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_assign_source_162, 2, tmp_list_element_1 );
    tmp_list_element_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_FORMULA4 );

    if (unlikely( tmp_list_element_1 == NULL ))
    {
        tmp_list_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XL_FORMULA4 );
    }

    if ( tmp_list_element_1 == NULL )
    {
        Py_DECREF( tmp_assign_source_162 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9829 ], 33, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 243;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_assign_source_162, 3, tmp_list_element_1 );
    tmp_list_element_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_LABEL );

    if (unlikely( tmp_list_element_1 == NULL ))
    {
        tmp_list_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XL_LABEL );
    }

    if ( tmp_list_element_1 == NULL )
    {
        Py_DECREF( tmp_assign_source_162 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9862 ], 30, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 244;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_assign_source_162, 4, tmp_list_element_1 );
    tmp_list_element_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_LABELSST );

    if (unlikely( tmp_list_element_1 == NULL ))
    {
        tmp_list_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XL_LABELSST );
    }

    if ( tmp_list_element_1 == NULL )
    {
        Py_DECREF( tmp_assign_source_162 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9892 ], 33, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 245;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_assign_source_162, 5, tmp_list_element_1 );
    tmp_list_element_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_MULRK );

    if (unlikely( tmp_list_element_1 == NULL ))
    {
        tmp_list_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XL_MULRK );
    }

    if ( tmp_list_element_1 == NULL )
    {
        Py_DECREF( tmp_assign_source_162 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9925 ], 30, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 246;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_assign_source_162, 6, tmp_list_element_1 );
    tmp_list_element_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_NUMBER );

    if (unlikely( tmp_list_element_1 == NULL ))
    {
        tmp_list_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XL_NUMBER );
    }

    if ( tmp_list_element_1 == NULL )
    {
        Py_DECREF( tmp_assign_source_162 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9955 ], 31, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 247;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_assign_source_162, 7, tmp_list_element_1 );
    tmp_list_element_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_RK );

    if (unlikely( tmp_list_element_1 == NULL ))
    {
        tmp_list_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XL_RK );
    }

    if ( tmp_list_element_1 == NULL )
    {
        Py_DECREF( tmp_assign_source_162 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 9986 ], 27, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 248;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_assign_source_162, 8, tmp_list_element_1 );
    tmp_list_element_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_XL_RSTRING );

    if (unlikely( tmp_list_element_1 == NULL ))
    {
        tmp_list_element_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_XL_RSTRING );
    }

    if ( tmp_list_element_1 == NULL )
    {
        Py_DECREF( tmp_assign_source_162 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10013 ], 32, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 249;
        goto frame_exception_exit_1;
    }

    Py_INCREF( tmp_list_element_1 );
    PyList_SET_ITEM( tmp_assign_source_162, 9, tmp_list_element_1 );
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__cell_opcode_list, tmp_assign_source_162 );
    tmp_assign_source_163 = PyDict_New();
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__cell_opcode_dict, tmp_assign_source_163 );
    tmp_iter_arg_3 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__cell_opcode_list );

    if (unlikely( tmp_iter_arg_3 == NULL ))
    {
        tmp_iter_arg_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__cell_opcode_list );
    }

    if ( tmp_iter_arg_3 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10045 ], 39, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 252;
        goto frame_exception_exit_1;
    }

    tmp_assign_source_164 = MAKE_ITERATOR( tmp_iter_arg_3 );
    if ( tmp_assign_source_164 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 252;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_1__for_iterator == NULL );
    tmp_for_loop_1__for_iterator = tmp_assign_source_164;

    // Tried code
    loop_start_1:;
    tmp_next_source_1 = tmp_for_loop_1__for_iterator;

    tmp_assign_source_165 = ITERATOR_NEXT( tmp_next_source_1 );
    if ( tmp_assign_source_165 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_1;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_module->f_lineno = 252;
            goto try_finally_handler_5;
        }
    }

    {
        PyObject *old = tmp_for_loop_1__iter_value;
        tmp_for_loop_1__iter_value = tmp_assign_source_165;
        Py_XDECREF( old );
    }

    tmp_assign_source_166 = tmp_for_loop_1__iter_value;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__cell_opcode, tmp_assign_source_166 );
    tmp_ass_subvalue_1 = const_int_pos_1;
    tmp_ass_subscribed_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__cell_opcode_dict );

    if (unlikely( tmp_ass_subscribed_1 == NULL ))
    {
        tmp_ass_subscribed_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__cell_opcode_dict );
    }

    if ( tmp_ass_subscribed_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10084 ], 39, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 253;
        goto try_finally_handler_5;
    }

    tmp_ass_subscript_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__cell_opcode );

    if (unlikely( tmp_ass_subscript_1 == NULL ))
    {
        tmp_ass_subscript_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__cell_opcode );
    }

    if ( tmp_ass_subscript_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10123 ], 34, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 253;
        goto try_finally_handler_5;
    }

    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 253;
        goto try_finally_handler_5;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 252;
        goto try_finally_handler_5;
    }
    goto loop_start_1;
    loop_end_1:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_5 = frame_module->f_lineno;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    frame_module->f_lineno = tmp_tried_lineno_5;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_5 != NULL )
    {
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;

        goto frame_exception_exit_1;
    }

    goto finally_end_5;
    finally_end_5:;
    tmp_assign_source_167 = MAKE_FUNCTION_function_3_is_cell_opcode_of_xlrd$biffh(  );
    if ( tmp_assign_source_167 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_167 );

        frame_module->f_lineno = 255;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_is_cell_opcode, tmp_assign_source_167 );
    tmp_defaults_1 = PyTuple_New( 1 );
    tmp_tuple_element_3 = LOOKUP_BUILTIN( const_str_plain_setattr );
    if ( tmp_tuple_element_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_defaults_1 );

        frame_module->f_lineno = 258;
        goto frame_exception_exit_1;
    }
    Py_INCREF( tmp_tuple_element_3 );
    PyTuple_SET_ITEM( tmp_defaults_1, 0, tmp_tuple_element_3 );
    tmp_assign_source_168 = MAKE_FUNCTION_function_4_upkbits_of_xlrd$biffh( tmp_defaults_1 );
    if ( tmp_assign_source_168 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_168 );

        frame_module->f_lineno = 258;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_upkbits, tmp_assign_source_168 );
    tmp_defaults_2 = PyTuple_New( 2 );
    tmp_tuple_element_4 = LOOKUP_BUILTIN( const_str_plain_setattr );
    if ( tmp_tuple_element_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_defaults_2 );

        frame_module->f_lineno = 262;
        goto frame_exception_exit_1;
    }
    Py_INCREF( tmp_tuple_element_4 );
    PyTuple_SET_ITEM( tmp_defaults_2, 0, tmp_tuple_element_4 );
    tmp_tuple_element_4 = LOOKUP_BUILTIN( const_str_plain_int );
    if ( tmp_tuple_element_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_defaults_2 );

        frame_module->f_lineno = 262;
        goto frame_exception_exit_1;
    }
    Py_INCREF( tmp_tuple_element_4 );
    PyTuple_SET_ITEM( tmp_defaults_2, 1, tmp_tuple_element_4 );
    tmp_assign_source_169 = MAKE_FUNCTION_function_5_upkbitsL_of_xlrd$biffh( tmp_defaults_2 );
    if ( tmp_assign_source_169 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_169 );

        frame_module->f_lineno = 262;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_upkbitsL, tmp_assign_source_169 );
    tmp_defaults_3 = const_tuple_int_pos_1_tuple;
    tmp_assign_source_170 = MAKE_FUNCTION_function_6_unpack_string_of_xlrd$biffh( INCREASE_REFCOUNT( tmp_defaults_3 ) );
    if ( tmp_assign_source_170 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_170 );

        frame_module->f_lineno = 266;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack_string, tmp_assign_source_170 );
    tmp_defaults_4 = const_tuple_int_pos_1_none_tuple;
    tmp_assign_source_171 = MAKE_FUNCTION_function_7_unpack_string_update_pos_of_xlrd$biffh( INCREASE_REFCOUNT( tmp_defaults_4 ) );
    if ( tmp_assign_source_171 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_171 );

        frame_module->f_lineno = 271;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack_string_update_pos, tmp_assign_source_171 );
    tmp_defaults_5 = const_tuple_int_pos_2_tuple;
    tmp_assign_source_172 = MAKE_FUNCTION_function_8_unpack_unicode_of_xlrd$biffh( INCREASE_REFCOUNT( tmp_defaults_5 ) );
    if ( tmp_assign_source_172 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_172 );

        frame_module->f_lineno = 281;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack_unicode, tmp_assign_source_172 );
    tmp_defaults_6 = const_tuple_int_pos_2_none_tuple;
    tmp_assign_source_173 = MAKE_FUNCTION_function_9_unpack_unicode_update_pos_of_xlrd$biffh( INCREASE_REFCOUNT( tmp_defaults_6 ) );
    if ( tmp_assign_source_173 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_173 );

        frame_module->f_lineno = 320;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_unpack_unicode_update_pos, tmp_assign_source_173 );
    tmp_defaults_7 = const_tuple_int_pos_6_tuple;
    tmp_assign_source_174 = MAKE_FUNCTION_function_10_unpack_cell_range_address_list_update_pos_of_xlrd$biffh( INCREASE_REFCOUNT( tmp_defaults_7 ) );
    if ( tmp_assign_source_174 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_174 );

        frame_module->f_lineno = 355;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_digest_ca7becc9d49589e000ad29ce1ad566e8, tmp_assign_source_174 );
    tmp_assign_source_175 = const_str_digest_96756a074b4136fc9b772237c96d84d0;
    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__brecstrg, tmp_assign_source_175 );
    tmp_assign_source_176 = PyDict_New();
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_biff_rec_name_dict, tmp_assign_source_176 );
    tmp_source_name_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__brecstrg );

    if (unlikely( tmp_source_name_1 == NULL ))
    {
        tmp_source_name_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__brecstrg );
    }

    if ( tmp_source_name_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10157 ], 31, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 536;
        goto frame_exception_exit_1;
    }

    tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_splitlines );
    if ( tmp_called_name_3 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 536;
        goto frame_exception_exit_1;
    }
    frame_module->f_lineno = 536;
    tmp_iter_arg_4 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
    Py_DECREF( tmp_called_name_3 );
    if ( tmp_iter_arg_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 536;
        goto frame_exception_exit_1;
    }
    tmp_assign_source_177 = MAKE_ITERATOR( tmp_iter_arg_4 );
    Py_DECREF( tmp_iter_arg_4 );
    if ( tmp_assign_source_177 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 536;
        goto frame_exception_exit_1;
    }
    assert( tmp_for_loop_2__for_iterator == NULL );
    tmp_for_loop_2__for_iterator = tmp_assign_source_177;

    // Tried code
    loop_start_2:;
    tmp_next_source_2 = tmp_for_loop_2__for_iterator;

    tmp_assign_source_178 = ITERATOR_NEXT( tmp_next_source_2 );
    if ( tmp_assign_source_178 == NULL )
    {
        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
        {

            goto loop_end_2;
        }
        else
        {

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            frame_module->f_lineno = 536;
            goto try_finally_handler_6;
        }
    }

    {
        PyObject *old = tmp_for_loop_2__iter_value;
        tmp_for_loop_2__iter_value = tmp_assign_source_178;
        Py_XDECREF( old );
    }

    tmp_assign_source_179 = tmp_for_loop_2__iter_value;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__buff, tmp_assign_source_179 );
    // Tried code
    tmp_source_name_2 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__buff );

    if (unlikely( tmp_source_name_2 == NULL ))
    {
        tmp_source_name_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__buff );
    }

    if ( tmp_source_name_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10188 ], 27, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 537;
        goto try_finally_handler_7;
    }

    tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_split );
    if ( tmp_called_name_4 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 537;
        goto try_finally_handler_7;
    }
    frame_module->f_lineno = 537;
    tmp_iter_arg_5 = CALL_FUNCTION_NO_ARGS( tmp_called_name_4 );
    Py_DECREF( tmp_called_name_4 );
    if ( tmp_iter_arg_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 537;
        goto try_finally_handler_7;
    }
    tmp_assign_source_180 = MAKE_ITERATOR( tmp_iter_arg_5 );
    Py_DECREF( tmp_iter_arg_5 );
    if ( tmp_assign_source_180 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 537;
        goto try_finally_handler_7;
    }
    {
        PyObject *old = tmp_tuple_unpack_3__source_iter;
        tmp_tuple_unpack_3__source_iter = tmp_assign_source_180;
        Py_XDECREF( old );
    }

    tmp_unpack_13 = tmp_tuple_unpack_3__source_iter;

    tmp_assign_source_181 = UNPACK_NEXT( tmp_unpack_13, 0 );
    if ( tmp_assign_source_181 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_module->f_lineno = 537;
        goto try_finally_handler_7;
    }
    {
        PyObject *old = tmp_tuple_unpack_3__element_1;
        tmp_tuple_unpack_3__element_1 = tmp_assign_source_181;
        Py_XDECREF( old );
    }

    tmp_unpack_14 = tmp_tuple_unpack_3__source_iter;

    tmp_assign_source_182 = UNPACK_NEXT( tmp_unpack_14, 1 );
    if ( tmp_assign_source_182 == NULL )
    {
        if ( !ERROR_OCCURRED() )
        {
            exception_type = PyExc_StopIteration;
            Py_INCREF( exception_type );
            exception_value = NULL;
            exception_tb = NULL;
        }
        else
        {
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        }


        frame_module->f_lineno = 537;
        goto try_finally_handler_7;
    }
    {
        PyObject *old = tmp_tuple_unpack_3__element_2;
        tmp_tuple_unpack_3__element_2 = tmp_assign_source_182;
        Py_XDECREF( old );
    }

    tmp_iterator_name_3 = tmp_tuple_unpack_3__source_iter;

    // Check if iterator has left-over elements.
    CHECK_OBJECT( tmp_iterator_name_3 ); assert( HAS_ITERNEXT( tmp_iterator_name_3 ) );

    tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_3 )->tp_iternext)( tmp_iterator_name_3 );

    if (likely( tmp_iterator_attempt == NULL ))
    {
        PyObject *error = GET_ERROR_OCCURRED();

        if ( error != NULL )
        {
            if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
            {
                CLEAR_ERROR_OCCURRED();
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                goto try_finally_handler_7;
            }
        }
    }
    else
    {
        Py_DECREF( tmp_iterator_attempt );

        // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
        PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
        PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

        goto try_finally_handler_7;
    }
    tmp_assign_source_183 = tmp_tuple_unpack_3__element_1;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__numh, tmp_assign_source_183 );
    tmp_assign_source_184 = tmp_tuple_unpack_3__element_2;

    UPDATE_STRING_DICT0( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__name, tmp_assign_source_184 );
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_7:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_6 = frame_module->f_lineno;
    Py_XDECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    frame_module->f_lineno = tmp_tried_lineno_6;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_6 != NULL )
    {
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;

        goto try_finally_handler_6;
    }

    goto finally_end_6;
    finally_end_6:;
    tmp_ass_subvalue_2 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__name );

    if (unlikely( tmp_ass_subvalue_2 == NULL ))
    {
        tmp_ass_subvalue_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__name );
    }

    if ( tmp_ass_subvalue_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10215 ], 27, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 538;
        goto try_finally_handler_6;
    }

    tmp_ass_subscribed_2 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_biff_rec_name_dict );

    if (unlikely( tmp_ass_subscribed_2 == NULL ))
    {
        tmp_ass_subscribed_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_biff_rec_name_dict );
    }

    if ( tmp_ass_subscribed_2 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10242 ], 40, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 538;
        goto try_finally_handler_6;
    }

    tmp_int_value_1 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain__numh );

    if (unlikely( tmp_int_value_1 == NULL ))
    {
        tmp_int_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__numh );
    }

    if ( tmp_int_value_1 == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10282 ], 27, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 538;
        goto try_finally_handler_6;
    }

    tmp_int_base_1 = const_int_pos_16;
    tmp_ass_subscript_2 = TO_INT2( tmp_int_value_1, tmp_int_base_1 );
    if ( tmp_ass_subscript_2 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 538;
        goto try_finally_handler_6;
    }
    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
    Py_DECREF( tmp_ass_subscript_2 );
    if ( tmp_result == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 538;
        goto try_finally_handler_6;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        frame_module->f_lineno = 536;
        goto try_finally_handler_6;
    }
    goto loop_start_2;
    loop_end_2:;
    // Final block of try/finally
    // Tried block ends with no exception occurred, note that.
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    try_finally_handler_6:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;

    tmp_tried_lineno_7 = frame_module->f_lineno;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    frame_module->f_lineno = tmp_tried_lineno_7;
    // Re-raise as necessary after finally was executed.
    // Reraise exception if any.
    if ( exception_keeper_type_7 != NULL )
    {
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;

        goto frame_exception_exit_1;
    }

    goto finally_end_7;
    finally_end_7:;
    tmp_res = PyDict_DelItem( (PyObject *)moduledict_xlrd$biffh, const_str_plain__buff );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10188 ], 27, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 539;
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_xlrd$biffh, const_str_plain__name );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10215 ], 27, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 539;
        goto frame_exception_exit_1;
    }

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_xlrd$biffh, const_str_plain__brecstrg );
    if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

    if ( tmp_res == -1 )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 10157 ], 31, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 539;
        goto frame_exception_exit_1;
    }

    tmp_defaults_8 = PyTuple_New( 3 );
    tmp_tuple_element_5 = const_int_0;
    Py_INCREF( tmp_tuple_element_5 );
    PyTuple_SET_ITEM( tmp_defaults_8, 0, tmp_tuple_element_5 );
    tmp_source_name_3 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_3 == NULL ))
    {
        tmp_source_name_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_3 == NULL )
    {
        Py_DECREF( tmp_defaults_8 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 646 ], 25, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 541;
        goto frame_exception_exit_1;
    }

    tmp_tuple_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_stdout );
    if ( tmp_tuple_element_5 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_defaults_8 );

        frame_module->f_lineno = 541;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_defaults_8, 1, tmp_tuple_element_5 );
    tmp_tuple_element_5 = Py_False;
    Py_INCREF( tmp_tuple_element_5 );
    PyTuple_SET_ITEM( tmp_defaults_8, 2, tmp_tuple_element_5 );
    tmp_assign_source_185 = MAKE_FUNCTION_function_11_hex_char_dump_of_xlrd$biffh( tmp_defaults_8 );
    if ( tmp_assign_source_185 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_185 );

        frame_module->f_lineno = 541;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_hex_char_dump, tmp_assign_source_185 );
    tmp_defaults_9 = PyTuple_New( 3 );
    tmp_tuple_element_6 = const_int_0;
    Py_INCREF( tmp_tuple_element_6 );
    PyTuple_SET_ITEM( tmp_defaults_9, 0, tmp_tuple_element_6 );
    tmp_source_name_4 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_4 == NULL ))
    {
        tmp_source_name_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_4 == NULL )
    {
        Py_DECREF( tmp_defaults_9 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 646 ], 25, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 572;
        goto frame_exception_exit_1;
    }

    tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_stdout );
    if ( tmp_tuple_element_6 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_defaults_9 );

        frame_module->f_lineno = 572;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_defaults_9, 1, tmp_tuple_element_6 );
    tmp_tuple_element_6 = Py_False;
    Py_INCREF( tmp_tuple_element_6 );
    PyTuple_SET_ITEM( tmp_defaults_9, 2, tmp_tuple_element_6 );
    tmp_assign_source_186 = MAKE_FUNCTION_function_12_biff_dump_of_xlrd$biffh( tmp_defaults_9 );
    if ( tmp_assign_source_186 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_186 );

        frame_module->f_lineno = 572;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_biff_dump, tmp_assign_source_186 );
    tmp_defaults_10 = PyTuple_New( 1 );
    tmp_source_name_5 = GET_STRING_DICT_VALUE( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_sys );

    if (unlikely( tmp_source_name_5 == NULL ))
    {
        tmp_source_name_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
    }

    if ( tmp_source_name_5 == NULL )
    {
        Py_DECREF( tmp_defaults_10 );
        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = UNSTREAM_STRING( &constant_bin[ 646 ], 25, 0 );
        exception_tb = NULL;

        frame_module->f_lineno = 618;
        goto frame_exception_exit_1;
    }

    tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_stdout );
    if ( tmp_tuple_element_7 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_defaults_10 );

        frame_module->f_lineno = 618;
        goto frame_exception_exit_1;
    }
    PyTuple_SET_ITEM( tmp_defaults_10, 0, tmp_tuple_element_7 );
    tmp_assign_source_187 = MAKE_FUNCTION_function_13_biff_count_records_of_xlrd$biffh( tmp_defaults_10 );
    if ( tmp_assign_source_187 == NULL )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
        Py_DECREF( tmp_assign_source_187 );

        frame_module->f_lineno = 618;
        goto frame_exception_exit_1;
    }
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_biff_count_records, tmp_assign_source_187 );

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_module );
#endif
    popFrameStack();

    assertFrameObject( frame_module );
    Py_DECREF( frame_module );

    goto frame_no_exception_1;
    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_module );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_module );
    }
    else if ( exception_tb->tb_frame != frame_module )
    {
        PyTracebackObject *traceback_new = MAKE_TRACEBACK( frame_module );
        traceback_new->tb_next = exception_tb;
        exception_tb = traceback_new;
    }

    // Put the previous frame back on top.
    popFrameStack();

#if PYTHON_VERSION >= 340
    frame_module->f_executing -= 1;
#endif
    Py_DECREF( frame_module );

    // Return the error.
    goto module_exception_exit;
    frame_no_exception_1:;
    tmp_assign_source_188 = PyDict_Copy( const_dict_3ea0089e9e3dc196147eafc31e4c1ee9 );
    UPDATE_STRING_DICT1( moduledict_xlrd$biffh, (Nuitka_StringObject *)const_str_plain_encoding_from_codepage, tmp_assign_source_188 );

    return MOD_RETURN_VALUE( module_xlrd$biffh );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
